
class GraphView extends MovieClip {

	var mc_depth:Number;					//movie clip depth

	//graph default settings
	var graphWidth:Number = 300;
	var graphHeight:Number = 300;
	var gridXmin:Number = -10;
	var gridXmax:Number = 10;
	var gridXscale:Number = 1;
	var gridYmin:Number = -10;
	var gridYmax:Number = 10;
	var gridYscale:Number = 1;
	var showRegionsInColor = true;

	//factors used to convert graph values to pixel values -- calculated when axis is drawn
	var x_pixels_per_unit:Number;		//pixels per unit on x axis
	var y_pixels_per_unit:Number;		//pixels per unit on y axis
	var x_axis_zero:Number;				//the pixel value where x=0 (x position of y-axis)
	var y_axis_zero:Number;				//the pixel value where y=0 (y position of x-axis)

	//line stroke and colors
	var gridStroke:Number = 1;
	var gridColor:Number = 0xCCCCCC;
	var axisStroke = 1;
	var axisColor = 0x000000;
	var plotColorA:Number = 0xFF0000;
	var plotColorB:Number = 0x336699;
	var plotStroke:Number = 3;	//plot line settings, red line

	//movie clips
	var parent_mc:MovieClip;	//main module
	var grid:MovieClip;
	var axis:MovieClip;
	var plot:MovieClip;
	var regionsMC:MovieClip;
	var gridLabelFormat:TextFormat;

	//tick mark and label settings
	var buffer_x:Number = 4;				//space between axis and grid labels for x-axis
	var buffer_y:Number = 5;				//and for y-axis
	var tickMarkLength:Number = 4;
	var oldLabelNumX:Number;
	var oldLabelNumY:Number;
	var calcVal:Number;
	var displayVal:String;
	var labelMC:MovieClip;
	
	//keeping track of regions
	var regions:Regions;
	var intersectionPoint = new Object();
	var selectedRegion:String = "";
	

	function GraphView() {
		mc_depth = this.getNextHighestDepth();
		
		//white background fill
		var myBG:MovieClip = this.createEmptyMovieClip("myBG", mc_depth++);
		myBG.beginFill(0xFFFFFF, 100);
		myBG.lineStyle(gridStroke, gridColor, 100, true);
		myBG.moveTo(0, 0);
		myBG.lineTo(0, graphHeight);
		myBG.lineTo(graphWidth, graphHeight);
		myBG.lineTo(graphWidth, 0);
		myBG.lineTo(0,0);
		
		//grid and axis setup
		grid = this.createEmptyMovieClip("grid", mc_depth++);
		axis = this.createEmptyMovieClip("axis", mc_depth++);
		plot = this.createEmptyMovieClip("plot", mc_depth++);
		regionsMC = this.createEmptyMovieClip("regionsMC", mc_depth++);
		var plotMask = myBG.duplicateMovieClip();
		var regionMask = myBG.duplicateMovieClip();
		
		plot.setMask(plotMask);	//mask plot to hide lines outside of view area
		regionsMC.setMask(regionMask); //mask regions to hide regions outside of view area
		
		gridLabelFormat = new TextFormat();			//format for grid labels on each axis
		gridLabelFormat.font = 'Arial';
		gridLabelFormat.size = 11;
		
		drawGridAndAxis();				
		drawLines();
		drawRegions();
	}
	
	function drawRegions(){
		regions = new Regions(regionsMC, this);
		var numberOfInequalities:Number = parent_mc.numberOfInequalities;
		var lineA = parent_mc.lineA;
		var lineB = parent_mc.lineB;

		trace("in drawRegions(): number of inequalities " + numberOfInequalities);
		regions.clearRegions();
		
		if(numberOfInequalities == 1){
			if(lineA.startPoint.x != undefined && isLineOnGraph(lineA)){
				trace("there is a line A");
				trace(lineA.startPoint.x);
				regions.draw2or3Regions(lineA, null, showRegionsInColor);
			}
		}else if(numberOfInequalities == 2){
			if(lineB.startPoint.x != undefined && isLineOnGraph(lineB) && lineA.startPoint.x != undefined && isLineOnGraph(lineA)){
				var intersectionPt = lineIntersection(lineA, lineB);
				trace("intersection is at " + intersectionPt.x + ", " + intersectionPt.y);				
				
				switch(intersectionPt){
					//in this case we have parallel lines, with the same slope
					case undefined:
						regions.draw2or3Regions(lineA, lineB, showRegionsInColor);
						break;
					default:
						//in this case we have lines that intersect on the graph
						if(pointIsOnTheGraph(intersectionPt)){
							regions.draw4Regions(intersectionPt, lineA, lineB, showRegionsInColor);
							//in this case we have lines that intersect, but not on the graph
						} else {
							trace("intersection is off the graph");
							regions.draw3Regions(lineA, lineB, showRegionsInColor);
						}
						break;					
					
				}
				//if we have clicked the add equation button but haven't entered a valid equation for line A or B or we deleted the equation
				//then we just do the region for one line
			} else {
				if(lineA.startPoint.x != undefined && isLineOnGraph(lineA)){
					trace("there is a line A");
					trace(lineA.startPoint.x);
					regions.draw2or3Regions(lineA, null, showRegionsInColor);
				} else if(lineB.startPoint.x != undefined && isLineOnGraph(lineB)){
					regions.draw2or3Regions(lineB, null, showRegionsInColor);
				}
			}
			
		}
		regions.addClickListeners();
		if(parent_mc.selectedRegion){
			regions.selectRegion(parent_mc.selectedRegion);
		}
	}

	function drawLines() {
		plot.clear();
		plotLine(plot, plotColorA, parent_mc.lineA);
		//check if equation B is shown
		
		if (parent_mc.numberOfInequalities > 1) {

			plotLine(plot, plotColorB, parent_mc.lineB);	
		}
	}

	//function plotLine(mc:MovieClip, plotColor:Number, slope:Number, intercept:Number, prefix:String, inequality:String, xValue:Number) {
	function plotLine(mc:MovieClip, plotColor:Number, line:Object){
		var slope:Number = line.slope;
		var intercept:Number = line.intercept;
		var prefix:String = line.prefix;
		var inequality:String = line.inequality;
		var xValue:Number = line.xValue;
		//trace ('prefix = ' + prefix + ', inequality = ' + inequality);
		var startPoint, endPoint;
		var pixelStartXY, pixelEndXY;
		

		mc.lineStyle(plotStroke, plotColor, 100, true);

		if (prefix == "y") {
			trace("slope is " + slope + " and intercept is " + intercept);
			if (slope != undefined && !isNaN(slope) && intercept != undefined && !isNaN(intercept)) {	//if the values are valid, plot the line
			
				if(slope == 0){
					startPoint = {x:gridXmin, y:intercept};
					endPoint = {x:gridXmax, y:intercept};	
			
				}
				else if(slope > 0){
				
					startPoint = getEdgePointTopOrRight(intercept, slope);
					endPoint = getEdgePointBottomOrLeft(intercept, slope);
				} else {
				
					startPoint = getEdgePointTopOrLeft(intercept, slope);
					endPoint = getEdgePointBottomOrRight(intercept, slope);
				}
				pixelStartXY = graphToPixels(startPoint.x, startPoint.y);
				pixelEndXY = graphToPixels(endPoint.x, endPoint.y);

				if(inequality == "lt" || inequality == "gt"){				
					dashedLineTo(mc, pixelStartXY.x, pixelStartXY.y, pixelEndXY.x, pixelEndXY.y, 6);
				} else {
					solidLineTo(mc, pixelStartXY.x, pixelStartXY.y, pixelEndXY.x, pixelEndXY.y);
				}
				line.startPoint = startPoint;
				line.endPoint = endPoint;
				
			}

		} else if (prefix == "x") {
			
			if (xValue != undefined && !isNaN(xValue)) {	//if the x value is defined, plot a vertical line

				startPoint = {x:xValue, y: gridYmax};
				endPoint = {x:xValue, y:gridYmin};

				pixelStartXY = graphToPixels(startPoint.x, startPoint.y);
				pixelEndXY = graphToPixels(endPoint.x, endPoint.y);
				if(inequality == "lt" || inequality == "gt"){
					dashedLineTo(mc, pixelStartXY.x, pixelStartXY.y, pixelEndXY.x, pixelEndXY.y, 6);
				} else {
					solidLineTo(mc, pixelStartXY.x, pixelStartXY.y, pixelEndXY.x, pixelEndXY.y);
				}
			}
			
			line.startPoint = startPoint;
			line.endPoint = endPoint;
		}
		

	}

	public function graphToPixels(x:Number, y:Number) {
		var pixelObject = new Object;

		var pixelX = graphToPixelsX(x);
		var pixelY = graphToPixelsY(y);

		pixelObject.x = pixelX;
		pixelObject.y = pixelY;
		return pixelObject;
	}
	public function graphToPixelsX(x:Number){
		
		return x_axis_zero + (x * x_pixels_per_unit);
	}
	public function graphToPixelsY(y:Number){
		return  y_axis_zero - (y * y_pixels_per_unit);
	}
	function dashedLineTo(mc:MovieClip, x1:Number, y1:Number, x2:Number, y2:Number, dashLen:Number){
		mc.moveTo(x1, y1);
		
		var dX = x2 - x1;
		var dY = y2 - y1;
		var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
		var dashX = dX / dashes;
		var dashY = dY / dashes;
		
		var q = 0;
		while (q++ < dashes) {
			x1 += dashX;
			y1 += dashY;
			mc[q % 2 == 0 ? 'moveTo' : 'lineTo'](x1, y1);
		}
		mc[q % 2 == 0 ? 'moveTo' : 'lineTo'](x2, y2);
		
		
	}
	function solidLineTo(mc:MovieClip, x1:Number, y1:Number, x2:Number, y2:Number){
		
		mc.moveTo(x1, y1);
		mc.lineTo(x2, y2);
		
	}
	function getEdgePointTopOrRight(yIntercept:Number, slope:Number){
		var point = {x:0, y:0};
		//checking the top
		var tempX = (gridYmax - yIntercept)/slope;
		if(isOnTheGraph(tempX, "x")){
			point.x = tempX;
			point.y = gridYmax;
		} else {
			point.x = gridXmax;
			point.y = slope * gridXmax + yIntercept;;
		}
			return point;
	}
	function getEdgePointBottomOrLeft(yIntercept:Number, slope:Number){
		var point = {x:0, y:0};
		//checking the bottom
		var tempX = (gridYmin - yIntercept)/slope;
		if(isOnTheGraph(tempX, "x")){
			point.x = tempX;
			point.y = gridYmin;
		} else {
			point.x = gridXmin;
			point.y = slope * gridXmin + yIntercept;;
		}
			return point;
	}
	function getEdgePointTopOrLeft(yIntercept:Number, slope:Number){
		var point = {x:0, y:0};
		//checking the top
		var tempX = (gridYmax - yIntercept)/slope;
		if(isOnTheGraph(tempX, "x")){
			point.x = tempX;
			point.y = gridYmax;
		} else {
			point.x = gridXmin;
			point.y = slope * gridXmin + yIntercept;;
		}
			return point;
	}
	function getEdgePointBottomOrRight(yIntercept:Number, slope:Number){
		var point = {x:0, y:0};
		//checking the bottom
		var tempX = (gridYmin - yIntercept)/slope;
		if(isOnTheGraph(tempX, "x")){
			point.x = tempX;
			point.y = gridYmin;
		} else {
			point.x = gridXmax;
			point.y = slope * gridXmax + yIntercept;;
		}
			return point;
	}
	
	function isOnTheGraph(val:Number, type:String){
		var onTheGraph = false;
		if(type == "x"){
			onTheGraph = !!(val >= gridXmin && val <=gridXmax);
		} else if(type == "y"){
			onTheGraph = !!(val >= gridYmin && val <=gridYmax);
		}
		return onTheGraph;
		
	}
	function pointIsOnTheGraph(pt:Object){
		var xIsOnTheGraph = false;
		var yIsOnTheGraph = false;
		var isOnTheGraph = false;
		xIsOnTheGraph = !!(pt.x >= gridXmin && pt.x <=gridXmax);
		yIsOnTheGraph = !!(pt.y >= gridYmin && pt.y <=gridYmax);
		return !!(xIsOnTheGraph && yIsOnTheGraph);
				
	}
	
	function lineIntersection( lineA:Line, lineB:Line )
	{

		var x1A:Number = lineA.startPoint.x;
		var y1A:Number = lineA.startPoint.y;
		var x2A:Number = lineA.endPoint.x;
		var y2A:Number = lineA.endPoint.y;
		var x1B:Number = lineB.startPoint.x;
		var y1B:Number = lineB.startPoint.y;
		var x2B:Number = lineB.endPoint.x;
		var y2B:Number = lineB.endPoint.y;

		var slopeA = (lineA.prefix == "x")? Infinity : lineA.slope;
		var slopeB = (lineB.prefix == "x")? Infinity : lineB.slope;
		
		// if the directional constants are equal, the lines are parallel,
		// meaning there is no intersection point.
		if( slopeA == slopeB ) return undefined;
		
		var x,y;
		
		// an infinite directional constant means the line is vertical
		if( slopeA == Infinity )
		{
			// so the intersection must be at the x coordinate of the line
			x = x1A;
			
			// compute y offset constants for line 2
			var m2 = y1B - slopeB * x1B;
			
			// use
			// y = k * x + m
			// to get y coordinate
			y = slopeB * x + m2;
		}
			// same as above for line 2
		else if( slopeB == Infinity )
		{
			var m1 = y1A - slopeA * x1A;
			
			x = x1B;
			y = slopeA * x + m1;
		}
			// if neither of the lines are vertical
		else
		{
			// compute y offset constants for both lines
			var m1 = y1A - slopeA * x1A;
			var m2 = y1B - slopeB * x1B;
			
			// compute x
			x = (m1-m2) / (slopeB-slopeA);
			
			// use
			// y = k * x + m
			// to get y coordinate
			y = slopeA * x + m1;
		}
		
		return {x:x,y:y};
	}
	
	function inRange( v, vA, vB )
	{
		return v <= vA != v < vB;
	}


	//function to update the grid and axis
	function drawGridAndAxis() {
		var gridNumberX:Number = (gridXmax - gridXmin)/gridXscale;
		var gridNumberY:Number = (gridYmax - gridYmin)/gridYscale;
		var x_pos:Number = 0;
		var y_pos:Number = 0;
		var x_spacer:Number = graphWidth/gridNumberX;
		var y_spacer:Number = graphHeight/gridNumberY;

		//grid drawing
		grid.clear();
		grid.lineStyle(gridStroke, gridColor, 100, true);		//drawing vertical lines
		for (var i:Number = 0; i <= gridNumberX; i++) {			//start at left and move right
			grid.moveTo(x_pos, y_pos);
			grid.lineTo(x_pos, y_pos + graphHeight);
			x_pos += x_spacer;
		}
		x_pos = 0;
		y_pos = graphHeight;							//drawing horizontal lines
		for (i = 0; i <= gridNumberY; i++) {			//start at bottom and move up
			grid.moveTo(x_pos, y_pos);
			grid.lineTo(x_pos + graphWidth, y_pos);
			y_pos -= y_spacer;
		}

		//axis drawing
		axis.clear();
		axis.lineStyle(axisStroke, axisColor, 100, true);

		//clean up old labels
		for (var i:Number = 0; i < oldLabelNumX; i++) {
			axis['x'+i].removeTextField();
		}
		for (var i:Number = 0; i < oldLabelNumY; i++) {
			axis['y'+i].removeTextField();
		}

		//check if x axis is within view area
		x_pixels_per_unit = graphWidth / (gridXmax - gridXmin);			//pixels per unit on x axis
		y_pixels_per_unit = graphHeight / (gridYmax - gridYmin);		//pixels per unit on y axis

		x_axis_zero = (0 - gridXmin) * x_pixels_per_unit;					//the pixel value where x=0 (x position of y-axis)
		y_axis_zero = graphHeight - (0 - gridYmin) * y_pixels_per_unit;		//the pixel value where y=0 (y position of x-axis)

		//draw x axis if within viewing area
		if (y_axis_zero >= 0 && y_axis_zero <= graphHeight) {
			//x axis
			 axis.moveTo(0, y_axis_zero);
			 axis.lineTo(graphWidth, y_axis_zero);

			//draw tick marks and create labels
			x_pos = 0;
			y_pos = y_axis_zero + buffer_x;

			for (i = 0; i <= gridNumberX; i++) {
				calcVal = correctFloatingPointError(gridXmin + (i*gridXscale));
				displayVal = String(calcVal);
				var gLabelX:TextField = newGridLabel(axis, mc_depth++, x_pos, y_pos, displayVal, 'x'+i);
				axis.moveTo(x_pos, y_pos - buffer_x + tickMarkLength);
				axis.lineTo(x_pos, y_pos - buffer_x - tickMarkLength);
				x_pos += x_spacer;
				if (calcVal == 0 && y_axis_zero != graphHeight) {	//leave off x label for 0 unless x-axis is at bottom edge
					gLabelX._alpha = 0;
				} else {
					gLabelX._x -= gLabelX._width/2;
				}
			}
			oldLabelNumX = i;
		}

		//draw y axis if within viewing area
		if (x_axis_zero >= 0 && x_axis_zero <= graphWidth) {

			//y axis
			 axis.moveTo(x_axis_zero, 0);
			 axis.lineTo(x_axis_zero, graphHeight);
		
			//draw tick marks and create labels
			x_pos = x_axis_zero - buffer_y;
			y_pos = graphHeight;
			for (i = 0; i <= gridNumberY; i++) {
				calcVal = correctFloatingPointError(gridYmin + (i*gridYscale));
				if(i < gridNumberY){
				var calcValOfNextOne:Number = correctFloatingPointError(gridYmin + ((i + 1)*gridYscale));
				}
				displayVal = String(calcVal);
				var gLabelY:TextField = newGridLabel(axis, mc_depth++, x_pos, y_pos, displayVal, 'y'+i);
				axis.moveTo(x_pos + buffer_y - tickMarkLength, y_pos);
				axis.lineTo(x_pos + buffer_y + tickMarkLength, y_pos);
				y_pos -= y_spacer;
				gLabelY._x -= gLabelY._width;
	
				if(calcValOfNextOne == 0 && gridYscale < 12 && gridXscale < 12){ //leave off the first y label below the x axis if the scale is small (so it doesn't overlap)
					gLabelY._alpha = 0;
				} else if (calcVal == 0 && x_axis_zero != 0) {	//leave off the y label for 0 unless y-axis is at left edge
					gLabelY._alpha = 0;
				} else {
					gLabelY._y -= gLabelY._height/2;
				}
				
				
			}
			oldLabelNumY = i;
		}

	}

	function newGridLabel(mc:MovieClip, d:Number, myX:Number, myY:Number, myTxt:String, myID:String) {
		var newLabel:TextField = mc.createTextField(myID, d++, myX, myY, 1, 1);
		//replace negative signs with en dashes
		if (myTxt.indexOf('-') > -1) {
			myTxt = stringReplace(myTxt,'-','\u2013');
		}
		newLabel.text = myTxt;
		newLabel.selectable = false;
		newLabel.autoSize = true;
		//newLabel.embedFonts = true;
		newLabel.setTextFormat(gridLabelFormat);
		return newLabel;
	}
	
	function isLineOnGraph(line:Line){
		trace("line startPoint is " + line.startPoint.x + ", " + line.startPoint.y + " and line endPoint is " + line.endPoint.x + ", " + line.endPoint.y);
		if(line.prefix == "x"){
			if(line.xValue >=gridXmin && line.xValue <= gridXmax){
				return true;
			}						
		}
		else if(line.prefix == "y"){
			if(pointIsOnTheGraph(line.startPoint) && pointIsOnTheGraph(line.endPoint)){
				return true;
			}
			
		}		
		return false;
	}

	public function correctFloatingPointError(number:Number):Number {
		//default returns (10000 * number) / 10000
		//should correct very small floating point errors
		var precision:Number = 5;
		var correction:Number = Math.pow(10, precision);
		return Math.round(correction * number) / correction;
	}

	function stringReplace(str:String, searchStr:String, replaceStr:String):String {
		return str.split(searchStr).join(replaceStr);
	}

}