
	 class Regions
	{
		 var regionsMC:MovieClip;
		 var _graph:GraphView;
		 var aRegions:Array = new Array();
		 //these are just for convenience
		 private var gridXmax:Number;
		 private var gridYmax:Number;
		 private var gridXmin:Number;
		 private var gridYmin:Number;
		 private var regionColors:Object = {region_1: 0xFF0000, region_2: 0x336698, region_3:0x00CBCB, region_4: 0xFF9800};
		 
		 
		 function Regions(rMC:MovieClip, graph:GraphView)
		{
			 this.regionsMC = rMC;
			 this._graph = graph;
			 this.gridXmax = graph.gridXmax;
			 this.gridYmax = graph.gridYmax;
			 this.gridXmin = graph.gridXmin;
			 this.gridYmin = graph.gridYmin;
		}
		 function clearRegions(){
			 
			 for(var i in regionsMC){
				 regionsMC[i].removeMovieClip();
			 }
			 aRegions = [];
			 
		 }
		 function addClickListeners(){
			 var that:Regions = this;	
			 for(var i in regionsMC){
				 regionsMC[i].onRelease = function(evt:Object){
					 
					var region:Region = that.findRegionWithNameOfMovieClip(this._name);
					//first set the selectedRegion  to none
					//that._graph.selectedRegion = "";
					that._graph.parent_mc.selectedRegion = "";
					if(!region.isSelected()){
						//turn off any selected regions
						that.deSelectRegions();	
						that._graph.parent_mc.selectedRegion = this._name;
						
						
					} 
					that._graph.parent_mc.updateGraphSettings();
					region.toggleStatus();
				 }
			 }
 
		 }
		 function selectRegion(regionName){
			 
			 var region:Region = findRegionWithNameOfMovieClip(regionName);
			 region.select();
			 
		 }
		
		function draw2or3Regions(lineA:Line, lineB:Line, showRegionsInColor){
			var regionPtsArr:Array = pointsFor2or3Regions(lineA, lineB);
			trace("region pts arr " + regionPtsArr.length);
			for(var i:Number = 0; i < regionPtsArr.length; i++){
				var nameNum:Number = i + 1;
				var nameString:String = "region_" + nameNum.toString();	
				var color:Number = (showRegionsInColor) ? regionColors[nameString]:null;
				var region:Region = new Region(regionsMC, this._graph, regionPtsArr[i], nameString, color);
				aRegions.push(region);
			}
			
		}
		function draw3Regions(lineA:Line, lineB:Line, showRegionsInColor){
			var middleRegionCenterPt:Object = findMiddleRegionCenterPt(lineA, lineB);
			//this puts the points in order by where the lines are in relation to each other
			var outerRegionsObjects:Array = createObjectsForOuterRegions(lineA, lineB);
			//these don't include the corners yet
			var linePtsFirstRegion:Array = linePointsForOuterRegions(middleRegionCenterPt, outerRegionsObjects[0]);
			var linePtsSecondRegion:Array = linePointsForOuterRegions(middleRegionCenterPt, outerRegionsObjects[1]);			
			var linePtsMiddleRegion:Array = linePointsForRegions(middleRegionCenterPt, lineA, lineB);
			
			var regionPtsArr:Array = pointsFor3Regions(middleRegionCenterPt, linePtsFirstRegion, linePtsSecondRegion, linePtsMiddleRegion);
			for(var i:Number = 0; i < regionPtsArr.length; i++){
				var nameNum:Number = i + 1;
				var nameString:String = "region_" + nameNum.toString();		
				var color:Number = (showRegionsInColor) ? regionColors[nameString]:null;
				var region:Region = new Region(regionsMC, this._graph, regionPtsArr[i], nameString, color);
				aRegions.push(region);
			}
			
			
		}
		function findMiddleRegionCenterPt(lineA:Line, lineB:Line):Object{
			var centerPt:Object = {x:0, y:0};
			trace("line A goes from " + lineA.startPoint.x + ", " + lineA.startPoint.y + " to " + lineA.endPoint.x + ", " + lineA.endPoint.y);
			trace("line B goes from " + lineB.startPoint.x + ", " + lineB.startPoint.y + " to " + lineB.endPoint.x + ", " + lineB.endPoint.y);
			centerPt.x = (lineA.startPoint.x + lineA.endPoint.x + lineB.startPoint.x + lineB.endPoint.x)/4.0;
			centerPt.y = (lineA.startPoint.y + lineA.endPoint.y + lineB.startPoint.y + lineB.endPoint.y)/4.0;
			trace("center point is " + centerPt.x + ", " + centerPt.y);
			return centerPt;
						
		}
		
		function draw4Regions(intersectionPt:Object, lineA:Line, lineB:Line, showRegionsInColor){
			//first sort the points by their angle with the intersection
			var linePtsArr:Array = linePointsForRegions(intersectionPt, lineA, lineB);
			var regionPtsArr:Array = pointsFor4Regions(intersectionPt, linePtsArr);
			for(var i:Number = 0; i < regionPtsArr.length; i++){
				var nameNum:Number = i + 1;
				var nameString:String = "region_" + nameNum.toString();		
				var color:Number = (showRegionsInColor) ? regionColors[nameString]:null;
				var region:Region = new Region(regionsMC, this._graph, regionPtsArr[i], nameString, color);
				aRegions.push(region);
			}
			
		}
		function pointsFor2or3Regions(lineA:Line, lineB:Line){
			var arr1 = [];
			var arr2 = [];
			var arr3 = [];
			var firstLine:Line;
			var secondLine:Line;
			if(lineB === undefined || lineB === null || lineB.startPoint === undefined || lineB.startPoint === null){
				firstLine = lineA;
				trace("lineB is undefined or null");
				secondLine = null;
			} else if(lineA === undefined || lineA === null || lineA.startPoint === undefined || lineA.startPoint === null){
				firstLine = lineB;
				secondLine = null;
			} else {
				// if(lineA.slope == Infinity){
				if(lineA.prefix == "x"){
					//the first one will be the one on the right 
					firstLine = (lineA.startPoint.x > lineB.startPoint.x)? lineA : lineB;
					secondLine = (lineA.startPoint.x < lineB.startPoint.x)? lineA : lineB;
				} else if(lineA.slope == 0){
					//the first one will be the top one
					firstLine = (lineA.startPoint.y > lineB.startPoint.y)? lineA: lineB;
					secondLine = (lineA.startPoint.y < lineB.startPoint.y)? lineA: lineB;				
				 
				} else if (lineA.slope > 0){
					if(lineA.startPoint.x == lineB.startPoint.x){
						//if they have the same start x, the first one will be the one on the bottom (on the right)
						firstLine = (lineA.startPoint.y < lineB.startPoint.y)? lineA : lineB;
						secondLine = (lineA.startPoint.y > lineB.startPoint.y)? lineA : lineB;
					} else {
						//otherwise the first one will be the one on the right
						firstLine = (lineA.startPoint.x > lineB.startPoint.x)? lineA : lineB;
						secondLine = (lineA.startPoint.x < lineB.startPoint.x)? lineA : lineB;
					}
					//if slope is negative
				} else {
					if(lineA.startPoint.x == lineB.startPoint.x){
						//the first one will be the one on the top
						firstLine = (lineA.startPoint.y > lineB.startPoint.y)? lineA : lineB;
						secondLine = (lineA.startPoint.y < lineB.startPoint.y)? lineA : lineB;					
						
					} else {
						//otherwise the first one will be the one on the right
						firstLine = (lineA.startPoint.x > lineB.startPoint.x)? lineA : lineB;
						secondLine = (lineA.startPoint.x < lineB.startPoint.x)? lineA : lineB;
					}
				}
				
			}
			if(firstLine.prefix == "x"){
				arr1 = createArrayOfPointsRightVerticalRegion(firstLine);
				//if(lineB == undefined || lineB.startPoint === null || lineB.startPoint === undefined){
				if(secondLine === null){
					arr2 = createArrayOfPointsLeftVerticalRegion(firstLine);
				}
				else {
					arr2 = createArrayOfPointsLeftVerticalRegion(secondLine);
					arr3 = createArrayOfPointsMiddleRegion(firstLine, secondLine);
				}
				
			} else {
			//this is for parallel lines, the slopes will be the same
			switch(firstLine.slope){
				case 0:
					arr1 = createArrayOfPointsTopRegion(firstLine);
					//if(lineB == undefined || lineB.startPoint === null || lineB.startPoint === undefined){
					if(secondLine === null){
						arr2 = createArrayOfPointsBottomRegion(firstLine);
					} else {
						arr2 = createArrayOfPointsBottomRegion(secondLine);
						arr3 = createArrayOfPointsMiddleRegion(firstLine, secondLine);
					}
					break;
			/*	case Infinity:
					arr1 = createArrayOfPointsRightVerticalRegion(firstLine);
					if(lineB == undefined || lineB.startPoint === null || lineB.startPoint === undefined){
						arr2 = createArrayOfPointsLeftVerticalRegion(firstLine);
					}
					else {
						arr2 = createArrayOfPointsLeftVerticalRegion(secondLine);
						arr3 = createArrayOfPointsMiddleRegion(firstLine, secondLine);
					}
					
					break;
				*/
				default:
					if(firstLine.slope > 0){
						arr1 = createArrayOfPointsRightRegionPositiveSlope(firstLine);
						//if(lineB == undefined || lineB.startPoint === null || lineB.startPoint === undefined){
						if(secondLine === null){
							arr2 = createArrayOfPointsLeftRegionPositiveSlope(firstLine);
						} else {
							arr2 = createArrayOfPointsLeftRegionPositiveSlope(secondLine);
							arr3 = createArrayOfPointsMiddleRegionPositiveSlope(firstLine, secondLine);
						}
						
					}
						//slope is negative
					else {
						arr1 = createArrayOfPointsRightRegionNegativeSlope(firstLine);
						if(secondLine === null){
							arr2 = createArrayOfPointsLeftRegionNegativeSlope(firstLine);
						} else {
							arr2 = createArrayOfPointsLeftRegionNegativeSlope(secondLine);
							arr3 = createArrayOfPointsMiddleRegionNegativeSlope(firstLine, secondLine);
						}
						
						
					}
					break;
			}
			}
			var arr = [];
			arr.push(arr1, arr2);
			if(arr3.length > 0){
				arr.push(arr3);
			}
			return arr;
		}
		function createObjectsForOuterRegions(lineA:Line, lineB:Line){
			var arr = [];

			var firstLine:Line;
			var secondLine:Line;
			var firstLineObj:Object = {line:null, position: null, firstPoint:null, secondPoint:null};
			var secondLineObj:Object = {line:null, position:null, firstPoint:null, secondPooint:null};
						
				
				if(lineA.slope == 0 || lineB.slope == 0){
					//the first one will be the top one
					firstLine = (lineA.startPoint.y > lineB.startPoint.y || lineA.endPoint.y > lineB.endPoint.y)? lineA: lineB; //it doesn't matter if we test for the start or the end point
					firstLineObj.line = firstLine;
					trace("first line slope is " + firstLine.slope);
					firstLineObj.position = "top";
					firstLineObj.firstPoint = (firstLine.slope > 0)?firstLine.endPoint : firstLine.startPoint;
					firstLineObj.secondPoint = (firstLine.slope > 0)?firstLine.startPoint : firstLine.endPoint;
					secondLine = (lineA.startPoint.y < lineB.startPoint.y || lineA.endPoint.y < lineB.endPoint.y)? lineA: lineB;
					secondLineObj.line = secondLine;
					secondLineObj.position = "bottom";
					secondLineObj.firstPoint = (secondLine.slope > 0)? secondLine.startPoint : secondLine.endPoint;
					secondLineObj.secondPoint = (secondLine.slope > 0)? secondLine.endPoint : secondLine.startPoint;
			//	} else if(lineA.slope == Infinity || lineB.slope == Infinity){
				} else if (lineA.prefix == "x" || lineB.prefix == "x"){
					//the first one will be the one on the right 
					firstLine = (lineA.startPoint.x > lineB.startPoint.x || lineA.endPoint.x > lineB.endPoint.x)? lineA : lineB;
					firstLineObj.line = firstLine;
					firstLineObj.position = "right";
					firstLineObj.firstPoint = firstLine.startPoint;
					firstLineObj.secondPoint = firstLine.endPoint;
					secondLine = (lineA.startPoint.x < lineB.startPoint.x || lineA.endPoint.x < lineB.endPoint.x)? lineA : lineB;
					secondLineObj.line = secondLine;
					secondLineObj.position = "left";
					secondLineObj.firstPoint = secondLine.endPoint;
					secondLineObj.secondPoint = secondLine.startPoint;
					//if both slopes are positive
				} else if (lineA.slope > 0 && lineB.slope > 0){
					if(lineA.startPoint.x == lineB.startPoint.x){
						//if they have the same start x, the first one will be the one on the bottom (on the right)
						firstLine = (lineA.startPoint.y < lineB.startPoint.y || lineA.endPoint.y < lineB.endPoint.y)? lineA : lineB;
						firstLineObj.line = firstLine;
						firstLineObj.position = "bottom";
						firstLineObj.firstPoint = firstLine.startPoint;
						firstLineObj.secondPoint = firstLine.endPoint;
						secondLine = (lineA.startPoint.y > lineB.startPoint.y || lineA.endPoint.y > lineB.endPoint.y)? lineA : lineB;
						secondLineObj.line = secondLine;						
						secondLineObj.position = "top";
						secondLineObj.firstPoint = secondLine.endPoint;
						secondLineObj.secondPoint = secondLine.startPoint;
					} else {
						//otherwise the first one will be the one on the right
						firstLine = (lineA.startPoint.x > lineB.startPoint.x || lineA.endPoint.x > lineB.endPoint.x)? lineA : lineB;
						firstLineObj.line = firstLine;
						firstLineObj.position = "right";
						firstLineObj.firstPoint = firstLine.startPoint;
						firstLineObj.secondPoint = firstLine.endPoint;
						secondLine = (lineA.startPoint.x < lineB.startPoint.x || lineA.endPoint.x < lineB.endPoint.x)? lineA : lineB;
						secondLineObj.line = secondLine;
						secondLineObj.position = "left";
						secondLineObj.firstPoint = secondLine.endPoint;
						secondLineObj.secondPoint = secondLine.startPoint;
					}
					//if both slopes are negative
				} else if (lineA.slope < 0 && lineB.slope < 0){
					if(lineA.startPoint.x == lineB.startPoint.x){
						//the first one will be the one on the top
						firstLine = (lineA.startPoint.y > lineB.startPoint.y || lineA.endPoint.y > lineB.endPoint.y)? lineA : lineB;
						firstLineObj.line = firstLine;
						firstLineObj.position = "top";
						firstLineObj.firstPoint = firstLine.startPoint;
						firstLineObj.secondPoint = firstLine.endPoint;
						
						secondLine = (lineA.startPoint.y < lineB.startPoint.y || lineA.endPoint.y < lineB.endPoint.y)? lineA : lineB;
						secondLineObj.line = secondLine;
						secondLineObj.position = "bottom";
						secondLineObj.firstPoint = secondLine.endPoint;
						secondLineObj.secondPoint = secondLine.startPoint;
						
					} else {
						//otherwise the first one will be the one on the right
						firstLine = (lineA.startPoint.x > lineB.startPoint.x || lineA.endPoint.x > lineB.endPoint.x)? lineA : lineB;
						firstLineObj.line = firstLine;
						firstLineObj.position = "right";
						firstLineObj.firstPoint = firstLine.startPoint;
						firstLineObj.secondPoint = firstLine.endPoint;
						secondLine = (lineA.startPoint.x < lineB.startPoint.x || lineA.endPoint.x < lineB.endPoint.x)? lineA : lineB;
						secondLineObj.line = secondLine;
						secondLineObj.position = "left";
						secondLineObj.firstPoint = secondLine.endPoint;
						secondLineObj.secondPoint = secondLine.startPoint;
					}
				} else if (lineA.slope > 0 && lineB.slope < 0){
					if(lineA.startPoint.x == lineB.endPoint.x){
						// the first one will be the one on the bottom
						firstLine = (lineA.startPoint.y < lineB.endPoint.y)? lineA : lineB;
						firstLineObj.line = firstLine;
						firstLineObj.position = "bottom";
						firstLineObj.firstPoint = (firstLine.slope < 0)? firstLine.endPoint : firstLine.startPoint;
						firstLineObj.secondPoint = (firstLine.slope < 0)? firstLine.startPoint : firstLine.endPoint;
						secondLine = (lineA.startPoint.y > lineB.endPoint.y)? lineA : lineB;
						secondLineObj.line = secondLine;						
						secondLineObj.position = "top";
						secondLineObj.firstPoint = (secondLine.slope < 0)? secondLine.endPoint : secondLine.startPoint;
						secondLineObj.secondPoint = (secondLine.slope < 0)? secondLine.startPoint : secondLine.endPoint;
					} else {
						//the first one will be the one on the right
						firstLine = (lineA.startPoint.x > lineB.startPoint.x)? lineA : lineB;
						firstLineObj.line = firstLine;
						firstLineObj.position = "right";
						firstLineObj.firstPoint = firstLine.startPoint;
						firstLineObj.secondPoint =  firstLine.endPoint;
						secondLine = (lineA.startPoint.x < lineB.startPoint.x)? lineA : lineB;
						secondLineObj.line = secondLine;
						secondLineObj.position = "left";
						secondLineObj.firstPoint =  secondLine.endPoint;
						secondLineObj.secondPoint = secondLine.startPoint;
					}
				} else if (lineA.slope < 0 && lineB.slope > 0){
					if(lineA.endPoint.x == lineB.startPoint.x){
						// the first one will be the one on the bottom
						firstLine = (lineA.endPoint.y < lineB.startPoint.y)? lineA : lineB;
						firstLineObj.line = firstLine;
						firstLineObj.position = "bottom";
						firstLineObj.firstPoint = (firstLine.slope < 0)? firstLine.endPoint : firstLine.startPoint;
						firstLineObj.secondPoint = (firstLine.slope < 0)? firstLine.startPoint : firstLine.endPoint;
						secondLine = (lineA.endPoint.y > lineB.startPoint.y)? lineA : lineB;
						secondLineObj.line = secondLine;						
						secondLineObj.position = "top";
						secondLineObj.firstPoint = (secondLine.slope < 0)? secondLine.startPoint : secondLine.endPoint;
						secondLineObj.secondPoint = (secondLine.slope < 0)? secondLine.endPoint : secondLine.startPoint;
					} else {
						//the first one will be the one on the right
						firstLine = (lineA.startPoint.x > lineB.startPoint.x)? lineA : lineB;
						firstLineObj.line = firstLine;
						firstLineObj.position = "right";
						firstLineObj.firstPoint = (firstLine.slope < 0)? firstLine.startPoint : firstLine.endPoint;
						firstLineObj.secondPoint = (firstLine.slope < 0)? firstLine.endPoint : firstLine.startPoint;
						secondLine = (lineA.startPoint.x < lineB.startPoint.x)? lineA : lineB;
						secondLineObj.line = secondLine;
						secondLineObj.position = "left";
						secondLineObj.firstPoint = (secondLine.slope < 0)? secondLine.endPoint : secondLine.startPoint;
						secondLineObj.secondPoint = (secondLine.slope < 0)? secondLine.startPoint : secondLine.startPoint;
					
				}
				
			}
			
			arr.push(firstLineObj, secondLineObj);
			
			return arr;
		}
		function linePointsForRegions(referencePt, lineA:Line, lineB:Line){
			var arr = [];
			var returnArray = [];
			var ptArr = [lineA.startPoint, lineA.endPoint, lineB.startPoint, lineB.endPoint];
			for(var i:Number = 0; i < ptArr.length; i++){
				var theAngle = findAngleBetweenTwoPoints(referencePt, ptArr[i]);
				var obj:Object = {point: ptArr[i], angle:theAngle};
				arr.push(obj);
			}
			arr.sortOn("angle", Array.NUMERIC);
			
			return arr;		
		}
		function linePointsForOuterRegions(referencePt, lineObj){
			var arr = [];
			var returnArray = [];
						
			var ptArr = [lineObj.firstPoint, lineObj.secondPoint];
			for(var i:Number = 0; i < ptArr.length; i++){
				var theAngle = findAngleBetweenTwoPoints(referencePt, ptArr[i]);
				var obj:Object = {point: ptArr[i], angle:theAngle};
				arr.push(obj);
			}
			
			return arr;		
		}
		
		function pointsFor3Regions(middleRegionCenterPt:Object, firstRegionPtArr, secondRegionPtArr, ptArr){
			var arrOfRegions = [];
			var allCorners = [{x:gridXmax, y:gridYmax}, {x:gridXmax, y:gridYmin}, {x:gridXmin, y:gridYmin}, {x:gridXmin, y:gridYmax}];
			//do the first region
			var ptsFirstRegion = [];
			var corners = [];
			trace("middleRegionCenterPt is " + middleRegionCenterPt.x + ", " + middleRegionCenterPt.y);
			//do the first region
			ptsFirstRegion.push(firstRegionPtArr[0].point);
			trace("all corners before removing used points is " + allCorners.length);
			corners = findCornersBetweenTwoPointsRemovingUsedPoints(firstRegionPtArr[0].point, firstRegionPtArr[0].angle, firstRegionPtArr[1].point, firstRegionPtArr[1].angle, middleRegionCenterPt, allCorners);
			trace("all corners after removing used points for first region is " + allCorners.length);
			ptsFirstRegion = ptsFirstRegion.concat(corners);
			ptsFirstRegion.push(firstRegionPtArr[1].point);
			arrOfRegions.push(ptsFirstRegion);
			
			var ptsSecondRegion = [];
			//do the second region
			ptsSecondRegion.push(secondRegionPtArr[0].point);
			trace("all corners before removing used points is " + allCorners.length);
			corners = findCornersBetweenTwoPointsRemovingUsedPoints(secondRegionPtArr[0].point, secondRegionPtArr[0].angle, secondRegionPtArr[1].point, secondRegionPtArr[1].angle, middleRegionCenterPt, allCorners);
			trace("all corners after removing used points for second region is " + allCorners.length);
			ptsSecondRegion = ptsSecondRegion.concat(corners);
			ptsSecondRegion.push(secondRegionPtArr[1].point);
			arrOfRegions.push(ptsSecondRegion);
			
			//do the middle region: all the remaining corners will belong to it
			var ptsMiddleRegion = [];
			ptsMiddleRegion.push(ptArr[0].point);
			corners = findCornersBetweenTwoPointsRemovingUsedPoints(ptArr[0].point, ptArr[0].angle, ptArr[1].point, ptArr[1].angle, middleRegionCenterPt, allCorners);
			trace("all corners after removing used points is " + allCorners.length);
			ptsMiddleRegion = ptsMiddleRegion.concat(corners);
			ptsMiddleRegion.push(ptArr[1].point);
			corners = findCornersBetweenTwoPointsRemovingUsedPoints(ptArr[1].point, ptArr[1].angle, ptArr[2].point, ptArr[2].angle, middleRegionCenterPt, allCorners);
			trace("all corners after removing used points is " + allCorners.length);
			ptsMiddleRegion = ptsMiddleRegion.concat(corners);
			ptsMiddleRegion.push(ptArr[2].point);
			corners = findCornersBetweenTwoPointsRemovingUsedPoints(ptArr[2].point, ptArr[2].angle, ptArr[3].point, ptArr[3].angle, middleRegionCenterPt, allCorners);
			trace("all corners after removing used points is " + allCorners.length);
			ptsMiddleRegion = ptsMiddleRegion.concat(corners);
			ptsMiddleRegion.push(ptArr[3].point);
			corners = findCornersBetweenTwoPointsRemovingUsedPoints(ptArr[3].point, ptArr[3].angle, ptArr[0].point, ptArr[0].angle, middleRegionCenterPt, allCorners);
			trace("all corners after removing used points is " + allCorners.length);
			ptsMiddleRegion = ptsMiddleRegion.concat(corners);
			arrOfRegions.push(ptsMiddleRegion);
			
			return arrOfRegions;
			
			
		}
		
		function pointsFor4Regions(intersectionPt, ptArr){
			var arrOfRegions = [];
			for(var i:Number = 0; i < ptArr.length; i++){
				var ptsThisRegion = [];
				ptsThisRegion.push(ptArr[i].point);
				var corners:Array;
				//if this is the last one
				if(i == ptArr.length -1){
					corners = findCornersBetweenTwoPoints(ptArr[i].point, ptArr[i].angle, ptArr[0].point, ptArr[0].angle, intersectionPt, true);
				} else {
					corners = findCornersBetweenTwoPoints(ptArr[i].point, ptArr[i].angle, ptArr[i + 1].point, ptArr[i + 1].angle, intersectionPt);
				}
				if(corners.length){
					trace("there are " + corners.length + " corners");
					ptsThisRegion = ptsThisRegion.concat(corners);
					trace("after concat the array of ptsThisRegion has " + ptsThisRegion.length);
				}
				//if we are on the last one, add the first one as the next to last point
				if(i == ptArr.length -1){
					ptsThisRegion.push(ptArr[0].point);
					//otherwise add the next point
				} else {
					ptsThisRegion.push(ptArr[i + 1].point);
				}
				//the intersection point is always the last one before we close the shape
				ptsThisRegion.push(intersectionPt);
				arrOfRegions.push(ptsThisRegion);
				trace("the array has " + ptsThisRegion.length + " points");
				trace("-----------------------------------------");
			}
			return arrOfRegions;
			
		}
		
		function findCornersBetweenTwoPoints(firstPoint, firstAngle, secondPoint, secondAngle, intersectionPt, secondPointIsStartingPoint){
			var cornersBetweenTwoPoints = [];
			var cornersToAddLast = [];
			var allCorners = [{x:gridXmax, y:gridYmax}, {x:gridXmax, y:gridYmin}, {x:gridXmin, y:gridYmin}, {x:gridXmin, y:gridYmax}];
			for(var i:Number = 0; i < allCorners.length; i++){
			
				var cornerAngle = findAngleBetweenTwoPoints(intersectionPt, allCorners[i]);
				if(secondPointIsStartingPoint){
					
					if(!isBetween(cornerAngle, firstAngle, secondAngle)){
						if(cornerAngle <= 180){
							cornersToAddLast.push(allCorners[i]);
						} else {
						cornersBetweenTwoPoints.push(allCorners[i]);
						}
					} 
				}else{

					if(isBetween(cornerAngle, firstAngle, secondAngle)){
						cornersBetweenTwoPoints.push(allCorners[i]);
					}
				}
				
			}
			if(cornersToAddLast.length){
				cornersBetweenTwoPoints = cornersBetweenTwoPoints.concat(cornersToAddLast);
			}
			return cornersBetweenTwoPoints;
			
		}
		function findCornersBetweenTwoPointsRemovingUsedPoints(firstPoint, firstAngle, secondPoint, secondAngle, middleRegionCenterPt, remainingCorners){
		
			var cornersBetweenTwoPoints = [];
			var cornersToRemove = [];
			var cornersToAddLast = [];
			
			for(var i:Number = 0; i < remainingCorners.length; i++){
				
				var cornerAngle = findAngleBetweenTwoPoints(middleRegionCenterPt, remainingCorners[i]);
				
				if(secondAngle < firstAngle){
					
					if(!isBetween(cornerAngle, firstAngle, secondAngle)){	
						if(cornerAngle <= 180){
							cornersToAddLast.push(remainingCorners[i]);
						} else {
						cornersBetweenTwoPoints.push(remainingCorners[i]);
						}
						cornersToRemove.push(i);
					} 
				}else{
					
					if(isBetween(cornerAngle, firstAngle, secondAngle)){						
						cornersBetweenTwoPoints.push(remainingCorners[i]);
						cornersToRemove.push(i);
					}
				}
				
			}
			if(cornersToAddLast.length){
				cornersBetweenTwoPoints = cornersBetweenTwoPoints.concat(cornersToAddLast);
			}
			for (var i:Number = cornersToRemove.length - 1; i >=0 ; i--){
				remainingCorners.splice(cornersToRemove[i], 1);
			}
			return cornersBetweenTwoPoints;
			
			
		}
		//TODO - for testing only - remove 
		function traceAnglesOfLineSegments(intersectionPt:Object, firstLine:Line, secondLine:Line){
			trace("First point angle " + findAngleBetweenTwoPoints(intersectionPt, firstLine.startPoint));
			trace("second point angle " + findAngleBetweenTwoPoints(intersectionPt, secondLine.endPoint));
			trace("third point angle " + findAngleBetweenTwoPoints(intersectionPt, firstLine.endPoint));
			trace("fourth point angle " + findAngleBetweenTwoPoints(intersectionPt, secondLine.startPoint));
		}
		
		function createArrayOfPointsTopRegion(line:Line){
			var arr = [];
			arr.push(line.startPoint, {x:gridXmin, y:gridYmax}, {x:gridXmax, y:gridYmax}, line.endPoint);
			return arr;
		}
		function createArrayOfPointsBottomRegion(line:Line){
			var arr = [];
			arr.push(line.startPoint, {x:gridXmin, y:gridYmin}, {x:gridXmax, y:gridYmin}, line.endPoint);
			return arr;
			
		}
		function createArrayOfPointsMiddleRegion(firstLine:Line, secondLine:Line){
			var arr=[];
			arr.push(firstLine.startPoint, firstLine.endPoint, secondLine.endPoint, secondLine.startPoint);
			return arr;
		}
		function createArrayOfPointsLeftVerticalRegion(line:Line){
			var arr = [];
			arr.push(line.startPoint, {x:gridXmin, y:gridYmax}, {x:gridXmin, y:gridYmin}, line.endPoint); 
			
			return arr;
		}
		function createArrayOfPointsRightVerticalRegion(line:Line){
			var arr=[];
			arr.push(line.startPoint, {x:gridXmax, y:gridYmax}, {x:gridXmax, y:gridYmin}, line.endPoint); 
			return arr;
		}
		function createArrayOfPointsRightRegionPositiveSlope(line:Line){
			var arr = [];
			arr.push(line.startPoint);
			if(line.startPoint.x < gridXmax){
				arr.push({x:gridXmax, y:gridYmax});
			}
			arr.push({x:gridXmax, y:gridYmin});
			if(line.endPoint.x == gridXmin){
				arr.push({x:gridXmin, y:gridYmin});
			}
			arr.push(line.endPoint);
			return arr;
			
		}
		function createArrayOfPointsMiddleRegionPositiveSlope(firstLine:Line, secondLine:Line){
			var arr = [];
			arr.push(secondLine.startPoint);
			if((secondLine.startPoint.x != firstLine.startPoint.x) && (secondLine.startPoint.y != firstLine.startPoint.y)){
				arr.push({x:gridXmax, y:gridYmax});
			}
			arr.push(firstLine.startPoint);
			arr.push(firstLine.endPoint);
			
			if((secondLine.endPoint.x != firstLine.endPoint.x) && (secondLine.endPoint.y != firstLine.endPoint.y)){
				arr.push({x:gridXmin, y:gridYmin});
			}
			arr.push(secondLine.endPoint);
			return arr;
			
		}
		
		function createArrayOfPointsLeftRegionPositiveSlope(line:Line){
			var arr=[];
			arr.push(line.startPoint);
			if(line.startPoint.y < gridYmax){
				arr.push({x:gridXmax, y:gridYmax});
			}
			arr.push({x:gridXmin, y:gridYmax});
			if(line.endPoint.x > gridXmin){
				arr.push({x:gridXmin, y:gridYmin});
			}
			arr.push(line.endPoint);
			return arr;
		}
		function createArrayOfPointsRightRegionNegativeSlope(line:Line){
			var arr = [];
			arr.push(line.startPoint);
			if(line.startPoint.y < gridYmax){
				arr.push({x:gridXmin, y:gridYmax});
			}
			arr.push({x:gridXmax, y:gridYmax});
			if(line.endPoint.x < gridXmax){
				arr.push({x:gridXmax, y:gridYmin});
			}
			arr.push(line.endPoint);
			return arr;
		}
		function createArrayOfPointsMiddleRegionNegativeSlope(firstLine:Line, secondLine:Line){
			var arr = [];
			arr.push(secondLine.startPoint);
			if((secondLine.startPoint.x != firstLine.startPoint.x) && (secondLine.startPoint.y != firstLine.startPoint.y)){
				arr.push({x:gridXmin, y:gridYmax});
			}
			arr.push(firstLine.startPoint);
			arr.push(firstLine.endPoint);
			
			if((secondLine.endPoint.x != firstLine.endPoint.x) && (secondLine.endPoint.y != firstLine.endPoint.y)){
				arr.push({x:gridXmax, y:gridYmin});
			}
			arr.push(secondLine.endPoint);
			return arr;
			
		}
		function createArrayOfPointsLeftRegionNegativeSlope(line:Line){
			var arr = [];
			arr.push(line.startPoint);
			if(line.startPoint.x > gridXmin){
				arr.push({x:gridXmin, y:gridYmax});
			}
			arr.push({x:gridXmin, y:gridYmin});
			if(line.endPoint.x == gridXmax){
				arr.push({x:gridXmax, y:gridYmin});
			}
			arr.push(line.endPoint);
			return arr;		
			
		}
		function isBetween(myNum:Number, first:Number, last:Number){
			return (first < last ? myNum >= first && myNum <= last : myNum >= last && myNum <= first);
		}
		function findAngleBetweenTwoPoints(point1:Object, point2:Object):Number{
			var x1:Number = point1.x;
			var x2:Number = point2.x;
			var y1:Number = point1.y;
			var y2:Number = point2.y;
			var angleInRadians:Number;
			var angleinDgrees:Number;
			
			angleInRadians = findAngleGivenX1Y1X2Y2(_graph.graphToPixelsX(x1), _graph.graphToPixelsY(y1), _graph.graphToPixelsX(x2), _graph.graphToPixelsY(y2));
			angleInRadians += Math.PI / 2;
			var angleInDegrees = degFromRad(angleInRadians);
			var angleInDegrees = (angleInDegrees < 0)? 360 + angleInDegrees : angleInDegrees
			return angleInDegrees;
		}
		function findAngleGivenX1Y1X2Y2(x1:Number, y1:Number, x2:Number, y2:Number):Number {
			
			var dy = y2 - y1;
			var dx = x2 - x1;
			return Math.atan2(dy, dx);
			
		}
		function degFromRad(p_radInput:Number):Number {
			return  (180 / Math.PI ) * p_radInput;
		}
		
		function findRegionWithNameOfMovieClip(mcName:String):Region{
			var regionToReturn:Region;
			trace("region array has " + aRegions.length);
			for (var i:Number = 0; i < aRegions.length; i++){
				 var region:Region = Region(aRegions[i]);
				 trace("i is " + i);
				 trace("aRegions[i].regionMC._name is " + aRegions[i].regionMC._name);
				
				if(region.regionMC._name == mcName){
					regionToReturn = region;
					break;
				}
				
				
			}
			return regionToReturn;
		}
		function deSelectRegions(){
			for (var i:Number = 0; i < aRegions.length; i++){
				var region:Region = Region(aRegions[i]);
				region.deSelect();
				
				
			}
		}
		
	}
