
	 class Region
	{
		private static var UNSELECTED_COLOR:Number = 0xCCCCCC;
		private static var SELECTED_COLOR:Number = 0xFF0000;
		private static var UNSELECTED_ALPHA:Number = 10;
		private static var UNSELECTED_ALPHA_NO_SHADING_COLOR:Number = 0;
		private static var SELECTED_ALPHA:Number = 50;
		private var _color:Number;
		private var _alpha:Number;
		private var _vertices:Array;
		public var regionMC:MovieClip;
		private var shapeFillMC:MovieClip;
		private var _graph:GraphView; //this is a reference to the graph so we can access the graphToPixels methods
		private var _parent_mc:MovieClip;
		private var _selected:Boolean;
		private var _unselectedColor:Number;
		private var _unselectedAlpha:Number;

		
		function Region(parentMC:MovieClip, graph:GraphView, ptArr:Array, nameString:String, nColor:Number)
		{
			var nDepth:Number = parentMC.getNextHighestDepth();
			
			regionMC = parentMC.createEmptyMovieClip(nameString, nDepth);
			_unselectedColor = (nColor)? nColor: Region.UNSELECTED_COLOR;
			_unselectedAlpha = (nColor)? Region.UNSELECTED_ALPHA: Region.UNSELECTED_ALPHA_NO_SHADING_COLOR;
			//we will select it separately
			//_color = (graph.selectedRegion == nameString)? Region.SELECTED_COLOR :_unselectedColor;
			_color = (nColor)? nColor: Region.UNSELECTED_COLOR;
			//_alpha = (graph.selectedRegion == nameString)? Region.SELECTED_ALPHA :_unselectedAlpha;
			_alpha = (nColor)? Region.UNSELECTED_ALPHA: Region.UNSELECTED_ALPHA_NO_SHADING_COLOR;
			_vertices = ptArr;
			//_selected = (graph.selectedRegion == nameString)? true : false;
			_selected = false;
			shapeFillMC = regionMC.createEmptyMovieClip("shapeFillMC", regionMC.getNextHighestDepth());
			_parent_mc = parentMC;
			_graph = graph;
			drawRegion();
		}
	function drawRegion(){
		regionMC.shapeFillMC.clear();
		regionMC.shapeFillMC.lineStyle(0, 0, 0);
		regionMC.shapeFillMC.beginFill(this._color, this._alpha);	
		var nX:Number;
		var nY:Number;
		
		var nStartX:Number = this._graph.graphToPixelsX(this._vertices[0].x);
		var nStartY:Number = this._graph.graphToPixelsY(this._vertices[0].y);
		regionMC.shapeFillMC.moveTo(nStartX, nStartY);
		
		for(var i:Number = 1; i < this._vertices.length; i++){
			nX = this._graph.graphToPixelsX(_vertices[i].x);
			nY = this._graph.graphToPixelsY(_vertices[i].y);
			regionMC.shapeFillMC.lineTo(nX, nY);
		}
		
		regionMC.shapeFillMC.lineTo(nStartX, nStartY);
		regionMC.shapeFillMC.endFill();	
		
	}
	function toggleStatus(){

		this._selected = !this.isSelected();

		this._color = (this.isSelected())? Region.SELECTED_COLOR :this._unselectedColor;
		this._alpha = (this.isSelected())? Region.SELECTED_ALPHA :this._unselectedAlpha;

		this.drawRegion();
	}
	function deSelect(){

		if(this.isSelected()){
			
			this.toggleStatus();
		}
	
	}
	function select(){
		if(!this.isSelected()){
			
			this.toggleStatus();
		}
	
		
	}
	function isSelected():Boolean{
		return this._selected;
		
	}
	function getColor():Number{
		return this._color;
	}
	
}
