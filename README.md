# Graded SWFs ##

ActionScript 2.0 and Flash source code for the Graded SWFs used in the IBIS homework application

### List of most frequently used SWFs, i.e. ones used across many different questions, as opposed to in one or two ###

* 016 - Drop down menu
* 022 - Ranking module
* 027 - Labeling module
* 030 - Graphs, the Line-mover, aka Econ. Line-mover
* 037 - Clickable area

### Complete list of included source files by directory: ###

* 000 - External SWF API (original API documents for the gradeable SWF, aka "Complex External SWF")
* 001 - Titration interactive (loads variables from Algo values)
* 002 - Wittig pt. 1 (drag and drop activity with animation and answer set by Algo)
* 003 - Wittig, pt. 2 (part 2 used in combination with part 1)
* 004 - Carbonyl ranking (single-use ranking modue)
* 005 - Sorting acids and bases (deprecated, early version of sorting module)
* 006 - Ranking acids (deprecated, early version of ranking module)
* 007 - MMR and IR graphs (loads external images set by Algo)
* 008 - Ranking module generic (deprecated, early version of ranking module)
* 009 - Bond polarity arrow (toggle image)
* 010 - Density archimedes (lab with Algo values)
* 011 - Coffee cup calorimeter (lab with Algo values)
* 012 - Equilibrium arrow (toggle image)
* 013 - Molecular orbital diagram (toggle orbital diagram)
* 014 - Student data entry (numeric entry with calculation and Algo values)
* 015 - Ranking dynamic sets (deprecated)
* 016 - Drop down menu (of different sizes, widely used)
* 017 - Short answer multiple correct
* 018 - Sorting module (deprecated, early version of sorting)
* 019 - Graduated cylinder (lab with Algo values)
* 020 - Sorting module dynamic (deprecated, the graded SWF version of sorting module which became a *real* IBIS module)
* 021 - Electron configuration (diagram builder, unfinished)
* 022 - Ranking module dynamic (the ranking modules widely in use, with varying sizes)
* 023 - Spectra viewing module (tabbed external image viewer)
* 024 - Charge drop down menu (customized drop down menu)
* 025 - Image loader - displays external images based on Algos
* 026 - Short answer small (numeric entry)
* 027 - Labeling module (the labeling module widely in use, with varying sizes)
* 028 - Sugar-Fischer projection (specific molecular diagram builder)
* 029 - Basic kinetics lab (student entry table)
* 030 - Graphs, the Line-mover, aka Econ. Line-mover (widely in use, mostly Econ. but also Physics and Astro.)
* 031 - Griddable answer (high school standardized test entry)
* 032 - WWU labs (conductivity, spectroscopy, stoichiometry labs)
* 033 - Line of reasonable fit (high school math)
* 034 - Inequalities (high school math)
* 035 - Solving systems of equations (high school math)
* 036 - Graphing functions (high school math)
* 037 - Clickable area (HE Biology and others)
* 038 - Curved arrow toggle (diagram toggle, variation on equilibrium arrow)
* shared (ActionScript classes used in across multiple SWFs)

### Who do I talk to? ###

* Jeff Sims authored most of these
* José Gómez and Verna Hartinger also contributed