class LineFitGradedSwf extends MovieClip {
	var gradeable_id:String;
	var focused:Boolean;
	var directions:MovieClip;
	var lineFitModule:MovieClip;	//movie clip containing the module
	var debug:TextField;
	var tabType:String;
	
	function LineFitGradedSwf() {
	}
	
	function checkAnswer($authored_object) {

		trace ('checkAnswer________________________________');

		this.lineFitModule.debug.text = "";
		//var match:Boolean = true;
		var pointsMatch:Boolean = true;
		var linesMatch:Boolean = true;
		var equationsMatch:Boolean = true;
		//check points if graded
		trace ('allowPointEditing ' + this.lineFitModule.allowPointEditing)
		if (!this.lineFitModule.allowPointEditing) {
			//ignore points for grading if student can't edit points
			$authored_object.myGradingOptions.wildcardOptions.allPoints = false;
			trace ('allowPointEditing trigger ignore')
		}
		if ($authored_object.myGradingOptions.wildcardOptions.allPoints) {
			var authorPoints = $authored_object.myGraph.myPoints;
			var studentPoints = this.lineFitModule.myGraph.myPoints;
			var matchingPoint:Boolean, checkX:Number, checkY:Number, thisX:Number, thisY:Number;
			for (var p in authorPoints) {
				matchingPoint = false;
				checkX = authorPoints[p].x;
				checkY = authorPoints[p].y;
				for (var s in studentPoints) {
					thisX = studentPoints[s].x;
					thisY = studentPoints[s].y;
					if (checkX == thisX && checkY == thisY) {
						matchingPoint = true;
						break;
					}
				}
				if (!matchingPoint) {
					pointsMatch = false;
					break;
				}
			}
			if (authorPoints.length != studentPoints.length) {
				pointsMatch = false;
			}
			trace ('authorPoints.length: ' + authorPoints.length);
			trace ('studentPoints.length: ' + studentPoints.length);
		}
		trace ('points match: ' + pointsMatch);

		//check line if graded
		if ($authored_object.myGradingOptions.wildcardOptions.line) {
			//if graph slope graded
			if ($authored_object.myGradingOptions.wildcardOptions.gSlope) {	
				var authorSlope:Number = $authored_object.myEquation.slopeEntry;
				var studentSlope:Number = this.lineFitModule.myGraph.mySlope;

				//check for blank graph, could be either undefined or NaN
				var studentBlankGraphSlope:Boolean = false;
				var authorBlankGraphSlope:Boolean = false;
				if (studentSlope == undefined || isNaN(studentSlope)) {
					studentBlankGraphSlope = true;
				}
				if (authorSlope == undefined || isNaN(authorSlope)) {
					authorBlankGraphSlope = true;
				}

				if (studentBlankGraphSlope != authorBlankGraphSlope) {
					linesMatch = false;
				}
				
				var slopeRangePos = $authored_object.myGradingOptions.graphSlopeTolerancePos;
				var slopeRangeNeg = $authored_object.myGradingOptions.graphSlopeToleranceNeg;

				if (!(studentSlope <= authorSlope + slopeRangePos && studentSlope >= authorSlope - slopeRangeNeg)) {
					linesMatch = false;
				}
			}

			//if graph intercept graded
			if ($authored_object.myGradingOptions.wildcardOptions.gIntercept) {	
				var authorInt:Number = $authored_object.myEquation.interceptEntry;
				var studentInt:Number = this.lineFitModule.myGraph.myIntercept;
				
				//check for blank graph, could be either undefined or NaN
				var studentBlankGraphInt:Boolean = false;
				var authorBlankGraphInt:Boolean = false;
				if (studentInt == undefined || isNaN(studentInt)) {
					studentBlankGraphInt = true;
				}
				if (authorInt == undefined || isNaN(authorInt)) {
					authorBlankGraphInt = true;
				}

				if (studentBlankGraphInt != authorBlankGraphInt) {
					linesMatch = false;
				}

				var intRangePos = $authored_object.myGradingOptions.graphInterceptTolerancePos;
				var intRangeNeg = $authored_object.myGradingOptions.graphInterceptToleranceNeg;

				if (!(studentInt <= authorInt + intRangePos && studentInt >= authorInt - intRangeNeg)) {
					linesMatch = false;
				}
			}

		}
		trace ('line match: ' + linesMatch);

		//check equation if graded
		//this.lineFitModule.debug.text += "\nequ graded = " + $authored_object.myGradingOptions.wildcardOptions.equation;
		trace ('allowEquationEntry ' + this.lineFitModule.allowEquationEntry)
		if (!this.lineFitModule.allowEquationEntry) {
			//ignore equation for grading if student can't edit equation
			$authored_object.myGradingOptions.wildcardOptions.equation = false;
			trace ('allowEquationEntry trigger ignore')
		}
		if ($authored_object.myGradingOptions.wildcardOptions.equation) {
			//if equation slope graded
			if ($authored_object.myGradingOptions.wildcardOptions.eSlope) {	
				var authorSlopeEntry:Number = $authored_object.myEquation.slopeEntry;
				var studentSlopeEntry:Number = this.lineFitModule.myEquation.slopeEntry;

				//check for blank entry -- can be undefined or empty string or slope is 0 and intercept is NaN
				var studentSlopeBlank:Boolean = false;
				var authorSlopeBlank:Boolean = false;
				if (studentSlopeEntry == undefined || studentSlopeEntry == "" || (studentSlopeEntry == 0 && isNaN(this.lineFitModule.myEquation.interceptEntry))) {
					studentSlopeBlank = true;
				}
				if (authorSlopeEntry == undefined || authorSlopeEntry == "") {
					authorSlopeBlank = true;
				}
				if (studentSlopeBlank != authorSlopeBlank) {
					equationsMatch = false;
				}

				var eSlopeRangePos = $authored_object.myGradingOptions.equationSlopeTolerancePos;
				var eSlopeRangeNeg = $authored_object.myGradingOptions.equationSlopeToleranceNeg;

				if (!(studentSlopeEntry <= authorSlopeEntry + eSlopeRangePos && studentSlopeEntry >= authorSlopeEntry - eSlopeRangeNeg)) {
					equationsMatch = false;
					//this.lineFitModule.debug.text += "\nslope no match"
				}
			}
			//if equation intercept graded
			if ($authored_object.myGradingOptions.wildcardOptions.eIntercept) {	
				var authorIntEntry:Number = $authored_object.myEquation.interceptEntry;
				var studentIntEntry:Number = this.lineFitModule.myEquation.interceptEntry;

				//check for blank entry -- can be undefined or empty string or slope is 0 and intercept is NaN
				var studentIntBlank:Boolean = false;
				var authorIntBlank:Boolean = false;
				if (studentIntEntry == undefined || studentIntEntry == "" || (studentSlopeEntry == 0 && isNaN(studentIntEntry))) {
					studentIntBlank = true;
				}
				if (authorIntEntry == undefined || authorIntEntry == "") {
					authorIntBlank = true;
				}
				if (studentIntBlank != authorIntBlank) {
					equationsMatch = false;
				}

				var eIntRangePos = $authored_object.myGradingOptions.equationInterceptTolerancePos;
				var eIntRangeNeg = $authored_object.myGradingOptions.equationInterceptToleranceNeg;

				if (!(studentIntEntry <= authorIntEntry + eIntRangePos && studentIntEntry >= authorIntEntry - eIntRangeNeg)) {
					equationsMatch = false;
					//this.lineFitModule.debug.text += "\nint no match"
				}
			}
			//extra stuff in equation
			if (this.lineFitModule.myEquation.extraStuff.length) {
				//this.lineFitModule.debug.text = 'extra stuff in equation';
				equationsMatch = false;
			}	
		}
		trace ('equation match: ' + equationsMatch);
		
		trace ('\nauthorSlopeEntry: ' + authorSlopeEntry);
		trace ('authorIntEntry: ' + authorIntEntry);
		trace ('\nstudentSlopeEntry: ' + studentSlopeEntry);
		trace ('studentIntEntry: ' + studentIntEntry);
		trace ('___________________________________________\n');
		//trace ('_____author equation');
		//helpers.TraceRecursive.traceObject($authored_object.myEquation);
		//trace ('\n____student graph');
		//helpers.TraceRecursive.traceObject(this.lineFitModule.myGraph);
		if (pointsMatch && linesMatch && equationsMatch) {
			return true;
		} else {
			return false;
		}
	}
	/********
	this function is called when the user clicks on this module to edit it
	include registering of all key listeners and mouse listeners as well as enabling buttons, etc.
	********/
	function setFocus() {
		this.directions._visible = true;
		this.focused = true;
	}
	/*******
	this function is called when the user clicks on a different module or on the stage and thus kills focus on this module
	include removal of all key listeners and mouse listeners as well as disabling any buttons, etc.
	*******/
	function killFocus() {
		this.focused = false;
		this.directions._visible = false;
	}
	/***** REQUIRED FUNCTIONS FOR COMPLEX SWF GRADING *******/
	/*********
	this function is called each time the user switches between tabs so that the current gradeable data will be stored
	return any object/array/number/string or combination of the above
	the object should be as simple as possible but must contain all information required for uniquely grading different responses
	**********/
	function getGradeableData() {
		var rtn:Object = new Object();

		tabType = _global.core.Globals.tabType;
		//settings constant across whole question
		if (tabType == "question") {
			rtn.mySettings = this.lineFitModule.mySettings;
		}
		rtn.myGraph = this.lineFitModule.myGraph;
		rtn.myEquation = this.lineFitModule.myEquation;
		rtn.myGradingOptions = this.lineFitModule.myGradingOptions;

		//trace ('________sgetGradeableData___________');
		//helpers.TraceRecursive.traceObject(rtn);

		return rtn;
	}
	/**********
	this function is called each time the user switches to a response tab so that the gradeable data for that tab will be displayed
	the incoming parameter will match exactly the format of whatever is returned from the getGradeableData function
	at this point the module should be modified to redisplay exactly what would have been present when the user last viewed the tab with the associated gradeableData
	***********/
	var isSetup:Boolean = false;
	function setGradeableData($v) {
		tabType = _global.core.Globals.tabType;
		if (tabType == "question" or isSetup==false) {
			this.lineFitModule.mySettings = $v.mySettings;
			isSetup = true;
		}
		this.lineFitModule.myGraph = $v.myGraph;
		this.lineFitModule.myEquation = $v.myEquation;
		this.lineFitModule.myGradingOptions = $v.myGradingOptions;
		
		this.lineFitModule.reload();

		//trace ('________setGradeableData___________');
		//helpers.TraceRecursive.traceObject($v);
	}
}
