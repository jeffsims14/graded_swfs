class AxisLabels extends MovieClip{

	private var axis_x_label:String;
	private var axis_y_label:String;
	private var myGraph:Object;
	private var mc:MovieClip;
	private var d:Number;
	private var labelFormat:TextFormat;
	private var appMode:String;
	private var xyOffset = 26;
	private var yxOffset = 36;
	
	function AxisLabels(mainClass, labelMC) {
		myGraph = mainClass;
		mc = labelMC;
		d = mc.getNextHighestDepth();
		appMode = myGraph.appMode;

		axis_x_label = myGraph.axis_x_label;
		axis_y_label = myGraph.axis_y_label;

		if (axis_x_label == undefined) {
			axis_x_label = "x axis";
		}
		if (axis_y_label == undefined) {
			axis_y_label = "y axis";
		}

		labelFormat = new TextFormat();
		labelFormat.font = "Arial Reduced";
		labelFormat.align = "center";

		//trace ('x label: ' + axis_x_label);
		//trace ('y label: ' + axis_y_label);

		init();
	}

	public function setVisibility() {
		var showAxisLabels = myGraph.showAxisLabels;
		mc.xAxisLabel._visible = showAxisLabels;
		mc.yAxisLabel._visible = showAxisLabels;
		mc.xHandle._visible = showAxisLabels;
		mc.yHandle._visible = showAxisLabels;
		if (showAxisLabels && myGraph.showXgrid) {
			myGraph.panels.equationPanel._y = 350;
		} else {
			myGraph.panels.equationPanel._y = 330;
		}
	}

	public function init() {

		var xAxisLabel:TextField = newAxisLabel(axis_x_label, "xAxisLabel", 0);
		var yAxisLabel:TextField = newAxisLabel(axis_y_label, "yAxisLabel", 270);
		
		//initial text centering on axis
		positionAxesLabels();
		
		//make axis labels movable in author mode
		if (appMode == "author") { // and tabType == "question") {
			//if (axis_x_label != "") {
				mc.xHandle.removeMovieClip();
				mc.yHandle.removeMovieClip();
				var xHandle:MovieClip = mc.attachMovie('axisLabelHandle', 'xHandle', d++);
				//allow author to drag and drop to set custom position
				makeDraggable(xHandle, xAxisLabel, "xaxis");
			//}
			//if (axis_y_label != "") {
				var yHandle:MovieClip = mc.attachMovie('axisLabelHandle', 'yHandle', d++);
				//allow author to drag and drop to set custom position
				makeDraggable(yHandle, yAxisLabel, "yaxis");
			//}
		}
		setVisibility();
	}

	public function updateLabels() {
		var myLookup = new AlgoLookup();
		var xLabel = myLookup.findAlgoValue(myGraph.axis_x_label);
		var yLabel = myLookup.findAlgoValue(myGraph.axis_y_label);
		mc.xAxisLabel.htmlText = xLabel;
		mc.yAxisLabel.htmlText = yLabel;
		positionAxesLabels();
		if (appMode == "author") {
			mc.xHandle._x = mc.xAxisLabel._x;
			mc.xHandle._y = mc.xAxisLabel._y;
			mc.yHandle._x = mc.yAxisLabel._x;
			mc.yHandle._y = mc.yAxisLabel._y;
		}
	}

	private function newAxisLabel(labelText, myName, angle):TextField {

		var axisLabel:TextField = mc.createTextField(myName, d++, 0, 0, 1, 1);
		var classref:Object = this;

		var myLookup = new AlgoLookup();
		labelText = myLookup.findAlgoValue(labelText);

		if (appMode == "author") {
			axisLabel.type = "input";
			axisLabel.selectable = true;
			
			axisLabel.onChanged = function() {
				//this.axis_x_label = xAxisLabel.text;
				this.setTextFormat(labelFormat);
				classref.positionAxesLabels();
				classref.myGraph.updateSettings();
			};
		} else {
			axisLabel.selectable = false;
		}
		
		axisLabel.setNewTextFormat(labelFormat);
		axisLabel.html = true;
		axisLabel.autoSize = true;
		axisLabel.embedFonts = true;
		axisLabel.htmlText = labelText;
		axisLabel._rotation = angle;
		
		return axisLabel;
	}
	
	public function positionAxesLabels(){
		var xLbl = mc.xAxisLabel;
		var yLbl = mc.yAxisLabel;
		
		var xTxtWidth:Number = xLbl.textWidth;
		var yTxtWidth:Number = yLbl.textWidth;
		var graphW:Number = myGraph.graphWidth;
		var graphH:Number = myGraph.graphHeight;
		
		var xxPos = myGraph.graphXposition + graphW / 2 - xTxtWidth/2;
		var xyPos = myGraph.graphYposition + graphH + xyOffset;

		var yxPos = myGraph.graphXposition - yxOffset;
		var yyPos = myGraph.graphYposition + graphH / 2 + yTxtWidth/2;

		//update original positions for axis label handles
		updateOriginalPos(mc.xHandle, xLbl, xxPos, xyPos, "xaxis");
		updateOriginalPos(mc.yHandle, yLbl, yxPos, yyPos, "yaxis");
		
		//custom placement
		xxPos -=  myGraph.axisLabelPositions[0];
		xyPos -=  myGraph.axisLabelPositions[1];
		yxPos -=  myGraph.axisLabelPositions[2];
		yyPos -=  myGraph.axisLabelPositions[3];

		xLbl._x = xxPos;
		xLbl._y = xyPos;
		
		yLbl._x = yxPos;
		yLbl._y = yyPos;

		myGraph.axis_x_label = mc.xAxisLabel.text;
		myGraph.axis_y_label = mc.yAxisLabel.text;
	}

	//setup for draggable labels to allow custom positions
	private function makeDraggable(buttonMC:MovieClip, tf:TextField, labelType:String) {
		var classref = this;
		
		buttonMC._x = tf._x;
		buttonMC._y = tf._y;
			
		if (labelType == "yaxis") {
			buttonMC._rotation = 270;
		} else if (labelType == "xaxis") {
			//nothing else
		} else {
			
			buttonMC._x = tf._x;
			buttonMC._y = tf._y;
			
			//invisible background to make it button-like
			buttonMC.lineStyle(1,0x999999,0);
			buttonMC.beginFill(0xFFFFFF,0);
			buttonMC.moveTo(0,0);
			buttonMC.lineTo(tf._width, 0);
			buttonMC.lineTo(tf._width, tf._height);
			buttonMC.lineTo(0, tf._height);
			buttonMC.lineTo(0,0);
		}
		
		//make it draggable
		buttonMC.onPress = function() {
			startDrag(this, false);
			this.onMouseMove = function() {
				tf._x = this._x;
				tf._y = this._y;
			}
		}
		buttonMC.onRelease = buttonMC.onReleaseOutside = function() {
			this.stopDrag();
			tf._x = this._x;
			tf._y = this._y;
			this.onMouseMove = undefined;
			classref.saveLabelPosition(0, 0, labelType);
		}
	}
	
	//setup for draggable labels to allow custom positions
	private function updateOriginalPos(buttonMC:MovieClip, tf:TextField, originalX:Number, originalY:Number, labelType:String) {
		var classref = this;
		
		buttonMC._x = tf._x;
		buttonMC._y = tf._y;
		
		
		buttonMC.onRelease = buttonMC.onReleaseOutside = function() {
			this.stopDrag();
			tf._x = this._x;
			tf._y = this._y;
			this.onMouseMove = undefined;
			//update saved plot with new position
			classref.saveLabelPosition(originalX - tf._x, originalY - tf._y, labelType);
		}
	}
	//update custom label positions
	public function saveLabelPosition(adjustX:Number, adjustY:Number, labelType:String) {
		switch(labelType) {
			case "xaxis":
			myGraph.axisLabelPositions[0] = adjustX;
			myGraph.axisLabelPositions[1] = adjustY;
			break;
			case "yaxis":
			myGraph.axisLabelPositions[2] = adjustX;
			myGraph.axisLabelPositions[3] = adjustY;
			break;
		}
		myGraph.updateSettings();
	}
	
}