class LineFit extends MovieClip {

	//saved in graded swf in myGraph (points and line)
	var myPoints:Array;
	var myLine:Line;
	var mySlope:Number;
	var myIntercept:Number;

	//saved in graded swf in mySettings (graph settings)
	var gridXmin:Number = 0;
	var gridXmax:Number = 10;
	var gridXstep:Number = 1;
	var gridYmin:Number = 0;
	var gridYmax:Number = 10;
	var gridYstep:Number = 1;
	var showXgrid:Boolean = true;		//show x grid values
	var showYgrid:Boolean = true;		// show y grid values
	var showZeroLines:Boolean = true;	//toggle to draw mid-axis lines at zero (applies to both x- and y-axis)
	var axis_x_label:String;
	var axis_y_label:String;
	var axisLabelPositions:Array = [0,0,0,0];	//custom placement for axis labels
	var showAxisLabels:Boolean = false;			//toggle axis labels (main labels) on or off

	//graph settings, not variable
	var showGrid:Boolean = true;		//grid on or off
	var axisArrowSize = 18, plotArrowSize = 24;
	var axisExtension = 24;

	var axis, rowArray, samplePoints, midAxis, panels, plot, range, labeling, editPanels, gradingPanels;
	var plotColor:Number = 0xFF0000, plotStroke:Number = 3;	//plot line settings, red line
	var dotSize:Number = 5, dotStroke:Number = 1;
	var drawMode:String, toggleGroup:Array;

	var graphWidth:Number = 300;
	var graphHeight:Number = 300;
	var d = 1;		//movieClip level depth
	var tabNum = 1;	//tab index for input fields

	//graph placement
	var graphXposition:Number = 0;
	var graphYposition:Number = 0;
	var edgeSpace:Number = 10;	//extending hit area on outside edge of graph

	var graph:MovieClip;
	var grid:MovieClip;
	var myGrid:GridSetup;
	var myPointDrawing:PointDrawing;
	var myAxisLabels:AxisLabels;
	var myGraphOptions:SetupOptions;

	var myGraph:Object;				//saved info on graphed points and line (set per tab)
	var myEquation:Object;			//saved info on equation (set per tab)
	var mySettings:Object;			//saved graph settings (set question wide)
	var myGradingOptions:Object;	//saved grading options (set per tab)
	var equationDisplay:TextField;  // interceptDisplay:TextField, interceptSignToggle:TextField;
	var slopeEntry:Number;	//student entry slope
	var interceptEntry:Number;	//student entry y-intercept
	var equationEntry:String;  //student entry whole equation
	var extraStuff:String;		//extra stuff in student equation
	var appMode, tabType;
	var graphSlopeTolerancePos:Number;
	var graphSlopeToleranceNeg:Number;
	var graphInterceptTolerancePos:Number;
	var graphInterceptToleranceNeg:Number;
	var equationSlopeTolerancePos:Number;
	var equationSlopeToleranceNeg:Number;
	var equationInterceptTolerancePos:Number;
	var equationInterceptToleranceNeg:Number;
	var ifSlopeGreater:Number;
	var ifSlopeLess:Number;
	var ifInterceptGreater:Number;
	var ifInterceptLess:Number;
	var wildcardOptions:Object;
	var gridChanged:Boolean = false;
	var debug:TextField;

	function LineFit() {
		//debug text
		//debug = this.createTextField('debug',d++,0,430,550,300);
		//debug.text = 'hello'

		init();
	}

// INIT function called on first load
	function init() {
		appMode = _global.core.Globals.appMode;
		//appMode = "author";

		graph = this.createEmptyMovieClip("graph_mc", d++);
		grid = this.createEmptyMovieClip("grid_mc", d++);
		axis = this.createEmptyMovieClip("axis_mc", d++);
		midAxis = this.createEmptyMovieClip("axis_mc", d++);
		panels = this.createEmptyMovieClip("panels_mc", d++);
		range = this.createEmptyMovieClip("range_mc", d++);
		plot = this.createEmptyMovieClip("plot_mc", d++);
		gradingPanels = this.createEmptyMovieClip("gradingPanels", d++);
		labeling = this.createEmptyMovieClip("labeling_mc", d++);
		//editPanels = this.createEmptyMovieClip("ePanels_mc", d++);
		
		//draw white bg, hit area for graph
		graph.beginFill(0xFFFFFF, 100);
		graph.moveTo(graphXposition - edgeSpace, graphYposition - edgeSpace);
		graph.lineTo(graphXposition - edgeSpace, graphYposition + graphHeight + edgeSpace);
		graph.lineTo(graphXposition + graphWidth + edgeSpace, graphYposition + graphHeight + edgeSpace);
		graph.lineTo(graphXposition + graphWidth + edgeSpace, graphYposition - edgeSpace);

		//reloadSettings( mySettings );
		//if (mySettings != undefined) {
			
			//reloadSettings( mySettings );
		//}
		//axis and grid setup
		myAxisLabels = new AxisLabels(this, labeling);
		myGrid = new GridSetup(this, grid);
		myGrid.drawGrid();
		myGrid.drawAxis(axis);
		myPointDrawing = new PointDrawing(this);

		//default mode
		drawMode = "point";

		//click action for graph
		graph.onPress = function() {
			_parent.clickGraph();
		}

		//button setup
		/* ADD POINTS, DRAW LINE PANEL */
		var toolsPanel = panels.attachMovie("Panel", "toolsPanel", d++, {w:100, h:120, _x:320, _y:2});	//panel for mode buttons

		var addPointsButton = toolsPanel.attachMovie("SimpleButton", "addPointsButton", d++, {w:80, h:20, _x:10, _y:10, id:"point", labelText:"+/&#8211; Points"});
		addPointsButton.setAction( switchMode );
		addPointsButton.highlighted = true;

		var drawLineButton = toolsPanel.attachMovie("SimpleButton", "drawLineButton", d++, {w:80, h:20, _x:10, _y:50, id:"line", labelText:"Draw Line"});
		drawLineButton.setAction( switchMode );

		toggleGroup = [addPointsButton, drawLineButton];

		var revertButton = toolsPanel.attachMovie("SimpleButton", "revertButton", d++, {w:24, h:20, _x:71, _y:90});
		revertButton.setIcon("revertIcon");
		revertButton.setAction( revert );

		/* EQUATION PANEL 
		var equPanelY = 330;
		if (showAxisLabels) {
			equPanelY = 350;
		}*/
		var equationPanel = panels.attachMovie("Panel", "equationPanel", d++, {w:280, h:35, _x:10, _y:330});	//panel for equation
		
		//static equation format
		var equFormat = new TextFormat();
		equFormat.font = 'Times';
		equFormat.color = 0x000000;
		equFormat.size = 18;
		equFormat.align = 'left';

		//input equation format
		var inputFormat = new TextFormat();
		inputFormat.font = 'Times';
		inputFormat.color = 0x000000;
		inputFormat.size = 18;
		inputFormat.align = 'left';
		inputFormat.indent = 6;

		var equLabelY:TextField = createEquationLabel(equationPanel, equFormat, '<i>y</i>  =', 26, 5);
		//var equLabelX:TextField = createEquationLabel(equationPanel, equFormat, '<i>x</i>', 118, 5);
		//interceptSignToggle = createEquationLabel(equationPanel, equFormat, '+', 142, 5);
		//interceptSignToggle = equationPanel.attachMovie('SignToggle', 'interceptSignToggle', d++, {_x:142, _y:8});
		
		equationDisplay = createInputField(equationPanel, inputFormat, '', 62, 5, 180, 22);
		//interceptDisplay = createInputField(equationPanel, inputFormat, '', 170, 8, 64, 20);

		equationDisplay.onChanged = function() {
			_parent._parent._parent.updateEquation(this.text);
		}
		equationDisplay.onKillFocus = function() {
			_parent._parent._parent.updateTextDisplays();
		}
		/*
		interceptDisplay.onChanged = function() {
			_parent._parent._parent.updateValue(this.text, "intercept");
		}
		interceptDisplay.onKillFocus = function() {
			_parent._parent._parent.updateTextDisplays();
		}*/


		//set up for grading options in author app
		if (appMode == "author") {
			//default grading values
			var defaultSlopeTolerance:Number = 0.1;
			var defaultInterceptTolerance:Number = 0.5;

			//graph tolerance
			graphSlopeTolerancePos = defaultSlopeTolerance;
			graphSlopeToleranceNeg = defaultSlopeTolerance;
			graphInterceptTolerancePos = defaultInterceptTolerance;
			graphInterceptToleranceNeg = defaultInterceptTolerance;


			//equation tolerance
			equationSlopeTolerancePos = defaultSlopeTolerance;
			equationSlopeToleranceNeg = defaultSlopeTolerance;
			equationInterceptTolerancePos = defaultInterceptTolerance;
			equationInterceptToleranceNeg = defaultInterceptTolerance;

			//equation conditionals
			ifSlopeGreater = defaultSlopeTolerance;
			ifSlopeLess = defaultSlopeTolerance;
			ifInterceptGreater = defaultInterceptTolerance;
			ifInterceptLess = defaultInterceptTolerance;
			wildcardOptions = new Object();
			wildcardOptions.allPoints = true;
			wildcardOptions.line = true;
			wildcardOptions.equation = true;
			wildcardOptions.gSlope = true;
			wildcardOptions.gIntercept = true;
			wildcardOptions.eSlope = true;
			wildcardOptions.eIntercept = true;

			gradingPanels._visible = false;
			var graphSubPanelM = gradingPanels.attachMovie("Panel", "graphSubPanelM", d++, {w:120, h:60, _x:320, _y:127});
			var graphSubPanelB = gradingPanels.attachMovie("Panel", "graphSubPanelB", d++, {w:120, h:60, _x:320, _y:192});
			var graphSlopeTolerancePosSetting = gradingPanels.attachMovie("PanelSetting", "graphSlopeTolerancePosSetting", d++, {_x:336, _y:136, labelText:"<i>m</i> = +", header:"", myDefault: graphSlopeTolerancePos});
			var graphSlopeToleranceNegSetting = gradingPanels.attachMovie("PanelSetting", "graphSlopeToleranceNegSetting", d++, {_x:336, _y:161, labelText:"<i>m</i> = \u2013", myDefault: graphSlopeToleranceNeg});
			
			var graphInterceptTolerancePosSetting = gradingPanels.attachMovie("PanelSetting", "graphInterceptTolerancePosSetting", d++, {_x:339, _y:200, labelText:"<i>b</i> = +", myDefault: graphInterceptTolerancePos});
			var graphInterceptToleranceNegSetting = gradingPanels.attachMovie("PanelSetting", "graphInterceptToleranceNegSetting", d++, {_x:339, _y:225, labelText:"<i>b</i> = \u2013", myDefault: graphInterceptToleranceNeg});

			var equationGradingPanelM = gradingPanels.attachMovie("Panel", "equationGradingPanelM", d++, {w:98, h:65, _x:295, _y:350});
			var equationGradingPanelB = gradingPanels.attachMovie("Panel", "equationGradingPanelB", d++, {w:98, h:65, _x:398, _y:350});
			var equationSlopeTolerancePosSetting = gradingPanels.attachMovie("PanelSetting", "equationSlopeTolerancePosSetting", d++, {_x:298, _y:362, labelText:"<i>m</i> = +", myDefault: equationSlopeTolerancePos});
			var equationSlopeToleranceNegSetting = gradingPanels.attachMovie("PanelSetting", "equationSlopeToleranceNegSetting", d++, {_x:298, _y:386, labelText:"<i>m</i> = \u2013", myDefault: equationSlopeToleranceNeg});

			var equationInterceptTolerancePosSetting = gradingPanels.attachMovie("PanelSetting", "equationInterceptTolerancePosSetting", d++, {_x:402, _y:362, labelText:"<i>b</i> = +", myDefault: equationInterceptTolerancePos});
			var equationInterceptToleranceNegSetting = gradingPanels.attachMovie("PanelSetting", "equationInterceptToleranceNegSetting", d++, {_x:402, _y:386, labelText:"<i>b</i> = \u2013", myDefault: equationInterceptToleranceNeg});


			//mask for plot in author view to mask the accepted range shaded area
			var graphMask:MovieClip = this.createEmptyMovieClip("graphMask", d++);
			graphMask.beginFill(0xFFFFFF, 100);
			graphMask.moveTo(graphXposition, graphYposition);
			graphMask.lineTo(graphXposition, graphYposition + graphHeight);
			graphMask.lineTo(graphXposition + graphWidth, graphYposition + graphHeight);
			graphMask.lineTo(graphXposition + graphWidth, graphYposition);
			range.setMask(graphMask);

			//graph setup options
			myGraphOptions = new SetupOptions(this);
		}

		reload();	//load any saved settings
	}

	function updateEquation(newVal) {
		//replace en dashes with negative sign for math parsing
		if (newVal.indexOf('\u2013') > -1) {
			newVal = stringReplace( String(newVal),'\u2013','-');
		}
		//remove spaces

		var eString:String = stringReplace(newVal, " ", "");
		//parse values from equation entry

		//find all operators
		var operatorIndex:Array = [0];	//list of all operators to find separation points, always begin with start of entry
		for (var i=1; i<eString.length; i++) {
			if (eString.charAt(i) == '+' || eString.charAt(i) == '-') {
				operatorIndex.push(i);
			}
		}

		var addendArray:Array = [];	//list of each segment between operators
		var startIndex:Number, subLength:Number, segment:String;
		
		for (var o=0; o<operatorIndex.length; o++) {
			startIndex = operatorIndex[o];

			if (operatorIndex[o+1] == undefined) {
				segment = eString.substr(startIndex);				//last operator, include everything following
			} else {
				subLength = operatorIndex[o+1] - startIndex;
				segment = eString.substr(startIndex, subLength);	//section between this and next operator
			}
			addendArray.push( segment );
		}
		
		trace (addendArray.join(' | '));

		//find segment containing x, this is slope ( m * x + ... )
		var slopeId:Number, interceptId:Number;
		for (var i=0; i<addendArray.length; i++) {
			if (addendArray[i].indexOf('x') > -1) {
				if (slopeId == undefined) {
					slopeId = i;
				}
			} else if (interceptId == undefined) {
				interceptId = i;
			}
		}
		if (slopeId == undefined && addendArray.length == 1) {
			slopeEntry = 0;
		} else {
			var slopeAddend = addendArray[slopeId];					//string containing x
			slopeAddend = stringReplace( slopeAddend,'x','');		//remove x
			if (slopeAddend == '') {
				slopeEntry = 1;
			} else if (slopeAddend == '-') {
				slopeEntry = -1;
			} else {
				slopeEntry = Number(slopeAddend);
			}
		}

		//find segment without x, this is y-intercept ( ... + b )
		if (interceptId == undefined) {
			interceptEntry = 0;
		} else {
			var interceptAddend = addendArray[interceptId];
			interceptEntry = Number(interceptAddend);
		}

		//anything not intercept or slope is extra stuff
		extraStuff = "";
		for (var i=0; i<addendArray.length; i++) {
			if (i != slopeId && i != interceptId) {
				extraStuff += addendArray[i];
			}
		}
		//look for extra x's
		var xCount:Number = 0;
		for (var i=0; i<eString.length; i++) {
			if (eString.charAt(i) == 'x') {
				xCount++;
				if (xCount > 1) {
					extraStuff += 'x';
				}
			}
		}

		//equation string as entered by student
		equationEntry = newVal;
		

		trace ('slope = ' + slopeEntry);
		trace ('intercept = ' + interceptEntry);
		trace ('extra stuff = ' + extraStuff);
		//trace ('equationEntry = ' + equationEntry);

		
		myEquation = saveEquation ( myEquation );	// <---- CALL THIS WHENEVER EQUATION IS CHANGED
		if (newVal.indexOf('-') > -1) {
			newVal = stringReplace( String(newVal),'-','\u2013');
			equationDisplay.htmlText = newVal;
		}
		if (newVal.indexOf('x') > -1) {
			newVal = stringReplace( String(newVal),'x','<i>x</i>');
			equationDisplay.htmlText = newVal;
		}
		if (appMode=="author") {
			drawLineFromEquation();
		}
	}

	function updateTextDisplays() {
		var equString;
		if (equationEntry == undefined) {
			equationDisplay.htmlText = "";
		} else {
			equString = stringReplace( equationEntry, '-', '\u2013');
			equString = stringReplace( equString,  'x','<i>x</i>');
			equationDisplay.htmlText = equString;
		}
		/*
		if (isNaN(slopeEntry)) {
			equationDisplay.text = "";
		} else {
			var slopeString = stringReplace( String(slopeEntry), '-', '\u2013');
			equationDisplay.text = slopeString;
		}
		
		if (isNaN(interceptEntry)) {
			interceptDisplay.text = "";
		} else {
			var interceptString = stringReplace( String(interceptEntry), '-', '\u2013');
			interceptDisplay.text = interceptString;
		}*/
	}


	function createEquationLabel(mc:MovieClip, txtFormat:TextFormat, labelText:String, x:Number, y:Number):TextField {
		var newEquLabel:TextField = mc.createTextField("newEquLabel", d++, x, y, 40, 30);
		newEquLabel.setNewTextFormat(txtFormat);
		newEquLabel.selectable = false;
		//newEquLabel.autoSize = true;
		//newEquLabel.background = true;
		//newEquLabel.backgroundColor = 0xEEEEEE;
		newEquLabel.html = true;
		newEquLabel.htmlText = labelText;
		return newEquLabel;
	}

	function createInputField(mc:MovieClip, txtFormat:TextFormat, labelText:String, x:Number, y:Number, w:Number, h:Number):TextField {
		var inputField:TextField = mc.createTextField("inputField", d++, x, y, w, h);
		inputField.setNewTextFormat(txtFormat);
		inputField.selectable = true;
		inputField.autoSize = false;
		inputField.type = "input";
		//inputField.maxChars = 36;
		inputField.restrict = "0-9 \u2013 \\-\\.\\x\\+";
		inputField.background = true;
		inputField.backgroundColor = 0xFFFFFF;
		inputField.border = true;
		inputField.borderColor = 0x999999;
		inputField.html = true;
		inputField.htmlText = labelText;
		inputField.tabIndex = tabNum++;
		return inputField;
	}

// RELOAD function called on each tab to display saved data
//var firstLoad:Boolean = true;
	function reload() {	
		tabType = _global.core.Globals.tabType;
		//tabType = "response";

		//check if saved settings exist
		if (mySettings == null) {
			//if not, initalize the settings and graph save objects
			mySettings = new Object();
			myGraph = new Object();
			myEquation = new Object();
			myGradingOptions = new Object();

			//save the settings for recall in graded swf
			mySettings = saveSettings ( mySettings );	// <---- CALL THIS WHENEVER SETTINGS ARE CHANGED
			myEquation = saveEquation ( myEquation );	// <---- CALL THIS WHENEVER EQUATION IS CHANGED
			myGradingOptions = saveGradingOptions ( myGradingOptions );	// <---- CALL THIS WHENEVER EQUATION IS CHANGED

			//initial setup
			resetData();
		} else {
			//reload the graph and settings
			reloadSettings( mySettings );
			reloadGraph( myGraph );
			reloadEquation( myEquation );
			reloadGradingOptions( myGradingOptions );

			//update the graph and displays
			
			myAxisLabels.updateLabels();
			myAxisLabels.setVisibility();
			myGrid.drawGrid();

			if (gridChanged) {
				updateLineForNewGrid();
			}

			updateGraph();
			updateDisplays();
		}
		//if on a response tab in author app, show grading options
		if (appMode == "author") {
			if (tabType == "response") {
				showGradingOptions();
			} else {
				hideGradingOptions();
			}
			if (tabType == "question") {
				myGraphOptions.showOptions();
			} else {
				myGraphOptions.hideOptions();
			}
			//debug.text = tabType;
		}
		/*if (firstLoad) {
			firstLoad = false;
			debug.text = "";
			for (var i in mySettings) {
				debug.text += i + ': ' + mySettings[i] + '\n';
			}
		}*/
	}

	var settingsArray:Array;
	function showGradingOptions() {
		//remove existing buttons
		if (settingsArray.length > 0) {
			for (var i=0; i<settingsArray.length; i++) {
				settingsArray[i].removeMovieClip(this);
			}
		}

		//create wildcard grading buttons
		settingsArray = new Array();
		var equationPanelY = panels.equationPanel._y;
		var pointGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "pointGrading", d++, {_x:415, _y:17, id:"allPoints"});
		var lineGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "lineGrading", d++, {_x:415, _y:57, id:"line"});
		var equationGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "equationGrading", d++, {_x:267, _y:equationPanelY+5, id:"equation"});

		var graphSlopeGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "graphSlopeGrading", d++, {_x:415, _y:130, id:"gSlope"});

		var graphIntGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "graphIntGrading", d++, {_x:415, _y:195, id:"gIntercept"});

		var equSlopeGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "equSlopeGrading", d++, {_x:372, _y:equationPanelY+5, id:"eSlope"});

		var equIntGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "equIntGrading", d++, {_x:472, _y:equationPanelY+5, id:"eIntercept"});

		gradingPanels.equationGradingPanelM._y = equationPanelY;
		gradingPanels.equationGradingPanelB._y = equationPanelY;
		gradingPanels.equationSlopeTolerancePosSetting._y = equationPanelY + 12;
		gradingPanels.equationInterceptTolerancePosSetting._y = equationPanelY + 12;
		gradingPanels.equationSlopeToleranceNegSetting._y = equationPanelY + 36;
		gradingPanels.equationInterceptToleranceNegSetting._y = equationPanelY + 36;
		
		panels.toolsPanel.myBG._width = 120;
		//panels.toolsPanel.myBG._height = 240;
		panels.toolsPanel.revertButton._x = 92;
		gradingPanels._visible = true;
		adjustPanelAlpha();
	}
	function hideGradingOptions() {
		panels.toolsPanel.myBG._width = 100;
		//panels.toolsPanel.myBG._height = 120;
		panels.toolsPanel.revertButton._x = 71;
		gradingPanels._visible = false;
		panels.equationPanel._alpha = 100;
	}
	function toggleSetting(buttonType:String, id:String, buttonMC:MovieClip) {
		var myVal:Boolean;
		switch (buttonType) {
			case "graded":
				myVal = wildcardOptions[id];
				if (myVal) {
					buttonMC.eye.gotoAndStop(2);
				} else {
					buttonMC.eye.gotoAndStop(1);
				}
				wildcardOptions[id] = !myVal;
				if (id=="line") {
					//graph slope and intercept inherits settings from line
					wildcardOptions["gSlope"] = !myVal;
					wildcardOptions["gIntercept"] = !myVal;
					showGradingOptions();
				} else if (id=="equation") {
					//equation slope and intercept inherits settings from equation
					wildcardOptions["eSlope"] = !myVal;
					wildcardOptions["eIntercept"] = !myVal;
					showGradingOptions();
				} else if ((id=="gSlope" || id=="gIntercept") && !myVal) {
					//line must be true if either graph slope or intercept are true
					wildcardOptions["line"] = !myVal;
					showGradingOptions();
				} else if ((id=="eSlope" || id=="eIntercept") && !myVal) {
					//equation must be true if either equation slope or intercept are true
					wildcardOptions["equation"] = !myVal;
					showGradingOptions();
				}
				if (!wildcardOptions["gSlope"] && !wildcardOptions["gIntercept"]) {
					//both line settings are false so line is ignored
					wildcardOptions["line"] = false;
					showGradingOptions();
				}
				if (!wildcardOptions["eSlope"] && !wildcardOptions["eIntercept"]) {
					//both equation settings are false so equation is ignored
					wildcardOptions["equation"] = false;
					showGradingOptions();
				}
			break;
		}
		updateGraph();
		adjustPanelAlpha();
	}

//SAVE and RELOAD functions for Graph, Equation, Settings and Grading Options
	function saveSettings (s:Object):Object {
		s.gridXstep = gridXstep;
		s.gridXmax = gridXmax;
		s.gridYstep = gridYstep;
		s.gridYmax = gridYmax;
		s.axis_x_label = axis_x_label;
		s.axis_y_label = axis_y_label;
		s.axisLabelPositions = axisLabelPositions;
		s.showAxisLabels = showAxisLabels;
		s.showXgrid = showXgrid;
		s.showYgrid = showYgrid;
		return s;
	}
	function reloadSettings(s:Object) {
		gridXstep = s.gridXstep;
		gridXmax = s.gridXmax;
		gridYstep = s.gridYstep;
		gridYmax = s.gridYmax;
		axis_x_label = s.axis_x_label;
		axis_y_label = s.axis_y_label;
		axisLabelPositions = s.axisLabelPositions;
		showAxisLabels = s.showAxisLabels;
		showXgrid = s.showXgrid;
		showYgrid = s.showYgrid;
	}
	function updateSettings() {	//can be called when grid or axis settings change
		mySettings = saveSettings ( mySettings );	// <---- CALL THIS WHENEVER SETTINGS ARE CHANGED
		trace ('update')
	}

	function saveGraph (g:Object):Object {
		g.myPoints = myPoints;
		g.myLine = myLine;
		g.mySlope = mySlope;
		g.myIntercept = myIntercept;
		return g;
	}
	function reloadGraph (g:Object) {
		myPoints = g.myPoints;
		myLine = g.myLine;
		mySlope = g.mySlope;
		myIntercept = g.myIntercept;
	}

	function saveEquation (e:Object):Object {
		e.slopeEntry = slopeEntry;
		e.interceptEntry = interceptEntry;
		e.equationEntry = equationEntry;
		e.extraStuff = extraStuff;
		//for (var i in e) {
		//	trace (i + " = " + e[i]);
		//}
		return e;
	}
	function reloadEquation (e:Object) {
		slopeEntry = e.slopeEntry;
		interceptEntry = e.interceptEntry;
		equationEntry = e.equationEntry;
		extraStuff = e.extraStuff;
	}

	function saveGradingOptions (g:Object):Object {
		g.graphSlopeTolerancePos = graphSlopeTolerancePos;
		g.graphSlopeToleranceNeg = graphSlopeToleranceNeg;
		g.graphInterceptTolerancePos = graphInterceptTolerancePos;
		g.graphInterceptToleranceNeg = graphInterceptToleranceNeg;
		g.equationSlopeTolerancePos = equationSlopeTolerancePos;
		g.equationSlopeToleranceNeg = equationSlopeToleranceNeg;
		g.equationInterceptTolerancePos = equationInterceptTolerancePos;
		g.equationInterceptToleranceNeg = equationInterceptToleranceNeg;
		g.ifSlopeGreater = ifSlopeGreater;
		g.ifSlopeLess = ifSlopeLess;
		g.ifInterceptGreater = ifInterceptGreater;
		g.ifInterceptLess = ifInterceptLess;
		g.wildcardOptions = wildcardOptions
		return g;
	}
	function reloadGradingOptions (g:Object) {
		graphSlopeTolerancePos = g.graphSlopeTolerancePos;
		graphSlopeToleranceNeg = g.graphSlopeToleranceNeg;
		graphInterceptTolerancePos = g.graphInterceptTolerancePos;
		graphInterceptToleranceNeg = g.graphInterceptToleranceNeg;
		equationSlopeTolerancePos = g.equationSlopeTolerancePos;
		equationSlopeToleranceNeg = g.equationSlopeToleranceNeg;
		equationInterceptTolerancePos = g.equationInterceptTolerancePos;
		equationInterceptToleranceNeg = g.equationInterceptToleranceNeg;
		ifSlopeGreater = g.ifSlopeGreater;
		ifSlopeLess = g.ifSlopeLess;
		ifInterceptGreater = g.ifInterceptGreater;
		ifInterceptLess = g.ifInterceptLess;
		wildcardOptions = g.wildcardOptions;

		gradingPanels.graphSlopeTolerancePosSetting.myInput.text = graphSlopeTolerancePos;
		gradingPanels.graphSlopeToleranceNegSetting.myInput.text = graphSlopeToleranceNeg;
		gradingPanels.graphInterceptTolerancePosSetting.myInput.text = graphInterceptTolerancePos;
		gradingPanels.graphInterceptToleranceNegSetting.myInput.text = graphInterceptToleranceNeg;
		gradingPanels.equationSlopeTolerancePosSetting.myInput.text = equationSlopeTolerancePos;
		gradingPanels.equationSlopeToleranceNegSetting.myInput.text = equationSlopeToleranceNeg;
		gradingPanels.equationInterceptTolerancePosSetting.myInput.text = equationInterceptTolerancePos;
		gradingPanels.equationInterceptToleranceNegSetting.myInput.text = equationInterceptToleranceNeg;
	}

	function updateGradingOptions() {
		graphSlopeTolerancePos = Number(gradingPanels.graphSlopeTolerancePosSetting.myInput.text);
		graphSlopeToleranceNeg = Number(gradingPanels.graphSlopeToleranceNegSetting.myInput.text);
		graphInterceptTolerancePos = Number(gradingPanels.graphInterceptTolerancePosSetting.myInput.text);
		graphInterceptToleranceNeg = Number(gradingPanels.graphInterceptToleranceNegSetting.myInput.text);
		equationSlopeTolerancePos = Number(gradingPanels.equationSlopeTolerancePosSetting.myInput.text);
		equationSlopeToleranceNeg = Number(gradingPanels.equationSlopeToleranceNegSetting.myInput.text);
		equationInterceptTolerancePos = Number(gradingPanels.equationInterceptTolerancePosSetting.myInput.text);
		equationInterceptToleranceNeg = Number(gradingPanels.equationInterceptToleranceNegSetting.myInput.text);
		myGradingOptions = saveGradingOptions ( myGradingOptions );	// <---- CALL THIS WHENEVER EQUATION IS CHANGED
		updateGraph();
	}

	function adjustPanelAlpha() {
		var closedAlpha = 20;
		if (!wildcardOptions.equation) {
			panels.equationPanel._alpha = closedAlpha;
		} else {
			panels.equationPanel._alpha = 100;
		}
		if (!wildcardOptions.gSlope) {
			gradingPanels.graphSubPanelM._alpha = closedAlpha;
			gradingPanels.graphSlopeTolerancePosSetting._alpha = closedAlpha;
			gradingPanels.graphSlopeToleranceNegSetting._alpha = closedAlpha;
		} else {
			gradingPanels.graphSubPanelM._alpha = 100;
			gradingPanels.graphSlopeTolerancePosSetting._alpha = 100;
			gradingPanels.graphSlopeToleranceNegSetting._alpha = 100;
		}
		if (!wildcardOptions.gIntercept) {
			gradingPanels.graphSubPanelB._alpha = closedAlpha;
			gradingPanels.graphInterceptTolerancePosSetting._alpha = closedAlpha;
			gradingPanels.graphInterceptToleranceNegSetting._alpha = closedAlpha;
		} else {
			gradingPanels.graphSubPanelB._alpha = 100;
			gradingPanels.graphInterceptTolerancePosSetting._alpha = 100;
			gradingPanels.graphInterceptToleranceNegSetting._alpha = 100;
		}
		if (!wildcardOptions.eSlope) {
			gradingPanels.equationGradingPanelM._alpha = closedAlpha;
			gradingPanels.equationSlopeTolerancePosSetting._alpha = closedAlpha;
			gradingPanels.equationSlopeToleranceNegSetting._alpha = closedAlpha;
		} else {
			gradingPanels.equationGradingPanelM._alpha = 100;
			gradingPanels.equationSlopeTolerancePosSetting._alpha = 100;
			gradingPanels.equationSlopeToleranceNegSetting._alpha = 100;
		}
		if (!wildcardOptions.eIntercept) {
			gradingPanels.equationGradingPanelB._alpha = closedAlpha;
			gradingPanels.equationInterceptTolerancePosSetting._alpha = closedAlpha;
			gradingPanels.equationInterceptToleranceNegSetting._alpha = closedAlpha;
		} else {
			gradingPanels.equationGradingPanelB._alpha = 100;
			gradingPanels.equationInterceptTolerancePosSetting._alpha = 100;
			gradingPanels.equationInterceptToleranceNegSetting._alpha = 100;
		}
		
	}

	function switchMode (mode:String, target_mc:MovieClip) {
		for (var b in target_mc.toggleGroup) {
			target_mc.toggleGroup[b].highlighted = false;
		}
		target_mc.drawMode = mode;
	}
	function revert (id:String, target_mc:MovieClip) {
		target_mc.resetData();
	}

	function resetData() {
		myPoints = [];
		myLine = null;
		mySlope = undefined;
		if (appMode == "author") {
			myIntercept = undefined;
			equationEntry = undefined;
		}

		updateGraph();
		updateDisplays();
		//click on graph to add points
		/*$(graph).off();
		$(this).off();
	    $(graph).mousedown( function( event ) { clickGraph(event); });
	    $(message).html(startMsg);*/
	}

	function resetLine() {
		myLine = null;
		updateGraph();
		updateDisplays();
		beginDrawLine();
	}

	function clickGraph() {
		switch (drawMode) {
			case "point":
				addPoint();
				break;

			case "line":
				beginDrawLine();
				break;
		}
	}

	function addPoint() {
		graph.onRelease = graph.onReleaseOutside = undefined;

		var graphXY = myGrid.mouseToGraphXY(_xmouse, _ymouse);
		
		//snap to grid
		var newX = myGrid.snapToX(graphXY[0]);
		var newY = myGrid.snapToY(graphXY[1]);

		//round to nearest integer -- use if snap to grid can toggle off
		//var newX = Math.round(graphXY[0]);
		//var newY = Math.round(graphXY[1]);
		
		var pointExists = false;
		//check if point already exists
		for (var p in myPoints) {
			if (myPoints[p].x == newX && myPoints[p].y == newY) {
				pointExists = true;
				//already exists, so delete it
				myPoints.splice(p, 1);
				break;
			}
		}

		if (!pointExists) {				//add new points
			if (myPoints.length < 10) {
				myPoints.push(new Point(newX, newY));
			} else {
				myPoints.splice(0, 1);
				myPoints.push(new Point(newX, newY));
			}
		}
		updateGraph();
		updateDisplays();
	}

	function beginDrawLine() {

		if (myLine) {
			//check mouse x,y proximity to arrowhead
			var x0 = _xmouse;
			var y0 = _ymouse;
			var pixelEnd1 = myGrid.pixelValues(myLine.end1.x, myLine.end1.y);
			var x1 = pixelEnd1.x;
			var y1 = pixelEnd1.y;
			var distToEndpoint = Math.sqrt((x1-x0) * (x1-x0) + (y1-y0) * (y1-y0));
			if (distToEndpoint < 50) {	//if close enough, adjust line
				adjustLine();
			} else {
				myLine = null;
				updateGraph();
				startLine();	//if far away, reset line
			}
			
		} else {
			startLine();
		}
		

		graph.onRelease = graph.onReleaseOutside = function() {
			_parent.endLine();
		}
	}

	var tempLine;
	function startLine() {
		var graphXY = myGrid.mouseToGraphXY(_xmouse, _ymouse);

		//begin at closest axis
		var newX = graphXY[0];
		var newY = graphXY[1];
		if (newX < newY) {
			newX = 0;
		} else {
			newY = 0;
		}
		tempLine = new Line(newX, newY, graphXY[0], graphXY[1]);
		updateGraph();

		this.onMouseMove = function() {
			lineDrawing();
		}
		lineDrawing();
	}

	function lineDrawing() {
		var graphXY = myGrid.mouseToGraphXY(_xmouse, _ymouse);
		tempLine.end1.x = graphXY[0];
		tempLine.end1.y = graphXY[1];
		updateGraph();
		updateDisplays();
	}

	function endLine() {
		var graphXY = myGrid.mouseToGraphXY(_xmouse, _ymouse);

		//snap y-intercept and slope to degree of precision
		//find slope and b as drawn
		mySlope = Number(getSlope(tempLine.end0.x, tempLine.end0.y, graphXY[0], graphXY[1]));
		myIntercept = tempLine.end0.y - mySlope * tempLine.end0.x;
		myIntercept = Number(formatDecimals(myIntercept, 2));

		myLine = endPointsFromEquation(mySlope, myIntercept, tempLine.end0.x, tempLine.end0.y);

		//trace (mySlope)
		//trace (myIntercept)
		//extend line to edge of graph quadrant
		//var  quadrantEdge = getQuadrantEdge(tempLine.end0.x, tempLine.end0.y, graphXY[0], graphXY[1]);
		//if (quadrantEdge!= null) {
		//var newX = quadrantEdge[0];
		//var newY = quadrantEdge[1];
		//myLine = new Line(tempLine.end0.x, tempLine.end0.y, newX, newY);


		tempLine = null;
		updateGraph();
		updateDisplays();

		this.onMouseMove = undefined;
	}

	function adjustLine() {	//by clicking on graph
		var graphXY = myGrid.mouseToGraphXY(_xmouse, _ymouse);
		var newX = graphXY[0];
		var newY = graphXY[1];

		tempLine = new Line( myLine.end0.x, myLine.end0.y, newX, newY );
		myLine = null;
		
		updateGraph();

		this.onMouseMove = function() {
			lineDrawing();
		}
	}

	//given slope and y-intercept, determine endpoints for drawing line in the quadrant
	function endPointsFromEquation(m:Number, b:Number, fromX:Number, fromY:Number):Line {
		var xIntercept;	//where crosses x-axis
		if (isFinite(m)) {
			xIntercept = (gridYmin - b) / m;
		} else {
			//for vertical lines
			xIntercept = fromX;
		}

		//does line cross y-axis in quadrant?
		if (b >= gridYmin && b <= gridYmax) {
			//cross y axis
			var newX0 = gridXmin;
			var newY0 = b;
		} else if (xIntercept >= gridXmin && xIntercept <= gridXmax) {
			//does line cross x-axis in quadrant
			var newX0 = xIntercept;
			var newY0 = gridYmin;
		}

		var quadrantEdge = getQuadrantEdge(newX0, newY0, null, null, m, b);
		var newX1 = quadrantEdge[0];
		var newY1 = quadrantEdge[1];

		if (fromY == gridYmin && newX0 == gridXmin && m != 0 && fromX != gridXmin) {
			//drawing from x-axis to y-axis, so reverse the endpoints
			newX1 = newX0;
			newY1 = newY0;
			newX0 = quadrantEdge[0];
			newY0 = quadrantEdge[1];
		}
		var thisLine:Line = new Line(newX0, newY0, newX1, newY1);
		return thisLine;
	}

	//redraw line based on values in equation -- author app only
	function drawLineFromEquation() {
		var m = slopeEntry;
		var b = interceptEntry;
		if (isNaN(m) || isNaN(b)) {
			if (isNaN(b)) {
				plot.clear();
				range.clear();
			}
			return;
		}

		if (isLineOnGraph(m, b)) {
			myLine = endPointsFromEquation(m, b, myLine.end0.x, myLine.end0.y);
			mySlope = Number(m);
			myIntercept = Number(b);
			updateGraph();
		} else {
			trace ('not on graph')
			//line not in quadrant
			plot.clear();
			range.clear();
		}
	}

	function drawAllowedRange(mc) {
		var x0 = myLine.end0.x;
		var y0 = myLine.end0.y;
		var x1 = myLine.end1.x;
		var y1 = myLine.end1.y;
		var m = slopeEntry;
		var b = interceptEntry;
		var min_x0, min_y0, min_x1, min_y1;
		var max_x0, max_y0, max_x1, max_y1;

		var max_m = m + graphSlopeTolerancePos;
		var min_m = m - graphSlopeToleranceNeg;
		var max_b = b + graphInterceptTolerancePos;
		var min_b = b - graphInterceptToleranceNeg;
		if (!isFinite(m) || isNaN(graphSlopeTolerancePos) || isNaN(graphSlopeToleranceNeg) || isNaN(graphInterceptTolerancePos) || isNaN(graphInterceptToleranceNeg)) {
			return;		//don't draw range if slope is infinite (vertical lines) or if any value is undefined
		}

		if (wildcardOptions.line && !wildcardOptions.gIntercept) {
			max_b = b;
			min_b = b;
		}

		//translate endpoint0 vertically
		max_x0 = x0;
		max_y0 = max_m * x0 + max_b;
		min_x0 = x0;
		min_y0 = min_m * x0 + min_b;

		//translate endpoint1 vertically
		max_x1 = x1;
		max_y1 = max_m * x1 + max_b;
		min_x1 = x1;
		min_y1 = min_m * x1 + min_b;

		//get pixel values for graph
		var min_px0 = myGrid.pixelValues(min_x0, min_y0);
		var min_px1 = myGrid.pixelValues(min_x1, min_y1);
		var max_px0 = myGrid.pixelValues(max_x0, max_y0);
		var max_px1 = myGrid.pixelValues(max_x1, max_y1);

		var fillAlpha:Number = 20;
		var lineAlpha:Number = 60;
		var drawRange:Boolean = true;

		if (!wildcardOptions.line) {
			fillAlpha = 5;
			lineAlpha = 5;
		}
		if (wildcardOptions.line && !wildcardOptions.gIntercept) {
			// y-intercept winked
			fillAlpha = 0;
			//lineAlpha = 50;
			//var min_px0 = myGrid.pixelValues(min_x0, y0);
			//var max_px0 = myGrid.pixelValues(max_x0, y0);
		}
		if (wildcardOptions.line && !wildcardOptions.gSlope) {
			//slope is winked out, so we draw the allowed area on the y-axis
			drawRange = false;
			var max_b_px = myGrid.pixelValues(0, max_b);
			var min_b_px = myGrid.pixelValues(0, min_b);
			var max_x = 20;//myGrid.pixelValues(gridXstep/2, 0);
			//fillAlpha = 50;
			lineAlpha = 60;
			mc.beginFill(0x999999, fillAlpha);
			mc.lineStyle(.5, 0x999999, lineAlpha);
			mc.moveTo(max_x, max_b_px.y);
			mc.lineTo(max_b_px.x, max_b_px.y);
			mc.lineStyle();
			mc.lineTo(min_b_px.x, min_b_px.y);
			mc.lineStyle(.5, 0x999999, lineAlpha);
			mc.lineTo(max_x, min_b_px.y);
			dashedVertLine(mc, max_x, min_b_px.y, max_x, max_b_px.y, 4, 1, 0x666666, 100);
		}
		
		if (drawRange) {
		//draw the range
			mc.moveTo(min_px0.x, min_px0.y);
			mc.beginFill(0x999999, fillAlpha);
			mc.lineStyle(.5, 0x999999, lineAlpha);
			mc.lineTo(min_px1.x, min_px1.y);
			mc.lineStyle();
			mc.lineTo(max_px1.x, max_px1.y);
			mc.lineStyle(.5, 0x999999, lineAlpha);
			mc.lineTo(max_px0.x, max_px0.y);
			mc.lineStyle();
			mc.lineTo(min_px0.x, min_px0.y);
		}
	}
	function dashedVertLine(mc, fromX, fromY, toX, toY, dashLength, stroke, fill, alpha) {
		var dist = Math.abs(fromY - toY);
		var numDashes = dist / dashLength;
		mc.moveTo(fromX, fromY);
		for (var i=1; i<numDashes; i+=2) {
			mc.lineStyle(stroke, fill, alpha);
			mc.lineTo(fromX, fromY - i*dashLength);
			mc.lineStyle();
			mc.lineTo(fromX, fromY - (i+1)*dashLength);
		}
		mc.lineTo(toX, toY);
	}


	function isLineOnGraph(m, b) {
		//test if the line is ON the graph
		if (b > gridYmin && b <= gridYmax) {
			return true;
		}
		var y = m * gridXmax + b;
		if (b < gridYmax) {
			if (y > 0) {
				return true;
			} else if (y < 0) {
				return false;
			}
		} else if (b > gridYmax) {
			if (y > 0) {
				return false;
			} else if (y < 0) {
				return true;
			}
		}
		return false;
	}
	function drawnFromAnAxis(x0,y0,x1,y1, m) {
		if (x0 == gridXmin) {
			//line starts on y axis
			if (y0 <= gridYmax && y0 >= gridYmin) {	//line starts within quadrant
				return true;
			} else {
				return false;
			}
		} else {
			//line starts on x axis
			if (x0 <= gridXmax && x0 >= gridXmin) {	//line starts within quadrant
				return true;
			} else if (m > 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	function updateGraph() {
		//redraw the plot
		drawGraph(plot);
		//save graph data for recall in graded swf
		myGraph = saveGraph ( myGraph ); // <----- CALL THIS WHENEVER THE GRAPH IS CHANGED
	}
	function updateLineForNewGrid() {
		gridChanged = true;
		//this function called when grid is updated so end points can be recalculated
		if (myLine) {
			myLine = endPointsFromEquation(mySlope, myIntercept, myLine.end0.x, myLine.end0.y);
		}
		updateGraph();
	}

	function updateDisplays() {
		//display line info if line exists
		var lineExists = false;
		if (myLine) {
			var x0 = myLine.end0.x;
			var y0 = myLine.end0.y;
			var x1 = myLine.end1.x;
			var y1 = myLine.end1.y;
			lineExists = true;
		} else if (tempLine) {
			var x0 = tempLine.end0.x;
			var y0 = tempLine.end0.y;
			var x1 = tempLine.end1.x;
			var y1 = tempLine.end1.y;
			lineExists = true;
		}

		if (appMode == "author") {
			if (lineExists) {
				displayLineInfo(x0, y0, x1, y1);
				//trace (x0+','+y0 + ' to ' + x1+','+y1)
			} else {
				hideLineInfo();
				slopeEntry = undefined;
				interceptEntry = undefined;
				myEquation = saveEquation ( myEquation );	// <---- CALL THIS WHENEVER EQUATION IS CHANGED
			}
		} else {
			if (tabType == "solution") {
				displayLineInfo(x0, y0, x1, y1);
			} else {
				updateTextDisplays();
			}
		}
	}

	var maxHeight = 26, fontSize = 22;	//settings for equation text

	function displayLineInfo(x0, y0, x1, y1) {

		//line equation display
		var slope, mSign, bSign, bString, equString, slopeString;
		slope = getSlope(x0, y0, x1, y1);

		var b:Number = y0 - slope * x0;
		b = formatDecimals(b, 2);

		if (!isFinite(b)) {
			//bSign = "+";
			bString = "";//<i>" + b + "</i>"; //b is infinity
		} else {
			if ( b < 0 ) {
				bString = ' \u2013 ' + stringReplace( String(b), '-', '');
			} else {
				bString = ' + ' + String(b); 
			}
		}

		if (!isFinite(slope)) {
			slopeString = "";//"<i>" + slope + "</i>"; //slope is infinity
		} else {
			slopeString = stringReplace( slope, '-', '\u2013');
		}

		if (slopeString == "") {
			if (bString == "") {
				equationDisplay.htmlText = "";
			} else {
				equationDisplay.htmlText = stringReplace( String(b), '-', '\u2013');
			}
		} else {
			equationDisplay.htmlText = slopeString + '<i>x</i>' + bString;
		}
		
		//debug.text = 'slope = ' + slope;
		//debug.text += '\n intercept = ' + b;
		//interceptDisplay.text = bString;

		slopeEntry = Number(slope);
		interceptEntry = Number(b);
		myEquation = saveEquation ( myEquation );	// <---- CALL THIS WHENEVER EQUATION IS CHANGED
	}
	function hideLineInfo() {
		equationDisplay.text = "";
		//interceptDisplay.text = "";
	}

 	function stringReplace(str:String, searchStr:String, replaceStr:String):String {
		return str.split(searchStr).join(replaceStr);
	} 

	function getSlope(x0, y0, x1, y1) {
		var slope = (y1-y0) / (x1-x0);
		slope = formatDecimals(slope, 2);
		return slope;
	}

	function drawGraph(mc) {
		var dotAlpha:Number = 100;
		var lineAlpha:Number = 100;
		mc.clear();
		if (appMode == "author") {
			range.clear();
			if (!wildcardOptions.allPoints) {
				dotAlpha = 20;
			}
			if (!wildcardOptions.line) {
				lineAlpha = 20;
			}
		}
		mc.fillStyle = plotColor;
		
		for (var p in myPoints) {
			//find x,y in pixel values
			var pixelXY = myGrid.pixelValues(myPoints[p].x, myPoints[p].y);
			myPointDrawing.drawPoint(mc, dotSize, pixelXY.x, pixelXY.y, dotAlpha);
		}
		if (tempLine) {
			var pixelEnd0 = myGrid.pixelValues(tempLine.end0.x, tempLine.end0.y);
			var pixelEnd1 = myGrid.pixelValues(tempLine.end1.x, tempLine.end1.y);
			drawLine(mc, pixelEnd0.x, pixelEnd0.y, pixelEnd1.x, pixelEnd1.y, plotColor, plotStroke, lineAlpha);
		}
		if (myLine) {
			if (appMode == "author" && tabType == "response") {
				drawAllowedRange(range);
			}
			var pixelEnd0 = myGrid.pixelValues(myLine.end0.x, myLine.end0.y);
			var pixelEnd1 = myGrid.pixelValues(myLine.end1.x, myLine.end1.y);
			drawLine(mc, pixelEnd0.x, pixelEnd0.y, pixelEnd1.x, pixelEnd1.y, plotColor, plotStroke, lineAlpha);

			//var drawnSlope = (myLine.end1.y - myLine.end0.y) / (myLine.end1.x - myLine.end0.x);
			//trace ('drawnSlope = ' + drawnSlope);
		}
	}

	function drawLine(mc, x0, y0, x1, y1, style, stroke, alpha) {
		if (Math.sqrt((x1-x0) * (x1-x0) + (y1-y0) * (y1-y0)) > plotArrowSize) {	//add arrowhead if line is long enough
			lineWithArrowhead(mc, x0, y0, x1, y1, Math.PI/10, plotArrowSize, plotColor, plotStroke, alpha);
		} else {
			//draw line
			mc.lineStyle(stroke, style, alpha);
			mc.moveTo(x0, y0);
			mc.lineTo(x1, y1);
		}
	}

	function lineWithArrowhead(mc, x0, y0, x1, y1, angle, d, style, stroke, alpha) {
		//find angle of line
		var lineangle = Math.atan2(y1-y0, x1-x0);
		//find length of sides of arrowhead
		var h = Math.abs(d/Math.cos(angle));

		var angle1 = lineangle+Math.PI+angle;
		var topx = x1 + Math.cos(angle1)*h;
		var topy = y1 + Math.sin(angle1)*h;
		var angle2 = lineangle + Math.PI - angle;
		var botx = x1 + Math.cos(angle2)*h;
		var boty = y1 + Math.sin(angle2)*h;
		var midx = (topx + botx) / 2;
		var midy = (topy + boty) / 2;
		var centerx = (midx + x1) / 2;
		var centery = (midy + y1) / 2;
		var notchx = (midx + centerx) / 2;
		var notchy = (midy + centery) / 2;

		//draw line
		mc.lineStyle(stroke, style, alpha);
		mc.moveTo(x0, y0);
		mc.lineTo(centerx, centery);
		
		//draw arrowhead
		mc.lineStyle(1, style, alpha, true, true, "round", "miter");
		mc.moveTo(topx, topy);
		mc.beginFill(style, alpha);
		mc.curveTo(centerx, centery, x1, y1);
		mc.curveTo(centerx, centery, botx, boty);
		mc.curveTo(notchx, notchy, topx, topy);
	}

	//extend a point to the edge of the quadrant
	function getQuadrantEdge(x0, y0, x1, y1, m, b) {	//given existing point return a new value for x1,y1 at the edge of graphs
		if (x1 != null && y1 != null) {	//
			//if x1, y1 are given calculate m, b
			var m = (y1-y0) / (x1-x0);
			var b = y0 - m * x0;
		} else {
			//if x1, y1 are unknown, m and b must be given
			x1 = gridXmax;
			y1 = gridYmin;
		}

		//find intercept at the maximum x (right edge)
		var newX0 = gridXmax;
		var newY0 = m * newX0 + b;

		//find intercept at the maximum y (top edge)
		var newY1 = gridYmax;
		var newX1 = (newY1 - b) / m;

		//find intercept at minimum x (left edge)
		var newX2 = gridXmin;
		var newY2 = m * newX2 + b;

		//find intercept at the minimum y (bottom edge)
		var newY3 = gridYmin;
		var newX3 = (newY3 - b) / m;

		//horiz. and vert. line cases
		if (m==0) {
			if (x0 <= x1) {
				 //console.log('case-zeroA')
				// console.log('b: '+b)
				return [newX0, newY0];
			} else {
				 //console.log('case-zeroB')
				return [newX2, newY2];
			}
		} else if (!isFinite(m)) {
			if (y0 <= y1) {
				 //trace('case-infA')
				return [x0, gridYmax];
			} else {
				 //trace('case-infB')
				return [x0, gridYmin];
			}
		}

		//test which point lies in the quadrant
		if (newY0 <= gridYmax && newY0 > gridYmin) {
			 //console.log('case0')
			return [newX0, newY0];
		} else if (newX1 <= gridXmax && newX1 > gridXmin) {
			 //console.log('case1')
			return [newX1, newY1];
		} else if (newX3 <= gridXmax && newX3 > gridXmin && y0 != gridYmin) {	//y0 != gridYmin so a line starting on x-axis won't end on itself
			 //console.log('case2')
			return [newX3, newY3];
		} else if (newY2 <= gridYmax && newY2 >= gridYmin && x0 != gridXmin) {	//x0 != gridXmin so a line starting on y-axis won't end on itself
			 //console.log('case3')
			return [newX2, newY2];
		} else {
			// console.log(b)
			//console.log('no point found inside the quadrant');
			return [x0, y0];	//return original x,y
			//return null;	
		}
	}
	//___________Formatting decimals_______
function formatDecimals(num:Number, digits:Number) {
	//if no decimal places needed, we're done
	if (digits<=0) {
		return Math.round(num);
	}
	if (isNaN(num) || num==Number.NEGATIVE_INFINITY || num==Number.POSITIVE_INFINITY ) {
		return num;
	}
	//round the number to specified decimal places   
	//e.g. 12.3456 to 3 digits (12.346) -> mult. by 1000, round, div. by 1000
	var tenToPower = Math.pow(10, digits);
	var cropped = String(Math.round(num*tenToPower)/tenToPower);
	//add decimal point if missing
	if (cropped.indexOf(".") == -1) {
		cropped += ".0";
		//e.g. 5 -> 5.0 (at least one zero is needed)
	}
	//finally, force correct number of zeroes; add some if necessary   
	var halves = cropped.split(".");
	//grab numbers to the right of the decimal
	//compare digits in right half of string to digits wanted
	var zerosNeeded = digits-halves[1].length;
	//number of zeros to add
	for (var i = 1; i<=zerosNeeded; i++) {
		cropped += "0";
	}
	return (cropped);
}
}