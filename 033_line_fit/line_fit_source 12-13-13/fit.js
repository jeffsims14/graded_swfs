var axis, graph, graph_ctx, myPoints, myLine, rowArray, samplePoints;
var adjIncr = .1;
var xMax = 10, xMin = 0;
var yMax = 10, yMin = 0;

function init() {

	//intro button
	$('#introNextButton').click( function() {
		$('#introscreen').hide();
		$('#content').fadeIn('fast');
	});

	//help button
	$('#helpButton').click( function() {
		if($(this).prop('checked')) {
			message.style.visibility = "visible";
		} else {
			message.style.visibility = "hidden";
		}
	})

	//axis and graph setup
	axis = document.getElementById("axis");
    var axis_ctx = axis.getContext("2d");
	drawAxis(axis_ctx);

	graph = document.getElementById("graph");
    graph_ctx = graph.getContext("2d");

    //other buttons
    $(resetDataButton).click( function() {
		resetData();
	});
	$(generateDataButton).click( function() {
		generateData();
	});
	$(drawLineButton).click( function() {
		beginDrawLine();
	});
	$(resetLineButton).click( function() {
		resetLine();
	});
	$(slopeUp).click( function() {
		adjustSlope(adjIncr);
	});
	$(slopeDown).click( function() {
		adjustSlope(-adjIncr);
	});
	$(yIntUp).click( function() {
		adjustIntercept(adjIncr);
	});
	$(yIntDown).click( function() {
		adjustIntercept(-adjIncr);
	});
	$(xIntUp).click( function() {
		adjustXIntercept(adjIncr);
	});
	$(xIntDown).click( function() {
		adjustXIntercept(-adjIncr);
	});

	rowArray = [$(row1), $(row2), $(row3), $(row4), $(row5), $(row6), $(row7), $(row8), $(row9), $(row10)];

	//available points for "generate data" options
	samplePoints = [
			[ [1,2], [2,0], [2,9], [4,7], [6,2], [6,8], [7,1], [9,7], [10,5], [10,7] ],	//sample 1
			[ [2,9], [3,3], [3,6], [5,5], [6,2], [6,9], [7,4], [7,7], [9,4], [10,1] ],	//sample 2
			[ [2,4], [2,7], [5,4], [5,7], [6,4], [6,7], [9,4], [9,7] ],					//sample 3
			[ [1,6], [2,10], [3,3], [4,7], [6,6], [7,4], [7,8], [8,7], [9,9], [10,8] ],	//sample 4
			[ [1,1], [2,3], [3,4], [4,3], [4,6], [5,4], [5,6], [5,8], [6,7], [6,9] ],	//sample 5
			[ [1,8], [1,9], [2,6], [2,8], [3,5], [5,2], [5,4], [5,5], [7,3], [9,0] ],	//sample 6
			[ [1,10], [2,2], [3,5], [4,7], [5,9], [6,5], [7,8], [8,3], [9,1], [10,7] ],	//sample 7
			[ [2,0], [2,9], [3,3], [3,7], [5,6], [6,10], [7,2], [8,5], [8,9], [10,3] ],	//sample 8
			[ [3,7], [5,2], [5,6], [6,4], [6,7], [6,9], [7,4], [8,6], [9,2], [10,0] ],	//sample 9
			[ [2,1], [3,2], [4,5], [5,2], [5,6], [6,4], [6,7], [7,5], [7,8], [8,7] ]	//sample 10
	];
	
	//initial setup
	resetData();
}

function resetData() {
	myPoints = [];
	myLine = null;

	hideExtraDisplays();

	updateGraph();
	updateDisplays();
	//click on graph to add points
	$(graph).off();
	$(this).off();
    $(graph).mousedown( function( event ) { clickGraph(event); });
    $(message).html(startMsg);
}
var startMsg = 'Click the graph to create coordinate points, or click &ldquo;Generate Data&rdquo; to have the table filled automatically. A maximum of ten points can be plotted. You can delete any point by clicking it again.';
var defaultMsg = 'Click the graph to add new points. A maximum of ten points can be plotted. You can delete any point by clicking it again.';
var maxMsg = 'You&rsquo;ve added the maximum of ten points. Adding new points will delete older ones. Click &ldquo;Draw Line&rdquo; when ready for the next step.';
var drawMsg = 'Click and drag from anywhere on the <i>x</i> or <i>y</i>-axis to create a line.';
var adjustMsg = 'To adjust the line, drag the arrowhead, or use the buttons above and below the slope and <i>y</i>-intercept of the equation.';

function resetLine() {
	myLine = null;
	updateGraph();
	updateDisplays();
	beginDrawLine();
	//$(graph).off();
	//$(this).off();
	//$(graph).mousedown( function( event ) { clickGraph(event); });
	//$(message).html(defaultMsg);
}
function hideExtraDisplays() {
	$(dataTable).hide();
	$(correlationLabel).hide();
	$(slopeLabel).hide();
	$(aboveLabel).hide();
	$(belowLabel).hide();
	$(onLabel).hide();
	$(sumLabel).hide();
	$("#graph-controls").hide();
}

var endXY;
function mouseToGraphXY(e) {
	//translate mouse X,Y to graph X,Y
	var graphOffset = $(graph).offset();
	var mouseX = e.pageX - graphOffset.left;
	var mouseY = e.pageY - graphOffset.top;
	if (isNaN(e.pageX) || isNaN(e.pageY)) {	//for touch events, get touch PageX and PageY
		if (e.originalEvent.touches.length) {
			var mouseX = e.originalEvent.touches[0].pageX - graphOffset.left;
			var mouseY = e.originalEvent.touches[0].pageY - graphOffset.top;
			endXY = [ mouseX, mouseY ];
		} else {			//for touch end, use the last recorded X,Y
			mouseX = endXY[0];
			mouseY = endXY[1];
		}
	}
	var newX = (mouseX - marginX) / gridSpacing;
	var newY = ((mouseY * -1) + marginY + graphWidth) / gridSpacing;
	newX = Math.max(newX, 0);
	newX = Math.min(newX, 10);
	newY = Math.max(newY, 0);
	newY = Math.min(newY, 10);
	return [newX, newY];
}

function clickGraph(e) {
	var graphXY = mouseToGraphXY(e);

	//round to nearest integer
	var newX = Math.round(graphXY[0]);
	var newY = Math.round(graphXY[1]);
	
	var pointExists = false;
	//check if point already exists
	for (p in myPoints) {
		if (myPoints[p].x == newX && myPoints[p].y == newY) {
			pointExists = true;
			//already exists, so delete it
			myPoints.splice(p, 1);
			$(message).html(defaultMsg);
			break;
		}
	}

	if (!pointExists) {				//add new points
		if (myPoints.length < 10) {
			myPoints.push(new Point(newX, newY));
		} else {
			myPoints.splice(0, 1);
			myPoints.push(new Point(newX, newY));
		}
		if (myPoints.length == 10) {
			$(message).html(maxMsg);
		}
	}
	updateGraph();
	updateDisplays();
}

function Point (x, y) {
	this.x = x;
	this.y = y;
}

function Line (x0, y0, x1, y1) {
	this.end0 = new Point(x0, y0);
	this.end1 = new Point(x1, y1);
}

function beginDrawLine() {
	$(message).html(drawMsg);
	//if(!$(helpButton).prop('checked')) {
	//	$(helpButton).trigger('click');	//show message text if hidden
		//$(helpButton).prop('checked', true);
	//}
	//click on graph to draw line
	$(graph).off();
	$(this).off();

    $(graph).on({ 'mousedown' : function( event ) { startLine(event); } });
	$(graph).on({ 'touchstart' : function( event ) { startLine(event); } });

	$(this).on({ 'mouseup' : function( event ) { endLine(event); } });
	$(this).on({ 'touchend' : function( event ) { endLine(event); } });

	$(drawLineButton).button("disable");

	//should highlight the axis here
}

var tempLine;
function startLine(e) {
	e.preventDefault();
	var graphXY = mouseToGraphXY(e);

	//begin at closest axis
	var newX = graphXY[0];
	var newY = graphXY[1];
	if (newX < newY) {
		newX = 0;
	} else {
		newY = 0;
	}
	tempLine = new Line(newX, newY, graphXY[0], graphXY[1]);
	updateGraph();

	$(graph).on({ 'mousemove' : function( event ) { lineDrawing(event); } });
	$(graph).on({ 'touchmove' : function( event ) { lineDrawing(event); } });
}

function lineDrawing(e) {
	e.preventDefault();
	var graphXY = mouseToGraphXY(e);

	//draw temporary line (and extend line to edge of graph quadrant)
	//var  quadrantEdge = getQuadrantEdge(tempLine.end0.x, tempLine.end0.y, graphXY[0], graphXY[1]);
	tempLine.end1.x = graphXY[0];
	tempLine.end1.y = graphXY[1];
	updateGraph();
	updateDisplays();
	//$(message).html( 'x,y: ' + graphXY );
}

function endLine(e) {
	$(message).html(adjustMsg);
	var graphXY = mouseToGraphXY(e);

	//extend line to edge of graph quadrant
	var  quadrantEdge = getQuadrantEdge(tempLine.end0.x, tempLine.end0.y, graphXY[0], graphXY[1]);
	//if (quadrantEdge!= null) {
		var newX = quadrantEdge[0];
		var newY = quadrantEdge[1];
		myLine = new Line(tempLine.end0.x, tempLine.end0.y, newX, newY);
	//} else {
		//$(message).html('The line could not be drawn. Please try again.');
	//}
	tempLine = null;
	updateGraph();
	updateDisplays();
	$(graph).off();
	$(this).off();
	//allow click on graph to adjust endpoint no.2
	$(graph).on({ 'mousedown' : function( event ) { adjustLine(event); } });
	$(graph).on({ 'touchstart' : function( event ) { adjustLine(event); } });
}

function adjustLine(e) {	//by clicking on graph
	e.preventDefault();
	var graphXY = mouseToGraphXY(e);
	var newX = graphXY[0];
	var newY = graphXY[1];

	tempLine = new Line( myLine.end0.x, myLine.end0.y, newX, newY );
	myLine = null;
	
	updateGraph();

	$(graph).on({ 'mousemove' : function( event ) { lineDrawing(event); } });
	$(graph).on({ 'touchmove' : function( event ) { lineDrawing(event); } });

	$(this).on({ 'mouseup' : function( event ) { endLine(event); } });
	$(this).on({ 'touchend' : function( event ) { endLine(event); } });
}

//adjust line with +/- toggle buttons
function adjustSlope(n) {
	var x0 = myLine.end0.x;
	var y0 = myLine.end0.y;
	var x1 = myLine.end1.x;
	var y1 = myLine.end1.y;
	var m = getSlope(x0, y0, x1, y1);

	//increment slope
	//increment slope
	var b = y0 - m * x0;
	m = Math.round(m * 10) / 10;	//round to tenths
	m += n;
	// var b = y0 - m * x0;

	if (y0 == yMin) {
		//line originates on x-axis, so this endpoint will change
		x0 = (yMin - b) / m;
		y0 = yMin;
		//myLine.end0.x = x0;
		//myLine.end0.y = y0;
	}

	//find new x,y for endpoint where new line reaches quadrant edge
	var  quadrantEdge = getQuadrantEdge(x0, y0, null, null, m, b);
	var newX1 = quadrantEdge[0];
	var newY1 = quadrantEdge[1];
	
	if (isLineOnGraph(m, b) && drawnFromAnAxis(x0, y0, newX1, newY1, m)) {
		//update line
		myLine.end0.x = x0;
		myLine.end0.y = y0;
		myLine.end1.x = newX1;
		myLine.end1.y = newY1;
		updateGraph();
		updateDisplays();
	} else {
		//disable button
		if (n > 0) {
			$(slopeUp).button("disable");
		} else {
			$(slopeDown).button("disable");
		}
	}
}
function adjustIntercept(n) {
	var x0 = myLine.end0.x;
	var y0 = myLine.end0.y;
	var x1 = myLine.end1.x;
	var y1 = myLine.end1.y;
	var m = getSlope(x0, y0, x1, y1);
	var b = y0 - m * x0;
	b = roundToPrecision(b, 2);

	//increment y-intercept
	b += n;

	console.log('before: ' +x0 +','+ y0+','+x1+','+y1)
	console.log('m = ' + m)
	//console.log(isLineOnGraph(m, b));

	//find start endpoint (on axis)
	//check if crosses x or y axis
	console.log('x0 = ' + x0)
	if ( x0 == xMin && y0+n >= yMin && m != 0 ) {		//crosses y axis, x = 0, so adjust y value
		//cross y axis
		var newX0 = xMin;
		var newY0 = y0 + n;
		var newX1 = x1;
		var newY1 = y1 + n;
	} else if (m == 0) {
		//cross y axis, slope zero
		var newX0 = xMin;
		var newY0 = y0 + n;
		var newX1 = xMax;
		var newY1 = y1 + n;
	} else {			//crosses x axis, y = 0
		//cross x axis
		var newX0 = (yMin - b) / m;
		var newY0 = yMin;
		var newX1 = x1;
		var newY1 = y1 + n;
		if (newX0 < xMin) {
			//if x is less than xMin, the line now crosses the y axis (after incrementing y-intercept)
			newX0 = xMin;
			newY0 = y0 + n;
			newX1 = x1;
			newY1 = y1 + n;
		}
	}

	console.log('after: ' + newX0 +','+ newY0+','+newX1+','+newY1)
	var mNew = getSlope(newX0, newY0, newX1, newY1);
	var bNew = roundToPrecision(newY0 - mNew * newX0, 2);
	console.log('m = ' + mNew)
	console.log('b = ' + bNew)
	console.log(isLineOnGraph(mNew, bNew));
	if (isLineOnGraph(mNew, bNew) && drawnFromAnAxis(newX0, newY0, newX1, newY1, mNew)) {

		//find other endpoint
		var  quadrantEdge = getQuadrantEdge(newX0, newY0, newX1, newY1);
		var newX1 = quadrantEdge[0];
		var newY1 = quadrantEdge[1];

		//update line
		myLine.end0.x = roundToPrecision(newX0, 5);
		myLine.end0.y = roundToPrecision(newY0, 5);
		myLine.end1.x = roundToPrecision(newX1, 5);
		myLine.end1.y = roundToPrecision(newY1, 5);
		updateGraph();
		updateDisplays();
	} else {
		//disable button
		if (n > 0) {
			$(yIntUp).button("disable");
		} else {
			$(yIntDown).button("disable");
		}
	}
}
function isLineOnGraph(m, b) {
	//test if the line is ON the graph
	if (b > 0 && b < 10) {
		return true;
	}
	var y = m * 10 + b;
	if (b < 10) {
		if (y > 0) {
			return true;
		} else if (y < 0) {
			return false;
		}
	} else if (b > 10) {
		if (y > 0) {
			return false;
		} else if (y < 0) {
			return true;
		}
	}
	return false;
}
function drawnFromAnAxis(x0,y0,x1,y1, m) {
	if (x0 == xMin) {
		//line starts on y axis
		if (y0 <= yMax && y0 >= yMin) {	//line starts within quadrant
			return true;
		} else {
			return false;
		}
	} else {
		//line starts on x axis
		if (x0 <= xMax && x0 >= xMin) {	//line starts within quadrant
			return true;
		} else if (m > 0) {
			return true;
		} else {
			return false;
		}
	}
}
function adjustXIntercept(n) {
	var x0 = myLine.end0.x;
	var x1 = myLine.end1.x;

	var newX0 = x0 + n;
	var newX1 = x1 + n;
	if (newX0 < 0 || newX1 < 0) {
		newX0 = newX1 = 0;
	} else if (newX0 > 10 || newX1 > 10) {
		newX0 = newX1 = 10;
	}

	myLine.end0.x = newX0;
	myLine.end1.x = newX1;
	updateGraph();
	updateDisplays();
}

var lastPicked, randomPicker;
function generateData () {
	myPoints = [];
	if (lastPicked || lastPicked==0) {
		while (lastPicked == randomPicker) {
			randomPicker = Math.floor( Math.random() * samplePoints.length);
		}
	} else {
		randomPicker = Math.floor( Math.random() * samplePoints.length);
	}
	lastPicked = randomPicker;
	var sampleSet = samplePoints[randomPicker];
	for (var i=0; i<sampleSet.length; i++) {
		myPoints.push(new Point( sampleSet[i][0], sampleSet[i][1]));
	}
	updateGraph();
	updateDisplays();
	$(message).html(maxMsg);
}

function updateGraph() {
	drawGraph(graph_ctx);
}

function updateDisplays() {
	for (var p=0; p < myPoints.length; p++) {	//fill data in rows
		var x = myPoints[p].x;
		var y = myPoints[p].y;
		myRow = rowArray[p];
		$(myRow).css('visibility', 'visible');
		$(myRow).find(".col0").html(x);
		$(myRow).find(".col1").html(y);
	}
	for (var i = p; i<rowArray.length; i++) {	//clear remaining rows
		myRow = rowArray[i];
		$(myRow).css('visibility', 'hidden');
	}
	if (myPoints.length) {
		$(dataTable).show();
	} else {
		$(dataTable).hide();
	}

	//display line info if line exists
	var lineExists = false;
	if (myLine) {
		var x0 = myLine.end0.x;
		var y0 = myLine.end0.y;
		var x1 = myLine.end1.x;
		var y1 = myLine.end1.y;
		lineExists = true;
	} else if (tempLine) {
		var x0 = tempLine.end0.x;
		var y0 = tempLine.end0.y;
		var x1 = tempLine.end1.x;
		var y1 = tempLine.end1.y;
		lineExists = true;
	}
	if (lineExists) {
		displayLineInfo(x0, y0, x1, y1);
		$(columnHider).hide();
	} else {
		hideLineInfo();
		$(columnHider).show();
	}

	//if >= 2 points, show correlation display and active draw line button
	if (myPoints.length >= 2) {
		displayCorrelation();
		if (!myLine && !tempLine) {
			$("#graph-controls").show();
			$(drawLineButton).button("enable");
		}
	} else {
		hideCorrelation();
		$("#graph-controls").hide();
		$(drawLineButton).button("disable");
	}

	//if max no. of points added, and no line drawn, change help text
	// if (myPoints.length == 10 && !lineExists) {
	// 	$(message).html('You’ve reached the maximum of ten points.<br />');
	// }

	//if line exists, enable reset line button
	if (myLine) {
		$(resetLineButton).button("enable");
	} else {
		$(resetLineButton).button("disable");
	}

	//reset data button is active if > 0 points
	//generate data button is active if = 0 points
	 if (!myPoints.length) {					
		$(resetDataButton).button("disable");
		$(generateDataButton).button("enable");
	 } else {
	 	$(resetDataButton).button("enable");
		$(generateDataButton).button("disable");
	 }
}

function displayCorrelation() {
	//calculate corellation of data
	//form arrays of x and y coordinates
	var xArray = [];
	var yArray = [];
	for (p in myPoints) {
		xArray.push(myPoints[p].x);
		yArray.push(myPoints[p].y);
	}
	var correlationValue = getPearsonsCorrelation(xArray, yArray);
	if (correlationValue > 0) {
		$(correlationDisplay).html('Positive');
	} else if (correlationValue < 0) {
		$(correlationDisplay).html('Negative');
	} else {
		$(correlationDisplay).html('None');
	}
	$(correlationLabel).show();
	$(correlationDisplay).show();
	if (correlationValue != 0 && !isNaN(correlationValue)) {
		$(meter).show();
		var needleAngle = Math.abs(correlationValue) * 180 - 90;
		var stringRotate = "rotate(" + needleAngle + "deg)";
		$(correlationNeedle).css('transform', stringRotate);
	} else {
		$(meter).hide();
	}
}
function hideCorrelation() {
	$(correlationDisplay).html('');
	$(meter).hide();
	$(correlationLabel).hide();
	$(correlationDisplay).hide();
}

var maxHeight = 26, fontSize = 22;	//settings for equation text

function displayLineInfo(x0, y0, x1, y1) {

	$(slopeLabel).show();
	$(aboveLabel).show();
	$(belowLabel).show();
	$(onLabel).show();
	$(sumLabel).show();

	//line equation display
	var slope, mSign, bSign, equString, slopeString;
	slope = getSlope(x0, y0, x1, y1);
	if (slope > 0) {
		$(slopeDisplay).html('Positive');
		mSign = "";
	} else if (slope < 0) {
		$(slopeDisplay).html('Negative');
		mSign = "&ndash;";
	} else if (slope==0) {
		$(slopeDisplay).html('Zero');
	}
	b = y0 - slope * x0;
	if (b >= 0) {
		bSign = "&plus; ";
	} else {
		bSign = "&ndash; ";
	}
	if (!isFinite(slope)) {
		if (x0==0) {
			equString = "<i>x</i> &equals; " + x0;
		} else {
			equString = "<i>x</i> &equals; " + x0.toFixed(1);
		}
		$(equationButtons).hide();
		$(equationButtonsB).show();	//buttons to allow x intercept adjusting
		if (x0 <= 0 || x1 <= 0) {
			$(xIntDown).button("disable");
		} else {
			$(xIntDown).button("enable");
		}
		if (x0 >= 10 || x1 >= 10) {
			$(xIntUp).button("disable");
		} else {
			$(xIntUp).button("enable");
		}

	} else { //if (slope > 0 || slope < 0) {
		if (slope < 0.05 && slope > 0) {
			slopeString = slope.toFixed(2);
		} else if (slope > -0.05 && slope < 0) {
			slopeString = slope.toFixed(2);
		} else {
			slopeString = slope.toFixed(1);
		}
		equString = "<i>y</i> &equals; " + slopeString + "<i>x</i> " + bSign + Math.abs(b).toFixed(1);
		equString = equString.replace("-", "&ndash;");
		$(equationButtons).show();
		$(equationButtonsB).hide();
	}
	$(equationDisplay).html(equString);

	//reduce font size if equation doesn't fit in text area
	$(equationDisplay).css('font-size', fontSize);
	var equHeight = $(equationDisplay).height();
	while (equHeight > maxHeight && fontSize > 12) {
		fontSize --;
		$(equationDisplay).css('font-size', fontSize);
		equHeight = $(equationDisplay).height();
	}

	//displacement info for each point
	var myDisplacement, pointsAbove = 0, pointsBelow = 0, pointsOn = 0, sumOfDisplacement = 0;
	for (var p=0; p < myPoints.length; p++) {	//fill data in rows
		var x = myPoints[p].x;
		var y = myPoints[p].y;
		myRow = rowArray[p];
		
		myDisplacement = y - (slope * x + b);

		if (isFinite(myDisplacement)) {
			if (myDisplacement >= 0.1) {
				pointsAbove ++;
				strDisp = myDisplacement.toFixed(1);
			} else if (myDisplacement <= -0.1) {
				pointsBelow ++;
				strDisp = myDisplacement.toFixed(1);
			} else {
				pointsOn ++;
				strDisp = "0";
			}
			sumOfDisplacement += myDisplacement;
			strDisp = strDisp.replace("-", "&ndash;");
			$(myRow).find(".col2").html(strDisp);
		}
	}
	if (isFinite(slope)) {
		$(aboveDisplay).html(pointsAbove);
		$(belowDisplay).html(pointsBelow);
		$(onDisplay).html(pointsOn);
		strSumDisplacement = sumOfDisplacement.toFixed(1);
		strSumDisplacement = strSumDisplacement.replace("-", "&ndash;");
		$(sumDisplay).html(strSumDisplacement);
	} else {
		$(slopeDisplay).html('');
		$(aboveDisplay).html('');
		$(belowDisplay).html('');
		$(onDisplay).html('');
		$(sumDisplay).html('');
		//$(equationButtons).hide();
	}


	$(slopeUp).button("enable");
	$(slopeDown).button("enable");
	$(yIntUp).button("enable");
	$(yIntDown).button("enable");

}
function hideLineInfo() {
	$(slopeDisplay).html('');
	$(equationDisplay).html('');
	$(aboveDisplay).html('');
	$(belowDisplay).html('');
	$(onDisplay).html('');
	$(sumDisplay).html('');
	$(equationButtons).hide();
	$(equationButtonsB).hide();

	$(slopeLabel).hide();
	$(aboveLabel).hide();
	$(belowLabel).hide();
	$(onLabel).hide();
	$(sumLabel).hide();
}

var gridColor = "#ccc", gridStroke = 1;	//grid line settings
var axisColor = "#000", axisStroke = 3;
var plotColor = "#FF0000", plotStroke = 3;	//plot line settings, red line
var dotSize = 5;

//graph settings
var marginX = 24.5, marginY = 32.5, graphHeight = 340, graphWidth = 340, gridSpacing = 34, tickWidth = 6;
var axisArrowSize = 18, plotArrowSize = 24;
var axisExtension = 24;

function getSlope(x0, y0, x1, y1) {
	var slope = (y1-y0) / (x1-x0);
	slope = roundToPrecision(slope, 3);
	return slope;
}
function roundToPrecision(n, p) {
	return parseFloat( n.toFixed(p) );
}

function drawGraph(ctx) {
	ctx.clearRect(0, 0, graph.width, graph.height);
	ctx.fillStyle = plotColor;
	for (p in myPoints) {
		var x = myPoints[p].x * gridSpacing + marginX;
		var y = marginY + graphHeight - myPoints[p].y * gridSpacing;
		ctx.beginPath();
		ctx.arc(x, y, dotSize, 0, 2*Math.PI);
		ctx.fill();
	}
	if (tempLine) {
		var x0 = tempLine.end0.x * gridSpacing + marginX;
		var y0 =  marginY + graphHeight - tempLine.end0.y * gridSpacing;
		var x1 = tempLine.end1.x * gridSpacing + marginX;
		var y1 =  marginY + graphHeight - tempLine.end1.y * gridSpacing;
		drawLine(ctx, x0, y0, x1, y1, plotColor, plotStroke);
	}
	if (myLine) {
		var x0 = myLine.end0.x * gridSpacing + marginX;
		var y0 =  marginY + graphHeight - myLine.end0.y * gridSpacing;
		var x1 = myLine.end1.x * gridSpacing + marginX;
		var y1 =  marginY + graphHeight - myLine.end1.y * gridSpacing;
		drawLine(ctx, x0, y0, x1, y1, plotColor, plotStroke);
		//lineWithArrowhead(ctx, x0, y0, x1, y1, Math.PI/10, plotArrowSize, plotColor, plotStroke);
	}
}

function drawAxis(ctx) {
	ctx.clearRect(0, 0, axis.width, axis.height);
	//draw the grid
	ctx.beginPath();
	for (var x = marginX; x<= marginX + graphWidth; x += gridSpacing) {
		ctx.moveTo(x, marginY);
		ctx.lineTo(x, marginY + graphHeight);
	}
	for (var y = marginY + graphHeight; y>= marginY; y -= gridSpacing) {
		ctx.moveTo(marginX, y);
		ctx.lineTo(marginX + graphWidth, y);
	}
	ctx.strokeStyle = gridColor;
	ctx.lineWidth = gridStroke;
	ctx.lineCap = "square";
	ctx.stroke();

	//draw the axis
	var x0 = marginX;	//origin x
	var y0 = marginY + graphHeight;	//origin y
	var y1 = marginY - axisExtension;	//y axis end
	var x1 = marginX + graphWidth + axisExtension;	//x axis end

	lineWithArrowhead(ctx, x0, y0, x1, y0, Math.PI/10, axisArrowSize, axisColor, axisStroke);
	lineWithArrowhead(ctx, x0, y0, x0, y1, Math.PI/10, axisArrowSize, axisColor, axisStroke);

	//draw tick marks
	var labelStart = 1;
	ctx.textAlign = "center";
	ctx.font="14px serif";
	for (var x = marginX + gridSpacing; x<= marginX + graphWidth; x += gridSpacing) {
		ctx.moveTo(x, marginY + graphHeight - tickWidth);
		ctx.lineTo(x, marginY + graphHeight + tickWidth);
		ctx.fillText(labelStart++, x, marginY + graphHeight + tickWidth + 14);
	}
	labelStart = 1;
	ctx.textAlign = "right";
	for (var y = marginY + graphHeight - gridSpacing; y>= marginY; y -= gridSpacing) {
		ctx.moveTo(marginX - tickWidth, y);
		ctx.lineTo(marginX + tickWidth, y);
		ctx.fillText(labelStart++, marginX - tickWidth - 4, y + 4);
	}
	ctx.textAlign = "center";
	ctx.font="italic 14px serif";
	ctx.fillText('x', marginX + graphWidth + 16, marginY + graphHeight - 10);
	ctx.fillText('y', marginX + 18, marginY - 12);

	ctx.strokeStyle = axisColor;
	ctx.lineWidth = gridStroke;
	ctx.stroke();
}

function drawLine(ctx, x0, y0, x1, y1, style, stroke) {

	if (Math.sqrt((x1-x0) * (x1-x0) + (y1-y0) * (y1-y0)) > plotArrowSize) {	//add arrowhead if line is long enough
		lineWithArrowhead(ctx, x0, y0, x1, y1, Math.PI/10, plotArrowSize, plotColor, plotStroke);
	} else {
		//draw line
		ctx.beginPath();
		ctx.moveTo(x0, y0);
		ctx.lineTo(x1, y1);

		ctx.strokeStyle = style;
		ctx.lineWidth = stroke;
		ctx.stroke();

	}
}

function lineWithArrowhead(ctx, x0, y0, x1, y1, angle, d, style, stroke) {
	//find angle of line
	var lineangle = Math.atan2(y1-y0, x1-x0);
	//find length of sides of arrowhead
	var h = Math.abs(d/Math.cos(angle));

	var angle1 = lineangle+Math.PI+angle;
	var topx = x1 + Math.cos(angle1)*h;
	var topy = y1 + Math.sin(angle1)*h;
	var angle2 = lineangle + Math.PI - angle;
	var botx = x1 + Math.cos(angle2)*h;
	var boty = y1 + Math.sin(angle2)*h;
	var midx = (topx + botx) / 2;
	var midy = (topy + boty) / 2;
	var centerx = (midx + x1) / 2;
	var centery = (midy + y1) / 2;
	var notchx = (midx + centerx) / 2;
	var notchy = (midy + centery) / 2;

	//draw line
	ctx.beginPath();
	ctx.moveTo(x0, y0);
	ctx.lineTo(centerx, centery);

	ctx.strokeStyle = style;
	ctx.lineWidth = stroke;
	ctx.stroke();
	
	//draw arrowhead
	ctx.moveTo(topx, topy);
	ctx.quadraticCurveTo(centerx, centery, x1, y1);
	ctx.quadraticCurveTo(centerx, centery, botx, boty);
	ctx.quadraticCurveTo(notchx, notchy, topx, topy);

	ctx.lineCap = "round";
	//ctx.lineJoin = "miter";
	ctx.lineWidth = 1;
	ctx.stroke();
	
	ctx.fillStyle = style;
	ctx.fill();
}

//function to calculate correlation of data
function getPearsonsCorrelation(x, y) 
{
	var shortestArrayLength = 0;
	if(x.length == y.length)
	{
		shortestArrayLength = x.length;
	}
	else if(x.length > y.length)
	{
		shortestArrayLength = y.length;
		//console.log('x has more items in it, the last ' + (x.length - shortestArrayLength) + ' item(s) will be ignored');
	}
	else
	{
		shortestArrayLength = x.length;
		//console.log('y has more items in it, the last ' + (y.length - shortestArrayLength) + ' item(s) will be ignored');
	}
 
	var xy = [];
	var x2 = [];
	var y2 = [];
 
	for(var i=0; i<shortestArrayLength; i++)
	{
		xy.push(x[i] * y[i]);
		x2.push(x[i] * x[i]);
		y2.push(y[i] * y[i]);
	}
 
	var sum_x = 0;
	var sum_y = 0;
	var sum_xy = 0;
	var sum_x2 = 0;
	var sum_y2 = 0;
 
	for(var i=0; i<shortestArrayLength; i++)
	{
		sum_x += x[i];
		sum_y += y[i];
		sum_xy += xy[i];
		sum_x2 += x2[i];
		sum_y2 += y2[i];
	}
 
	var step1 = (shortestArrayLength * sum_xy) - (sum_x * sum_y);
	var step2 = (shortestArrayLength * sum_x2) - (sum_x * sum_x);
	var step3 = (shortestArrayLength * sum_y2) - (sum_y * sum_y);
	var step4 = Math.sqrt(step2 * step3);
	var answer = step1 / step4;
 
	return answer;
}

//extend a point to the edge of the quadrant
function getQuadrantEdge(x0, y0, x1, y1, m, b) {	//given existing point return a new value for x1,y1 at the edge of graphs
	if (x1 != null && y1 != null) {	//
		//if x1, y1 are given calculate m, b
		var m = (y1-y0) / (x1-x0);
		var b = y0 - m * x0;
	} else {
		//if x1, y1 are unknown, m and b must be given
		x1 = xMax;
		y1 = yMin;
	}

	//find intercept at the maximum x (right edge)
	var newX0 = xMax;
	var newY0 = m * newX0 + b;

	//find intercept at the maximum y (top edge)
	var newY1 = yMax;
	var newX1 = (newY1 - b) / m;

	//find intercept at minimum x (left edge)
	var newX2 = xMin;
	var newY2 = m * newX2 + b;

	//find intercept at the minimum y (bottom edge)
	var newY3 = yMin;
	var newX3 = (newY3 - b) / m;

	//horiz. and vert. line cases
	if (m==0) {
		if (x0 <= x1) {
			 console.log('case-zeroA')
			// console.log('b: '+b)
			return [newX0, newY0];
		} else {
			 //console.log('case-zeroB')
			return [newX2, newY2];
		}
	} else if (!isFinite(m)) {
		if (y0 <= y1) {
			 //console.log('case-infA')
			return [x0, yMax];
		} else {
			 //console.log('case-infB')
			return [x0, yMin];
		}
	}

	//test which point lies in the quadrant
	if (newY0 <= yMax && newY0 > yMin) {
		 //console.log('case0')
		return [newX0, newY0];
	} else if (newX1 <= xMax && newX1 > xMin) {
		 //console.log('case1')
		return [newX1, newY1];
	} else if (newX3 <= xMax && newX3 > xMin && y0 != yMin) {	//y0 != yMin so a line starting on x-axis won't end on itself
		 //console.log('case2')
		return [newX3, newY3];
	} else if (newY2 <= yMax && newY2 >= yMin && x0 != xMin) {	//x0 != xMin so a line starting on y-axis won't end on itself
		 //console.log('case3')
		return [newX2, newY2];
	} else {
		// console.log(b)
		console.log('no point found inside the quadrant');
		return [x0, y0];	//return original x,y
		//return null;	
	}
}