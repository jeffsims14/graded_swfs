class Panel extends MovieClip {

	private var w:Number;
	private var h:Number;
	private var fillColor:Number = 0xE0DED0;
	private var fillAlpha:Number = 100;
	private var lineColorA:Number = 0xAAAAAA;
	private var lineColorB:Number = 0x666666;
	private var lineAlpha:Number = 50;
	private var stroke:Number = 1;
	public var myBG:MovieClip;

	function Panel() {
		myBG = this.createEmptyMovieClip("myBG", this.getNextHighestDepth());
		myBG.beginFill(fillColor, fillAlpha);
		myBG.lineStyle(stroke, lineColorA, lineAlpha, true, "none");
		myBG.moveTo(w, 0);
		myBG.lineTo(0, 0);
		myBG.lineTo(0, h);
		myBG.lineStyle(stroke, lineColorB, lineAlpha, true, "none");
		myBG.lineTo(w, h);
	}

}