class SignToggle extends MovieClip {
	var myButton:MovieClip;

	function SignToggle() {
		this.myButton.onPress = function() {
			_parent.toggleMe();
		}
	}

	function toggleMe() {
		if (this._currentframe==1) {
			this.gotoAndStop(2);
		} else {
			this.gotoAndStop(1);
		}
	}
}