class PanelSetting extends MovieClip {

	private var w:Number;			//width
	private var h:Number;			//height
	private var labelText:String;	//setting label
	private var header:String;		//header text
	private var id:String;			//id is passed in setAction function
	private var _parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	private var panelFormat, inputFormat;
	private var myDefault;

	function PanelSetting() {
		d = this.getNextHighestDepth();
		//button label
		if (labelText) {
			var settinglabel:TextField = this.createTextField("settinglabel", d++, 0, 0, w, h);
			if (!w || !h) {
				settinglabel.autoSize = true;
			}
			panelFormat = new TextFormat();
			panelFormat.font = 'Times';
			panelFormat.color = 0x000000;
			panelFormat.size = 14;
			panelFormat.align = 'center';

			inputFormat = new TextFormat();
			inputFormat.font = 'Arial';
			inputFormat.color = 0x000000;
			inputFormat.size = 12;
			inputFormat.align = 'center';

			settinglabel.setNewTextFormat(panelFormat);
			settinglabel.selectable = false;
			settinglabel.html = true;
			settinglabel.htmlText = labelText;
			
		}
		if (header) {
			var headerLabel:TextField = this.createTextField("headerLabel", d++, 2, -22, 1, 1);
			headerLabel.setNewTextFormat(panelFormat);
			headerLabel.selectable = false;
			headerLabel.html = true;
			headerLabel.htmlText = header;
			headerLabel.autoSize = true;
		}

		if (myDefault == undefined) {
			myDefault = '';
		}

		var myInput:TextField = createInputText(this, settinglabel._width + 2, 0, myDefault, "myInput", 30, 17);
		myInput.onChanged = function() {
			_parent._parent._parent.updateGradingOptions();
		}
		myInput.tabIndex = this._parent._parent.tabNum++;
	}

	////////////////////////////////// CREATE INPUT TEXT FIELD
	function createInputText(mc:MovieClip, myX:Number, myY:Number, myText:String, inst:String, myW:Number, myH:Number, myColor:Number) {
		var newInputText:TextField = mc.createTextField(inst, d++, myX, myY, myW, myH);
		newInputText.text = myText;
		newInputText.border = true;
		newInputText.borderColor = 0x999999;
		newInputText.background = true;
		newInputText.backgroundColor = 0xFFFFFF;
		newInputText.type = "input";
		newInputText.restrict = "0-9 \\. \\-";
		newInputText.setTextFormat(inputFormat);
		newInputText.setNewTextFormat(inputFormat);
		newInputText.textColor = myColor;
		return newInputText;
	}
	
}