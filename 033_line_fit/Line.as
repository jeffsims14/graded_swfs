class Line {

	var end0:Point;
	var end1:Point;

	public function Line (x0:Number, y0:Number, x1:Number, y1:Number) {
		this.end0 = new Point(x0, y0);
		this.end1 = new Point(x1, y1);
	}
}