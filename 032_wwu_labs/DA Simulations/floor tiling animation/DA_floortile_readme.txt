----------------
::Floor Tile Sim Authoring Notes::

In the background, the Graded SWF (Tile Sim SWF) expects an algo named "randomize" that is an Algo List with 1, 2, or 3. This algo is used by the author to select either 12, 16, or 24 inch tiles - and sets the floor dimensions appropriately. If that algo isnt found (or isnt 1,2,or 3) it attempts to set such an algo. 

Author must also set an algo named "answer" that is dependent on what option "randomize" chooses at run-time.

Tile Options, Tile Size, Floor Dimensions, Total Tiles:
option 1: 12inch, 	6x11 (1.8288m x 3.3528m),	66 total 
option 2: 16 inch,	 5x9 (2.032m x 3.6576m), 	45 total
option 3: 24 inch, 	4x7 (2.4384m x 4.2672m), 	28 total

From that main "randomize" algo, you can expand and create dependent algos to craft your question accordingly. For example, make list algos so you can display the tile/floor size in some question text, floor dimensions, or total number of grapes.
------------------

v20110919