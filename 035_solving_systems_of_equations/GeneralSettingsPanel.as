class GeneralSettingsPanel extends Panel {
	
	var d:Number;
	
	var checkBox:MovieClip;
	var label:TextField;
	
	var parent_mc:MovieClip;	//main module
	
	function GeneralSettingsPanel() {
		trace("creating general settings panel");
		
		d = this.getNextHighestDepth();
		
		checkBox = this.attachMovie("checkbox", "checkBox", d++, {w: 20, h:20, _x:20, _y:20});
		checkBox._checked = false;
		checkBox.onRelease = function (){
			if(this._checked){
				this._gotoAndStop("_off");
				this._checked = false;
				
			} else {
				this._gotoAndStop("_on");
				this._checked = true;
			}
			
		}
		
		
		//label format
		var labelFormat = new TextFormat();
		labelFormat.font = 'Arial';
		labelFormat.color = 0x000000;
		labelFormat.size = 12;
		labelFormat.align = 'left';
		
		//label                                   x, y, width, height
		label = this.createTextField("label", d++, 40, 25, 150, 20);
		label.setNewTextFormat(labelFormat);
		label.selectable = false;
		label.html = true;
		label.htmlText = "shade regions";
		
		
	}
}
