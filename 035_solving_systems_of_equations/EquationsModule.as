
class EquationsModule extends MovieClip {

	var questionSettings:Object;			//settings saved across the entire question (graph settings, etc.)
	var graphSettings:Object;					//settings saved on each tab (student manipulations, grading options, etc.)

	var appMode:String;						//global App mode (author or student)
	var tabType:String;						//global Tab type (question or response)

	var myGraphView:MovieClip;
	var panels:MovieClip, gradingPanels:MovieClip;
	var settings_icon:MovieClip;
	var myWinPanel:MovieClip;
	var d = 1;								//movieClip level depth
	var tabNum:Number;						//tab index counter

	var lineA:Line;
	var lineB:Line;
	var settingsArray:Array;
	var wildcardOptions:Object;
	var debug:TextField;
	var intersectionPoint:Point;
	

	function EquationsModule() {
		//debug text
		debug = this.createTextField('debug',500,0,330,550,300);
		debug.text = 'hello';
		init();
	}

// INIT function called on first load

	function init() {
		appMode = _global.core.Globals.appMode;	//get the global app mode ("author" or "student")
		appMode = "author";						//uncomment for testing
		
		//set up graph view
		myGraphView = this.attachMovie("GraphView", "myGraphView", d++, {graphWidth:340, graphHeight:340, _x:0, _y:0, parent_mc:this});
		
		//movie clips to hold panels
		panels = this.createEmptyMovieClip("panels_mc", d++);
		gradingPanels = this.createEmptyMovieClip("gradingPanels", d++);
		
		//set up equation entry panel
		tabNum = 0;
		var myEquPanel = panels.attachMovie("EquationPanel", "equationPanel", d++, {w:245, h:100, _x:350, _y:30, parent_mc:this});
		

		//set up window panel
		tabNum = 2;
		myWinPanel = panels.attachMovie("WindowPanel", "myWinPanel", d++, {w:200, h:150, _x:372, _y:170, parent_mc:this});
		myWinPanel.myGraph = myGraphView;	//reference for window panel to control graph
		myWinPanel._visible = false; //we start out invisible and are turned on by the settings icon
		
		//create two lines
		lineA = new Line();
		lineB = new Line();
		
		
		//set up for grading options if we are in Author App
		if (appMode == "author") {
			trace("appmode is author");
			//set default grading values and any other UI setup for the Author App
			//show the gear			
			tabNum = 4;
			settings_icon = this.attachMovie("settings_icon", "settings_icon", d++, { _x:560, _y:10, parent_mc:this});
			settings_icon._visible = false;
			tabNum = 6;
			
			settings_icon.onRelease = function(){
				//what to do
				if(!this._parent.myWinPanel._visible){
					this._parent.myWinPanel.updateUIAfterReload();					
					this._parent.myWinPanel._visible = true;
				}
				else {
					this._parent.myWinPanel._visible = false;
					
				}
			}
			wildcardOptions = new Object();
			wildcardOptions.gradeIntersection = true;
			wildcardOptions.gradePrefixLineA = true;
			wildcardOptions.gradeEquationLineA = true;
			wildcardOptions.gradePrefixLineB = true;
			wildcardOptions.gradeEquationLineB = true;
			gradingPanels._visible = false;
			
			var defaultIntersectionPositionTolerance:Number = 50;
			myGraphView.intersectionPositionTolerance = defaultIntersectionPositionTolerance;
			
			var gradeIntersection = gradingPanels.attachMovie("GradedSetting", "gradeIntersection", 1, {_x:10, _y:10, id: "gradeIntersection", parent_mc:this});
			var gradePrefixLineA = gradingPanels.attachMovie("GradedSetting", "gradePrefixLineA", 2, {_x:360, _y:32, id: "gradePrefixLineA", parent_mc:this});
			var gradeEquationLineA = gradingPanels.attachMovie("GradedSetting", "gradeEquationLineA", 3, {_x:460, _y:32, id: "gradeEquationLineA", parent_mc:this});			
			var gradePrefixLineB = gradingPanels.attachMovie("GradedSetting", "gradePrefixLineB", 4, {_x:360, _y:78, id: "gradePrefixLineB", parent_mc:this});
			var gradeEquationLineB = gradingPanels.attachMovie("GradedSetting", "gradeEquationLineB", 5, {_x:460, _y:78, id: "gradeEquationLineB", parent_mc:this});
		//	var toleranceGradingPanel = gradingPanels.attachMovie("Panel", "toleranceGradingPanel", 6, {w:245, h:65, _x:350, _y:170});
		//	var intersectionPositionToleranceSetting = gradingPanels.attachMovie("PanelSetting", "intersectionPositionToleranceSetting", 8, {_x:360, _y:190, labelText:"Solution Tolerance %", myDefault: intersectionPositionTolerance});						
			var toleranceGradingPanel:MovieClip = gradingPanels.attachMovie("ToleranceSettingPanel", "toleranceSettingPanel", 8, {w:245, h:65, _x:350, _y:170, parent_mc:this});
			//toleranceGradingPanel.myGraph = myGraphView;	//reference for panel to control graph	
			gradingPanels._visible = true;
			trace("toleranceGradingPanel is visible: " + gradingPanels.toleranceSettingPanel._visible);
			trace("toleranceGradingPanel width is " +  gradingPanels.toleranceSettingPanel._width);
		}
		
		
		reload();	//reload to load any saved settings
	}

// RELOAD function called every time we switch to a new tab to recall saved data

	function reload() {	
		debug.text = "";
			
		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)
		tabType = "response";					//uncomment for testing 
		//tabType = "question";					//uncomment for testing
		
		//debug.text += "mode " + appMode + "\n";
		//debug.text += "tab type " + tabType + "\n";
		
		//check if saved settings exist
		if (questionSettings == null) {
			//if not, initalize the settings and other objects
			questionSettings = new Object();
			graphSettings = new Object();

			//save the settings for recall in graded swf
			updateQuestionSettings();			
			updateGraphSettings();
			//showDebugInfo(graphSettings);

			//continue any other initial setup
			
		} else {
			//Settings exist, so we reload the settings and other stuff
			reloadQuestionSettings( questionSettings );
			reloadGraphSettings( graphSettings );
			//showDebugInfo(graphSettings);
			
			/*
			//here update the display with the loaded settings
			
			*/
			updateUIInputs();
		}
		
		updateGraph();

		
		//if on a response tab in author app, show grading options
		if (appMode == "author") {

			if (tabType == "response") {
				showGradingOptions();		//extra grading options are shown on response tabs
			} else {
				hideGradingOptions();		//otherwise hide the grading options
			}

			//if on a question tab in author app, show general settings UI

			if (tabType == "question") {
				showGeneralSettings();		//general settings are shown on the question tab
			} else {
				hideGeneralSettings();		//otherwise hide the general settings
			}
		}
	}
	function showDebugInfo(graphSettings){
		for (var o in graphSettings){
			if(o != "wildcardOptions"){
				debug.text += o + ': ' + graphSettings[o] + "\n";
				var subSetting:Object = graphSettings[o];
				for (var ob in subSetting){
					debug.text += "    " + ob + " : " + subSetting[ob] + "\n";
					var subSubSetting:Object = subSetting[ob];
					for (var obj in subSubSetting){
						debug.text += "        " + obj + " : " + subSubSetting[obj] + "\n";
					}
				}
			}
			
		}		
		
	}
	function traceDebugInfo(object){
		var debugtext;
		for (var o in object){
			
				debugtext += o + ': ' + object[o] + "\n";
				var subObject:Object = object[o];
				for (var ob in subObject){
					debugtext += "    " + ob + " : " + subObject[ob] + "\n";
					var subSubObject:Object = subObject[ob];
					for (var obj in subSubObject){
						debugtext += "        " + obj + " : " + subSubObject[obj] + "\n";
					}
				}
			}
			trace(debugtext);
			
		
	}
	function traceSimpleDebugInfo(object){
		var debugtext;
		for (var o in object){
			
			debugtext += o + ': ' + object[o] + "\n";
			/*
			var subObject:Object = object[o];
			for (var ob in subObject){
				debugtext += "    " + ob + " : " + subObject[ob] + "\n";
				var subSubObject:Object = subObject[ob];
				
			}
			*/
		}
		trace(debugtext);
		
		
	}
	function showSimpleDebugInfo(object){

		for (var o in object){
			
			debug.text += o + ': ' + object[o] + "\n";
			/*
			var subObject:Object = object[o];
			for (var ob in subObject){
			debugtext += "    " + ob + " : " + subObject[ob] + "\n";
			var subSubObject:Object = subObject[ob];
			
			}
			*/
		}
		
		
	}

//SAVE and RELOAD functions for General Settings and any other Student or Authored Data
	function saveQuestionSettings (s:Object):Object {
		//save any data to an object to be saved and recalled
		//s.anySetting = anySetting;
		//saving all graph-related settings - these are set for the whole question
		s.gridXmin = myGraphView.gridXmin;
		s.gridXmax = myGraphView.gridXmax;
		s.gridXscale = myGraphView.gridXscale;
		s.gridYmin = myGraphView.gridYmin;
		s.gridYmax = myGraphView.gridYmax;
		s.gridYscale = myGraphView.gridYscale;
		s.tolerance = myGraphView.tolerance;

		return s;
	}
	function reloadQuestionSettings(s:Object) {
		//recall the data from saved object when switching tabs
		//anySetting = s.anySetting;
		//reloading graph-related settings - these are set for the whole question
		myGraphView.gridXmin = s.gridXmin;
		myGraphView.gridXmax = s.gridXmax;
		myGraphView.gridXscale = s.gridXscale;
		myGraphView.gridYmin = s.gridYmin;
		myGraphView.gridYmax = s.gridYmax;
		myGraphView.gridYscale = s.gridYscale;
		myGraphView.tolerance = s.tolerance;

	}

	//when a setting is changed, call this function to save it
	function updateQuestionSettings() {	
		questionSettings = saveQuestionSettings ( questionSettings );	// <---- CALL THIS WHENEVER SETTINGS ARE CHANGED
	}

	//for other items in the module that need to be saved and recalled
	function saveGraphSettings (s:Object):Object {
		
		//saving selected region
		//s.selectedRegion = myGraphView.selectedRegion;
		s.intersectionPoint = intersectionPoint;
		
		//saving Lines
		s.lineA = lineA;
		s.lineB = lineB;	
		
		//saving grading options
		s.wildcardOptions = wildcardOptions;
		s.intersectionPositionTolerance = myGraphView.intersectionPositionTolerance;
		

		return s;
	}
	function reloadGraphSettings (s:Object) {
		//recall the data from saved object when switching tabs
		//myStuff = s.myStuff;		
		
		//reloading selected region
		//myGraphView.selectedRegion = s.selectedRegion;
		intersectionPoint = s.intersectionPoint;
		
		//reloading lines
		lineA = s.lineA;
		lineB = s.lineB;
		
		//reloading wildcardOptions
		wildcardOptions = s.wildcardOptions;
		myGraphView.intersectionPositionTolerance = s.intersectionPositionTolerance;
		//refresh the input box
			
		
	}

	//when a tab is changed, call this function to save it
	function updateGraphSettings() {	
		graphSettings = saveGraphSettings ( graphSettings );					// <---- CALL THIS WHENEVER TAB IS CHANGED

		//for (var o in graphSettings) {
		//	trace (o + ': ' + graphSettings[o]);	//trace of saved data
		//}
	}

//SHOW AND HIDE GRADING OPTIONS FOR AUTHORS
	function showGradingOptions() {
		//refresh UI with extra grading options for authors
		//note: the GradedSetting class uses this settings Array
		if (settingsArray.length > 0) {
			for (var i=0; i<settingsArray.length; i++) {
				settingsArray[i].removeMovieClip(this);
			}
		}
		
		//create wildcard grading buttons
		settingsArray = new Array();
		
		var gradeIntersection = gradingPanels.attachMovie("GradedSetting", "gradeIntersection", 1, {_x:10, _y:10, id: "gradeIntersection", parent_mc:this});
		
		var gradePrefixLineA = gradingPanels.attachMovie("GradedSetting", "gradePrefixLineA", 2, {_x:360, _y:32, id: "gradePrefixLineA", parent_mc:this});
		var gradeEquationLineA = gradingPanels.attachMovie("GradedSetting", "gradeEquationLineA", 3, {_x:460, _y:32, id: "gradeEquationLineA", parent_mc:this});
		
		var gradePrefixLineB = gradingPanels.attachMovie("GradedSetting", "gradePrefixLineB", 4, {_x:360, _y:78, id: "gradePrefixLineB", parent_mc:this});;
		var gradeEquationLineB = gradingPanels.attachMovie("GradedSetting", "gradeEquationLineB", 5, {_x:460, _y:78, id: "gradeEquationLineB", parent_mc:this});
		//var toleranceGradingPanel = gradingPanels.attachMovie("Panel", "toleranceGradingPanel", 6, {w:245, h:65, _x:350, _y:170});
		//var intersectionPositionToleranceSetting = gradingPanels.attachMovie("PanelSetting", "intersectionPositionToleranceSetting", 8, {_x:360, _y:190, labelText:"Solution Tolerance %", myDefault: intersectionPositionTolerance});						
		var toleranceGradingPanel:MovieClip = gradingPanels.attachMovie("ToleranceSettingPanel", "toleranceGradingPanel", 8, {w:245, h:65, _x:350, _y:170, parent_mc:this});
		toleranceGradingPanel.myGraph = myGraphView;	//reference for panel to control graph	
		gradingPanels._visible = true;
		trace("toleranceGradingPanel is visible: " + gradingPanels.toleranceSettingPanel._visible);
		trace("toleranceGradingPanel width is " +  gradingPanels.toleranceSettingPanel._width);
	}

	function hideGradingOptions() {
		//hide all grading options
		gradingPanels._visible = false;
	}


//SHOW AND HIDE THE GENERAL QUESTION SETTINGS FOR AUTHORS
	function showGeneralSettings() {
		//show the main settings UI for the author to set
		settings_icon._visible = true;

	}

	function hideGeneralSettings() {
		//hide the main settings UI
		settings_icon._visible = false;
		panels.myWinPanel._visible = false;
	}

	//update graph view
	function updateGraph() {

		myGraphView.drawGridAndAxis();		
		myGraphView.drawLines();
		myGraphView.showIntersection();

		//save anything that has changed
		updateGraphSettings();
	}
	
	function updateUIInputs(){
		panels.equationPanel.updateUIAfterReload(lineA, lineB);	
		gradingPanels.toleranceGradingPanel.updateUIAfterReload();
		
				
	}
	
	function toggleSetting(buttonType:String, id:String, buttonMC:MovieClip){
		trace("toggling setting");
		var myVal:Boolean;
		var target:MovieClip = MovieClip(buttonMC._target);
		//"buttonMC" is "this" being sentfrom the GradedSetting class 
		trace(buttonMC._target);
		switch (buttonType) {
			case "graded":
				trace("graded");
				myVal = wildcardOptions[id];
				trace("id is " + id);
				trace("myVal is " + myVal);
				if (myVal) {
					buttonMC.eye.gotoAndStop(2);
	
				} else {
					buttonMC.eye.gotoAndStop(1);
				}
				wildcardOptions[id] = !myVal;				
				break;
			
		}
		
	}
	


//OTHER POSSIBLY USEFUL FUNCTIONS:

//Example of looking up an Algo value from the Global Algo Model using AlgoLookup Class
	/*
		var myLookup = new AlgoLookup();										//AlgoLookup is a Class that calls the global Algo model
		var myLabel:String = 'hello';											//any text field set by authors
		var customLabelFromAlgo:String = myLookup.findAlgoValue(myLabel);		//replace a String with custom String if a matching Algo is found
	*/

//FUNCTION TO FORMAT DECIMALS IN NUMBER DISPLAYS

	/*
	//___________Formatting decimals_______ for displaying numbers rounded to a specific decimal place

	function formatDecimals(num:Number, digits:Number) {
		//if no decimal places needed, we're done
		if (digits<=0) {
			return Math.round(num);
		}
		if (isNaN(num) || num==Number.NEGATIVE_INFINITY || num==Number.POSITIVE_INFINITY ) {
			return num;
		}
		//round the number to specified decimal places   
		//e.g. 12.3456 to 3 digits (12.346) -> mult. by 1000, round, div. by 1000
		var tenToPower = Math.pow(10, digits);
		var cropped = String(Math.round(num*tenToPower)/tenToPower);
		//add decimal point if missing
		if (cropped.indexOf(".") == -1) {
			cropped += ".0";
			//e.g. 5 -> 5.0 (at least one zero is needed)
		}
		//finally, force correct number of zeroes; add some if necessary   
		var halves = cropped.split(".");
		//grab numbers to the right of the decimal
		//compare digits in right half of string to digits wanted
		var zerosNeeded = digits-halves[1].length;
		//number of zeros to add
		for (var i = 1; i<=zerosNeeded; i++) {
			cropped += "0";
		}
		return (cropped);
	}
	*/
	}
