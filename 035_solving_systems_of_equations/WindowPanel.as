
class WindowPanel extends Panel {

	var d:Number;
	var myGraph:MovieClip;

	//settings movie clips
	var xMinSetting:MovieClip;
	var xMaxSetting:MovieClip;
	var xScaleSetting:MovieClip;
	var yMinSetting:MovieClip;
	var yMaxSetting:MovieClip;
	var yScaleSetting:MovieClip;
	var cb_show:MovieClip;

	var parent_mc:MovieClip;	//main module
	var panelFormat;
	

	function WindowPanel() {

		d = this.getNextHighestDepth();

		var headerFormat = new TextFormat();
		headerFormat.font = 'Arial';
		headerFormat.color = 0x333333;
		headerFormat.size = 12;
		headerFormat.align = 'center';

		//header text
		var headerLabel:TextField = this.createTextField("headerLabel", d++, 0, 5, 200, 20);
		headerLabel.setNewTextFormat(headerFormat);
		headerLabel.selectable = false;
		headerLabel.html = true;
		headerLabel.htmlText = "<b>Window</b>";
		
		//text (for checkbox label)
		panelFormat = new TextFormat();
		panelFormat.font = 'Arial';
		panelFormat.size = 12;
		
		//x settings

		xMaxSetting = this.attachMovie("PanelSetting", "xMaxSetting", d++, {_x:15, _y:30, w: 40, h:18, labelText:"<i>x</i> max: ", myDefault:10, id:"gridXmax" });

		xMinSetting = this.attachMovie("PanelSetting", "xMinSetting", d++, {_x:15, _y:60, w: 40, h:18, labelText:"<i>x</i> min: ", myDefault:-10, id:"gridXmin" });

		xScaleSetting = this.attachMovie("PanelSetting", "xMinSetting", d++, {_x:15, _y:90, w: 40, h:18, labelText:"scale: ", myDefault:1, id:"gridXscale" });

		//y settings

		yMaxSetting = this.attachMovie("PanelSetting", "yMaxSetting", d++, {_x:110, _y:30, w: 40, h:18, labelText:"<i>y</i> max: ", myDefault:10, id:"gridYmax" });

		yMinSetting = this.attachMovie("PanelSetting", "yMinSetting", d++, {_x:110, _y:60, w: 40, h:18, labelText:"<i>y</i> min: ", myDefault:-10, id:"gridYmin" });

		yScaleSetting = this.attachMovie("PanelSetting", "yMinSetting", d++, {_x:110, _y:90, w: 40, h:18, labelText:"scale: ", myDefault:1, id:"gridYscale" });
		
		//show regions in color
		
		var checkboxLabel:TextField = createStaticText(this, 41, 118, "Show regions in color");
		cb_show = this.attachMovie("checkbox", "cb_show", d++, {_x:25, _y:120});
		cb_show.b_ckbox.hit_area._width = checkboxLabel._width + 12;
		cb_show.onPress = function(){	
			var myGraph = this._parent.myGraph;
				myGraph.showRegionsInColor = !myGraph.showRegionsInColor;
				if(myGraph.showRegionsInColor){
					this.gotoAndStop(2);
				} else {
					this.gotoAndStop(1);
				}
				this._parent.parent_mc.updateGraph();
				this._parent.parent_mc.updateQuestionSettings();
			
		}
	}


	var limitMax = 100;
	var limitMin = -100;
	function updateSettings(id:String, txt:String) {
		//called when any setting is changed
		var newVal:Number = Number( txt );
		if (!isNaN( newVal )) {
			switch (id) {
				case "gridXmin":
					if (newVal >= limitMin && newVal <= myGraph.gridXmax) {	//min must be above the limitMin and below the current max
						myGraph.gridXmin = newVal;
					} else {
						xMinSetting.myInput.text = myGraph.gridXmin;	//revert
					}
				break;
				case "gridXmax":
					if (newVal <= limitMax && newVal >= myGraph.gridXmin) {	//max must be below the limitMax and above the current min
						myGraph.gridXmax = newVal;
					} else {
						xMaxSetting.myInput.text = myGraph.gridXmax;	//revert
					}
				break;
				case "gridXscale":
					if (newVal > 0 && newVal <= limitMax) {	//scale must be above 0 and below limitMax
						myGraph.gridXscale = newVal;
					} else {
						xScaleSetting.myInput.text = myGraph.gridXscale;	//revert
					}
				break;
				case "gridYmin":
					if (newVal >= limitMin && newVal <= myGraph.gridYmax) {	//same filters for y scale
						myGraph.gridYmin = newVal;
					} else {
						yMinSetting.myInput.text = myGraph.gridYmin;	//revert
					}
					
				break;
				case "gridYmax":
					if (newVal <= limitMax && newVal >= myGraph.gridYmin) {
						myGraph.gridYmax = newVal;
					} else {
						yMaxSetting.myInput.text = myGraph.gridYmax;	//revert
					}
				break;
				case "gridYscale":
					if (newVal > 0 && newVal <= limitMax) {
						myGraph.gridYscale = newVal;
					} else {
						yScaleSetting.myInput.text = myGraph.gridYscale;	//revert
					}
				
				break;
			}
			parent_mc.updateQuestionSettings();
			parent_mc.updateGraph();
		}
		
	}
	function createStaticText(mc:MovieClip, myX:Number, myY:Number, myText:String, myColor:Number, inst:String) {
		if (!inst) {
			inst = myText;
		}
		var newStaticText:TextField = mc.createTextField(inst, d++, myX, myY, 1, 1);
		newStaticText.html = true;
		newStaticText.multiline = true;
		newStaticText.htmlText = myText;
		newStaticText.selectable = false;
		newStaticText.autoSize = true;
		newStaticText.setTextFormat(panelFormat);
		newStaticText.textColor = myColor;
		return newStaticText;
	}

	function updateUIAfterReload(){
		
		xMinSetting.myInput.text = myGraph.gridXmin;
		xMaxSetting.myInput.text = myGraph.gridXmax;
		xScaleSetting.myInput.text = myGraph.gridXscale;
		yMinSetting.myInput.text = myGraph.gridYmin;
		yMaxSetting.myInput.text = myGraph.gridYmax;
		yScaleSetting.myInput.text = myGraph.gridYscale;
		if(myGraph.showRegionsInColor){
			cb_show.gotoAndStop(2);
		} else {
			cb_show.gotoAndStop(1);
		}
		
	}


}