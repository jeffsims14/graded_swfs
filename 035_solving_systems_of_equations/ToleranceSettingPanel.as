class ToleranceSettingPanel extends Panel {
	
	private var d:Number;
	var myGraph:MovieClip;
	
	//settings movie clip
	var intersectionPositionToleranceSetting:MovieClip;
	
	
	var parent_mc:MovieClip;	//main module
	var panelFormat;
	
	
	function ToleranceSettingPanel() {
		trace("option settings panel is being initialized");
		d = this.getNextHighestDepth();
		
		
		//text (for checkbox label)
		panelFormat = new TextFormat();
		panelFormat.font = 'Arial';
		panelFormat.size = 12;
		
		// settings
		intersectionPositionToleranceSetting = this.attachMovie("PanelSetting", "intersectionPositionToleranceSetting", d++, {_x:15, _y:30, labelText:"Solution Tolerance", myDefault: 50});
		var suffix:TextField = createStaticText(this, intersectionPositionToleranceSetting._x + intersectionPositionToleranceSetting._width + 2, 30, "%");
	}	
	
	function updateSettings(id:String, txt:String) {
		var limitMax = 500;
		var limitMin = 0;
		//called when any setting is changed
		var newVal:Number = Number( txt );
		if (!isNaN( newVal )) {
			if (newVal >= limitMin && newVal <= limitMax) {	//min must be above the limitMin and below the limitMax
				myGraph.intersectionPositionTolerance = newVal;
			} else {
				intersectionPositionToleranceSetting.myInput.text = myGraph.intersectionPositionTolerance;	//revert
			}
			
			parent_mc.updateGraphSettings();
			parent_mc.updateGraph();
		}
	
	}
	function createStaticText(mc:MovieClip, myX:Number, myY:Number, myText:String, myColor:Number, inst:String) {
	if (!inst) {
	inst = myText;
	}
	var newStaticText:TextField = mc.createTextField(inst, d++, myX, myY, 1, 1);
	newStaticText.html = true;
	newStaticText.multiline = true;
	newStaticText.htmlText = myText;
	newStaticText.selectable = false;
	newStaticText.autoSize = true;
	newStaticText.setTextFormat(panelFormat);
	newStaticText.textColor = myColor;
	return newStaticText;
	}
	
	function updateUIAfterReload(){
	
	intersectionPositionToleranceSetting.myInput.text = myGraph.intersectionPositionTolerance;
	
	}
	
	
}// ActionScript file// ActionScript file