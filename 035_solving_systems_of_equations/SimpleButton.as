class SimpleButton extends MovieClip {

	private var w:Number;			//width
	private var h:Number;			//height
	private var labelText:String;	//button label
	private var id:String;			//id is passed in setAction function
	private var _up:MovieClip;		//button state MCs
	private var _over:MovieClip;
	private var _down:MovieClip;
	private var _highlight:MovieClip;	//selected state
	private var _parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	private var buttonLabel:TextField;

	function SimpleButton() {
		//create button state MCs
		d = this.getNextHighestDepth();
		_up = this.createEmptyMovieClip('_up', d++);
		_over = this.createEmptyMovieClip('_over', d++);
		_down = this.createEmptyMovieClip('_down', d++);

		drawRoundedRectangle(_up, w + 2, h + 2, 10, 0xFFFFFF, 80, 0xCCCCCC, 2, 100);
		drawRoundedRectangle(_over, w + 2, h + 2, 10, 0xFFFFFF, 100, 0x336699, 2, 100);
		drawRoundedRectangle(_down, w + 2, h + 2, 10, 0xEEEEEE, 100, 0x336699, 2, 100);
		_over._alpha = 0;
		_down._alpha = 0;

		//selected highlight
		_highlight = this.createEmptyMovieClip('_highlight', d++);
		drawRoundedRectangle(_highlight, w + 2, h + 2, 10, 0xFFFFFF, 75, 0x336699, 2, 100);
		_highlight._alpha = 0;

		//default button actions
		this.onRollOver = function() {
			_up._alpha = 0;
			_over._alpha = 100;
			_down._alpha = 0;
		}
		this.onRollOut = function() {
			_up._alpha = 100;
			_over._alpha = 0;
			_down._alpha = 0;
		}
		this.onPress = function() {
			_up._alpha = 0;
			_over._alpha = 0;
			_down._alpha = 100;
		}
		this.onRelease = this.onReleaseOutside = function() {
			_up._alpha = 100;
			_over._alpha = 0;
			_down._alpha = 0;
		}

		//button label
		if (labelText!= undefined) {
			buttonLabel = this.createTextField("buttonLabel", d++, -1, 2, w, h);

			var panelFormat = new TextFormat();
			panelFormat.font = 'Arial';
			panelFormat.color = 0x000000;
			panelFormat.size = 12;
			panelFormat.align = 'center';

			buttonLabel.setNewTextFormat(panelFormat);
			buttonLabel.embedFonts = true;
			buttonLabel.selectable = false;
			buttonLabel.html = true;
			buttonLabel.htmlText = labelText;
		}
		//buttonLabel.autoSize = true;

		if (!_parent_mc) {
			_parent_mc = this._parent;
		}
	}

	//change button label
	public function setLabel( newLabel:String) {
		buttonLabel.htmlText = newLabel;
	}

	//set the onPress action in addition to default button states
	public function setAction ( clickAction:Function ) {
		this.onPress = function() {
			_up._alpha = 0;
			_over._alpha = 0;
			_down._alpha = 100;
			clickAction( this.id, _parent_mc );
			//this.highlighted = true;
		}
	}

	//make a simple icon button
	public function setIcon ( id:String ) {
		this.attachMovie(id, "myIcon", d++, {_x:6, _y:1});
		_up.clear();
		_over.clear();
		_down.clear();
		_highlight.clear();
		drawRoundedRectangle(_up, w + 2, h + 2, 10, 0xEEEEEE, 0);
		drawRoundedRectangle(_over, w + 2, h + 2, 10, 0xFFFFFF, 80);
		drawRoundedRectangle(_down, w + 2, h + 2, 10, 0xEEEEEE, 50);
	}

	//turn highlight on or off
	public function set highlighted( hl:Boolean ) {
		if (hl) {
			_highlight._alpha = 100;
		} else {
			_highlight._alpha = 0;
		}
	}

	public function drawRoundedRectangle(target_mc:MovieClip, boxWidth:Number, boxHeight:Number, cornerRadius:Number, fillColor:Number, fillAlpha:Number, strokeColor:Number, stroke:Number, strokeAlpha:Number):Void {
	    with (target_mc) {
	        beginFill(fillColor, fillAlpha);
	        if (strokeColor != undefined) {
	        	if (stroke == undefined) {
	        		stroke = 1;
	        	}
	        	if (strokeAlpha == undefined) {
	        		strokeAlpha = 100;
	        	}
	        	lineStyle(stroke, strokeColor, strokeAlpha);
	        }
	        moveTo(cornerRadius, 0);
	        lineTo(boxWidth - cornerRadius, 0);
	        curveTo(boxWidth, 0, boxWidth, cornerRadius);
	        lineTo(boxWidth, cornerRadius);
	        lineTo(boxWidth, boxHeight - cornerRadius);
	        curveTo(boxWidth, boxHeight, boxWidth - cornerRadius, boxHeight);
	        lineTo(boxWidth - cornerRadius, boxHeight);
	        lineTo(cornerRadius, boxHeight);
	        curveTo(0, boxHeight, 0, boxHeight - cornerRadius);
	        lineTo(0, boxHeight - cornerRadius);
	        lineTo(0, cornerRadius);
	        curveTo(0, 0, cornerRadius, 0);
	        lineTo(cornerRadius, 0);
	        endFill();
    	}
	}
}