class DropDownMenu extends MovieClip {

	var dropDownMenu:MovieClip;		//dropdown menu movie clip
	var myChoice:Number;			//numeric value of chosen menu item
	var defaultChoice:Number;		//item to show when first loaded
	var myID:String;				//unique ID for each dropdown
	var my_parent:MovieClip;		//parent movie clip

	function DropDownMenu() {
		my_parent = _parent;
	}

	function updateChoice() {

		//numeric value of menu choice
		myChoice = dropDownMenu.myChoice;
		
		//update the parent (Equation Panel) with new choice
		my_parent.updateMenu(myID, myChoice);
	}

}