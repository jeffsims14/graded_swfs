

class EquationsGrading extends MovieClip {
	var gradeable_id:String;
	var focused:Boolean;
	var directions:MovieClip;
	var EquationsModuleSwf:MovieClip;	//Instance Name of the graded module's MovieClip. This is usually the MovieClip already on Stage.
	var tabType:String;
	
	function EquationsGrading() {
	}
	
	/***** REQUIRED FOR SIMPLE AND COMPLEX SWF GRADING *******/
	/********* 
		this function will receive the exact string/number/object or whatever was sent from the getGradeableData() function
	    it will be called once for each response that was authored by the user until a match is found
		for simple graded swf's -> return true/false based on whether what the student has currently entered in the module is correct
		for complex graded swf's -> return true/false based on whether what the student has currently entered in the module matches the response sent in (not necessarily whether it is right or wrong)
	**********/	


	function checkAnswer($authored_object) {
		var studentObject:MovieClip = this.EquationsModuleSwf;
		var myLineA:Line = studentObject.lineA;
		var myLineB:Line = studentObject.lineB;
		var myIntersection:Point = studentObject.intersection;
		//var mySelectedRegion = studentObject.selectedRegion;	
		var match:Boolean = true;
		var myGraphView:GraphView = studentObject.myGraphView;

		//check here if student response matches authored response
		//if not, match becomes false
		if($authored_object.graphSettings.wildcardOptions.gradeIntersection){
			var distanceInPixels = myGraphView.distanceBetweenTwoPointsInPixels($authored_object.myGraphView.intersectionPoint, myGraphView.intersectionPoint);
			studentObject.debug.text += "distance in pixels is " + distanceInPixels + "\n";
			var toleranceInPixels = ($authored_object.myGraphView.intersectionPositionTolerance/100 + 1) * myGraphView.dotSize;		
			studentObject.debug.text += "tolerance in pixels is " + toleranceInPixels + "\n";
			if(distanceInPixels > toleranceInPixels){
				match = false;
			}
		}
		
		if($authored_object.graphSettings.wildcardOptions.gradeEquationLineA){
			if($authored_object.graphSettings.lineA.prefix == "x"){
				var lineAXValueDiff:Number = Math.round(Math.abs(Number($authored_object.graphSettings.lineA.xValue) - Number(myLineA.xValue)) * 100)/100;
				if(lineAXValueDiff > 0){				
					match = false;
				}
			} else {

				var lineASlopeDiff:Number = Math.round(Math.abs(Number($authored_object.graphSettings.lineA.slope) - Number(myLineA.slope)) * 100)/100;
				var lineAInterceptDiff:Number = Math.round(Math.abs(Number($authored_object.graphSettings.lineA.intercept) - Number(myLineA.intercept)) * 100)/100;
				if(lineASlopeDiff > 0 || lineAInterceptDiff > 0){
					match = false;
				}
			}
		}
		if($authored_object.graphSettings.wildcardOptions.gradePrefixLineA){
			if ($authored_object.graphSettings.lineA.prefix != myLineA.prefix){
				match = false;
			}
		}

		if($authored_object.graphSettings.wildcardOptions.gradeEquationLineB){
	
			if($authored_object.graphSettings.lineB.prefix == "x"){
				var lineBXValueDiff:Number = Math.round(Math.abs(Number($authored_object.graphSettings.lineB.xValue) - Number(myLineB.xValue)) * 100)/100;
				if(lineBXValueDiff > 0){				
					match = false;
				}
			} else {
				var lineBSlopeDiff:Number = Math.round(Math.abs(Number($authored_object.graphSettings.lineB.slope) - Number(myLineB.slope)) * 100)/100;
				var lineBInterceptDiff:Number = Math.round(Math.abs(Number($authored_object.graphSettings.lineB.intercept) - Number(myLineB.intercept)) * 100)/100;
				if(lineBSlopeDiff > 0 || lineBInterceptDiff > 0){
					match = false;
				}
			}
		}
		if($authored_object.graphSettings.wildcardOptions.gradePrefixLineB){
			if ($authored_object.graphSettings.lineB.prefix != myLineB.prefix){
				match = false;
			}
		}
		
		return match;
		
	}
	/********
	this function is called when the user clicks on this module to edit it
	include registering of all key listeners and mouse listeners as well as enabling buttons, etc.
	********/
	function setFocus() {
		this.directions._visible = true;
		this.focused = true;
	}
	/*******
	this function is called when the user clicks on a different module or on the stage and thus kills focus on this module
	include removal of all key listeners and mouse listeners as well as disabling any buttons, etc.
	*******/
	function killFocus() {
		this.focused = false;
		this.directions._visible = false;
	}
	/***** REQUIRED FUNCTIONS FOR COMPLEX SWF GRADING *******/
	/*********
	this function is called each time the user switches between tabs so that the current gradeable data will be stored
	return any object/array/number/string or combination of the above
	the object should be as simple as possible but must contain all information required for uniquely grading different responses
	**********/
	function getGradeableData() {
		var rtn:Object = new Object();

		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)

		//get settings constant across whole question
		if (tabType == "question") {
			rtn.questionSettings = this.EquationsModuleSwf.questionSettings;
		}

		//get other objects that can be saved differently on each tab
		rtn.graphSettings = this.EquationsModuleSwf.graphSettings;

		return rtn;
	}
	/**********
	this function is called each time the user switches to a response tab so that the gradeable data for that tab will be displayed
	the incoming parameter will match exactly the format of whatever is returned from the getGradeableData function
	at this point the module should be modified to redisplay exactly what would have been present when the user last viewed the tab with the associated gradeableData
	***********/
	var isSetup:Boolean = false;
	function setGradeableData($v) {

		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)

		//set settings constant across whole question, or when module is loaded for the first time
		if (tabType == "question" or isSetup==false) {
			
			this.EquationsModuleSwf.questionSettings = $v.questionSettings;	//recall any saved general settings
			
			isSetup = true;
		}

		//recall any saved settings for other objects
		this.EquationsModuleSwf.graphSettings = $v.graphSettings;
		
		//reload the module with saved settings
		this.EquationsModuleSwf.reload();
	}
}
