class EquationPanel extends Panel {

	var inputFormat:TextFormat;
	var d:Number;
	var numberOfInequalities:Number = 1;;

	//entry MC's
	var xyDropdownA:MovieClip;
	var equDropdownA:MovieClip;
	var equationDisplayA:TextField;

	var xyDropdownB:MovieClip;
	var equDropdownB:MovieClip;
	var equationDisplayB:TextField;

	var addInequalityButton:MovieClip;

	var yInputRestrict:String = "0-9 \u2013 \\-\\.\\x\\+\\/";	//allowed text entry for y entry field;
	var xInputRestrict:String = "0-9 \u2013 \\-\\.\\/";	//allowed text entry for x entry field;

	var xyChoices:Array = ['x', 'y'];	//String value of menu choices for xy drop down menu
	var inequalityChoices:Array = ['lt','gt','lte','gte'];	//String value of menu choices for inequality drop down menu (lt = less than, lte = less than or equal to, etc.)

	var parent_mc:MovieClip;	//main module

	function EquationPanel() {
		d = this.getNextHighestDepth();

		//input equation format
		inputFormat = new TextFormat();
		inputFormat.font = 'Times';
		inputFormat.color = 0x000000;
		inputFormat.size = 18;
		inputFormat.align = 'left';
		inputFormat.indent = 6;

		//add or delete inequality button
		addInequalityButton = this.attachMovie("SimpleButton", "addInequalityButton", d++, {w:120, h:20, _x:62, _y:95, id:"toggle", labelText:"Remove inequality"});
		addInequalityButton.setAction( toggleInequality );

		var yPositionA = 4;
		var yPositionB = 50;
		var scale = 85;
		
		//inequality entry A
		xyDropdownA = this.attachMovie("DropDownMenuItal", "xyDropdownA", d+5, {_x:5, _y:yPositionA, menuItems : ['x', 'y'], defaultChoice:1, _xscale:scale, _yscale:scale, myID:'xyA' });
		equDropdownA = this.attachMovie("DropDownMenu", "equDropdownA", d+6, {_x:52, _y:yPositionA, menuItems : ['&#60;', '&#62;', '&#8804;', '&#8805;'], defaultChoice:0, _xscale:scale, _yscale:scale, myID:'eqA' });
		equationDisplayA = createInputField(this, inputFormat, '', 102, yPositionA + 2, 135, 22);

		equationDisplayA.onChanged = function() {
			_parent.updateEquation(this, 'A');
		}

		//inequality entry B
		xyDropdownB = this.attachMovie("DropDownMenuItal", "xyDropdownB", d++, {_x:5, _y:yPositionB, menuItems : ['x', 'y'], defaultChoice:1, _xscale:scale, _yscale:scale, myID:'xyB' });
		equDropdownB = this.attachMovie("DropDownMenu", "equDropdownB", d++, {_x:52, _y:yPositionB, menuItems : ['&#60;', '&#62;', '&#8804;', '&#8805;'], defaultChoice:0, _xscale:scale, _yscale:scale, myID:'eqB' });
		equationDisplayB = createInputField(this, inputFormat, '', 102, yPositionB + 2, 135, 22);

		equationDisplayB.onChanged = function() {
			_parent.updateEquation(this, 'B');
		}

		hideEntryB();

	}

	//called when 1st menu is changed between x and y
	function setEquationEntry(id:String, prefix:String) {
		var equationDisplay;	//entry field corresponding to this menu
		var myLine;
		switch (id) {
			case "xyA":	//the xy menu for equ. A

			equationDisplay = equationDisplayA;	//set corresponding equ. display and Line to A
			myLine = parent_mc.lineA;

			break;
			case "xyB":	//the xy menu for equ. B

			equationDisplay = equationDisplayB;	//set equ. display and Line to B
			myLine = parent_mc.lineB;

			break;
		}

		switch (prefix) {
			case "x":	//when menu is set to 'x'

			equationDisplay.restrict = xInputRestrict;	//only numbers are allowed
			equationDisplay.htmlText = myLine.xString;	//display previously saved text, if any

			break;

			case "y":	//when menu set to 'y'

			equationDisplay.restrict = yInputRestrict;	//numbers, + sign, and x are allowed
			equationDisplay.htmlText = myLine.yString;	//display previously saved text, if any

			break;
		}
	}

	//called when any drop down menu is updated, either choosing x or y in the 1st menu, or choosing the inequality for the 2nd menu
	function updateMenu(id:String, myChoice:Number) {
		switch (id) {
			case "xyA":
			if (parent_mc.lineA.prefix != xyChoices[myChoice]) {	//if xy menu is changed, update saved prefix and update entry field
				parent_mc.lineA.prefix = xyChoices[myChoice];
				setEquationEntry(id, xyChoices[myChoice]);
			}
			break;
			case "xyB":
			if (parent_mc.lineB.prefix != xyChoices[myChoice]) {
				parent_mc.lineB.prefix = xyChoices[myChoice];
				setEquationEntry(id, xyChoices[myChoice]);
			}
			break;
			case "eqA":
			parent_mc.lineA.inequality = inequalityChoices[myChoice];
			break;
			case "eqB":
			parent_mc.lineB.inequality = inequalityChoices[myChoice];
			break;
		}
		parent_mc.updateGraph();
	}

	//called when the text field for equation entry is changed
	function updateEquation(equationDisplay:MovieClip, id:String) {

		var newVal:String = equationDisplay.text;

		var myEquObject = parseEquationEntry(newVal);	//parse values out of entry text

		var myLine;

		switch (id) {
			case "A":	//entry A
			myLine = parent_mc.lineA;
			break;
			case "B":	//entry B
			myLine = parent_mc.lineB;
			break;
		}
		if (myLine.prefix == "y") {
			myLine.slope = myEquObject.slope;
			myLine.intercept = myEquObject.intercept;
		} else {
			myLine.xValue = myEquObject.intercept;
		}

		parent_mc.updateGraph();

		//replace minus signs with en dashes in display
		if (newVal.indexOf('-') > -1) {
			newVal = stringReplace( String(newVal),'-','\u2013');
			equationDisplay.htmlText = newVal;
		}
		//italicize x's in display
		if (newVal.indexOf('x') > -1) {
			newVal = stringReplace( String(newVal),'x','<i>x</i>');
			equationDisplay.htmlText = newVal;
		}

		//save entry strings for reloading
		if (myLine.prefix == "y") {
			myLine.yString = newVal;
		} else if (myLine.prefix == "x") {
			myLine.xString = newVal;
		}
	}

	//parse values from equation entry
	function parseEquationEntry(entryText:String):Object {
		var equationObject = new Object;
		var slope:Number;
		var intercept:Number;
		var extraStuff:String;

		//replace en dashes with negative sign for math parsing
		if (entryText.indexOf('\u2013') > -1) {
			entryText = stringReplace( String(entryText),'\u2013','-');
		}

		//remove spaces
		var eString:String = stringReplace(entryText, " ", "");

		//find all operators
		var operatorIndex:Array = [0];	//list of all operators to find separation points, always begin with start of entry
		for (var i=1; i<eString.length; i++) {
			if (eString.charAt(i) == '+' || eString.charAt(i) == '-') {
				operatorIndex.push(i);
			}
		}

		var addendArray:Array = [];	//list of each segment between operators
		var startIndex:Number, subLength:Number, segment:String;
		
		for (var o=0; o<operatorIndex.length; o++) {
			startIndex = operatorIndex[o];

			if (operatorIndex[o+1] == undefined) {
				segment = eString.substr(startIndex);				//last operator, include everything following
			} else {
				subLength = operatorIndex[o+1] - startIndex;
				segment = eString.substr(startIndex, subLength);	//section between this and next operator
			}
			addendArray.push( segment );
		}
		
		//trace (addendArray.join(' | '));

		//find segment containing x, this is slope ( m * x + ... )
		var slopeId:Number, interceptId:Number;
		for (var i=0; i<addendArray.length; i++) {
			if (addendArray[i].indexOf('x') > -1) {
				if (slopeId == undefined) {
					slopeId = i;
				}
			} else if (interceptId == undefined) {
				interceptId = i;
			}
		}
		if (slopeId == undefined && addendArray.length == 1) {
			slope = 0;
		} else {
			var slopeAddend = addendArray[slopeId];					//string containing x
			slopeAddend = stringReplace( slopeAddend,'x','');		//remove x
			if (slopeAddend == '') {
				slope = 1;
			} else if (slopeAddend == '-') {
				slope = -1;
			} else {
				if (slopeAddend.indexOf('/') > -1) {
					var numerator = slopeAddend.substr(0, slopeAddend.indexOf('/'));
					var denominator = slopeAddend.substr(slopeAddend.indexOf('/')+1);
					slope = Number ( numerator / denominator );
				} else {
					slope = Number(slopeAddend);
				}
			}
		}

		//find segment without x, this is y-intercept ( ... + b )
		if (interceptId == undefined) {
			intercept = 0;
		} else {
			var interceptAddend = addendArray[interceptId];
			if (interceptAddend.indexOf('/') > -1) {
				var numeratorI = interceptAddend.substr(0, interceptAddend.indexOf('/'));
				var denominatorI = interceptAddend.substr(interceptAddend.indexOf('/')+1);
				intercept = Number( numeratorI / denominatorI);
			} else {
				intercept = Number(interceptAddend);
			}
			
		}

		//anything not intercept or slope is extra stuff
		extraStuff = "";
		for (var i=0; i<addendArray.length; i++) {
			if (i != slopeId && i != interceptId) {
				extraStuff += addendArray[i];
			}
		}
		//look for extra x's
		var xCount:Number = 0;
		for (var i=0; i<eString.length; i++) {
			if (eString.charAt(i) == 'x') {
				xCount++;
				if (xCount > 1) {
					extraStuff += 'x';
				}
			}
		}
		
		equationObject.slope = slope;
		equationObject.intercept = intercept;
		equationObject.extraStuff = extraStuff;

		//trace ('slope = ' + slope);
		//trace ('intercept = ' + intercept);
		//trace ('extra stuff = ' + extraStuff);

		return equationObject;
	}

	function showEntryB() {
		xyDropdownB._visible = true;
		equDropdownB._visible = true;
		equationDisplayB._visible = true;
		addInequalityButton.setLabel("Remove inequality");
		this.myBG._height = 120;
		addInequalityButton._y = 90;
	}

	function hideEntryB() {
		xyDropdownB._visible = false;
		equDropdownB._visible = false;
		equationDisplayB._visible = false;
		addInequalityButton.setLabel("Add inequality");
		this.myBG._height = 80;
		addInequalityButton._y = 50;
	}

	function toggleInequality(id, myParent) {
		if (myParent.numberOfInequalities == 1) {
			myParent.numberOfInequalities++;
			myParent.showEntryB();
		} else {
			myParent.numberOfInequalities = 1;
			myParent.hideEntryB();
		}
		//update graph
		myParent._parent._parent.updateGraph();
	}

	function stringReplace(str:String, searchStr:String, replaceStr:String):String {
		return str.split(searchStr).join(replaceStr);
	}

}