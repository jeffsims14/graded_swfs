class WindowPanel extends Panel {

	var d:Number;
	var myGraph:MovieClip;

	//settings movie clips
	var xMinSetting:MovieClip;
	var xMaxSetting:MovieClip;
	var xScaleSetting:MovieClip;
	var yMinSetting:MovieClip;
	var yMaxSetting:MovieClip;
	var yScaleSetting:MovieClip;
	var unitSetting:MovieClip;

	var radioGroup:Array;
	var radioButtonA:MovieClip;
	var radioButtonB:MovieClip;
	var radioButtonC:MovieClip;
	var cb_lockParentFunction:MovieClip;

	var parent_mc:MovieClip;	//main module
	var headerFormat, panelFormat;
	var label4;
	

	function WindowPanel() {

		d = this.getNextHighestDepth();

		headerFormat = new TextFormat();
		headerFormat.font = 'Arial';
		headerFormat.color = 0x333333;
		headerFormat.size = 12;
		headerFormat.align = 'center';

		//header text
		createHeader("<b>Window</b>", 0, 5);

		//checkbox label format
		panelFormat = new TextFormat();
		panelFormat.font = 'Arial';
		panelFormat.size = 12;
		
		//x settings

		xMaxSetting = this.attachMovie("PanelSetting", "xMaxSetting", d++, {_x:15, _y:30, w: 40, h:18, labelText:"<i>x</i> max: ", myDefault:parent_mc.myGraphView.gridXmax, id:"gridXmax" });

		xMinSetting = this.attachMovie("PanelSetting", "xMinSetting", d++, {_x:15, _y:60, w: 40, h:18, labelText:"<i>x</i> min: ", myDefault:parent_mc.myGraphView.gridXmin, id:"gridXmin" });

		xScaleSetting = this.attachMovie("PanelSetting", "xScaleSetting", d++, {_x:15, _y:90, w: 40, h:18, labelText:"scale: ", myDefault:parent_mc.myGraphView.gridXscale, id:"gridXscale" });

		//y settings

		yMaxSetting = this.attachMovie("PanelSetting", "yMaxSetting", d++, {_x:110, _y:30, w: 40, h:18, labelText:"<i>y</i> max: ", myDefault:parent_mc.myGraphView.gridYmax, id:"gridYmax" });

		yMinSetting = this.attachMovie("PanelSetting", "yMinSetting", d++, {_x:110, _y:60, w: 40, h:18, labelText:"<i>y</i> min: ", myDefault:parent_mc.myGraphView.gridYmin, id:"gridYmin" });

		yScaleSetting = this.attachMovie("PanelSetting", "yScaleSetting", d++, {_x:110, _y:90, w: 40, h:18, labelText:"scale: ", myDefault:parent_mc.myGraphView.gridYscale, id:"gridYscale" });
		
		//units, i.e. the amount of translate shift with each arrow click
		
		unitSetting = this.attachMovie("PanelSetting", "unitSetting", d++, {_x:15, _y:120, w: 120, h:18, labelText:"shift units (<i>x</i> and <i>y</i>): ", myDefault:parent_mc.shiftUnits, id:"units" });


		//Settings for student modes: Normal (points and function), points only, function only

		createHeader("<b>Student Mode</b>", 0, 150);

		var currX = 35, currY = 170;
		var label1:TextField = createStaticText(this, currX, currY, "Function and Points");
		radioButtonA = this.attachMovie("RadioButton", "radioButtonA", d++, {_x:currX - 14, _y:currY +2, w:label1._width});
		

		var label2:TextField = createStaticText(this, currX, currY+=18, "Function only");
		radioButtonB = this.attachMovie("RadioButton", "radioButtonB", d++, {_x:currX - 14, _y:currY +2, w:label2._width});
		

		var label3:TextField = createStaticText(this, currX, currY+=18, "Points only");
		radioButtonC = this.attachMovie("RadioButton", "radioButtonC", d++, {_x:currX - 14, _y:currY +2, w:label3._width});

		radioGroup = [radioButtonA, radioButtonB, radioButtonC];

		var radioIDs = ["normal", "function", "points"];
		for (var r in radioGroup) {
			radioGroup[r].init(radioGroup, radioIDs[r]);
		}
		

		//option to disable Parent Function menu and transform options

		label4 = createStaticText(this, currX, currY+=32, "Lock parent function menu");
		cb_lockParentFunction = this.attachMovie("checkbox", "cb_pointsEdit", d++, {_x:currX - 14, _y:currY +2});
		cb_lockParentFunction.b_ckbox.hit_area._width = label4._width + 12;
		cb_lockParentFunction.onPress = function() {
			_parent.toggleParentFunctionLock();
		}
		
	}

	private function createHeader(str, x, y) {
		var headerLabel:TextField = this.createTextField("label"+str, d++, x, y, 200, 20);
		headerLabel.setNewTextFormat(headerFormat);
		headerLabel.selectable = false;
		headerLabel.html = true;
		headerLabel.htmlText = str;
	}


	var limitMax = 100;
	var limitMin = -100;
	function updateSettings(id:String, txt:String) {
		trace (id)
		//called when any setting is changed
		var newVal:Number = Number( txt );
		if (!isNaN( newVal )) {
			switch (id) {
				case "gridXmin":
					if (newVal >= limitMin && newVal <= myGraph.gridXmax) {	//min must be above the limitMin and below the current max
						myGraph.gridXmin = newVal;
					} else {
						xMinSetting.myInput.text = myGraph.gridXmin;	//revert
					}
				break;
				case "gridXmax":
					if (newVal <= limitMax && newVal >= myGraph.gridXmin) {	//max must be below the limitMax and above the current min
						myGraph.gridXmax = newVal;
					} else {
						xMaxSetting.myInput.text = myGraph.gridXmax;	//revert
					}
				break;
				case "gridXscale":
					if (newVal > 0 && newVal <= limitMax) {	//scale must be above 0 and below limitMax
						myGraph.gridXscale = newVal;
					} else {
						xScaleSetting.myInput.text = myGraph.gridXscale;	//revert
					}
				break;
				case "gridYmin":
					if (newVal >= limitMin && newVal <= myGraph.gridYmax) {	//same filters for y scale
						myGraph.gridYmin = newVal;
					} else {
						yMinSetting.myInput.text = myGraph.gridYmin;	//revert
					}
					
				break;
				case "gridYmax":
					if (newVal <= limitMax && newVal >= myGraph.gridYmin) {
						myGraph.gridYmax = newVal;
					} else {
						yMaxSetting.myInput.text = myGraph.gridYmax;	//revert
					}
				break;
				case "gridYscale":
					if (newVal > 0 && newVal <= limitMax) {
						myGraph.gridYscale = newVal;
					} else {
						yScaleSetting.myInput.text = myGraph.gridYscale;	//revert
					}
				
				break;
				case "units":
					if (newVal > 0 && newVal <= limitMax) {
						parent_mc.shiftUnits = newVal;
					} else {
						unitSetting.myInput.text = parent_mc.shiftUnits;	//revert
					}
					break;
			}
			parent_mc.updateQuestionSettings();
			parent_mc.updateGraph();
		}
		
	}
	public function toggleRadioGroup(id) {
		switch (id) {
			case "normal":
				parent_mc.allowPointEditing = true;
				parent_mc.allowFunctionEditing = true;
				enableCheckbox();
			break;
			case "function":
				parent_mc.allowPointEditing = false;
				parent_mc.allowFunctionEditing = true;
				enableCheckbox();
			break;
			case "points":
				parent_mc.allowPointEditing = true;
				parent_mc.allowFunctionEditing = false;
				disableCheckbox();
			break;
		}
		parent_mc.updateQuestionSettings();
	}

	public function toggleParentFunctionLock() {
		if (parent_mc.parentFunctionLocked) {
			parent_mc.parentFunctionLocked = false;
			cb_lockParentFunction.gotoAndStop(1);
		} else {
			parent_mc.parentFunctionLocked = true;
			cb_lockParentFunction.gotoAndStop(2);
		}
		parent_mc.updateQuestionSettings();
	}
	
	public function disableCheckbox() {
		cb_lockParentFunction._alpha = 50;
		cb_lockParentFunction.onPress = undefined;
		label4._alpha = 50;
	}
	public function enableCheckbox() {
		cb_lockParentFunction._alpha = 100;
		cb_lockParentFunction.onPress = function() {
			_parent.toggleParentFunctionLock();
		}
		label4._alpha = 100;
	}
	public function updateDisplay() {
		xMinSetting.myInput.text = myGraph.gridXmin;
		xMaxSetting.myInput.text = myGraph.gridXmax;
		xScaleSetting.myInput.text = myGraph.gridXscale;
		yMinSetting.myInput.text = myGraph.gridYmin;
		yMaxSetting.myInput.text = myGraph.gridYmax;
		yScaleSetting.myInput.text = myGraph.gridYscale;
		unitSetting.myInput.text = parent_mc.shiftUnits;
		if (!parent_mc.allowPointEditing) {
			radioButtonB.highlighted = true;
			enableCheckbox();
		} else if (!parent_mc.allowFunctionEditing) {
			radioButtonC.highlighted = true;
			disableCheckbox();
		} else {
			radioButtonA.highlighted = true;
			enableCheckbox();
		}
		if (parent_mc.parentFunctionLocked) {
			cb_lockParentFunction.gotoAndStop(2);
		}
	}

	////////////////////////////////// CREATE STATIC TEXT FIELD
	function createStaticText(mc:MovieClip, myX:Number, myY:Number, myText:String, myColor:Number, inst:String) {
		if (!inst) {
			inst = myText;
		}
		var newStaticText:TextField = mc.createTextField(inst, d++, myX, myY, 1, 1);
		newStaticText.html = true;
		newStaticText.multiline = true;
		newStaticText.htmlText = myText;
		newStaticText.selectable = false;
		newStaticText.autoSize = true;
		newStaticText.setTextFormat(panelFormat);
		newStaticText.textColor = myColor;
		return newStaticText;
	}

}