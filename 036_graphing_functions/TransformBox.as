class TransformBox extends MovieClip {
	var box:MovieClip;
	var boxW:Number = 150;
	var boxH:Number = 150;
	var zeroX:Number;
	var zeroY:Number;
	var anchorW:Number = 8;
	var anchorXA:MovieClip;
	var anchorXB:MovieClip;
	var anchorYA:MovieClip;
	var anchorYB:MovieClip;
	var anchorColor:Number = 0x336699;
	
	function TransformBox(myGraph:GraphView, d:Number) {
		box = myGraph.createEmptyMovieClip("box", d++, {_x:myGraph._x, _y:myGraph._y});
		zeroX = myGraph.x_axis_zero;
		zeroY = myGraph.y_axis_zero;
		drawBox();
		anchorXA = createAnchor("anchorXA", d++, zeroX - boxW/2, zeroY, this);
		anchorXB = createAnchor("anchorXB", d++, zeroX + boxW/2, zeroY, this);
		anchorYA = createAnchor("anchorYA", d++, zeroX, zeroY - boxH/2, this);
		anchorYB = createAnchor("anchorYB", d++, zeroX, zeroY + boxH/2, this);
	}
	
	function drawBox() {
		box.clear();
		box.lineStyle(2, anchorColor, 80);
		box.beginFill(0xFFFFFF, 0);
		box.moveTo(zeroX - boxW/2, zeroY - boxH/2);
		box.lineTo(zeroX + boxW/2, zeroY - boxH/2);
		box.lineTo(zeroX + boxW/2, zeroY + boxH/2);
		box.lineTo(zeroX - boxW/2, zeroY + boxH/2);
		box.lineTo(zeroX - boxW/2, zeroY - boxH/2);
	}
	
	function createAnchor(instance:String, d:Number, x:Number, y:Number, click_ref:MovieClip) {
		var newAnchor:MovieClip = box.createEmptyMovieClip(instance, d);
		newAnchor.beginFill(anchorColor, 100);
		newAnchor.lineStyle(1, anchorColor, 100);
		newAnchor.moveTo(x - anchorW, y - anchorW);
		newAnchor.lineTo(x + anchorW, y - anchorW);
		newAnchor.lineTo(x + anchorW, y + anchorW);
		newAnchor.lineTo(x - anchorW, y + anchorW);
		newAnchor.lineTo(x - anchorW, y - anchorW);
		newAnchor._alpha = 50;
		newAnchor.onRollOver = function() {
			this._alpha = 100;
		}
		newAnchor.onRollOut = function() {
			this._alpha = 50;
		}
		newAnchor.onPress = function() {
			click_ref.adjustTransform(this, click_ref);
		}
		newAnchor.onRelease = newAnchor.onReleaseOutside = function() {
			this._alpha = 50;
			this.onMouseMove = undefined;
		}
		return newAnchor;
	}
	
	function adjustTransform(anchor_mc:MovieClip, target_mc:MovieClip) {
		switch (anchor_mc._name) {
			case "anchorXA":
				//moving left side
				anchor_mc.onMouseMove = function() {
					trace (this._xmouse +','+ this._ymouse);
				}
				break;
			case "anchorXB":
				//moving right side
				
				break;
			case "anchorYA":
				//moving top side
				
				break;
			case "anchorYB":
				//moving bottom side
				
				break;
			
		}
	}
}