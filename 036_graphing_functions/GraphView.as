class GraphView extends MovieClip {

	var mc_depth:Number;					//movie clip depth

	//graph default settings
	var graphWidth:Number = 300;
	var graphHeight:Number = 300;
	var gridXmin:Number = -10;
	var gridXmax:Number = 10;
	var gridXscale:Number = 1;
	var gridYmin:Number = -10;
	var gridYmax:Number = 10;
	var gridYscale:Number = 1;

	//factors used to convert graph values to pixel values -- calculated when axis is drawn
	var x_pixels_per_unit:Number;		//pixels per unit on x axis
	var y_pixels_per_unit:Number;		//pixels per unit on y axis
	var x_axis_zero:Number;				//the pixel value where x=0 (x position of y-axis)
	var y_axis_zero:Number;				//the pixel value where y=0 (y position of x-axis)

	//line stroke and colors
	var gridStroke:Number = 1;
	var gridColor:Number = 0xCCCCCC;
	var axisStroke = 1;
	var axisColor = 0x000000;
	var plotColor:Number = 0xFF0000;
	var plotStroke:Number = 3;	//plot line settings, red line
	var dotSize:Number = 5, dotStroke:Number = 1, dotColor:Number = 0xFF0000;

	//movie clips
	var parent_mc:MovieClip;	//main module
	var grid:MovieClip;
	var axis:MovieClip;
	var plot:MovieClip;
	var points:MovieClip;
	var gridLabelFormat:TextFormat;

	//tick mark and label settings
	var buffer_x:Number = 4;				//space between axis and grid labels for x-axis
	var buffer_y:Number = 5;				//and for y-axis
	var tickMarkLength:Number = 4;
	var oldLabelNumX:Number;
	var oldLabelNumY:Number;
	var calcVal:Number;
	var displayVal:String;
	var labelMC:MovieClip;

	function GraphView() {
		mc_depth = this.getNextHighestDepth();

		//white background fill
		var myBG:MovieClip = this.createEmptyMovieClip("myBG", mc_depth++);
		myBG.beginFill(0xFFFFFF, 100);
		myBG.lineStyle(gridStroke, gridColor, 100, true);
		myBG.moveTo(0, 0);
		myBG.lineTo(0, graphHeight);
		myBG.lineTo(graphWidth, graphHeight);
		myBG.lineTo(graphWidth, 0);
		myBG.lineTo(0,0);
		myBG.endFill();

		myBG.onPress = function() {
			_parent.clickGraph();
		}
		//masking for plot
		var graphMask:MovieClip = this.createEmptyMovieClip("graphMask", mc_depth++);
		graphMask.beginFill(0xFFFFFF, 0);
		graphMask.lineStyle(gridStroke, gridColor, 100, true);
		graphMask.moveTo(0, 0);
		graphMask.lineTo(0, graphHeight);
		graphMask.lineTo(graphWidth, graphHeight);
		graphMask.lineTo(graphWidth, 0);
		graphMask.lineTo(0,0);
		graphMask.endFill();

		//grid and axis setup
		grid = this.createEmptyMovieClip("grid", mc_depth++);
		axis = this.createEmptyMovieClip("axis", mc_depth++);
		plot = this.createEmptyMovieClip("plot", mc_depth++);
		points = this.createEmptyMovieClip("points", mc_depth++);

		plot.setMask(graphMask);	//mask plot to hide lines outside of view area

		gridLabelFormat = new TextFormat();			//format for grid labels on each axis
		gridLabelFormat.font = 'Arial';
		gridLabelFormat.size = 11;

		drawGridAndAxis();
		drawFunction();
		
		//var myTransform:TransformBox = new TransformBox(this, mc_depth++);
	}

	function clickGraph() {
		var drawMode:String = parent_mc.drawMode;
		switch (drawMode) {
			case "point":
				addPoint();
				break;
			case "function":
				//beginDrawLine();
				break;
		}
	}

	function addPoint() {
		var myPoints:Array = parent_mc.myPoints;	//pointer to the myPoints Array

		//get coordinates for area clicked
		var graphXY:Point = mouseToGraphXY(_xmouse, _ymouse);
		
		var pointExists = false;
		//check if point already exists
		for (var p in myPoints) {
			if (myPoints[p].x == graphXY.x && myPoints[p].y == graphXY.y) {
				pointExists = true;
				//already exists, so delete it
				myPoints.splice(p, 1);
				break;
			}
		}

		if (!pointExists) {				//add new points
			if (myPoints.length < 10) {
				myPoints.push(new Point(graphXY.x, graphXY.y));
			} else {
				myPoints.splice(0, 1);
				myPoints.push(new Point(graphXY.x, graphXY.y));
			}
		}
		//parent_mc.debug.text = myPoints;
		drawPoints();
		parent_mc.updateResetButtons();
	}

	function drawPoints() {
		var myPoints:Array = parent_mc.myPoints;	//pointer to the myPoints Array
		points.clear();
		var pointXY:Point, myX:Number, myY:Number;
		for (var p in myPoints) {
			myX = myPoints[p].x;
			myY = myPoints[p].y;
			pointXY = graphToPixels(myX, myY);

			//draw point if within viewable graph
			if (myX <= gridXmax && myX >= gridXmin && myY <= gridYmax && myY >= gridYmin) {
				drawPoint(pointXY);
			}
		}
	}

	function drawFunction(thisFunction:ParentFunction, mainGraph:Boolean, range:String, maxMin:Number) {

		//var myFunction:ParentFunction = parent_mc.myFunction;
		
		var functionId:Number = thisFunction.id;

		var myX, myY, pixelXY;
		
		if (mainGraph) {
			plot.lineStyle(plotStroke, plotColor, 100, false);
		} else {
			if (maxMin == 0) {
				plot.lineStyle(0, 0, 0, false);
			} else {
				plot.lineStyle(2, plotColor, 25, false);
			}
		}

		if (functionId != undefined) {
			
			var stepX:Number = .05;
			//var stepX:Number = Math.round(100 * x_pixels_per_unit/30) / 100;
			//trace ('px/unit = ' + x_pixels_per_unit);
			//trace ('stepX = ' + stepX);
			
			var pointsdrawn:Number = 0;
			
			myX = gridXmin;
			
			var connector:Boolean = false;

			for (myX; myX <= gridXmax; myX += stepX) {
				
				myX = correctFloatingPointError(myX);
				myY = thisFunction.getY(myX, range, maxMin);
				//myY = correctFloatingPointError(myY);
				
				/*
				if (functionId == 2 || functionId == 5) {	//for square and cube root functions, when x is very close to 0, round to 0
					if (Math.abs(myX) < 1e-10) {
						myX = 0;
						myY = myFunction.getY(myX);
					}
				} else if (functionId >= 10) {
					//myX = correctFloatingPointError(myX);
					if (Math.abs(myX) < stepX) {
						myX = 1e-100 * parent_mc.xReflect;	//for log functions, choose x very close to 0
						myY = myFunction.getY(myX);
						
						//trace (myX+','+myY);
					}
					//trace (myX+','+myY);
				}
				*/
				
				//trace (myX+','+myY);
				//trace (thisFunction.xDilate);
				if (thisFunction.xDilate == 0) {
					//In the case of horizontal dilation going to zero, myY may be Infinity because it results in 1/xDilate or 1/0, which is undefined.
					//For the function y = 1/x (function id 3), it will become a horizontal line.
					//For other functions, allow y values to be Infinity, which will result in a blank graph because the graph is undefined.
				} else if (functionId == 3 && (myY == Number.POSITIVE_INFINITY || myY == Number.NEGATIVE_INFINITY)) {
						//trace ('xDilate: ' + parent_mc.xDilate);
						//create asymptotes for the function 1/x
					
						//curve going into first asymptote
						var tinyStepX:Number = 0.01;
						for (var newX:Number = myX - 0.04; newX < myX - tinyStepX; newX += tinyStepX) {
							newX = correctFloatingPointError(newX);
							var newY:Number = thisFunction.getY(newX, range, maxMin);
							//trace ('lineTo ' + newX + ',' + newY);
							pixelXY = graphToPixels(newX, newY);
							plot.lineTo(pixelXY.x, pixelXY.y);
						}
						
						//end of first asymptote at edge of graph
						newX += tinyStepX;
						if (newY < 0) {
							newY = gridYmin;
						} else {
							newY = gridYmax;
						}
						
						pixelXY = graphToPixels(newX, newY);
						plot.lineTo(pixelXY.x, pixelXY.y);
						
						//start of second asymptote at edge of graph
						newX += tinyStepX;
						if (newY == gridYmax) {
							newY = gridYmin;
						} else {
							newY = gridYmax;
						}
						
						//curve going out of second asymptote
						pixelXY = graphToPixels(newX, newY);
						plot.moveTo(pixelXY.x, pixelXY.y);
						//trace ('moveTo ' + newX + ',' + newY);
						for (newX += tinyStepX; newX < myX + 0.04; newX += tinyStepX) {
							newX = correctFloatingPointError(newX);
							newY = thisFunction.getY(newX, range, maxMin);
							//trace ('lineTo ' + newX + ',' + newY);
							pixelXY = graphToPixels(newX, newY);
							plot.lineTo(pixelXY.x, pixelXY.y);
						}
						myX = newX;
						myY = newY;
					
				} else if (myY == Number.POSITIVE_INFINITY) {	//For other functions, draw Infinity to edge of the graph to create asymptotes
					myY = gridYmax;
				} else if (myY == Number.NEGATIVE_INFINITY) {
					myY = gridYmin;
				}
				if (myY < 1000 && myY > -1000) {
					pixelXY = graphToPixels(myX, myY);
					if (connector) {
						plot.lineTo(pixelXY.x, pixelXY.y);	//if connecting, draw to here
					} else {
						plot.moveTo(pixelXY.x, pixelXY.y);	//if not, move to here
					}
					connector = true;	//following point will connect to here
					//trace (myX+','+myY)
				} else {
					//trace (myX+','+myY);
					connector = false;	//following point cannot connect
				}
//				trace (myX+','+myY);
				pointsdrawn++;
			}
			//trace ('points drawn: ' + pointsdrawn);
			//trace ('points per pixel: ' + pointsdrawn/graphWidth);
		}
	}

	function mouseToGraphXY(mouseX:Number, mouseY:Number) {
		//translate mouse X,Y to graph X,Y
		var posXY:Point = new Point();

		//get x,y value of point on graph
		posXY.x = (mouseX - x_axis_zero)/x_pixels_per_unit;
		posXY.y = (mouseY - y_axis_zero)/y_pixels_per_unit * -1;

		//find nearest grid point
		posXY.x = Math.round(posXY.x);
		posXY.y = Math.round(posXY.y);

		//keep within graph view area
		posXY.x = Math.max(posXY.x, gridXmin);
		posXY.x = Math.min(posXY.x, gridXmax);
		posXY.y  = Math.max(posXY.y, gridYmin);
		posXY.y  = Math.min(posXY.y, gridYmax);
		return posXY;
	}

	function graphToPixels(x:Number, y:Number) {
		var pixelObject:Point = new Point();

		pixelObject.x = x_axis_zero + (x * x_pixels_per_unit);
		pixelObject.y = y_axis_zero - (y * y_pixels_per_unit);

		return pixelObject;
	}


	//function to update the grid and axis
	function drawGridAndAxis() {
		var gridNumberX:Number = (gridXmax - gridXmin)/gridXscale;
		var gridNumberY:Number = (gridYmax - gridYmin)/gridYscale;
		var x_pos:Number = 0;
		var y_pos:Number = 0;
		var x_spacer:Number = graphWidth/gridNumberX;
		var y_spacer:Number = graphHeight/gridNumberY;

		//grid drawing
		grid.clear();
		grid.lineStyle(gridStroke, gridColor, 100, true);		//drawing vertical lines
		for (var i:Number = 0; i <= gridNumberX; i++) {			//start at left and move right
			grid.moveTo(x_pos, y_pos);
			grid.lineTo(x_pos, y_pos + graphHeight);
			x_pos += x_spacer;
		}
		x_pos = 0;
		y_pos = graphHeight;							//drawing horizontal lines
		for (i = 0; i <= gridNumberY; i++) {			//start at bottom and move up
			grid.moveTo(x_pos, y_pos);
			grid.lineTo(x_pos + graphWidth, y_pos);
			y_pos -= y_spacer;
		}

		//axis drawing
		axis.clear();
		axis.lineStyle(axisStroke, axisColor, 100, true);

		//clean up old labels
		for (var i:Number = 0; i < oldLabelNumX; i++) {
			axis['x'+i].removeTextField();
		}
		for (var i:Number = 0; i < oldLabelNumY; i++) {
			axis['y'+i].removeTextField();
		}

		//check if x axis is within view area
		x_pixels_per_unit = graphWidth / (gridXmax - gridXmin);			//pixels per unit on x axis
		y_pixels_per_unit = graphHeight / (gridYmax - gridYmin);		//pixels per unit on y axis

		x_axis_zero = (0 - gridXmin) * x_pixels_per_unit;					//the pixel value where x=0 (x position of y-axis)
		y_axis_zero = graphHeight - (0 - gridYmin) * y_pixels_per_unit;		//the pixel value where y=0 (y position of x-axis)

		//draw x axis if within viewing area
		if (y_axis_zero >= 0 && y_axis_zero <= graphHeight) {
			//x axis
			 axis.moveTo(0, y_axis_zero);
			 axis.lineTo(graphWidth, y_axis_zero);

			//draw tick marks and create labels
			x_pos = 0;
			y_pos = y_axis_zero + buffer_x;

			for (i = 0; i <= gridNumberX; i++) {
				calcVal = correctFloatingPointError(gridXmin + (i*gridXscale));
				displayVal = String(calcVal);
				var gLabelX:TextField = newGridLabel(axis, mc_depth++, x_pos, y_pos, displayVal, 'x'+i);
				axis.moveTo(x_pos, y_pos - buffer_x + tickMarkLength);
				axis.lineTo(x_pos, y_pos - buffer_x - tickMarkLength);
				x_pos += x_spacer;
				if (calcVal == 0 && y_axis_zero != graphHeight) {	//leave off x label for 0 unless x-axis is at bottom edge
					gLabelX._alpha = 0;
				} else {
					gLabelX._x -= gLabelX._width/2;
				}
			}
			oldLabelNumX = i;
		}

		//draw y axis if within viewing area
		if (x_axis_zero >= 0 && x_axis_zero <= graphWidth) {

			//y axis
			 axis.moveTo(x_axis_zero, 0);
			 axis.lineTo(x_axis_zero, graphHeight);
		
			//draw tick marks and create labels
			x_pos = x_axis_zero - buffer_y;
			y_pos = graphHeight;
			for (i = 0; i <= gridNumberY; i++) {
				calcVal = correctFloatingPointError(gridYmin + (i*gridYscale));
				displayVal = String(calcVal);
				var gLabelY:TextField = newGridLabel(axis, mc_depth++, x_pos, y_pos, displayVal, 'y'+i);
				axis.moveTo(x_pos + buffer_y - tickMarkLength, y_pos);
				axis.lineTo(x_pos + buffer_y + tickMarkLength, y_pos);
				y_pos -= y_spacer;
				gLabelY._x -= gLabelY._width;
				if (calcVal == 0 && x_axis_zero != 0) {	//leave off the y label for 0 unless y-axis is at left edge
					gLabelY._alpha = 0;
				} else if (calcVal == -1 && x_spacer < 24 && gridXmin < 0) {
					//hide the -1 label when not enough space
					gLabelY._alpha = 0;
				} else {
					gLabelY._y -= gLabelY._height/2;
				}
				
			}
			oldLabelNumY = i;
		}

	}

	function newGridLabel(mc:MovieClip, d:Number, myX:Number, myY:Number, myTxt:String, myID:String) {
		var newLabel:TextField = mc.createTextField(myID, d++, myX, myY, 1, 1);
		//replace negative signs with en dashes
		if (myTxt.indexOf('-') > -1) {
			myTxt = stringReplace(myTxt,'-','\u2013');
		}
		newLabel.text = myTxt;
		newLabel.selectable = false;
		newLabel.autoSize = true;
		//newLabel.embedFonts = true;
		newLabel.setTextFormat(gridLabelFormat);
		return newLabel;
	}


	public function correctFloatingPointError(number:Number):Number {
		//default returns (10000 * number) / 10000
		//should correct very small floating point errors
		var precision:Number = 5;
		var correction:Number = Math.pow(10, precision);
		return Math.round(correction * number) / correction;
	}

	function stringReplace(str:String, searchStr:String, replaceStr:String):String {
		return str.split(searchStr).join(replaceStr);
	}

	////////////////////////////////// DRAWING CIRCLES (FOR POINTS)
	function drawPoint(thisPoint:Point){
		var mc:MovieClip = points;
		var px:Number;
		var py:Number;
		var centerX:Number = thisPoint.x;
		var centerY:Number = thisPoint.y;
		mc.lineStyle(dotStroke, dotColor, 100);
		mc.beginFill(dotColor, 100);
		var a:Number;
		mc.moveTo(dotSize + centerX, centerY);
		a = Math.tan(22.5 * Math.PI/180);
		for (var angle = 45; angle<=360; angle += 45) {
			// endpoint:
			px = dotSize * Math.cos(angle * Math.PI/180)+centerX;
			py = dotSize * Math.sin(angle * Math.PI/180)+centerY;
			// control:
			// (angle-90 is used to give the correct sign)
			var cx =px + dotSize * a * Math.cos((angle-90) * Math.PI/180);
			var cy =py + dotSize * a * Math.sin((angle-90) * Math.PI/180);
			mc.curveTo(cx, cy, px, py);
		}
		mc.endFill();
	}
}