class RadioButton extends MovieClip {

	private var myRadioGroup:Array;
	private var myId:String;
	private var rb_select:MovieClip;
	private var d:Number;
	private var w:Number;
	private var h:Number;

	function RadioButton() {
		
	}

	public function init(myGroup:Array, id:String) {
		myRadioGroup = myGroup;
		myId = id;
		d = 0;//this.getNextHighestDepth();

		//create background for hit area
		var _bg:MovieClip = this.createEmptyMovieClip('_bg', d++);
		if (!w) {
			w = 120;
		}
		if (!h) {
			h = 18;
		}
		_bg.beginFill(0xFFFFFF, 0);
		_bg.moveTo(0, 0);
		_bg.lineTo(w + _x, 0);
	    _bg.lineTo(w + _x, h);
	    _bg.lineTo(0, h);
	    _bg.lineTo(0, 0);
	    _bg.endFill();

		this.onPress = clickMe;
	}

	public function clickMe() {
		for (var r in myRadioGroup) {
			myRadioGroup[r].highlighted = false;
		}
		highlighted = true;
		_parent.toggleRadioGroup(myId);
	}

	public function set highlighted( hl:Boolean ) {
		if (hl) {
			rb_select._alpha = 100;
		} else {
			rb_select._alpha = 0;
		}
	}

}