class GraphingFunctionsGrading extends MovieClip {
	var gradeable_id:String;
	var focused:Boolean;
	var directions:MovieClip;
	var GraphingFunctionsModule:MovieClip;	//Instance Name of the graded module's MovieClip. This is usually the MovieClip already on Stage.
	var tabType:String;
	
	function GraphingFunctionsGrading() {
	}
	
	/***** REQUIRED FOR SIMPLE AND COMPLEX SWF GRADING *******/
	/********* 
		this function will receive the exact string/number/object or whatever was sent from the getGradeableData() function
	    it will be called once for each response that was authored by the user until a match is found
		for simple graded swf's -> return true/false based on whether what the student has currently entered in the module is correct
		for complex graded swf's -> return true/false based on whether what the student has currently entered in the module matches the response sent in (not necessarily whether it is right or wrong)
	**********/	

	function checkAnswer($authored_object) {

		var match:Boolean = true;

		//check here if student response matches authored response
		//if not, match becomes false
		
		//compare points if they are graded AND if student can edit points
		if ($authored_object.graphSettings.wildcardOptions.points && this.GraphingFunctionsModule.allowPointEditing) {
			var authorPoints = $authored_object.graphSettings.myPoints;
			var studentPoints = this.GraphingFunctionsModule.graphSettings.myPoints;
			var matchingPoint:Boolean, checkX:Number, checkY:Number, thisX:Number, thisY:Number;
			for (var p in authorPoints) {
				matchingPoint = false;
				checkX = authorPoints[p].x;
				checkY = authorPoints[p].y;
				for (var s in studentPoints) {
					thisX = studentPoints[s].x;
					thisY = studentPoints[s].y;
					if (checkX == thisX && checkY == thisY) {
						matchingPoint = true;
						break;
					}
				}
				if (!matchingPoint) {
					match = false;
					break;
				}
			}
			if (authorPoints.length != studentPoints.length) {
				match = false;
			}
		}
		//this.GraphingFunctionsModule.debug.text = authorPoints.length + ',' + studentPoints.length;
		
		//compare parent function if graded
		var authorFunctionId:Number = $authored_object.graphSettings.myFunctionId;				//get function id's for use below
		var studentFunctionId:Number = this.GraphingFunctionsModule.graphSettings.myFunctionId;
		trace ('________check answer__________')
		if ($authored_object.graphSettings.wildcardOptions.parentFunction && this.GraphingFunctionsModule.allowFunctionEditing) {	//if the function is being graded, AND students can edit function check all these things

			//compare function id to see if it matches
			if (authorFunctionId == undefined && studentFunctionId == undefined) {
				trace('both are blank so this is a match');
			} else if (authorFunctionId != studentFunctionId) {
				trace('functions do not match');
				match = false;
			}
			trace ('authorFunctionId = ' + authorFunctionId);
			trace ('studentFunctionId = ' + studentFunctionId);
		
			var xDiff:Number, yDiff:Number;
			
			//compare translate adjustment if graded
			if ($authored_object.graphSettings.wildcardOptions.translate) {
				var authorTranslateX:Number = Number($authored_object.graphSettings.myTransforms[authorFunctionId].xShift);
				var authorTranslateY:Number = Number($authored_object.graphSettings.myTransforms[authorFunctionId].yShift);
				var studentTranslateX:Number = Number(this.GraphingFunctionsModule.graphSettings.myTransforms[studentFunctionId].xShift);
				var studentTranslateY:Number = Number(this.GraphingFunctionsModule.graphSettings.myTransforms[studentFunctionId].yShift);
				xDiff = Math.round(Math.abs(authorTranslateX - studentTranslateX) * 100) / 100;		//round to two decimals
				yDiff = Math.round(Math.abs(authorTranslateY - studentTranslateY) * 100) / 100;	//round to two decimals
				var translateToleranceX:Number = Number($authored_object.graphSettings.translateToleranceX);
				var translateToleranceY:Number = Number($authored_object.graphSettings.translateToleranceY);
				if (xDiff > translateToleranceX || yDiff > translateToleranceY) {
					match = false;
				}
			}
			//this.GraphingFunctionsModule.debug.text = authorTranslateX + ',' + authorTranslateY + ' | ' + studentTranslateX + ',' + studentTranslateY;
			
			//compare dilate adjustment if graded
			if ($authored_object.graphSettings.wildcardOptions.dilate) {
				var authorDilateX:Number = Number($authored_object.graphSettings.myTransforms[authorFunctionId].xDilate);
				var authorDilateY:Number = Number($authored_object.graphSettings.myTransforms[authorFunctionId].yDilate);
				var studentDilateX:Number = Number(this.GraphingFunctionsModule.graphSettings.myTransforms[studentFunctionId].xDilate);
				var studentDilateY:Number = Number(this.GraphingFunctionsModule.graphSettings.myTransforms[studentFunctionId].yDilate);
				xDiff = Math.round(Math.abs(authorDilateX - studentDilateX) * 100) / 100;	//round to two decimals
				yDiff = Math.round(Math.abs(authorDilateY - studentDilateY) * 100) / 100;	//round to two decimals
				var dilateToleranceX:Number = Number($authored_object.graphSettings.dilateToleranceX) / 100;	//convert this to a percent
				var dilateToleranceY:Number = Number($authored_object.graphSettings.dilateToleranceY) / 100;	//convert this to a percent
				if (xDiff > dilateToleranceX || yDiff > dilateToleranceY) {
					match = false;
				}
			}
			//this.GraphingFunctionsModule.debug.text += '\n' + authorDilateX + ',' + authorDilateY + ' | ' + studentDilateX + ',' + studentDilateY;
			//this.GraphingFunctionsModule.debug.text += '\n' + xDiff + ', ' + yDiff;
			
			//compare reflection if graded
			if ($authored_object.graphSettings.wildcardOptions.reflect) {
				var authorReflectX:Number = Number($authored_object.graphSettings.myTransforms[authorFunctionId].xReflect);
				var authorReflectY:Number = Number($authored_object.graphSettings.myTransforms[authorFunctionId].yReflect);
				var studentReflectX:Number = Number(this.GraphingFunctionsModule.graphSettings.myTransforms[studentFunctionId].xReflect);
				var studentReflectY:Number = Number(this.GraphingFunctionsModule.graphSettings.myTransforms[studentFunctionId].yReflect);

				if (isNaN(authorReflectX) && isNaN(authorReflectY) && isNaN(studentReflectX) && isNaN(studentReflectY)) {
					trace ('both graphs are blank so this is still a match')
				} else if (authorReflectX != studentReflectX || authorReflectY != studentReflectY) {
					match = false;
				}
				trace ('authorReflectX = ' + authorReflectX);
				trace ('studentReflectX = ' + studentReflectX);
			}
		}
		//this.GraphingFunctionsModule.debug.text += '\n' + authorReflectX + ',' + authorReflectY + ' | ' + studentReflectX + ',' + studentReflectY;
		trace ('match = ' + match);
		return match;
	}
	/********
	this function is called when the user clicks on this module to edit it
	include registering of all key listeners and mouse listeners as well as enabling buttons, etc.
	********/
	function setFocus() {
		this.directions._visible = true;
		this.focused = true;
	}
	/*******
	this function is called when the user clicks on a different module or on the stage and thus kills focus on this module
	include removal of all key listeners and mouse listeners as well as disabling any buttons, etc.
	*******/
	function killFocus() {
		this.focused = false;
		this.directions._visible = false;
	}
	/***** REQUIRED FUNCTIONS FOR COMPLEX SWF GRADING *******/
	/*********
	this function is called each time the user switches between tabs so that the current gradeable data will be stored
	return any object/array/number/string or combination of the above
	the object should be as simple as possible but must contain all information required for uniquely grading different responses
	**********/
	function getGradeableData() {
		var rtn:Object = new Object();
		
		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)

		//get settings constant across whole question
		if (tabType == "question") {
			rtn.questionSettings = this.GraphingFunctionsModule.questionSettings;
		}

		//get other objects that can be saved differently on each tab
		rtn.graphSettings = this.GraphingFunctionsModule.graphSettings;
		
		return rtn;
	}
	/**********
	this function is called each time the user switches to a response tab so that the gradeable data for that tab will be displayed
	the incoming parameter will match exactly the format of whatever is returned from the getGradeableData function
	at this point the module should be modified to redisplay exactly what would have been present when the user last viewed the tab with the associated gradeableData
	***********/
	var isSetup:Boolean = false;
	function setGradeableData($v) {

		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)

		//set settings constant across whole question, or when module is loaded for the first time
		if (tabType == "question" or isSetup==false) {
			
			this.GraphingFunctionsModule.questionSettings = $v.questionSettings;	//recall any saved general settings
			
			isSetup = true;
		}

		//recall any saved settings for other objects
		this.GraphingFunctionsModule.graphSettings = $v.graphSettings;
		
		//reload the module with saved settings
		this.GraphingFunctionsModule.reload();
	}
}
