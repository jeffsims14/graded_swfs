class ParentFunction {

	var id:Number;			//id of function from Drop Down Menu
	var xShift:Number;		//shift in x direction
	var yShift:Number;		//shift in y direction
	var xDilate:Number;		//horizontal dilation
	var yDilate:Number;		//vertical dilation
	var xReflect:Number;	//horizontal reflection
	var yReflect:Number;	//vertical reflection
	var totalFunctions:Number = 13;
	
	var savedTransforms:Object;	//to save transforms for each function
	
	var parent_mc:MovieClip;

	public function ParentFunction (mc:MovieClip) {
		parent_mc = mc;
		
		//object to store transforms on each function
		savedTransforms = new Object();
	
		for (var i = 0; i<totalFunctions; i++) {
			savedTransforms[i] = new Object();
			savedTransforms[i].xShift = parent_mc.xShift;
			savedTransforms[i].yShift = parent_mc.yShift;
			savedTransforms[i].xDilate = parent_mc.xDilate;
			savedTransforms[i].yDilate = parent_mc.yDilate;
			savedTransforms[i].xReflect = parent_mc.xReflect;
			savedTransforms[i].yReflect = parent_mc.yReflect;
		}
	}
	
	public function getY(x:Number, modify:String, adj:Number) {
		var y:Number;
		
		xShift = parent_mc.xShift;
		yShift = parent_mc.yShift;
		xDilate = parent_mc.xDilate;
		yDilate = parent_mc.yDilate;
		xReflect = parent_mc.xReflect;
		yReflect = parent_mc.yReflect;
		
		//adjustments for range views
		if (modify == "shiftX") {
			xShift += adj;
		} else if (modify == "shiftY") {
			yShift += adj;
		} else if (modify == "dilateX") {
			xDilate += adj/100;
		} else if (modify == "dilateY") {
			yDilate += adj/100;
		}
		
		var xAdjust:Number = (1/xDilate * xReflect) * (x - xShift);
		var yAdjust:Number = yDilate * yReflect;
		
		switch(this.id) {
			case 0:
				y = (yAdjust * xAdjust) + yShift;									//y = x
				break;
			case 1:
				y = ( yAdjust * ( xAdjust * xAdjust ) ) + yShift;					//x^2
				break;
			case 2:
				y = ( yAdjust * ( Math.sqrt( xAdjust ) ) ) + yShift;				//sqrt of x
				break;
			case 3:
				y = ( yAdjust * ( 1 / xAdjust ) ) + yShift;							// 1/x
				break;
			case 4:
				y = ( yAdjust * ( xAdjust * xAdjust * xAdjust ) ) + yShift;			//x^3
				break;
			case 5:
				if (xAdjust >= 0) {
					y = ( yAdjust * ( Math.pow( xAdjust, 1/3 ) ) ) + yShift;		//cube root of x
				} else {
					y = -1 * ( yAdjust * ( Math.pow( Math.abs(xAdjust), 1/3 ) ) ) + yShift;
				}
				
				break;
			case 6:
				y =  ( yAdjust * ( Math.pow( 2, xAdjust ) ) ) + yShift;				//2 to the x power
				break;
			case 7:
				y =  ( yAdjust * ( Math.pow( 10, xAdjust ) ) ) + yShift;			//10 to the x power
				break;
			case 8:
				y = ( yAdjust * ( Math.pow( Math.E, xAdjust ) ) ) + yShift;			//e to the x power
				break;
			case 9:
				y =( yAdjust * ( Math.abs( xAdjust ) ) ) + yShift;					// |x|
				break;
			case 10:
				y = ( yAdjust * ( Math.log( xAdjust ) / Math.LN2 ) ) + yShift;		//log base2 of x
				break;
			case 11:
				y = ( yAdjust * ( Math.log( xAdjust ) / Math.LN10 ) ) + yShift;		//log base10 of x
				break;
			case 12:
				y = ( yAdjust * ( Math.log( xAdjust ) ) ) + yShift;					//ln (natural log) of x
				break;
		}
		/*if (y == Number.POSITIVE_INFINITY) {
			//y = 999;
		} else if (y == Number.NEGATIVE_INFINITY) {
			//y = -999;
		}*/
		//trace (parent_mc.myGraphView.gridYmax);
		return y;
	}
	
	//set and get saved transforms of each function
	public function updateTransforms() {
		savedTransforms[this.id].xShift = parent_mc.xShift;
		savedTransforms[this.id].yShift = parent_mc.yShift;
		savedTransforms[this.id].xDilate = parent_mc.xDilate;
		savedTransforms[this.id].yDilate = parent_mc.yDilate;
		savedTransforms[this.id].xReflect = parent_mc.xReflect;
		savedTransforms[this.id].yReflect = parent_mc.yReflect;
	}
	
	public function getSavedTransforms() {
		parent_mc.xShift = savedTransforms[this.id].xShift;
		parent_mc.yShift = savedTransforms[this.id].yShift;
		parent_mc.xDilate = savedTransforms[this.id].xDilate;
		parent_mc.yDilate = savedTransforms[this.id].yDilate;
		parent_mc.xReflect = savedTransforms[this.id].xReflect;
		parent_mc.yReflect = savedTransforms[this.id].yReflect;
	}
	
	//reset the transformations for all functions
	public function resetTransforms() {
		for (var i = 0; i<totalFunctions; i++) {
			savedTransforms[i] = new Object();
			savedTransforms[i].xShift = parent_mc.defaultShift;
			savedTransforms[i].yShift = parent_mc.defaultShift;
			savedTransforms[i].xDilate = parent_mc.defaultTransform;
			savedTransforms[i].yDilate = parent_mc.defaultTransform;
			savedTransforms[i].xReflect = parent_mc.defaultTransform;
			savedTransforms[i].yReflect = parent_mc.defaultTransform;
		}
		updateTransforms();
	}
	
	/*public function SavedTransformsToArray():Array {
		var transformsArray:Array = new Array();
		
		for (var i = 0; i<totalFunctions; i++) {
			transformsArray[i] = new Array();
			transformsArray[i][0] = savedTransforms[i].xShift;
			transformsArray[i][1] = savedTransforms[i].yShift;
			transformsArray[i][2] = savedTransforms[i].xDilate;
			transformsArray[i][3] = savedTransforms[i].yDilate;
			transformsArray[i][4] = savedTransforms[i].xReflect;
			transformsArray[i][5] = savedTransforms[i].yReflect;
		}
		return transformsArray;
	}
	
	public function reloadSavedTransforms(transformsArray:Array) {
		for (var i = 0; i<totalFunctions; i++) {
			savedTransforms[i].xShift = transformsArray[i][0];
			savedTransforms[i].yShift = transformsArray[i][1];
			savedTransforms[i].xDilate = transformsArray[i][2];
			savedTransforms[i].yDilate = transformsArray[i][3];
			savedTransforms[i].xReflect = transformsArray[i][4];
			savedTransforms[i].yReflect = transformsArray[i][5];
		}
		getSavedTransforms();
	}*/
}