class SumPanel extends MovieClip {
	
	private var parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	
	private var tIconUp:MovieClip;
	private var tIconDown:MovieClip;
	private var tIconLeft:MovieClip;
	private var tIconRight:MovieClip;
	private var dIconX:MovieClip;
	private var dIconY:MovieClip;
	private var rIconX:MovieClip;
	private var rIconY:MovieClip;
	private var translateTextX:TextField;
	private var translateTextY:TextField;
	private var dilateTextX:TextField;
	private var dilateTextY:TextField;
	private var reflectTextX:TextField;
	private var reflectTextY:TextField;
	private var iconArray:Array;
	private var tfArray:Array;

	function SumPanel(){
		
		d = this.getNextHighestDepth();
		
		iconArray = new Array();
		tfArray = new Array();
		
		//icon setup
		tIconLeft = this.attachMovie("translateIconLeft", "tIconLeft", d++, {_x:0, _y:3, _visible:false});
		tIconRight = this.attachMovie("translateIconRight", "tIconRight", d++, {_x:0, _y:3, _visible:false});
		tIconUp = this.attachMovie("translateIconUp", "tIconUp", d++, {_x:22, _y:3, _visible:false});
		tIconDown = this.attachMovie("translateIconDown", "tIconDown", d++, {_x:22, _y:3, _visible:false});
		
		dIconX = this.attachMovie("dilateIconX", "dIconX", d++, {_x:0, _y:33, _visible:false});
		dIconY = this.attachMovie("dilateIconY", "dIconY", d++, {_x:37, _y:33, _visible:false});
		
		rIconX = this.attachMovie("reflectIconX", "rIconX", d++, {_x:3, _y:63, _visible:false});
		rIconY = this.attachMovie("reflectIconY", "rIconY", d++, {_x:22, _y:63, _visible:false});
		
		iconArray = [tIconLeft, tIconRight, tIconUp, tIconDown, dIconX, dIconY, rIconX, rIconY];
		
		//text displays
		var displayFormat = new TextFormat();
		displayFormat.font = 'Arial';
		displayFormat.color = 0x666666;
		displayFormat.size = 10;
		displayFormat.align = 'left';
		
		translateTextX = this.createTextField("translateTextX", d++, 10, 3, 1, 1);
		translateTextX.setNewTextFormat(displayFormat);
		translateTextX.selectable = false;
		translateTextX.autoSize = true;
		translateTextX.embedFonts = true;
		
		translateTextY = this.createTextField("translateTextY", d++, 30, 3, 1, 1);
		translateTextY.setNewTextFormat(displayFormat);
		translateTextY.selectable = false;
		translateTextY.autoSize = true;
		translateTextY.embedFonts = true;
		
		dilateTextX = this.createTextField("dilateTextX", d++, 11, 33, 1, 1);
		dilateTextX.setNewTextFormat(displayFormat);
		dilateTextX.selectable = false;
		dilateTextX.autoSize = true;
		dilateTextX.embedFonts = true;
		
		dilateTextY = this.createTextField("dilateTextY", d++, 46, 33, 1, 1);
		dilateTextY.setNewTextFormat(displayFormat);
		dilateTextY.selectable = false;
		dilateTextY.autoSize = true;
		dilateTextY.embedFonts = true;
		
		tfArray = [translateTextX, translateTextY, dilateTextX, dilateTextY];
	}
	
	
	public function updateDisplay() {
		
		for (var i=0; i<iconArray.length; i++) {
			iconArray[i]._visible = false;
		}
		for (var i=0; i<tfArray.length; i++) {
			tfArray[i].text = "";
		}
		
		var xShift = correctFloatingPointError(parent_mc.xShift);
		var yShift = correctFloatingPointError(parent_mc.yShift);
		var xDilate = correctFloatingPointError(parent_mc.xDilate);
		var yDilate = correctFloatingPointError(parent_mc.yDilate);
		var xReflect = correctFloatingPointError(parent_mc.xReflect);
		var yReflect = correctFloatingPointError(parent_mc.yReflect);
		
		if (xShift > 0) {
			translateTextX.text = xShift;
			tIconRight._visible = true;
		} else if (xShift < 0) {
			translateTextX.text = String( Math.abs(xShift) );
			tIconLeft._visible = true;
		}
		if (yShift > 0) {
			translateTextY.text = yShift;
			tIconUp._visible = true;
		} else if (yShift < 0) {
			translateTextY.text = String( Math.abs(yShift) );
			tIconDown._visible = true;
		}
		
		if (xDilate != 1) {
			dilateTextX.text = Math.round(xDilate*100) + '%';
			dIconX._visible = true;
		}
		if (yDilate != 1) {
			dilateTextY.text = Math.round(yDilate*100) + '%';
			dIconY._visible = true;
		}
		
		if (xReflect == -1) {
			rIconX._visible = true;
		}
		if (yReflect == -1) {
			rIconY._visible = true;
		}
		
		
		translateTextY.text;
		dilateTextX.text;
		dilateTextY.text;
		
		
	}
	
	public function correctFloatingPointError(number:Number):Number {
		//default returns (10000 * number) / 10000
		//should correct very small floating point errors
		var precision:Number = 5;
		var correction:Number = Math.pow(10, precision);
		return Math.round(correction * number) / correction;
	}
	
	
}