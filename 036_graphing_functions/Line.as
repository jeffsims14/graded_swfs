class Line {

	var slope:Number;		//slope, taken from equation entry
	var intercept:Number;	//y-intercept, taken from equation entry
	var prefix:String;		//left side of the line's equation, taken from drop down menu -- will be either 'x' or 'y'
	var inequality:String;	//inequality, either greater than, less than, greater than or equal to, OR less than or equal to
	var xValue:Number;		//when the left side of equation is x, this is the value entered for x
	var yString:String;		//the full equation entry text as entered, when prefix is y
	var xString:String;		//the full equation entry text as entered, when prefix is x

	public function Line (slope:Number, intercept:Number, prefix:String, inequality:String, xValue:Number) {
		this.slope = slope;
		this.intercept = intercept;
		this.prefix = prefix;
		this.inequality = inequality;
		this.xValue = xValue;

		//text entry, default is blank
		this.yString = "";
		this.xString = "";
	}
}