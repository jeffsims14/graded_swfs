class TranslatePanel extends MovieClip {

	private var parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	
	var xShift:Number;		//shift in x direction
	var yShift:Number;		//shift in y direction
	var shiftUnits:Number;	//amount of shift with each arrow click
	var myDisplay:TextField;
	var displayFormat:TextFormat;

	function TranslatePanel() {
		d = this.getNextHighestDepth();
		
		var marginX:Number = 25;
		var marginY:Number = 5;
		
		var upButton:MovieClip = this.attachMovie("IconButton", "upButton", d++, {w:25, h:25, _x:marginX+25, _y:marginY, iconId:"UpArrow", id:"up", _parent_mc:this});
		var downButton:MovieClip = this.attachMovie("IconButton", "downButton", d++, {w:25, h:25, _x:marginX+25, _y:marginY+50, iconId:"DownArrow", id:"down", _parent_mc:this});
		var leftButton:MovieClip = this.attachMovie("IconButton", "leftButton", d++, {w:25, h:25, _x:marginX, _y:marginY+25, iconId:"LeftArrow", id:"left", _parent_mc:this});
		var rightButton:MovieClip = this.attachMovie("IconButton", "rightButton", d++, {w:25, h:25, _x:marginX+50, _y:marginY+25, iconId:"RightArrow", id:"right", _parent_mc:this});
		
		upButton.setAction( arrowClicked );
		downButton.setAction( arrowClicked );
		leftButton.setAction( arrowClicked );
		rightButton.setAction( arrowClicked );
		
		myDisplay = this.createTextField("myDisplay", d++, marginX+90, marginY+20, 100, 40);
		
		displayFormat = new TextFormat();
		displayFormat.font = 'Arial';
		displayFormat.color = 0x000000;
		displayFormat.size = 12;
		displayFormat.align = 'left';
		
		myDisplay.setNewTextFormat(displayFormat);
		myDisplay.selectable = false;
		myDisplay.embedFonts = true;
		myDisplay.autoSize = true;
		
		updateDisplay();
	}
	
	function arrowClicked (id:String, target_mc:MovieClip) {
		shiftUnits = target_mc.parent_mc.shiftUnits;
		switch (id) {
			case "up":
				target_mc.parent_mc.yShift += shiftUnits;
				break;
			case "down":
				target_mc.parent_mc.yShift -= shiftUnits;
				break;
			case "left":
				target_mc.parent_mc.xShift -= shiftUnits;
				break;
			case "right":
				target_mc.parent_mc.xShift += shiftUnits;
				break;
		}
		target_mc.updateDisplay();
		target_mc.parent_mc.updateGraph();
	}
	
	public var mySummary:String;
	
	function updateDisplay() {
		xShift = correctFloatingPointError(parent_mc.xShift);
		yShift = correctFloatingPointError(parent_mc.yShift);
		var stringX:String = "";
		var stringY:String = "";
		mySummary = "";
		if (xShift == 1) {
			stringX = "Shift right " + xShift + " unit.";
		} else if (xShift > 0) {
			stringX = "Shift right " + xShift + " units.";
		} else if (xShift == -1) {
			stringX = "Shift left " + Math.abs(xShift) + " unit.";
		} else if (xShift < 0) {
			stringX = "Shift left " + Math.abs(xShift) + " units.";
		}
		if (yShift == 1) {
			stringY = "Shift up " + yShift + " unit.";
		} else if (yShift > 0) {
			stringY = "Shift up " + yShift + " units.";
		} else if (yShift == -1) {
			stringY = "Shift down " + Math.abs(yShift) + " unit.";
		} else if (yShift < 0) {
			stringY = "Shift down " + Math.abs(yShift) + " units.";
		}
		if (xShift == 0 && yShift == 0) {
			myDisplay.text = "";
			mySummary = "";
		} else if (xShift == 0 && yShift != 0) {
			myDisplay.text = stringY;
			mySummary = stringY;
		} else if (xShift != 0 && yShift == 0) {
			myDisplay.text = stringX;
			mySummary = stringX;
		} else if (xShift != 0 && yShift != 0) {
			myDisplay.text = stringY +"\n" + stringX;
			mySummary = stringY + " " + stringX;
		}
	}
	
	public function updateSettings(id:String, newVal:String) {
		var newValNum:Number = Number(newVal);
		if (!isNaN(newValNum)) {
			switch (id) {
				case "x":
					parent_mc.translateToleranceX = newValNum;
					break;
				case "y":
					parent_mc.translateToleranceY = newValNum;
					break;
			}
			parent_mc.updateGraph();	//which will also save these settings
		}
	}
	
	public function correctFloatingPointError(number:Number):Number {
		//default returns (10000 * number) / 10000
		//should correct very small floating point errors
		var precision:Number = 5;
		var correction:Number = Math.pow(10, precision);
		return Math.round(correction * number) / correction;
	}
}