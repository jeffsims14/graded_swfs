class IconButton extends MovieClip {

	private var w:Number;			//width
	private var h:Number;			//height
	private var id:String;			//id is passed in setAction function
	private var _up:MovieClip;		//button state MCs
	private var _over:MovieClip;
	private var _down:MovieClip;
	private var _highlight:MovieClip;	//selected state
	private var _parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	private var iconId:String;			//identifier for icon
	private var myIcon:MovieClip;

	function IconButton() {
		//create button state MCs
		d = this.getNextHighestDepth();

		//default button actions
		this.onRollOver = function() {
			myIcon.gotoAndStop(2);
		}
		this.onRollOut = function() {
			myIcon.gotoAndStop(1);
		}
		this.onPress = function() {
			myIcon.gotoAndStop(3);
		}
			
		this.onRelease = this.onReleaseOutside = function() {
			myIcon.gotoAndStop(1);
		}

		if (!_parent_mc) {
			_parent_mc = this._parent;
		}
		
		if (iconId) {
			setIcon(iconId);
		}
	}

	//set the onPress action in addition to default button states
	public function setAction ( clickAction:Function ) {
		this.onPress = function() {
			myIcon.gotoAndStop(3);
			clickAction( this.id, _parent_mc );
			this.highlighted = true;
		}
	}

	//make a simple icon button
	public function setIcon ( id:String ) {
		myIcon = this.attachMovie(id, "myIcon", d++, {_x:2, _y:2});
	}
}