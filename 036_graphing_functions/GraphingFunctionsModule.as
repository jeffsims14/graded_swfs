class GraphingFunctionsModule extends MovieClip {

	var questionSettings:Object;			//settings saved across the entire question (graph settings, etc.)
	var graphSettings:Object;				//settings saved on each tab (student manipulations, grading options, etc.)

	var appMode:String;						//global App mode (author or student)
	var tabType:String;						//global Tab type (question or response)

	var myGraphView:MovieClip;
	var panels:MovieClip, gradingPanels:MovieClip;
	var d = 1;								//movieClip level depth
	var tabNum:Number;						//tab index counter

	var drawMode:String, toggleGroup:Array;			//for Point or Function Mode switching
	var transformMode:String, transformGroup:Array;	//for Transform Mode switching

	var myPoints:Array;
	var myFunction:ParentFunction;
	var functionDropDown:MovieClip;
	var myTranslate:MovieClip, myDilate:MovieClip, myReflect:MovieClip, transformPanels:Array;
	var summaryPanel:MovieClip;	//to display sum of transformations
	var transformSummary:TextField;	//display of all transformations

	//transform settings
	var defaultShift:Number = 0;
	var defaultTransform:Number = 1;
	var xShift:Number = 0;		//shift in x direction
	var yShift:Number = 0;		//shift in y direction
	var xDilate:Number = 1;		//horizontal dilation
	var yDilate:Number = 1;		//vertical dilation
	var xReflect:Number = 1;	//horizontal reflection
	var yReflect:Number = 1;	//vertical reflection
	var shiftUnits:Number = 1;	//amount of shift with each arrow click (question-wide)
	
	//grading and tolerance settings
	var translateToleranceX:Number;
	var translateToleranceY:Number;
	var dilateToleranceX:Number;
	var dilateToleranceY:Number;
	var wildcardOptions:Object;

	//for drawing range of tolerance
	var showRanges:Boolean = false;

	//options to disable point mode or function mode
	var allowPointEditing:Boolean = true;			//point editing available to student, on by default
	var allowFunctionEditing:Boolean = true;		//selection of parent function available to student, on by default
	var parentFunctionLocked:Boolean = false;		//menu to change parent function locked for student, off by default
	
	var debug:TextField;

	function GraphingFunctionsModule() {
		init();
		
		debug = this.createTextField('debug',d++,0,400,550,300);
		//debug.text = 'hello'
	}

// INIT function called on first load

	function init() {
		appMode = _global.core.Globals.appMode;	//get the global app mode ("author" or "student")
		//appMode = "author";						//uncomment for testing

		//set up graph view
		myGraphView = this.attachMovie("GraphView", "myGraphView", d++, {graphWidth:340, graphHeight:340, _x:2, _y:0, parent_mc:this});

		//movie clips to hold panels
		panels = this.createEmptyMovieClip("panels_mc", d++);
		gradingPanels = this.createEmptyMovieClip("gradingPanels", d++);

		//set up equation entry panel
		tabNum = 0;

		//default modes
		drawMode = "point";
		//transformMode = "translate";

		var toolsPanel = panels.attachMovie("Panel", "toolsPanel", d++, {w:175, h:120, _x:350, _y:2});	//panel for mode buttons

		var addPointsButton = toolsPanel.attachMovie("SimpleButton", "addPointsButton", d++, {w:80, h:20, _x:10, _y:10, id:"point", labelText:"+/&#8211; Points"});
		addPointsButton.setAction( switchMode );
		addPointsButton.highlighted = true;

		var selectFunctionButton = toolsPanel.attachMovie("SimpleButton", "selectFunctionButton", d++, {w:145, h:20, _x:10, _y:50, id:"function", labelText:"Select Parent Function"});
		selectFunctionButton.setAction( switchMode );

		toggleGroup = [addPointsButton, selectFunctionButton];	//for Points and Function buttons
		
		//reset buttons
		var resetPointsButton = toolsPanel.attachMovie("SimpleButton", "resetPointsButton", d++, {w:24, h:20, _x:94, _y:10});
		resetPointsButton.setIcon("revertIcon");
		resetPointsButton.setAction( resetPoints );
		
		var resetFunctionButton = toolsPanel.attachMovie("SimpleButton", "resetFunctionButton", d++, {w:24, h:20, _x:144, _y:82});
		resetFunctionButton.setIcon("revertIcon");
		resetFunctionButton.setAction( resetFunction );

		//set up window panel
		tabNum = 2;
		var myWinPanel = toolsPanel.attachMovie("WindowPanel", "myWinPanel", d++, {w:200, h:264, _x:0, _y:125, parent_mc:this, _visible:false});
		myWinPanel.myGraph = myGraphView;	//reference for window panel to control graph
		
		//set up transform panel
		var translateButton = toolsPanel.attachMovie("SimpleButton", "translateButton", d++, {w:100, h:20, _x:10, _y:135, id:"translate", labelText:"Translate", _visible:false});
		var dilateButton = toolsPanel.attachMovie("SimpleButton", "dilateButton", d++, {w:100, h:20, _x:10, _y:165, id:"dilate", labelText:"Dilate", _visible:false});
		var reflectButton = toolsPanel.attachMovie("SimpleButton", "reflectButton", d++, {w:100, h:20, _x:10, _y:195, id:"reflect", labelText:"Reflect", _visible:false});
		translateButton.setAction( switchTransform );
		dilateButton.setAction( switchTransform );
		reflectButton.setAction( switchTransform );
		
		transformGroup = [translateButton, dilateButton, reflectButton];
		
		//UI for each transform option
		myTranslate = toolsPanel.attachMovie("TranslatePanel", "myTranslate", d++, {_y:240, _visible:false, parent_mc:this});
		myDilate = toolsPanel.attachMovie("DilatePanel", "myDilate", d++, {_y:240, _visible:false, parent_mc:this});
		myReflect = toolsPanel.attachMovie("ReflectPanel", "myReflect", d++, {_y:240, _visible:false, parent_mc:this});
		
		transformPanels = [myTranslate, myDilate, myReflect];
		
		//reset buttons for transforms
		var resetTranslateButton = toolsPanel.attachMovie("SimpleButton", "resetTranslateButton", d++, {w:24, h:20, _x:112, _y:135});
		resetTranslateButton.setIcon("revertIcon");
		resetTranslateButton.setAction( resetTranslate );
		
		var resetDilateButton = toolsPanel.attachMovie("SimpleButton", "resetDilateButton", d++, {w:24, h:20, _x:112, _y:165});
		resetDilateButton.setIcon("revertIcon");
		resetDilateButton.setAction( resetDilate );
		
		var resetReflectButton = toolsPanel.attachMovie("SimpleButton", "resetReflectButton", d++, {w:24, h:20, _x:112, _y:195});
		resetReflectButton.setIcon("revertIcon");
		resetReflectButton.setAction( resetReflect );

		//drop down menu
		functionDropDown = toolsPanel.attachMovie("FunctionDropDownMenu", "functionDropDown", d++, {_x:26, _y:80});
		functionDropDown.disable();
		
		//create the parent function
		myFunction = new ParentFunction(this);

		//create Array to track Points created
		myPoints = new Array();
		
		//display for summary of transformations
		//summaryPanel = toolsPanel.attachMovie("SumPanel", "summaryPanel", d++, {w:40, h:80, _x:132, _y:135, parent_mc:this});
		
		
		transformSummary = panels.createTextField("transformSummary", d++, 1, 350, 340, 60);
		
		var displayFormat = new TextFormat();
		displayFormat.font = 'Arial';
		displayFormat.color = 0x000000;
		displayFormat.size = 10;
		displayFormat.align = 'left';
		
		transformSummary.setNewTextFormat(displayFormat);
		transformSummary.selectable = false;
		transformSummary.embedFonts = true;

		//set up for grading options if we are in Author App
		if (appMode == "author") {
			//set default grading values and any other UI setup for the Author App
			var settingsButton = panels.attachMovie("settings_icon","settingsButton", d++, {_x:540, _y:12});
			settingsButton.onPress = function() {
				if (myWinPanel._visible) {
					_parent._parent.hideWindowPanel();
				} else {
					_parent._parent.showWindowPanel();
				}
			}
			
			//default grading tolerances
			var defaultTranslateTolerance:Number = 1;
			var defaultDilateTolerance:Number = 1;
			
			//initial tolerances
			translateToleranceX = defaultTranslateTolerance;
			translateToleranceY = defaultTranslateTolerance;
			dilateToleranceX = defaultDilateTolerance;
			dilateToleranceY = defaultDilateTolerance;
			
			//wildcard options to ignore certain things for grading
			wildcardOptions = new Object();
			wildcardOptions.points = true;
			wildcardOptions.parentFunction = true;
			wildcardOptions.translate = true;
			wildcardOptions.dilate = true;
			wildcardOptions.reflect = true;
			
			var translateToleranceXsetting = myTranslate.attachMovie("PanelSetting", "translateToleranceXsetting", d++, {_x:10, _y:132, labelText:"<i>x</i> = +/\u2013", header:"Translation tolerance:", myDefault: translateToleranceX, id:"x"});
			var translateToleranceYsetting = myTranslate.attachMovie("PanelSetting", "translateToleranceYsetting", d++, {_x:120, _y:132, labelText:"<i>y</i> = +/\u2013", myDefault: translateToleranceY, id:"y"});
			
			var dilateToleranceXsetting = myDilate.attachMovie("PanelSetting", "dilateToleranceXsetting", d++, {_x:10, _y:132, labelText:"<i>x</i> = +/\u2013", header:"Dilation tolerance:", myDefault: dilateToleranceX, unitLabel:"%", id:"x"});
			var dillateToleranceYsetting = myDilate.attachMovie("PanelSetting", "dilateToleranceYsetting", d++, {_x:120, _y:132, labelText:"<i>y</i> = +/\u2013", myDefault: dilateToleranceY, unitLabel:"%", id:"y"});
			
			//eye buttons for winking
			var pointGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "pointGrading", d++, {_x:470, _y:17, id:"points"});
			var functionGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "functionGrading", d++, {_x:520, _y:88, id:"parentFunction"});
			var translateGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "translateGrading", d++, {_x:490, _y:142, id:"translate"});
			var dilateGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "dilateGrading", d++, {_x:490, _y:172, id:"dilate"});
			var reflectGrading:MovieClip = gradingPanels.attachMovie("GradedSetting", "reflectGrading", d++, {_x:490, _y:202, id:"reflect"});	//formerly x:560
		}

		reload();	//reload to load any saved settings
	}

// RELOAD function called every time we switch to a new tab to recall saved data

	function reload() {	
		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)
		//tabType = "response";					//uncomment for testing 
		//tabType = "question";					//uncomment for testing

		//check if saved settings exist
		if (questionSettings == null) {
			//if not, initalize the settings and other objects

			questionSettings = new Object();
			graphSettings = new Object();

			//save the settings for recall in graded swf
			updateQuestionSettings();
			updateGraphSettings();

			//continue any other initial setup
			//debug.text = "created new settings";
		} else {
			//Settings exist, so we reload the settings and other stuff
			reloadQuestionSettings( questionSettings );
			reloadGraphSettings( graphSettings );
		}
		
		if (myFunction.id != undefined) {
			functionDropDown.setChoice(myFunction.id);
			showTransformTools();
		} else {
			functionDropDown.resetAll();
			hideTransformTools();
		}
		
		//update the transform UIs
		myTranslate.updateDisplay();
		myDilate.updateDisplay();
		myReflect.updateDisplay();
		
		//update the graph
		updateGraph();

		//if on a response tab in author app, show grading options
		if (appMode == "author") {

			if (tabType == "response") {
				showGradingOptions();		//extra grading options are shown on response tabs
			} else {
				hideGradingOptions();		//otherwise hide the grading options
			}

			//if on a question tab in author app, show general settings UI

			if (tabType == "question") {
				showGeneralSettings();		//general settings are shown on the question tab
			} else {
				hideGeneralSettings();		//otherwise hide the general settings
			}
		} else {
			checkIfModesDisabled();
		}
	}

//OPTIONS to allow disabling of point or function modes for students
	function checkIfModesDisabled() {
	//check if point editing or equation entry is disabled for students
		if (!allowPointEditing) {
			disablePointEditing();
		}
		if (!allowFunctionEditing) {
			disableFunctionEditing();
		}
		if (parentFunctionLocked) {
			lockFunctionMenu();
		}
	}

	function disablePointEditing() {
		//option (set by author) to disallow point editing by students
		drawMode = "function";						//force line drawing mode only
		panels.toolsPanel.addPointsButton._visible = false;
		panels.toolsPanel.resetPointsButton._visible = false;
		panels.toolsPanel.functionDropDown.enable();
		panels.toolsPanel.functionDropDown._x = 10;
		//panels.toolsPanel.myBG._height = 72;
		//panels.toolsPanel.myBG._y = 46;
		panels.toolsPanel.myBG._visible = false;
		panels.toolsPanel.selectFunctionButton.changeToLabel();
		panels.toolsPanel.selectFunctionButton._x = 0;
		myGraphView.myBG.onPress = undefined;
		myGraphView.myBG.useHandCursor = false;
	}

	function disableFunctionEditing() {
		//option (set by author) to hide the equation entry from students
		//panels.equationPanel._visible = false;
		drawMode = "point";
		panels.toolsPanel.selectFunctionButton._visible = false;
		panels.toolsPanel.functionDropDown._visible = false;
		panels.toolsPanel.resetFunctionButton._visible = false;
		panels.toolsPanel.resetPointsButton._x = 0;
		panels.toolsPanel.resetPointsButton._y = 0;
		//panels.toolsPanel.myBG._height = 40;
		panels.toolsPanel.myBG._visible = false;
		panels.toolsPanel.addPointsButton._visible = false;
		transformSummary._visible = false;
		//panels.toolsPanel.addPointsButton.changeToLabel();
	}

	function lockFunctionMenu() {
		functionDropDown.disable();
		panels.toolsPanel.resetFunctionButton._visible = false;
		panels.toolsPanel.selectFunctionButton._visible = false;
	}


//SAVE and RELOAD functions for General Settings and any other Student or Authored Data
	function saveQuestionSettings (s:Object):Object {
		//save any data to an object to be saved and recalled
		
		//question-wide graph-related settings
		s.gridXmin = myGraphView.gridXmin;
		s.gridXmax = myGraphView.gridXmax;
		s.gridXscale = myGraphView.gridXscale;
		s.gridYmin = myGraphView.gridYmin;
		s.gridYmax = myGraphView.gridYmax;
		s.gridYscale = myGraphView.gridYscale;
		s.shiftUnits = shiftUnits;
		s.allowPointEditing = allowPointEditing;
		s.allowFunctionEditing = allowFunctionEditing;
		s.parentFunctionLocked = parentFunctionLocked;

		return s;
	}
	function reloadQuestionSettings(s:Object) {
		//recall the data from saved object when switching tabs
		
		//question-wide graph-related settings
		myGraphView.gridXmin = s.gridXmin;
		myGraphView.gridXmax = s.gridXmax;
		myGraphView.gridXscale = s.gridXscale;
		myGraphView.gridYmin = s.gridYmin;
		myGraphView.gridYmax = s.gridYmax;
		myGraphView.gridYscale = s.gridYscale;
		shiftUnits = s.shiftUnits;
		if (s.allowPointEditing != undefined) {
			allowPointEditing = s.allowPointEditing;
		}
		if (s.allowFunctionEditing != undefined) {
			allowFunctionEditing = s.allowFunctionEditing;
		}
		if (s.parentFunctionLocked != undefined) {
			parentFunctionLocked = s.parentFunctionLocked;
		}
	}

	//when a setting is changed, call this function to save it
	function updateQuestionSettings() {	
		questionSettings = saveQuestionSettings ( questionSettings );	// <---- CALL THIS WHENEVER SETTINGS ARE CHANGED
	}

	//for other items in the module that need to be saved and recalled
	function saveGraphSettings (s:Object):Object {
		//save points
		s.myPoints = myPoints;
		
		//save parent function current id and all transforms
		//s.myFunction = myFunction;
		s.myFunctionId = myFunction.id;
		
		//s.savedTransforms = myFunction.SavedTransformsToArray();	//get saved transforms in form of Array
		s.myTransforms = myFunction.savedTransforms;
		
		//grading settings
		s.translateToleranceX = translateToleranceX;
		s.translateToleranceY = translateToleranceY;
		s.dilateToleranceX = dilateToleranceX;
		s.dilateToleranceY = dilateToleranceY;
		s.wildcardOptions = wildcardOptions;
		
		return s;
	}
	function reloadGraphSettings (s:Object) {
		//recall the data from saved object when switching tabs
		if (s.myPoints == undefined) {
			myPoints = new Array();
		} else {
			myPoints = s.myPoints;		//saved points
		}
		
		myFunction.id = s.myFunctionId;	//saved parent function and transforms
		
		//myFunction.reloadSavedTransforms( s.savedTransforms );	//load saved transforms Array
		myFunction.savedTransforms = s.myTransforms;
		myFunction.getSavedTransforms();
		
		//grading settings
		translateToleranceX = s.translateToleranceX;
		translateToleranceY = s.translateToleranceY;
		dilateToleranceX = s.dilateToleranceX;
		dilateToleranceY = s.dilateToleranceY;
		wildcardOptions = s.wildcardOptions;

		//auto turn off grading options if point or function modes are disabled for students
		if (!allowPointEditing && tabType == "response") {
			wildcardOptions.points = false;		//ignore point grading if can't edit points
		}
		if (!allowFunctionEditing && tabType == "response") {
			wildcardOptions.parentFunction = false;	//ignore function and transform grading if can't edit equation
			wildcardOptions.translate = false;
			wildcardOptions.dilate = false;
			wildcardOptions.reflect = false;
		}
		if (tabType != "response") {		//everything set to graded on questions for comparison
			wildcardOptions.points = true;
			wildcardOptions.parentFunction = true;
			wildcardOptions.translate = true;
			wildcardOptions.dilate = true;
			wildcardOptions.reflect = true;
		}
	}

	//when a tab is changed, call this function to save it
	function updateGraphSettings() {	
		graphSettings = saveGraphSettings ( graphSettings );					// <---- CALL THIS WHENEVER TAB IS CHANGED
	}

//SHOW AND HIDE GRADING OPTIONS FOR AUTHORS
	function showGradingOptions() {
		//refresh UI with extra grading options for authors
		gradingPanels._visible = true;
		panels.toolsPanel.myTranslate.translateToleranceXsetting._visible = true;
		panels.toolsPanel.myTranslate.translateToleranceYsetting._visible = true;
		panels.toolsPanel.myDilate.dilateToleranceXsetting._visible = true;
		panels.toolsPanel.myDilate.dilateToleranceYsetting._visible = true;
		
		panels.toolsPanel.myTranslate.translateToleranceXsetting.myInput.text = translateToleranceX;
		panels.toolsPanel.myTranslate.translateToleranceYsetting.myInput.text = translateToleranceY;
		panels.toolsPanel.myDilate.dilateToleranceXsetting.myInput.text = dilateToleranceX;
		panels.toolsPanel.myDilate.dilateToleranceYsetting.myInput.text = dilateToleranceY;
		
		if (myFunction.id != undefined) {
			gradingPanels.translateGrading._visible = true;
			gradingPanels.dilateGrading._visible = true;
			gradingPanels.reflectGrading._visible = true;
		} else {
			gradingPanels.translateGrading._visible = false;
			gradingPanels.dilateGrading._visible = false;
			gradingPanels.reflectGrading._visible = false;
		}
		
		//show range of grading tolerance on graph
		showRanges = true;
		updateGraph();
		
		panels.toolsPanel.myBG._width = 200;
		
		updateEyeButtons();
	}
	function updateEyeButtons() {
		gradingPanels.pointGrading.graded = wildcardOptions.points;
		gradingPanels.functionGrading.graded = wildcardOptions.parentFunction;
		gradingPanels.translateGrading.graded = wildcardOptions.translate;
		gradingPanels.dilateGrading.graded = wildcardOptions.dilate;
		gradingPanels.reflectGrading.graded = wildcardOptions.reflect;
	}

	function hideGradingOptions() {
		//hide all grading options
		gradingPanels._visible = false;
		panels.toolsPanel.myTranslate.translateToleranceXsetting._visible = false;
		panels.toolsPanel.myTranslate.translateToleranceYsetting._visible = false;
		panels.toolsPanel.myDilate.dilateToleranceXsetting._visible = false;
		panels.toolsPanel.myDilate.dilateToleranceYsetting._visible = false;
		
		panels.toolsPanel.myBG._width = 175;
		
		showRanges = false;
		updateGraph();
	}
	
//TOGGLE FUNCTION FOR CLICKING EYE BUTTONS
	function toggleSetting(buttonType:String, id:String, buttonMC:MovieClip) {
		var myVal:Boolean;
		switch (buttonType) {
			case "graded":
				myVal = wildcardOptions[id];
				if (myVal) {
					buttonMC.eye.gotoAndStop(2);
				} else {
					buttonMC.eye.gotoAndStop(1);
				}
				wildcardOptions[id] = !myVal;
				break;
		}
		updateGraph();
	}


//SHOW AND HIDE THE GENERAL QUESTION SETTINGS FOR AUTHORS
	function showGeneralSettings() {
		//show the main settings UI for the author to set
		panels.settingsButton._visible = true;
	}

	function hideGeneralSettings() {
		//hide the main settings UI
		panels.settingsButton._visible = false;
		hideWindowPanel();
	}


	//update graph view
	function updateGraph() {

		myGraphView.drawGridAndAxis();
		myGraphView.plot.clear();
		if (showRanges) {
			//in author mode when tolerances are shown
			if (transformMode == "translate" && wildcardOptions.translate) {	//when Translate panel is open and graded
				myGraphView.drawFunction(myFunction, false, "shiftX", translateToleranceX);
				myGraphView.drawFunction(myFunction, false, "shiftX", -translateToleranceX);
				myGraphView.drawFunction(myFunction, false, "shiftY", translateToleranceY);
				myGraphView.drawFunction(myFunction, false, "shiftY", -translateToleranceY);
			} else if (transformMode == "dilate" && wildcardOptions.dilate) {	//when Dilate panel is open and graded
				myGraphView.drawFunction(myFunction, false, "dilateX", dilateToleranceX);
				myGraphView.drawFunction(myFunction, false, "dilateX", -dilateToleranceX);
				myGraphView.drawFunction(myFunction, false, "dilateY", dilateToleranceY);
				myGraphView.drawFunction(myFunction, false, "dilateY", -dilateToleranceY);
			}
		}
		myGraphView.drawFunction(myFunction, true);
		
		myGraphView.drawPoints();
		
		//update saved transforms
		myFunction.updateTransforms();

		//save anything that has changed
		updateGraphSettings();
		
		//update reset button states
		updateResetButtons();
	}

	//Point or Function Mode
	function switchMode (mode:String, target_mc:MovieClip) {
		for (var b in target_mc.toggleGroup) {
			target_mc.toggleGroup[b].highlighted = false;
		}
		target_mc.drawMode = mode;
		if (mode == "point") {
			target_mc.functionDropDown.disable();
		} else {
			target_mc.functionDropDown.enable();
		}
	}
	
	//Transform Mode
	function switchTransform (mode:String, target_mc:MovieClip) {
		for (var b in target_mc.transformGroup) {
			target_mc.transformGroup[b].highlighted = false;
			target_mc.transformPanels[b]._visible = false;
		}
		target_mc.transformMode = mode;
		switch (mode) {
			case "translate":
				target_mc.transformPanels[0]._visible = true;
				break;
			case "dilate":
				target_mc.transformPanels[1]._visible = true;
				break;
			case "reflect":
				target_mc.transformPanels[2]._visible = true;
				break;
			default:
				break;
		}
		if (target_mc.appMode=="author" && target_mc.tabType == "response") {
			target_mc.updateGraph();	//to show the tolerance ranges
		}
	}
	
	//show or hide Transform Tools
	function showTransformTools() {
		if (myFunction.id != undefined && (allowFunctionEditing || appMode=="author")) {
			//show buttons
			panels.toolsPanel.translateButton._visible = true;
			panels.toolsPanel.dilateButton._visible = true;
			panels.toolsPanel.reflectButton._visible = true;
			
			panels.toolsPanel.resetTranslateButton._visible = true;
			panels.toolsPanel.resetDilateButton._visible = true;
			panels.toolsPanel.resetReflectButton._visible = true;
			
			if (panels.toolsPanel.myWinPanel._visible) {
				hideWindowPanel();
			}
			
			if (appMode=="author" && tabType == "response") {
				//turn on winking buttons if grading options are shown
				showGradingOptions();
			}
		}
	}
	function hideTransformTools() {
		//hide buttons
		panels.toolsPanel.translateButton._visible = false;
		panels.toolsPanel.dilateButton._visible = false;
		panels.toolsPanel.reflectButton._visible = false;
		switchTransform("", this);
		
		//hide reset buttons
		panels.toolsPanel.resetTranslateButton._visible = false;
		panels.toolsPanel.resetDilateButton._visible = false;
		panels.toolsPanel.resetReflectButton._visible = false;
		
		if (appMode=="author" && tabType == "response") {
			//call showGradingOptions to reset eye button visibility
			showGradingOptions();
		}
	}
	
	//function Drop Down Menu
	function updateMenu(myChoice:Number) {
		myFunction.id = myChoice;			//update function choice
		myFunction.getSavedTransforms();	//load any previously saved transforms
		
		//update the transform UIs
		myTranslate.updateDisplay();
		myDilate.updateDisplay();
		myReflect.updateDisplay();
		
		//update the graph
		updateGraph();
		
		showTransformTools();	//show these tools when a function is defined
	}
	
	//show or hide Window Zoom panel
	function showWindowPanel() {
		panels.toolsPanel.myWinPanel.updateDisplay();
		panels.toolsPanel.myWinPanel._visible = true;
		hideTransformTools();
	}
	function hideWindowPanel() {
		panels.toolsPanel.myWinPanel._visible = false;
		showTransformTools();
	}
	
	//reset actions
	function resetPoints(id:String, target_mc:MovieClip) {
		target_mc.myPoints = [];
		target_mc.updateGraph();
	}
	
	function resetFunction(id:String, target_mc:MovieClip) {
		target_mc.myFunction.id = undefined;
		target_mc.functionDropDown.resetAll();
		target_mc.myFunction.resetTransforms();
		target_mc.hideTransformTools();
		target_mc.updateGraph();
	}
	
	function resetTranslate(id:String, target_mc:MovieClip) {
		target_mc.xShift = target_mc.defaultShift;
		target_mc.yShift = target_mc.defaultShift;
		target_mc.myTranslate.updateDisplay();
		target_mc.updateGraph();
	}
	function resetDilate(id:String, target_mc:MovieClip) {
		target_mc.xDilate = target_mc.defaultTransform;
		target_mc.yDilate = target_mc.defaultTransform;
		target_mc.myDilate.updateDisplay();
		target_mc.updateGraph();
	}
	function resetReflect(id:String, target_mc:MovieClip) {
		target_mc.xReflect = target_mc.defaultTransform;
		target_mc.yReflect = target_mc.defaultTransform;
		target_mc.myReflect.updateDisplay();
		target_mc.updateGraph();
	}
	
	var summaryDisplayString:String;	//for the transform summary
	
	function updateResetButtons() {
		summaryDisplayString = "";
		if (myPoints.length) {
			panels.toolsPanel.resetPointsButton.enabled = true;
		} else {
			panels.toolsPanel.resetPointsButton.enabled = false;
		}
		if (myFunction.id != undefined) {
			panels.toolsPanel.resetFunctionButton.enabled = true;
		} else {
			panels.toolsPanel.resetFunctionButton.enabled = false;
		}
		if (xShift == defaultShift && yShift == defaultShift) {
			panels.toolsPanel.resetTranslateButton.enabled = false;
		} else {
			panels.toolsPanel.resetTranslateButton.enabled = true;
			summaryDisplayString += myTranslate.mySummary + "\n";
		}
		if (xDilate == defaultTransform && yDilate == defaultTransform) {
			panels.toolsPanel.resetDilateButton.enabled = false;
		} else {
			panels.toolsPanel.resetDilateButton.enabled = true;
			summaryDisplayString += myDilate.mySummary + "\n";
		}
		if (xReflect == defaultTransform && yReflect == defaultTransform) {
			panels.toolsPanel.resetReflectButton.enabled = false;
		} else {
			panels.toolsPanel.resetReflectButton.enabled = true;
			summaryDisplayString += myReflect.mySummary;
		}
		//at the same time, update the summary text
		transformSummary.text = summaryDisplayString;
		//summaryPanel.updateDisplay();
	}

}