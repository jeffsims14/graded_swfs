class ToggleButton extends SimpleButton {

	private var _highlight:MovieClip;	//selected state

	function ToggleButton() {
		//change the highlight from a border to a fill
		this._highlight.clear();
		drawRoundedRectangle(_highlight, w + 2, h + 2, 10, 0x336699, 50);
		
		//tweak down state
		this._down.clear();
		drawRoundedRectangle(_down, w + 2, h + 2, 10, 0xFFFFFF, 100, 0xCCCCCC, 2, 100);
	}
	
	public function setAction ( clickAction:Function ) {
		this.onPress = function() {
			_up._alpha = 0;
			_over._alpha = 0;
			_down._alpha = 100;
			clickAction( this.id, _parent_mc, this );
		}
	}
}