class ReflectPanel extends MovieClip {

	private var parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	
	var xReflect:Number = 1;		//horizontal reflection
	var yReflect:Number = 1;		//vertical reflection
	
	var horizButton:MovieClip;
	var vertButton:MovieClip;

	function ReflectPanel() {
		d = this.getNextHighestDepth();
		
		horizButton = this.attachMovie("ToggleButton", "horizButton", d++, {w:130, h:20, _x:20, _y:10, id:"horizontal", labelText:"Horizontal Reflection", _parent_mc:this});
		vertButton = this.attachMovie("ToggleButton", "vertButton", d++, {w:130, h:20, _x:20, _y:40, id:"vertical", labelText:"Vertical Reflection", _parent_mc:this});
		
		horizButton.setAction( reflect );
		vertButton.setAction( reflect );
		
		updateDisplay();
	}
	
	function reflect(id:String, target_mc:MovieClip, button_mc:MovieClip) {
		switch (id) {
			case "horizontal":
				target_mc.parent_mc.xReflect *= -1;
				break;
			case "vertical":
				target_mc.parent_mc.yReflect *= -1;
				break;
		}
		target_mc.updateDisplay();
		target_mc.parent_mc.updateGraph();
	}
	
	public var mySummary:String;
	
	function updateDisplay() {
		mySummary = "";
		if (parent_mc.xReflect == -1) {
			horizButton.highlighted = true;
			mySummary = "Horizontal reflection. "
		} else {
			horizButton.highlighted = false;
		}
		
		if (parent_mc.yReflect == -1) {
			vertButton.highlighted = true;
			mySummary += "Vertical reflection."
		} else {
			vertButton.highlighted = false;
		}
	}
}