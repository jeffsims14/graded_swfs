class DilatePanel extends MovieClip {

	private var parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	
	var xDilate:Number;
	var yDilate:Number;
	var xDilatePercentage:Number;
	var yDilatePercentage:Number;
	var mySliderX:MovieClip;
	var mySliderY:MovieClip;

	function DilatePanel() {
		d = this.getNextHighestDepth();
		
		xDilate = parent_mc.xDilate;
		yDilate = parent_mc.yDilate;
		xDilatePercentage = xDilate * 100;
		yDilatePercentage = yDilate * 100;
		
		mySliderX = this.attachMovie("SmallSlider","mySliderX", d++, {_x:12, _y:25, units:"%", titleLabel:"Horizontal"});
		mySliderX.init(500, 0, "xDilatePercentage", 0, false, false);
		
		mySliderY = this.attachMovie("SmallSlider","mySliderY", d++, {_x:12, _y:85, units:"%", titleLabel:"Vertical"});
		mySliderY.init(500, 0, "yDilatePercentage", 0, false, false);
	}
	
	//called when slider is moved
	function update(varName:String) {
		switch (varName) {
			case "xDilatePercentage":
				parent_mc.xDilate = mySliderX.myVal / 100;
				break;
			case "yDilatePercentage":
				parent_mc.yDilate = mySliderY.myVal / 100;
				break;
		}
		updateSummary();
		parent_mc.updateGraph();
	}
	
	//called when tolerance settings are changed
	public function updateSettings(id:String, newVal:String) {
		var newValNum:Number = Number(newVal);
		if (!isNaN(newValNum)) {
			switch (id) {
				case "x":
					parent_mc.dilateToleranceX = newValNum;
					break;
				case "y":
					parent_mc.dilateToleranceY = newValNum;
					break;
			}
			//parent_mc.updateGraphSettings();	
			parent_mc.updateGraph();			//which will also save these settings
		}
	}
	
	public var mySummary:String;
	
	//update all displays
	function updateDisplay() {
		xDilate = parent_mc.xDilate;
		yDilate = parent_mc.yDilate;
		xDilatePercentage = xDilate * 100;
		yDilatePercentage = yDilate * 100;
		
		mySliderX.setHandle(xDilatePercentage);
		mySliderY.setHandle(yDilatePercentage);
		
		updateSummary();
	}
	
	function updateSummary() {
		//xDilate = parent_mc.xDilate;
		//yDilate = parent_mc.yDilate;
		
		var xDilateDisplay = mySliderX.formatDecimals(  mySliderX.correctFloatingPointError(parent_mc.xDilate), 2 );
		var yDilateDisplay = mySliderX.formatDecimals(  mySliderX.correctFloatingPointError(parent_mc.yDilate), 2 )
		
		mySummary = "";
		
		var summaryY:String = "";
		
		if (parent_mc.yDilate > 1) {
			summaryY = "Vertical stretch by a factor of " + yDilateDisplay + ". ";
		} else if (parent_mc.yDilate < 1) {
			summaryY = "Vertical compression by a factor of " + yDilateDisplay + ". ";
		}
		
		var summaryX:String = "";
		if (parent_mc.xDilate > 1) {
			summaryX = "Horizontal stretch by a factor of " + xDilateDisplay + ".";
		} else if (parent_mc.xDilate < 1) {
			summaryX = "Horizontal compression by a factor of " + xDilateDisplay + ".";
		}
		
		if (summaryX == "") {
			mySummary = summaryY;
		} else if (summaryY == "") {
			mySummary = summaryX;
		} else {
			mySummary = summaryX + "\n" + summaryY;
		}
	}
	
}