class GradedSetting extends MovieClip {

	private var id:String;			//id is passed in setAction function
	private var _parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	private var eyeButton:MovieClip;

	function GradedSetting() {
		d = this.getNextHighestDepth();
		
		//eye toggle
		eyeButton = this.attachMovie("eyeButton", "ebutton", d++);
		eyeButton._parent_mc = this._parent._parent;

		eyeButton.onPress = function() {
			_parent_mc.toggleSetting('graded', _parent.id, this);
		}

		//add to settings array
		this._parent._parent.settingsArray.push( this );

		//show current setting
		var mySetting:Boolean = this._parent._parent.wildcardOptions[ id ];
		if (mySetting) {
			eyeButton.eye.gotoAndStop(1);
		} else {
			eyeButton.eye.gotoAndStop(2);
		}
	}
	
	public function set graded(g:Boolean) {
		if (g) {
			eyeButton.eye.gotoAndStop(1);
		} else {
			eyeButton.eye.gotoAndStop(2);
		}
	}
	
}