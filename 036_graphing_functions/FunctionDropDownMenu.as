class FunctionDropDownMenu extends DropDownMenu {

	var dropDownMenu:MovieClip;		//dropdown menu movie clip
	var myChoice:Number;			//numeric value of chosen menu item
	var defaultChoice:Number;		//item to show when first loaded
	var myID:String;				//unique ID for each dropdown
	var my_parent:MovieClip;		//parent movie clip
	var menuItems:Array;			//choices in menu
	var menuEnabled:Boolean;		//whether menu is enabled
	var arr1:MovieClip;				//button MC within Drop Down Menu

	function FunctionDropDownMenu() {
		my_parent = _parent._parent._parent;
		this._xscale = 85;
		this._yscale = 85;

		this.menuItems = [0,1,2,3,4,5,6,7,8,9,10,11,12];
	}

	function updateChoice() {
		//numeric value of menu choice
		myChoice = dropDownMenu.myChoice;
		
		//update the parent (Equation Panel) with new choice
		my_parent.updateMenu(myChoice);
	}
	
	public function disable() {
		this._alpha = 50;
		this.menuEnabled = false;
		dropDownMenu.arr1.useHandCursor = false;
	}
	public function enable() {
		this._alpha = 100;
		this.menuEnabled = true;
		dropDownMenu.arr1.useHandCursor = true;
	}
	
	public function setChoice(id:Number) {
		dropDownMenu.myChoice = id;
		var frameNum = id + 2;
		dropDownMenu.choiceDisplay.gotoAndStop(frameNum);
		//my_parent.updateMenu(id);
	}
	
	public function resetAll() {
		dropDownMenu.myChoice = undefined;
		dropDownMenu.choiceDisplay.gotoAndStop(1);
	}
}