class RadioButton extends MovieClip {

	private var w:Number;			//width
	private var h:Number;			//height
	private var labelText:String;	//button label
	private var id:String;			//id is passed in setAction function
	private var _up:MovieClip;		//button state MCs
	private var _over:MovieClip;
	private var _down:MovieClip;
	private var _highlight:MovieClip;	//selected state
	private var _parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	private var active:Boolean;
	private var roundedCorner:Number = 2;
	private var iconId:String;
	private var upFill:Number = 0xEEEEEE;
	private var upStroke:Number = 0xCCCCCC;
	private var overFill:Number = 0xCCCCCC;
	private var overStroke:Number = 0x666666;
	private var downFill:Number = 0x666666;
	private var downStroke:Number = 0xCCCCCC;
	private var highlightFill:Number = 0xA3C3EF;
	private var highlightStroke:Number = 0x666666;
	private var bg:MovieClip;

	function RadioButton() {
		//create button state MCs
		d = this.getNextHighestDepth();
		bg._width = w;
		bg._height = h;
		

		//default button actions
		this.onRollOver = function() {
			if (this.active) {
				bg._alpha = 100;
			}
			
		}
		this.onRollOut = function() {
			bg._alpha = 0;
		}
		this.onPress = function() {
			bg._alpha = 100;
		}
		this.onRelease = this.onReleaseOutside = function() {
			bg._alpha = 0;
		}

		//button label
		if (labelText!= undefined) {
			var buttonlabel:TextField = this.createTextField("buttonlabel", d++, 8, 2, w, h);

			var panelFormat = new TextFormat();
			panelFormat.font = 'Arial';
			panelFormat.color = 0x000000;
			panelFormat.size = 12;
			panelFormat.align = 'center';

			buttonlabel.setNewTextFormat(panelFormat);
			buttonlabel.selectable = false;
			buttonlabel.html = true;
			buttonlabel.htmlText = labelText;
			buttonlabel.embedFonts = true;

			bg._width = buttonlabel._width + 3;
		}
		//buttonlabel.autoSize = true;

		if (!_parent_mc) {
			_parent_mc = this._parent;
		}
		
		if (iconId) {
			setIcon(iconId);
		}
	}

	//set the onPress action in addition to default button states
	public function setAction ( clickAction:Function ) {
		this.onPress = function() {
			bg._alpha = 100;
			clickAction( this.id, _parent_mc );
			this.highlighted = true;
		}
	}

	//make a simple icon button
	public function setIcon ( id:String ) {
		this.attachMovie(id, "myIcon", d++, {_x:w/2, _y:h/2});
	}

	//turn highlight on or off
	public function set highlighted( hl:Boolean ) {
		if (hl) {
			gotoAndStop('on');
		} else {
			gotoAndStop('off');
		}
	}
	
	public function set enabled( enable:Boolean ) {
		if (enable) {
			this._alpha = 100;
			this.useHandCursor = true;
		} else {
			this._alpha = 15;
			this.useHandCursor = false;
		}
		this.active = enable;
	}
}