import com.motiondraw.LineGeneralization;

import flash.geom.Point;

class CustomShape extends MovieClip {
	
	private var startPoint:Point;			//start of drawing
	private var shapeMC:MovieClip;
	private var myCoords:Array;			//freehand coordinates
	private var endPoint:Point;			//end of drawing for square and circle
	
	private var parent_mc:MovieClip;	//main module
	
	private var myStroke:Number;
	private var myLineColor:Number;
	private var myFillColor:Number;
	private var myAlpha:Number;
	private var myFillAlpha:Number;
	
	private var myDraw:DrawingFunctions;
	private var myMode:Number;
	private var d:Number;
	private var DrawMC:MovieClip;	//DrawingArea mc
	private var cutout:Boolean = false;
	
	private var closeShapeButton:MovieClip;
	private var closeShapeButtonOver:MovieClip;
	private var classref;
	
	public var id:Number;
	public var hasVolume:Boolean;
	/*
	drawing modes: 	0 = freehand
					1 = polygon
					2 = rectangle
					3 = circle
					4 = delete
					5 = move
	*/
	var lineGeneral:LineGeneralization;
	
	function CustomShape() {

		classref = this;
		myDraw = new DrawingFunctions;
		lineGeneral = new LineGeneralization();
		
	}

	public function init(mc:MovieClip, x:Number, y:Number, myID:Number, parentMC:MovieClip) {
		d = mc.getNextHighestDepth();
		DrawMC = mc;
		shapeMC.id = myID;
		id = myID;
		startPoint = new Point(x, y);
		shapeMC = mc.createEmptyMovieClip("shape"+id, d++);
		parent_mc = parentMC;
		updateColors();
		hasVolume = false;
	}

	public function get myData():Object {
		var o:Object = new Object();

		o.startPoint = startPoint;
		o.endPoint = endPoint;
		o.myCoords = myCoords;
		o.myMode = myMode;
		o.id = id;
		o.cutout = cutout;

		return o;
	}

	public function reload(m) {
		shapeMC = DrawMC.createEmptyMovieClip("shape"+id, DrawMC.d++);
	}
	
	/*public function set startPoint(p:Point) {
		this.myStart = p;
	}
	
	public function get startPoint():Point {
		return this.myStart;
	}*/
	
	public function get myShape():MovieClip {
		return this.shapeMC;
	}
	
	public function freehand(x:Number, y:Number) {
		myMode = 0;	
		var nextP:Point = new Point(x, y);
		if (!myCoords.length) {
			myCoords = new Array();
		}
		myCoords.push( nextP );
	}
	
	public function polygon(x:Number, y:Number) {
		myMode = 1;	
		var nextP:Point = new Point(x, y);
		if (!myCoords.length) {
			myCoords = new Array();
			myDraw.drawCircle(shapeMC, x, y, 2, myFillColor, myFillAlpha, myStroke, myLineColor);
		}
		endPoint = nextP;
	}
	public function beginPolygon(x:Number, y:Number) {	//starting polygon shape
		myMode = 1;	
		myCoords = new Array();
		//create starting point MC -- clicking it will close polygon
		closeShapeButton = shapeMC.createEmptyMovieClip("closeShapeButton", d++);
		closeShapeButton.parent_mc = parent_mc.myDrawing;
		drawPolygon(false);
	}
	
	public function polygonAddAnchor(x:Number, y:Number) {
		var nextP:Point = new Point(x, y);
		myCoords.push( nextP );
		
		if (myCoords.length == 1) {
			//when at least one coordinate added, set click action for closeShapeButton
			closeShapeButtonOver = shapeMC.createEmptyMovieClip("closeShapeButtonOver", d++);
			closeShapeButtonOver.lineStyle(myStroke, myLineColor, myAlpha, true, "normal", "round", "round");
			myDraw.drawCircle(closeShapeButtonOver, startPoint.x, startPoint.y, 6, 0x81ADD9, 100, myStroke, myLineColor);
			closeShapeButtonOver._alpha = 0;
			
			closeShapeButton.onRollOver = function() {
				_parent.closeShapeButtonOver._alpha = 100;
			}
			closeShapeButton.onRollOut = function() {
				_parent.closeShapeButtonOver._alpha = 0;
			}
			closeShapeButton.onRelease = function() {
				_parent.closeShapeButtonOver._alpha = 0;
				parent_mc.endDraw();
			}
		}
	}
	
	public function rectangle(x:Number, y:Number) {
		myMode = 2;
		endPoint = new Point(x, y);
	}
	
	var keyListener:Object;
	var shiftDown:Boolean;
	public function circle(x:Number, y:Number) {
		myMode = 3;
		endPoint = new Point(x, y);
		
		//keyboard listener for SHIFT key to make circle
		if (!keyListener) {
			keyListener = new Object();
			keyListener.onKeyDown = function() {
				trace ('keydown');
				if (Key.isDown(Key.SHIFT)) {
					shiftDown = true;
				}
			};
			keyListener.onKeyUp = function() {
				shiftDown = false;
				drawCircle(false);
			};
			if (Key.isDown(Key.SHIFT)) {
				keyListener.shiftDown = true;
			}
			Key.addListener(keyListener);
		}
	}
	public function removeListener() {
		Key.removeListener(keyListener);
		shiftDown = false;
	}
	
	public function drawMe() {
		
		switch (myMode) {
			case 0:
				drawFreehand(false);
				break;
			case 1:
				drawPolygon(false);
				break;
			case 2:
				drawRectangle(false);
				break;
			case 3:
				drawCircle(false);
				break;
		}
	}
	
	public function drawFreehand(filled:Boolean) {
		shapeMC.clear();
		shapeMC.lineStyle(myStroke, myLineColor, myAlpha, true, "normal", "round", "round");
		
		if (filled) {
			shapeMC.beginFill(myFillColor, myFillAlpha);
			shapeMC.lineStyle(myStroke, myLineColor, myAlpha, true, "normal", "round", "round");
			lineGeneral.drawCurveNPts(shapeMC, myCoords);
		} else {
			shapeMC.moveTo(startPoint.x, startPoint.y);
			for (var i:Number=0; i< myCoords.length; i++) {
				shapeMC.lineTo(myCoords[i].x, myCoords[i].y);
			}
		}
	}
	public function closeFreehand() {
		//connect end to start
		myCoords.push( startPoint );
		if (myCoords != undefined) {
			hasVolume = true;
		}
		
		//apply smoothing and simplifying to line
		myCoords = lineGeneral.simplifyLang(5, 2, myCoords);
		myCoords = lineGeneral.smoothMcMaster(myCoords);
		
		//draw completed shape
		drawFreehand(true);
	}
	public function drawPolygon(filled:Boolean) {
		shapeMC.clear();
		shapeMC.lineStyle(myStroke, myLineColor, myAlpha, true, "normal", "round", "round");
		
		
		if (filled) {
			shapeMC.beginFill(myFillColor, myFillAlpha);
			shapeMC.lineStyle(myStroke, myLineColor, myAlpha, true, "normal", "round", "round");
		} else {
			//shapeMC.beginFill(myFillColor, 0);
			closeShapeButton.clear();
			closeShapeButton.lineStyle(myStroke, myLineColor, myAlpha, true, "normal", "round", "round");
			myDraw.drawCircle(closeShapeButton, startPoint.x, startPoint.y, 4, 0x81ADD9, 100, myStroke, myLineColor);
		}
		shapeMC.moveTo(startPoint.x, startPoint.y);
		for (var i:Number=0; i< myCoords.length; i++) {
			shapeMC.lineTo(myCoords[i].x, myCoords[i].y);
		}
		if (!filled) {
			shapeMC.lineTo(endPoint.x, endPoint.y);
		} else {
			shapeMC.endFill();
		}
	}
	
	public function closePolygon() {
		var closingPoint = new Point(startPoint.x, startPoint.y);
		myCoords.push( closingPoint );
		if (myCoords != undefined  && myCoords.length > 2) {
			hasVolume = true;
		}
		closeShapeButton.removeMovieClip();
		drawPolygon(true);
	}
	
	public function drawRectangle(filled:Boolean) {
		var p0:Point = startPoint;
		var p1:Point = endPoint;
		
		if (p1 == undefined) {
			return;
		} else {
			hasVolume = true;
		}
		
		shapeMC.clear();
		if (filled) {
			shapeMC.beginFill(myFillColor, myFillAlpha);
		} else {
			shapeMC.beginFill(myFillColor, 0);
		}
		shapeMC.lineStyle(myStroke, myLineColor, myAlpha, true, "normal");
		shapeMC.moveTo(p0.x, p0.y);
		shapeMC.lineTo(p1.x, p0.y);
		shapeMC.lineTo(p1.x, p1.y);
		shapeMC.lineTo(p0.x, p1.y);
		shapeMC.endFill();
	}
	
	var angleDelta = Math.PI / 4;
	
	public function drawCircle(filled:Boolean) {
		var p0:Point = startPoint;
		var p1:Point = endPoint;
		
		if (p1 == undefined) {
			return;
		} else {
			hasVolume = true;
		}
		
		var xRadius:Number = p1.x - p0.x;
		var yRadius:Number = p1.y - p0.y;
		var x:Number = p0.x;
		var y:Number = p0.y;
		
		//hold down SHIFT for circle
		if (keyListener.shiftDown) {
			if (yRadius > xRadius) {
				xRadius = yRadius;
				endPoint.x  = p0.x + xRadius;
			} else {
				yRadius = xRadius;
				endPoint.y  = p0.y + yRadius;
			}
		}
		
		var xCtrlDist = xRadius/Math.cos(angleDelta/2);
		var yCtrlDist = yRadius/Math.cos(angleDelta/2);
		
		shapeMC.clear();
		shapeMC.lineStyle(myStroke, myLineColor, myAlpha, true, "normal");
		
		shapeMC.moveTo(x + xRadius, y);
		
		if (filled) {
			shapeMC.beginFill(myFillColor, myFillAlpha);
		} else {
			shapeMC.beginFill(myFillColor, 0);
		}
		
		var rx, ry, ax, ay;
		var angle = 0;
		
		for (var i = 0; i < 8; i++) {
			angle += angleDelta;
			rx = x + Math.cos(angle-(angleDelta/2))*(xCtrlDist);
			ry = y + Math.sin(angle-(angleDelta/2))*(yCtrlDist);
			ax = x + Math.cos(angle)*xRadius;
			ay = y + Math.sin(angle)*yRadius;
			shapeMC.curveTo(rx, ry, ax, ay);
		}
		
		shapeMC.endFill();
	}
	
	public function clickToDelete() {
		shapeMC.parent_mc = parent_mc.myDrawing;
		shapeMC.classref = classref;
		shapeMC.onPress = function() {
			//trace ('id = ' + classref.id)
			parent_mc.removeShape(classref.id);
			//removeMovieClip(this);
		}
	}

	public function clickToCutout() {
		DrawMC.blendMode = "normal";
		shapeMC.blendMode = "normal";

		shapeMC.parent_mc = parent_mc.myDrawing;
		shapeMC.classref = classref;
		shapeMC.onPress = function() {
			if (classref.cutout) {
				classref.cutout = false;
			} else {
				classref.cutout = true;
				this.swapDepths(classref.DrawMC.getNextHighestDepth()); //move to top level
			}
			classref.redraw();
			parent_mc.updateCutouts(id, classref.cutout);			//to save change in authorShapes array
		}
	}
	
	var anchorMC:MovieClip, handleMC:MovieClip;
	
	public function clickToMove(mc) {
		if (myMode == 2 || myMode == 3) {	//if rectangle or oval

			//transform box
			anchorMC = mc.createEmptyMovieClip("anchor"+id, d++);

			var shapeW = Math.abs(startPoint.x - endPoint.x);
			var shapeH = Math.abs(startPoint.y - endPoint.y);

			drawTransformBox(anchorMC);

			handleMC = mc.createEmptyMovieClip("handle"+id, d++);

			handleMC.clear();
			handleMC.lineStyle(0, 0xA3C3EF, 100, true, "normal", "square", "miter");
			handleMC.beginFill(0xA3C3EF, 100);
			handleMC.attachMovie("scaleIcon", "scaleIconMC", d++);

			var handleX1 =  Math.max(startPoint.x, endPoint.x) - handleSize/2 - 1;
			var handleY1 =  Math.max(startPoint.y, endPoint.y) - handleSize/2 - 1;

			if (myMode == 3) {	//if oval
				handleX1 =  Math.max(startPoint.x + shapeW, endPoint.x) - handleSize/2 - 1;
				handleY1 =  Math.max(startPoint.y + shapeH, endPoint.y) - handleSize/2 - 1;
			}
			
			drawBox(handleMC, handleX1, handleY1, handleSize, handleSize);	//lower right handle
			handleMC.endFill();

			handleMC.scaleIconMC._x = handleX1;
			handleMC.scaleIconMC._y = handleY1;

			handleMC.classref = classref;
			var newX, newY, startX, startY;

			var startAnchor, startTransform, endTransform, origStart, origEnd;
			
			handleMC.onPress = function() {
				startDrag(false);
				//begin transform
				startX = _parent._xmouse;
				startY = _parent._ymouse;
				origStart = new Point(classref.startPoint.x, classref.startPoint.y);
				origEnd = new Point(classref.endPoint.x, classref.endPoint.y);

				this.onMouseMove = function() {
					classref.stretch(_parent._xmouse, _parent._ymouse, startX, startY, origStart, origEnd);
				}
			}
			handleMC.onRelease = handleMC.onReleaseOutside = function() {
				//end transform
				stopDrag();
				delete this.onMouseMove;
				classref.stretch(_parent._xmouse, _parent._ymouse, startX, startY, origStart, origEnd, true);
				//trace (_x +','+_y)
			}
			anchorMC._visible = false;
			handleMC._visible = false;
		}

		shapeMC.classref = classref;
		shapeMC.onPress = function() {
			//trace (id)
			classref.deselectAll();
			startDrag(false);
		}
		shapeMC.onRelease = shapeMC.onReleaseOutside = function() {
			stopDrag();
			classref.updatePosition(_x, _y);
			classref.anchorMC._visible = true;
			classref.handleMC._visible = true;
		}
	}
	private function deselectAll() {
		parent_mc.myDrawing.deselectAll();
	}

	private function stretch(newX, newY, startX, startY, origStart, origEnd, done) {
		var diffX = newX - startX;
		var diffY = newY - startY;

		drawTransformBox(anchorMC);
		updateTransform(origStart, origEnd, diffX, diffY);
		if (done) {
			//on release, check if flipped over x or y, return handle to lower right corner if so
			var origW = Math.abs(origStart.x - origEnd.x);
			var origH = Math.abs(origStart.y - origEnd.y);
			if (myMode == 3) {
				origW *= 2;
				origH *= 2;
			}
			if (diffX * -2 > origW) {
				//flipped over x
				handleMC._x += (diffX*-2 - origW);
			}
			if (diffY * -2 > origH) {
				//flipped over y
				handleMC._y += (diffY*-2 - origH);
			}
		}
	}

	var offsetValues:Array = [2, 4, 6, 26];
	var handleSize:Number = 12;

	private function drawTransformBox(mc) {
		mc.clear();
		mc.lineStyle(1, 0xA3C3EF, 100, true, "normal", "square", "miter");
		if (myMode == 2) {
			//rectangles
			drawBox(mc, Math.min(startPoint.x, endPoint.x), Math.min(startPoint.y, endPoint.y), Math.abs(startPoint.x - endPoint.x), Math.abs(startPoint.y - endPoint.y));
		} else if (myMode == 3) {
			//ovals
			var radiusX = Math.abs(startPoint.x - endPoint.x);
			var radiusY = Math.abs(startPoint.y - endPoint.y);
			drawBox(mc, Math.min(startPoint.x - radiusX, endPoint.x), Math.min(startPoint.y - radiusY, endPoint.y), radiusX*2, radiusY*2);
		}
		
	}

	public function updateTransform(origStart, origEnd, diffX, diffY) {
		if (myMode == 2) {
			if (origStart.x > origEnd.x) {
				diffX *= -1;
			}
			if (origStart.y > origEnd.y) {
				diffY *= -1;
			}
			startPoint.x = origStart.x - diffX;
			startPoint.y = origStart.y - diffY;
			endPoint.x = origEnd.x + diffX;
			endPoint.y = origEnd.y + diffY;
		} else {
			if (origStart.x > origEnd.x) {
				diffX *= -1;
			}
			if (origStart.y > origEnd.y) {
				diffY *= -1;
			}
			endPoint.x = origEnd.x + diffX;
			endPoint.y = origEnd.y + diffY;
		}
		redraw();
	}
	
	public function unclickable() {
		delete shapeMC.onPress;
		delete shapeMC.onRelease;
		delete shapeMC.onReleaseOutside;
		shapeMC.removeMovieClip(anchorMC);
	}
	
	//update all coordinates when dragged to new position, then move shapeMC back to 0,0
	public function updatePosition(x, y) {
		startPoint.x += x;
		startPoint.y += y;
		endPoint.x += x;
		endPoint.y += y;
		for (var i:Number=0; i< myCoords.length; i++) {
			myCoords[i].x += x;
			myCoords[i].y += y;
		}
		redraw();
		shapeMC._x = 0;
		shapeMC._y = 0;
		parent_mc.updateTabSettings();			//save to the parent module

		if (myMode == 2 || myMode == 3) {
			drawTransformBox(anchorMC);
			handleMC._x += x;
			handleMC._y += y;
		}
	}
	
	
	public function updateColors() {
		myStroke = parent_mc.authorStrokeMenu[parent_mc.authorSize];
		myLineColor = parent_mc.authorColorMenu[parent_mc.authorColorLine];
		myFillColor = parent_mc.authorColorMenu[parent_mc.authorColor];
		myAlpha = 100;
		myFillAlpha = parent_mc.authorAlpha;
	}
	
	public function redraw() {
		updateColors();

		if (cutout) {
			myStroke = 1;
			myLineColor = 0xFF0000;
			myFillColor = 0xFFFFFF;
			myFillAlpha = 100;
		}

		switch (myMode) {
			case 0:
				drawFreehand(true);
			break;
			case 1:
				drawPolygon(true);
			break;
			case 2:
				drawRectangle(true);
			break;
			case 3:
				drawCircle(true);
			break;
		}
	}

	public function recreate(o:Object, mc:MovieClip, parentMC:MovieClip) {
		d = mc.getNextHighestDepth();
		DrawMC = mc;
		parent_mc = parentMC;

		hasVolume = true;

		startPoint = o.startPoint;
		endPoint = o.endPoint;
		myCoords = o.myCoords;
		myMode = o.myMode;
		id = o.id;
		cutout = o.cutout;

		shapeMC = mc.createEmptyMovieClip("shape"+id, d++);
		shapeMC.id = id;
		
		redraw();

		if (cutout) {
			mc.blendMode = "layer";
			shapeMC.blendMode = "erase";
		}
	}

	private function drawBox(mc, x, y, w, h) {
		mc.moveTo(x, y);
		mc.lineTo(x+w, y);
		mc.lineTo(x+w, y+h);
		mc.lineTo(x, y+h);
		mc.lineTo(x, y);
	}
}