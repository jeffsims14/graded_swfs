class RadioGroup extends ButtonGroup {
	
	var d:Number;
	var numberOfButtons:Number;
	var parent_mc:MovieClip;	//my parent
	var myGroup:Array;
	var defaultChoice:Number = 0;
	var callback:Function;
	var buttonW:Number = 55;
	var buttonH:Number = 20;
	var maxWidth:Number;
	var labelFormat;
	var labelSet:Array;
	
	function RadioGroup() {
		d = parent_mc.d;
		
		labelFormat = new TextFormat();
		labelFormat.font = 'Arial';
		labelFormat.color = 0x333333;
		labelFormat.size = 12;
		labelFormat.align = 'left';
		
		maxWidth = parent_mc.w;
		
		//header text
		if (label) {
			var headerLabel:TextField = this.createTextField("headerLabel", d++, 0, 5, 200, 20);
			headerLabel.setNewTextFormat(labelFormat);
			headerLabel.selectable = false;
			headerLabel.html = true;
			headerLabel.htmlText = label;
		}
		myGroup = new Array();
		
		var spacer = 2;
		var newx = 0;
		var newy = 24;
		for (var i=0; i<numberOfButtons; i++) {
			if (newx + spacer > maxWidth) {
				newx = 0;
				newy += spacer;
			}
			var newButton = this.attachMovie("RadioButton", "myButton"+i, d++, {w:buttonW, h:buttonH, _x:newx, _y:newy, roundedCorner:0, id:i, labelText:labelSet[i], active:true});
			myGroup.push(newButton);
			newButton.setAction( toggleGroup );
			if (defaultChoice == i) {
				newButton.highlighted = true;	
			}
			newx += newButton._width + spacer;
		}
	}
}