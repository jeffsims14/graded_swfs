import flash.geom.Point;

class ClickableAreaModule extends MovieClip {

	var questionSettings:Object;			//settings saved across the entire question (graph settings, etc.)
	var tabSettings:Object;				//settings saved on each tab (student manipulations, grading options, etc.)
	var wildcardOptions:Object;

	var appMode:String;						//global App mode (author or student)
	var tabType:String;						//global Tab type (question or response)

	var panels:MovieClip, gradingPanels:MovieClip;
	var d = 1;								//movieClip level depth
	var tabNum:Number;						//tab index counter

	var myBackground:MovieClip;
	var backgroundImageName:String;			// = 'bg_testing_big.swf';	//path to background image
	var backgroundScale:Number = 100;		//background _xscale and _yscale
	var backgroundImageWidth:Number;		//determined by uploaded image
	var backgroundImageHeight:Number;
	var backgroundMode:Number = 0;			//whether background is an image or blank (0 = image, 1 = blank)
	
	var imageWidth:Number = 450;			//image dimensions for blank background
	var imageHeight:Number = 350;

	var myDrawing:MovieClip;
	var clickDetect:MovieClip;

	public var studentPoint:Point;			//x,y of student click
	public var authorShapes:Array;			//array of author-drawn shape objects
	
	//line and color settings
	public var dotSize:Number = 0;		//color for dot representing student click
	public var dotColor:Number = 0;		//default color is black
	public var dotColorLine:Number = 4;	//by default dot has no outline
	public var dotAlpha:Number = 50;	//default transparency is 50
	
	public var authorSize:Number = 0;	//color for author-drawn shapes
	public var authorColor:Number = 3;	//default color green
	public var authorColorLine:Number = 3;
	public var authorAlpha:Number = 50;
	
	public var sizeMenu:Array = [4, 8, 12, 16];
	public var strokeMenu:Array = [2, 4, 5, 6];
	public var colorMenu:Array = [0x000000, 0xFFFFFF, 0xFFFF33, 0x336699, null];
	
	public var authorStrokeMenu:Array = [2, 4, 6, 8];
	public var authorColorMenu:Array = [0x000000, 0xFFFFFF, 0xFFFF33, 0x339933];
	public var drawingMode:Number;
	
	//panels open or closed
	var settingsOpen:Boolean = false, drawingToolsOpen:Boolean = true;
	
	var debug:TextField;

	function ClickableAreaModule() {
		init();
		
		//debug = this.createTextField('debug',d++,0,400,50,20);
		//debug.text = 'hello'
	}

// INIT function called on first load

	function init() {
		appMode = _global.core.Globals.appMode;	//get the global app mode ("author" or "student")
		
		//appMode = "author";						//uncomment for testing
		//appMode = "student";

		//movie clips for background image, drawing
		myBackground = this.attachMovie("BackgroundImage", "myBackground", d++, {_x:10, _y:10});
		
		myDrawing = this.attachMovie("DrawingArea", "myDrawing", d++, {parent_mc:this});

		//load bg image
		if (backgroundImageName != undefined) {
			myBackground.loadImage(backgroundImageName);
		}

		//set up for tool panels and grading options if we are in Author App
		if (appMode == "author") {
			
			//movie clips to hold panels
			panels = this.createEmptyMovieClip("panels_mc", d++);
			//gradingPanels = this.createEmptyMovieClip("gradingPanels", d++);
			
			tabNum = 0;
			
			var displayFormat = new TextFormat();
			displayFormat.font = 'Arial';
			displayFormat.color = 0x000000;
			displayFormat.size = 10;
			displayFormat.align = 'left';
			
			//panel for global question settings
			var myConfigPanel = panels.attachMovie("ConfigPanel", "myConfigPanel", d++, {_x:464, _y:22, parent_mc:this, _visible:false});
			
			//panel for response tab drawing
			var myDrawingPanel = panels.attachMovie("DrawingPanel", "myDrawingPanel", d++, {_x:464, _y:22, parent_mc:this, _visible:false});
			
			//set default grading values and any other UI setup for the Author App
			var settingsButton = panels.attachMovie("settings_icon","settingsButton", d++, {_x:565, _y:10});
			settingsButton.onPress = function() {
				_parent._parent.togglePanels();
			}
			
			//panel for tool buttons
			var toolsPanel = panels.attachMovie("Panel", "toolsPanel", d++, {w:175, h:120, _x:350, _y:2, _visible:false});	
		}

		reload();	//reload to load any saved settings
	}

// RELOAD function called every time we switch to a new tab to recall saved data

	function reload() {	
		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)
		//tabType = "response";					//uncomment for testing 
		//tabType = "question";					//uncomment for testing
		//debug.text = tabType;
		//check if saved settings exist
		if (questionSettings == null) {
			//if not, initalize the settings and other objects

			questionSettings = new Object();
			tabSettings = new Object();

			//save the settings for recall in graded swf
			updateQuestionSettings();
			updateTabSettings();

			//continue any other initial setup
			//debug.text = "created new settings";
		} else {
			//Settings exist, so we reload the settings and other stuff
			reloadQuestionSettings( questionSettings );
			reloadTabSettings( tabSettings );
		}

		//update the background
		myBackground.reload();
		
		//update the drawing
		myDrawing.reload();

		//if on a response tab in author app, show grading options
		if (appMode == "author") {

			/*if (tabType == "response") {
				//showGradingOptions();		//extra grading options are shown on response tabs
				
			} else {
				//hideGradingOptions();		//otherwise hide the grading options
			}*/

			//if on a question tab in author app, show general settings UI

			if (tabType == "question") {
				showGeneralSettings();		//general settings are shown on the question tab
			} else {
				hideGeneralSettings();		//otherwise hide the general settings
			}

			//update author UI
			panels.myConfigPanel.reload();
			panels.myDrawingPanel.reload();

		}
	}


//SAVE and RELOAD functions for General Settings and any other Student or Authored Data
	function saveQuestionSettings (s:Object):Object {
		//save any data to an object to be saved and recalled

		//question-wide color settings
		s.dotSize = dotSize;
		s.dotColor = dotColor;
		s.dotColorLine = dotColorLine;
		s.dotAlpha = dotAlpha;

		s.backgroundImageName = backgroundImageName;
		s.backgroundScale = backgroundScale;
		s.backgroundMode = backgroundMode;
		s.imageWidth = imageWidth;
		s.imageHeight = imageHeight;

		return s;
	}
	function reloadQuestionSettings(s:Object) {
		//recall the data from saved object when switching tabs
		
		//question-wide color settings
		dotSize = s.dotSize;
		dotColor = s.dotColor;
		dotColorLine = s.dotColorLine;
		dotAlpha = s.dotAlpha;

		backgroundImageName = s.backgroundImageName;
		backgroundScale = s.backgroundScale;
		backgroundMode = s.backgroundMode;
		imageWidth = s.imageWidth;
		imageHeight = s.imageHeight;

		if (s.backgroundMode == undefined) {
			backgroundMode = 0;
			imageWidth = 450;
			imageHeight = 350;
		}
	}

	//when a setting is changed, call this function to save it
	function updateQuestionSettings() {	
		questionSettings = saveQuestionSettings ( questionSettings );	// <---- CALL THIS WHENEVER SETTINGS ARE CHANGED
	}

	//for other items in the module that need to be saved and recalled
	function saveTabSettings (s:Object):Object {

		s.authorSize = authorSize;
		s.authorColor = authorColor;
		s.authorColorLine = authorColorLine;
		s.authorAlpha = authorAlpha;

		//student click position and author-drawn shapes
		s.authorShapes = new Array();
		s.authorShapes = myDrawing.authorShapes.concat();
		

		if (appMode == "author") {
			//debug.text = String(myDrawing.authorShapes);
			s.studentPoint = undefined;
		} else {
			//var studentX = myDrawing.studentPoint.x;
			//var studentY = myDrawing.studentPoint.y;
			//s.studentPoint = new Point(studentX, studentY);
			s.studentPoint = myDrawing.studentPoint;
		}

		
		return s;
	}
	function reloadTabSettings (s:Object) {

		
		authorSize = s.authorSize;
		authorColor = s.authorColor;
		authorColorLine = s.authorColorLine;
		authorAlpha = s.authorAlpha;

		var authorShapes = new Array();
		authorShapes = s.authorShapes.concat();

		//update old swfs that don't have a cutout property yet
		if (authorShapes[0].cutout == undefined) {
			for (var i=0; i < authorShapes.length; i++) {
				if (authorShapes[i].cutout == undefined) {
					authorShapes[i].cutout = false;
				}
			}
		}

		myDrawing.authorShapes = authorShapes;

		if (appMode == "author") {
			//debug.text = String(myDrawing.authorShapes);
			myDrawing.studentPoint = undefined;
		} else {
			myDrawing.studentPoint = s.studentPoint;
		}
		
		//debug.text = String(myDrawing.studentPoint);
	}

	//when a tab is changed, call this function to save it
	function updateTabSettings() {
		tabSettings = saveTabSettings ( tabSettings );					// <---- CALL THIS WHENEVER TAB IS CHANGED
	}

//SHOW AND HIDE GRADING OPTIONS FOR AUTHORS
	function showGradingOptions() {
		//refresh UI with extra grading options for authors
		
	}
	function updateEyeButtons() {
		
	}

	function hideGradingOptions() {
		
	}
	
//TOGGLE FUNCTION FOR CLICKING EYE BUTTONS
	function toggleSetting(buttonType:String, id:String, buttonMC:MovieClip) {
		var myVal:Boolean;
		switch (buttonType) {
			case "graded":
				myVal = wildcardOptions[id];
				if (myVal) {
					buttonMC.eye.gotoAndStop(2);
				} else {
					buttonMC.eye.gotoAndStop(1);
				}
				wildcardOptions[id] = !myVal;
				break;
		}
	}


//SHOW AND HIDE THE GENERAL QUESTION SETTINGS FOR AUTHORS
	function showGeneralSettings() {
		//on question tab, hide drawing tools and set config panel to hidden at first
		hideDrawingTools();
		hideConfigPanel();
		settingsOpen = false;
	}

	function hideGeneralSettings() {
		//on solution or response tabs, hide general settings and show drawing tools
		hideConfigPanel();
		if (drawingToolsOpen) {
			showDrawingTools();
		}
	}
	
	function showDrawingTools() {
		panels.myDrawingPanel._visible = true;
	}
	
	function hideDrawingTools() {
		panels.myDrawingPanel._visible = false;
	}

//show or hide Config Settings panel
	function showConfigPanel() {
		panels.myConfigPanel._visible = true;
	}
	function hideConfigPanel() {
		panels.myConfigPanel._visible = false;
	}

//when settings Icon is clicked, toggle panel visibility
	function togglePanels() {
		if (tabType == "question") {
			if (settingsOpen) {
				hideConfigPanel();
				settingsOpen = false;
			} else {
				showConfigPanel();
				settingsOpen = true;
			}
		} else {
			if (drawingToolsOpen) {
				hideDrawingTools();
				drawingToolsOpen = false;
			} else {
				showDrawingTools();
				drawingToolsOpen = true;
			}
		}
	}
}