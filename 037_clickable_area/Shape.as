import flash.geom.Point;

class Shape extends MovieClip {
	
	private var _start:Point;
	
	function Shape(x:Number, y:Number) {
		startPoint = new Point(x, y);
	}
	
	public function set startPoint(p:Point) {
		this._start = p;
	}
	
	public function get startPoint():Point {
		return this._start;
	}
	
}