class Panel extends MovieClip {

	private var w:Number;
	private var h:Number;
	private var fillColor:Number = 0xE0DED0;
	private var fillAlpha:Number = 100;
	private var lineColorA:Number = 0xAAAAAA;
	private var lineColorB:Number = 0x666666;
	private var lineAlpha:Number = 50;
	private var stroke:Number = 1;
	public var myBG:MovieClip;
	private var d:Number;
	private var tabNum:Number = 0;

	function Panel() {
		myBG = this.createEmptyMovieClip("myBG", this.getNextHighestDepth());
		myBG.beginFill(fillColor, fillAlpha);
		myBG.lineStyle(stroke, lineColorA, lineAlpha, true, "none");
		myBG.moveTo(w, 0);
		myBG.lineTo(0, 0);
		myBG.lineTo(0, h);
		myBG.lineStyle(stroke, lineColorB, lineAlpha, true, "none");
		myBG.lineTo(w, h);
	}
	function createInputField(mc:MovieClip, txtFormat:TextFormat, labelText:String, x:Number, y:Number, w:Number, h:Number):TextField {
		var inputField:TextField = mc.createTextField("inputField", d++, x, y, w, h);
		inputField.setNewTextFormat(txtFormat);
		inputField.selectable = true;
		inputField.autoSize = false;
		inputField.type = "input";
		//inputField.maxChars = 36;
		inputField.restrict = "0-9 \u2013 \\-\\.\\x\\+\\/";
		inputField.background = true;
		inputField.backgroundColor = 0xFFFFFF;
		inputField.border = true;
		inputField.borderColor = 0x999999;
		inputField.html = true;
		inputField.htmlText = labelText;
		inputField.tabIndex = tabNum++;
		return inputField;
	}

}