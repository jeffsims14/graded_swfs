import flash.geom.Point;
import helpers.VarHelpers;

class DrawingArea extends MovieClip {

	var d:Number;	//MovieClip depth
	var parent_mc:MovieClip;	//main module

	var clickDetect:MovieClip;		//MovieClip to listen for clicks

	var canvas:MovieClip;			//MovieClip for drawing shapes
	var canvasDepth:Number;

	var hiddenCanvas:MovieClip;		//invisible MovieClip for drawing shapes and testing for point overlap

	var myWidth:Number = 600;
	var myHeight:Number = 400;
	var borderSize = 10;
	var appMode:String;
	var tabType:String;
	var drawingMode:Number = 0;

	var dotHandle:MovieClip;		//for showing student click
	
	var dotSize:Number;
	var dotColor:Number;
	var dotColorLine:Number;
	var dotAlpha:Number;
	var dotStroke:Number;
	
	var authorSize:Number;
	var authorColor:Number;
	var authorColorLine:Number;
	var authorAlpha:Number;
	
	var sizeMenu:Array;
	var strokeMenu:Array;
	var colorMenu:Array;
	
	var authorStrokeMenu:Array;
	var authorColorMenu:Array;
	
	/*
		drawing modes: 	0 = freehand
						1 = polygon
						2 = rectangle
						3 = circle
						4 = move
						5 = delete
						6 = cutout
	*/
	var authorModes:Array;
	var myDraw:DrawingFunctions;
	var studentPoint:Point;
	var authorShapes:Array;
	var shapeInProgress:CustomShape;
	var polygonDrawing:Boolean = false;
	var totalMCs:Number = 0;
	var testShape;
	//var testPoint;

	function DrawingArea() {
		init();
	}

	function init() {
		d = _parent.d;
		appMode = _parent.appMode;
		myDraw = new DrawingFunctions;
		
		sizeMenu = parent_mc.sizeMenu;
		strokeMenu = parent_mc.strokeMenu;
		colorMenu = parent_mc.colorMenu;
		
		//area for click detection
		clickDetect = this.createEmptyMovieClip("clickDetect", d++);
		drawClickDetect();

		//hidden area for overlap detection
		hiddenCanvas = this.createEmptyMovieClip("hiddenCanvas", d++);
		hiddenCanvas._alpha = 0;
		
		//area for drawing
		canvas = this.createEmptyMovieClip("canvas", d++);
		
		if (appMode == "author") {
			clickDetect.useHandCursor = false;
			studentPoint = undefined;
		} else {
			//rollover highlight for student view
			clickDetect.onRollOver = function() {
				_parent.parent_mc.myBackground.borderHighlight._alpha = 100;
			}
			clickDetect.onRollOut = function() {
				_parent.parent_mc.myBackground.borderHighlight._alpha = 0;
			}
		}
		clickDetect.onPress = function() {
			if (_parent.polygonDrawing) {
				_parent.addToPolygon();
			} else {
				_parent.beginDraw();
			}
		}
		clickDetect.onRelease = clickDetect.onReleaseOutside = function() {
			if (!_parent.polygonDrawing) {
				_parent.endDraw();
			}
		}

		authorShapes = new Array();
		
		dotHandle = this.createEmptyMovieClip("dotHandle", d++);
		dotHandle.onPress = function() {
			startDrag(dotHandle, false, -_parent._xmouse + _parent.borderSize, -_parent._ymouse + _parent.borderSize, _parent.myWidth - _parent._xmouse+ _parent.borderSize, _parent.myHeight - _parent._ymouse+ _parent.borderSize);

		}
		dotHandle.onRelease = dotHandle.onReleaseOutside = function() {
			stopDrag();
			_parent.updateDotPosition(_x, _y);
		}

		//________for testing overlap function___________
		//studentPoint = new Point(10, 10);
		//animateDot(dotHandle, studentPoint);
		//_______________________________________________
	}

	public function drawClickDetect() {
		//get size of background image
		if (parent_mc.backgroundImageWidth && parent_mc.backgroundImageHeight) {
			myWidth = parent_mc.backgroundImageWidth;
			myHeight = parent_mc.backgroundImageHeight;
		}

		clickDetect.clear();
		clickDetect.beginFill(0xFFFFFF, 0);
		clickDetect.moveTo(borderSize, borderSize);
		clickDetect.lineTo(myWidth + borderSize, borderSize);
		clickDetect.lineTo(myWidth + borderSize, myHeight + borderSize);
		clickDetect.lineTo(borderSize, myHeight + borderSize);
		clickDetect.endFill();
	}

	public function clearCanvas() {
		canvasDepth = canvas.getDepth();
		canvas.removeMovieClip(this);
		canvas = this.createEmptyMovieClip("canvas", canvasDepth);
	}
	public function clearHiddenCanvas() {
		var hiddenCanvasDepth = hiddenCanvas.getDepth();
		hiddenCanvas.removeMovieClip(this);
		hiddenCanvas = this.createEmptyMovieClip("hiddenCanvas", hiddenCanvasDepth);
	}

	public function reload() {

		if (appMode == "author") {
			if (_parent.tabType == "question") {
				authorShapes = [];
			}
			redrawCanvas();
			shapeId = getHighestId();
		} else {
			clearCanvas();
			if (_parent.tabType == "solution") {
				redrawCanvas();
			}
			updateStudentPoint();
		}
	}
	
	public function changeTool(t:Number) {
		if (drawingMode==1 && polygonDrawing) {
			//if polygon in progress, close it
			addToPolygon();
			endDraw();
		}
		drawingMode = t;
		
		switch (t) {
			case 0:
			case 1:
			case 2:
			case 3:
				//drawing tools, remove any click actions (move or delete)
				redrawCanvas();
			break;
				//editing tools
			case 4:
				//move
				shapesMoveable();
			break;
			case 5:
				//delete
				shapesDeletable();
			break;
			case 6:
				//cutout
				shapesCutout();
		}
		
	}
	
	var shapeId:Number = 0;
	public function beginDraw() {
		if (appMode == "author" && _parent.tabType != "question") {
			
			if (drawingMode >= 4) {
				return;	//if moving or deleting, stop here
			}
			
			//begin new shape
			var newShape = new CustomShape;
			newShape.init(canvas, this._xmouse, this._ymouse, shapeId++, parent_mc);
			//authorShapes.push(newShape);
			totalMCs++;
			
			if (drawingMode==1) {
				newDrawing(newShape);	//add point for each click on polygon
				newShape.beginPolygon(this._xmouse, this._ymouse);
				polygonDrawing = true;
			}
			this.onMouseMove = function() {
				newDrawing(newShape);
			}
			shapeInProgress = newShape;
		} else if (appMode != "author") {
			studentClick();
		}
	}

	public function newDrawing(myShape:CustomShape) {
		switch (drawingMode) {
			case 0:		//freehand
				myShape.freehand(this._xmouse, this._ymouse);
				break;
			case 1:		//polygon
				myShape.polygon(this._xmouse, this._ymouse);
				//myShape.drawMe();
				break;
			case 2:		//rectangle
				myShape.rectangle(this._xmouse, this._ymouse);
				break;
			case 3:		//circle
				myShape.circle(this._xmouse, this._ymouse);
				break;
			case 4:		//move
				
				break;
			case 5:		//delete
				
				break;
		}
		myShape.drawMe();
	}

	public function endDraw() {
		if (appMode == "author" && _parent.tabType != "question") {
			this.onMouseMove = undefined;
			switch (drawingMode) {
				case 0:		//freehand
					shapeInProgress.closeFreehand();
					break;
				case 1:		//polygon
					shapeInProgress.closePolygon();
					polygonDrawing = false;
					break;
				case 2:		//rectangle
					shapeInProgress.drawRectangle(true);
					break;
				case 3:		//circle
					shapeInProgress.drawCircle(true);
					shapeInProgress.removeListener();
					break;
			}
			//trace ('hasVolume ' + shapeInProgress.hasVolume);

			if (shapeInProgress.hasVolume) {
				//add Shape data to Shape Array
				authorShapes.push( shapeInProgress.myData );
				authorShapes.sortOn("cutout");			//this moves cutout shapes to end of array so they are applied last


				//automatically switch to select mode after using any other tool
				if (drawingMode != 4) {
					enterSelectMode();
				}

			} else {
				//don't create the empty shape
				shapeId = getHighestId();	//don't increment shapeId
			}

			redrawCanvas();
			
			//trace ('shapes = ' + authorShapes);

			shapeInProgress = undefined;

			parent_mc.updateTabSettings();			//save to the parent module
		} else {
			dotHandle.onRelease();	//this is called when student click goes right into a drag and then release
		}
	}

	public function redrawCanvas() {
		clearCanvas();

		for (var i=0; i<authorShapes.length; i++) {
			var recreatedShape = new CustomShape();
			recreatedShape.recreate(authorShapes[i], canvas, parent_mc);
		}

		//parent_mc.debug.text = authorShapes.length + ' shapes';
		if (drawingMode >= 4) {
			changeTool(drawingMode);				//reapply current drawing tool for select and delete modes
		}
	}

	public function enterSelectMode() {
		changeTool(4);
		parent_mc.panels.myDrawingPanel.toolButtons.setChoice(4);
	}

	//shape editing functions: add click actions to MOVE or DELETE
	private function shapesMoveable() {
		clearCanvas();
		for (var i=0; i<authorShapes.length; i++) {
			var recreatedShape = new CustomShape();
			recreatedShape.recreate(authorShapes[i], canvas, parent_mc);
			recreatedShape.clickToMove(canvas);
		}
	}
	private function shapesDeletable() {
		clearCanvas();
		for (var i=0; i<authorShapes.length; i++) {
			var recreatedShape = new CustomShape();
			recreatedShape.recreate(authorShapes[i], canvas, parent_mc);
			recreatedShape.clickToDelete();
		}
	}
	private function shapesCutout() {
		clearCanvas();
		for (var i=0; i<authorShapes.length; i++) {
			var recreatedShape = new CustomShape();
			recreatedShape.recreate(authorShapes[i], canvas, parent_mc);
			recreatedShape.clickToCutout();
		}
		authorShapes.sortOn("cutout");			//this moves cutout shapes to end of array so they are applied last
	}
	public function updateCutouts(id, cutout) {
		for (var i=0; i<authorShapes.length; i++) {
			if (authorShapes[i].id == id) {
				authorShapes[i].cutout = cutout;
			}
		}
		authorShapes.sortOn("cutout");			//this moves cutout shapes to end of array so they are applied last
		parent_mc.updateTabSettings();			//save to the parent module
		enterSelectMode();						//switch to select mode after deleting
	}

	public function deselectAll() {
		for (var i=0; i<=getHighestId(); i++) {
			canvas["anchor"+i]._visible = false;
			canvas["handle"+i]._visible = false;
		}
	}
	
	//check if shape contains point
	public function overlapping(p:Point, s:CustomShape):Boolean {
		//parent_mc.debug.text += '\npoint: ' + p;
		//parent_mc.debug.text += '\nmyShape xy: ' + s.startPoint.x +','+ s.startPoint.y;
		hiddenCanvas.localToGlobal(p);
		var hit:Boolean = s.myShape.hitTest(p.x, p.y, true);
		return hit;
	}

	//check a Shape Data Object for overlap with point
	public function checkForOverlap(checkPoint:Point, shapeObject:Object, stroke:Number):Boolean {
		
		//hiddenCanvas.globalToLocal(checkPoint);
		var testShape = new CustomShape();
		var testPoint = new Point(checkPoint.x, checkPoint.y);
		//use the authored stroke size
		parent_mc.authorSize = stroke;

		testShape.recreate(shapeObject, hiddenCanvas, parent_mc);
		
		var overlap:Boolean = overlapping(testPoint, testShape);
		clearHiddenCanvas();
		//return true;
		return overlap;
	}
	
	public function addToPolygon() {
		shapeInProgress.polygonAddAnchor(this._xmouse, this._ymouse);
	}
	
	public function removeShape(id:Number) {
		for (var i=0; i<authorShapes.length; i++) {
			if (id == authorShapes[i].id) {
				//var deleteMC = authorShapes[i].shapeMC;
				var deleteId = i;
			}
		}
		authorShapes.splice(deleteId, 1);

		if (drawingMode == 5) {
			shapesDeletable();
		} else {
			redrawCanvas();
		}
		shapeId = getHighestId();

		parent_mc.updateTabSettings();			//save to the parent module
		enterSelectMode();	//switch to select mode after deleting
	}
	
	public function studentClick() {
		if (studentPoint) {
			studentPoint.x = this._xmouse;
			studentPoint.y = this._ymouse;
		} else {
			studentPoint = new Point(this._xmouse, this._ymouse);
		}
		animateDot(dotHandle, studentPoint);

		dotHandle.onPress();	//begin dragging after dot is created

		parent_mc.updateTabSettings();			//save to the parent module
	}
	
	var growInterval:Number;
	var growCount:Number = 0;
	var growMax:Number = 10;
	var growDuration:Number = 10;
	var tweenArray = [.44, .52, .63, .75, .9, 1.07, 1.28, 1.5, 1.21, 1.07, 1];
					//[.44, .55, .67, .85, 1, 1.22, 1.46, 1.78, 1.34, 1.08, 1];
	var dotLineAlpha;
	
	private function animateDot(mc, myPoint) {
		//get size and color settings
		dotSize = parent_mc.sizeMenu[parent_mc.dotSize];
		dotColor = parent_mc.colorMenu[parent_mc.dotColor];
		dotColorLine = parent_mc.colorMenu[parent_mc.dotColorLine];
		dotAlpha = parent_mc.dotAlpha;
		dotStroke = parent_mc.strokeMenu[parent_mc.dotSize];

		dotLineAlpha = 100;

		if (dotColorLine == null) {
			//no outline, so make outline same color and alpha as fill
			dotLineAlpha = 0;
			dotSize += dotStroke/2;	//adjust size to make up for lost outline
		}
		if (dotColor == null) {
			//no fill, override transparency and set alpha to 0
			dotAlpha = 0;
		}
		
		if(growInterval != null) {
			clearInterval(growInterval);
		}
		growCount = 0;
		growInterval = setInterval(this, "growDot", growDuration, mc, myPoint.x, myPoint.y);
	}
	
	private function growDot(mc:MovieClip, x, y) {
		mc.clear();
		var scale = tweenArray[growCount++];
		myDraw.drawCircle(mc, x, y, dotSize * scale, dotColor, dotAlpha, dotStroke, dotColorLine, dotLineAlpha);
		if(growCount > growMax) {
			clearInterval(growInterval);
		}
	}
	
	public function updateDotPosition(x, y) {
		dotHandle._x = dotHandle._y = 0;
		studentPoint.x += x;
		studentPoint.y += y;
		updateStudentPoint();


		//_______________ the following routine is for testing overlap function ________________
		
		//check for overlap with any shape
		/*clearCanvas();
		var match = false;
		var matchCutout = false;
		for (var i=0; i<authorShapes.length; i++) {
			var testShape = new CustomShape();
			testShape.recreate(authorShapes[i], canvas, parent_mc);
			var testOverlap = checkForOverlap(studentPoint, testShape, parent_mc.authorSize);
			if (testOverlap) {
				match = true;
				if (testShape.cutout) {
					match = false;
					break;
				}
			}
		}

		trace ('overlapping: ' + match);
		redrawCanvas();*/
		//______________________________________________________________________________________


	}
	
	public function updateStudentPoint() {
		dotHandle.clear();
		if (studentPoint == undefined) {
			return;
		}

		dotSize = parent_mc.sizeMenu[parent_mc.dotSize];
		dotColor = parent_mc.colorMenu[parent_mc.dotColor];
		dotColorLine = parent_mc.colorMenu[parent_mc.dotColorLine];
		dotAlpha = parent_mc.dotAlpha;
		dotStroke = parent_mc.strokeMenu[parent_mc.dotSize];

		dotLineAlpha = 100;

		if (dotColorLine == null) {
			//no outline, so make outline same color and alpha as fill
			dotLineAlpha = 0;
			dotSize += dotStroke/2;	//adjust size to make up for lost outline
		}
		if (dotColor == null) {
			//no fill, override transparency and set alpha to 0
			dotAlpha = 0;
		}
		
		myDraw.drawCircle(dotHandle, studentPoint.x, studentPoint.y, dotSize, dotColor, dotAlpha, dotStroke, dotColorLine, dotLineAlpha);
	}
	
	public function updateAuthorShapes() {
		//for (var i=0; i<authorShapes.length; i++) {
		//	authorShapes[i].redraw();
		//}
		redrawCanvas();
	}

	/*public function reloadAuthorShapes() {
		for (var i=0; i<authorShapes.length; i++) {
			//authorShapes[i].reload();
			authorShapes[i].redraw();
		}
	}*/

	var clipboard:Array;

	public function copyShapes() {
		clipboard = helpers.VarHelpers.duplicateArray(authorShapes);
	}

	public function pasteShapes() {
		authorShapes = helpers.VarHelpers.duplicateArray(clipboard);
		//redrawCanvas();
		//shapeId = getHighestId();
		reload();
		parent_mc.updateTabSettings();			//save to the parent module
	}
	
	/* UNDO FUNCTION -- NOT CURRENTLY USED
	public function undo() {
		if (drawingMode==1 && polygonDrawing) {
			//if polygon in progress, close it
			addToPolygon();
			endDraw();
		}
		authorShapes.pop();
		redrawCanvas();
		shapeId = getHighestId();
		parent_mc.updateTabSettings();			//save to the parent module
	}
	*/

	private function getHighestId():Number {
		var highestId = 0;
		for (var i=0; i<authorShapes.length; i++) {
			if (authorShapes[i].id > highestId) {
				highestId = authorShapes[i].id;
			}
		}
		//add 1 which will become next new shape's id
		return highestId + 1;
	}
	
	public function clearAll() {
		//remove last shape
		authorShapes = [];
		redrawCanvas();
		shapeId = 0;
	}

}