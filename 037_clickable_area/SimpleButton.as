class SimpleButton extends MovieClip {

	private var w:Number;			//width
	private var h:Number;			//height
	private var labelText:String;	//button label
	private var id:String;			//id is passed in setAction function
	private var _up:MovieClip;		//button state MCs
	private var _over:MovieClip;
	private var _down:MovieClip;
	private var _highlight:MovieClip;	//selected state
	private var _parent_mc:MovieClip;	//my parent, where actions are sent
	private var d:Number;				//depth
	private var active:Boolean;
	private var roundedCorner:Number = 2;
	private var iconId:String;
	private var upFill:Number = 0xEEEEEE;
	private var upStroke:Number = 0xCCCCCC;
	private var overFill:Number = 0xCCCCCC;
	private var overStroke:Number = 0x666666;
	private var downFill:Number = 0x666666;
	private var downStroke:Number = 0xCCCCCC;
	private var highlightFill:Number = 0xA3C3EF;
	private var highlightStroke:Number = 0x666666;

	function SimpleButton() {
		//create button state MCs
		d = this.getNextHighestDepth();
		_up = this.createEmptyMovieClip('_up', d++);
		_over = this.createEmptyMovieClip('_over', d++);
		_down = this.createEmptyMovieClip('_down', d++);

		drawRoundedRectangle(_up, w + roundedCorner, h + roundedCorner, roundedCorner, upFill, 100, upStroke, 1, 0);
		drawRoundedRectangle(_over, w + roundedCorner, h + roundedCorner, roundedCorner, overFill, 100, overStroke, 1, 100);
		drawRoundedRectangle(_down, w + roundedCorner, h + roundedCorner, roundedCorner, downFill, 100, downStroke, 1, 100);
		_over._alpha = 0;
		_down._alpha = 0;

		//selected highlight
		_highlight = this.createEmptyMovieClip('_highlight', d++);
		drawRoundedRectangle(_highlight, w + roundedCorner, h + roundedCorner, roundedCorner, highlightFill, 95, highlightStroke, 2, 100);
		_highlight._alpha = 0;

		//default button actions
		this.onRollOver = function() {
			if (this.active) {
				_up._alpha = 0;
				_over._alpha = 100;
				_down._alpha = 0;
			}
			
		}
		this.onRollOut = function() {
			_up._alpha = 100;
			_over._alpha = 0;
			_down._alpha = 0;
		}
		this.onPress = function() {
			_up._alpha = 0;
			_over._alpha = 0;
			_down._alpha = 100;
		}
		this.onRelease = this.onReleaseOutside = function() {
			_up._alpha = 100;
			_over._alpha = 0;
			_down._alpha = 0;
		}

		//button label
		if (labelText!= undefined) {
			var buttonlabel:TextField = this.createTextField("buttonlabel", d++, 0, 2, w, h);

			var panelFormat = new TextFormat();
			panelFormat.font = 'Arial';
			panelFormat.color = 0x000000;
			panelFormat.size = 12;
			panelFormat.align = 'center';

			buttonlabel.setNewTextFormat(panelFormat);
			buttonlabel.selectable = false;
			buttonlabel.html = true;
			buttonlabel.htmlText = labelText;
			buttonlabel.embedFonts = true;
		}
		//buttonlabel.autoSize = true;

		if (!_parent_mc) {
			_parent_mc = this._parent;
		}
		
		if (iconId) {
			setIcon(iconId);
		}
	}

	//set the onPress action in addition to default button states
	public function setAction ( clickAction:Function ) {
		this.onPress = function() {
			_up._alpha = 0;
			_over._alpha = 0;
			_down._alpha = 100;
			clickAction( this.id, _parent_mc );
			this.highlighted = true;
		}
	}

	//make a simple icon button
	public function setIcon ( id:String ) {
		this.attachMovie(id, "myIcon", d++, {_x:w/2, _y:h/2});
	}

	//turn highlight on or off
	public function set highlighted( hl:Boolean ) {
		if (hl) {
			_highlight._alpha = 100;
		} else {
			_highlight._alpha = 0;
		}
	}

	public function drawRoundedRectangle(target_mc:MovieClip, boxWidth:Number, boxHeight:Number, cornerRadius:Number, fillColor:Number, fillAlpha:Number, strokeColor:Number, stroke:Number, strokeAlpha:Number):Void {
	    with (target_mc) {
	        beginFill(fillColor, fillAlpha);
	        if (strokeColor != undefined) {
	        	if (stroke == undefined) {
	        		stroke = 1;
	        	}
	        	if (strokeAlpha == undefined) {
	        		strokeAlpha = 100;
	        	}
	        	lineStyle(stroke, strokeColor, strokeAlpha);
	        }
	        moveTo(cornerRadius, 0);
	        lineTo(boxWidth - cornerRadius, 0);
	        curveTo(boxWidth, 0, boxWidth, cornerRadius);
	        lineTo(boxWidth, cornerRadius);
	        lineTo(boxWidth, boxHeight - cornerRadius);
	        curveTo(boxWidth, boxHeight, boxWidth - cornerRadius, boxHeight);
	        lineTo(boxWidth - cornerRadius, boxHeight);
	        lineTo(cornerRadius, boxHeight);
	        curveTo(0, boxHeight, 0, boxHeight - cornerRadius);
	        lineTo(0, boxHeight - cornerRadius);
	        lineTo(0, cornerRadius);
	        curveTo(0, 0, cornerRadius, 0);
	        lineTo(cornerRadius, 0);
	        endFill();
    	}
	}
	
	public function set enabled( enable:Boolean ) {
		if (enable) {
			this._alpha = 100;
			this.useHandCursor = true;
		} else {
			this._alpha = 15;
			this.useHandCursor = false;
		}
		this.active = enable;
	}
}