class BackgroundImage extends MovieClip {

	var d:Number;	//MovieClip depth
	var imageHolder:MovieClip;
	var imgLoader:MovieClipLoader;
	var borderHolder:MovieClip;
	var borderHighlight:MovieClip;
	var xyScale:Number;
	var backgroundImageName:String;
	var backgroundMode;				//whether background is an image or blank (0 = image, 1 = blank)
	var myBlankWidth:Number;
	var myBlankHeight:Number;
	var maxWidth = 594;
	var maxHeight = 394;

	function BackgroundImage() {
		d = _parent.d;
		xyScale = _parent.backgroundScale;
		backgroundMode = _parent.backgroundMode;
		borderHolder = this.createEmptyMovieClip("borderHolder", d++);
		borderHighlight = this.createEmptyMovieClip("borderHighlight", d++);
		imageHolder = this.createEmptyMovieClip("imageHolder", d++);
		myBlankWidth = _parent.imageWidth;
		myBlankHeight = _parent.imageHeight;
	}

	//loading external image file
	public function loadImage(imageName:String) {	
		backgroundImageName = imageName;	
		if (imageName.indexOf(".jpg") > 0 or imageName.indexOf(".jpeg") > 0 or imageName.indexOf(".swf") > 0 or imageName.indexOf(".png") > 0) {
			var loadListener = new Object();
			loadListener.onLoadError = function(target_mc:MovieClip, errorCode:String, httpStatus:Number) {
			//if the asset is unable to load, display an error message
				//trace ('image ' + imageName + ' not found');
				//trace ('error: ' + errorCode);
				//trace ('http status: ' + httpStatus);
				var warn_msg:String = 'The requested image "' + imageName + '" was not found.';
				if (_global.core.Globals.appMode == 'author') {
					warn_msg += "\n\nCheck the file name and be sure the image is uploaded.";
				} else {
					warn_msg += "\n\nPlease contact support@saplinglearning.com.";
				}
				_global.core.Globals.PopUps.createPopup({id:"debugMessage", label:"Background image could not be loaded", moveable:true, buttons:[{label:"OK"}]}, {location:"simple", path: warn_msg}, _root, true);
			}
			//if the asset loads successfully
			loadListener.onLoadInit = function(target_mc:MovieClip):Void {
				target_mc._parent.checkIfImageTooBig();
				target_mc._parent.drawBorder(target_mc._width, target_mc._height);
			}
			/*
			loadListener.onLoadStart = function(target_mc:MovieClip) {
				trace('loading......');
			}*/
			imgLoader = new MovieClipLoader();
			imgLoader.addListener(loadListener);
			imgLoader.loadClip(imageName, imageHolder);
			imageHolder._xscale = xyScale;
			imageHolder._yscale = xyScale;
		}
	}
	
	public function updateBg(url) {
		imgLoader.unloadClip(imageHolder);
		borderHolder.clear();
		loadImage(url);
		_parent.backgroundImageName = url;
		backgroundImageName = url;

	}

	public function updateZoom(scale:Number) {
		imageHolder._xscale = scale;
		imageHolder._yscale = scale;
		if (imageHolder._width > maxWidth || imageHolder._height > maxHeight) {
			var newScale = getMaximumScale(scale);
			scale = newScale;
			_parent.panels.myConfigPanel.scaleInput.text = newScale;
		}
		xyScale = scale;
		drawBorder (imageHolder._width, imageHolder._height);
		_parent.backgroundScale = scale;
	}

	private function getMaximumScale(scale:Number):Number {
		var newScale:Number = scale;
		while (imageHolder._width > maxWidth || imageHolder._height > maxHeight) {
			newScale -= 1;
			imageHolder._xscale = newScale;
			imageHolder._yscale = newScale;
		}
		return newScale;
	}

	private function checkIfImageTooBig() {
		if (imageHolder._width > maxWidth || imageHolder._height > maxHeight) {
			var newScale = getMaximumScale(xyScale);
			xyScale = newScale;
			_parent.panels.myConfigPanel.scaleInput.text = xyScale;
		}
	}

	public function updateBgMode(myMode:Number) {
		_parent.backgroundMode = myMode;
		backgroundMode = myMode;
		if (myMode == 0) {
			//image in background
			updateBg(_parent.backgroundImageName);
		} else {
			//blank background
			imgLoader.unloadClip(imageHolder);
			drawBorder(myBlankWidth, myBlankHeight);
		}
	}

	public function updateWidth(w:Number) {
		myBlankWidth = w;
		_parent.imageWidth = w;
		drawBorder(myBlankWidth, myBlankHeight);
	}
	public function updateHeight(h:Number) {
		myBlankHeight = h;
		_parent.imageHeight = h;
		drawBorder(myBlankWidth, myBlankHeight);
	}
	
	var borderSize = 10;
	public function drawBorder(imageWidth:Number, imageHeight:Number) {
		//draw blue frame
		borderHolder.clear();
		borderHolder.lineStyle(2, 0xA3C3EF, 100);
		borderHolder.beginFill(0xCEE2F1);

		borderHighlight.clear();
		borderHighlight.lineStyle(2, 0xAFD2F0, 100);
		borderHighlight.beginFill(0xE3EFF8);

		drawRectangle(borderHolder, -borderSize,-borderSize, imageWidth + borderSize, imageHeight + borderSize);
		drawRectangle(borderHighlight, -borderSize,-borderSize, imageWidth + borderSize, imageHeight + borderSize);

		if (backgroundMode == 0) {
			//for images, this creates a white background below the image
			//for blank backgrounds, this part will not be filled in
			borderHolder.endFill();
			borderHolder.beginFill(0xFFFFFF);
		}

		borderHolder.lineStyle(1, 0xFFFFFF, 0);
		borderHighlight.lineStyle(1, 0xFFFFFF, 0);

		drawRectangle(borderHolder, 0, 0, imageWidth, imageHeight);
		drawRectangle(borderHighlight, 0, 0, imageWidth, imageHeight);
		
		borderHolder.endFill();
		borderHighlight.endFill();
		borderHighlight._alpha = 0;

		_parent.backgroundImageWidth = imageWidth;
		_parent.backgroundImageHeight = imageHeight;
		_parent.myDrawing.drawClickDetect();	//redraw click detect based on image size
	}

	public function reload() {
		backgroundMode = _parent.backgroundMode;
		myBlankWidth = _parent.imageWidth;
		myBlankHeight = _parent.imageHeight;

		if (backgroundMode==0) {
			if (_parent.backgroundImageName != undefined && backgroundImageName != _parent.backgroundImageName) {
				//only load if defined and different from current image
				xyScale = _parent.backgroundScale;
				loadImage(_parent.backgroundImageName);
			}
		} else {
			if ( myBlankWidth != undefined && myBlankHeight != undefined) {
				drawBorder(myBlankWidth,  myBlankHeight);
			}
		}
	}

	private function drawRectangle(mc, x, y, w, h) {
		mc.moveTo(x, y);
		mc.lineTo(w, y);
		mc.lineTo(w, h);
		mc.lineTo(x, h);
		mc.lineTo(x,y);
	}
	
}