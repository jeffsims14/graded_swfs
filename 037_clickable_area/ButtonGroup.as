class ButtonGroup extends MovieClip {
	
	var d:Number;
	var numberOfButtons:Number;
	var label:String;
	var iconSet:Array;
	var parent_mc:MovieClip;	//my parent
	var myGroup:Array;
	var defaultChoice:Number = 0;
	var callback:Function;
	var buttonSize:Number = 28;
	var maxWidth:Number;
	var labelFormat;
	var extraLabel:String;	//for the additional Tools label
	
	function ButtonGroup() {
		d = parent_mc.d;
		
		labelFormat = new TextFormat();
		labelFormat.font = 'Arial';
		labelFormat.color = 0x333333;
		labelFormat.size = 12;
		labelFormat.align = 'left';
		
		maxWidth = parent_mc.w;

		var xspacer = buttonSize+3;
		var yspacer = buttonSize+22;
		
		//header text
		if (label) {
			var headerLabel:TextField = this.createTextField("headerLabel", d++, 0, 5, 200, 20);
			headerLabel.setNewTextFormat(labelFormat);
			headerLabel.selectable = false;
			headerLabel.html = true;
			headerLabel.htmlText = label;

			if  (extraLabel) {
				var headerLabel2:TextField = this.createTextField("headerLabel2", d++, 0, 5+yspacer, 200, 20);
				headerLabel2.setNewTextFormat(labelFormat);
				headerLabel2.selectable = false;
				headerLabel2.html = true;
				headerLabel2.htmlText = extraLabel;
			}
			
		}
		myGroup = new Array();
		
		var newx = 0;
		var newy = buttonSize-4;
		for (var i=0; i<numberOfButtons; i++) {
			if (newx + xspacer > maxWidth) {
				newx = 0;
				newy += yspacer;
			}
			var newButton = this.attachMovie("SimpleButton", "myButton"+i, d++, {w:buttonSize, h:buttonSize, _x:newx, _y:newy, roundedCorner:0, id:i, iconId:iconSet[i], active:true});
			myGroup.push(newButton);
			newButton.setAction( toggleGroup );
			if (defaultChoice == i) {
				newButton.highlighted = true;	
			}
			newx += xspacer;
		}
	}
	
	public function toggleGroup(id, parent) {
		for (var i=0; i<parent.myGroup.length; i++) {
			parent.myGroup[i].highlighted = false;
		}
		parent.callback( id );
	}
	public function setChoice(c:Number) {
		for (var i=0; i<myGroup.length; i++) {
			if (i==c) {
				myGroup[c].highlighted = true;
			} else {
				myGroup[i].highlighted = false;
			}
		}	
	}
	
	
}