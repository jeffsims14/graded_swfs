class ConfigPanel extends Panel {

	var d:Number;
	
	//background
	private var w:Number = 134;
	private var h:Number = 375;
	private var fillColor:Number = 0xEEEEEE;//0xE0DED0;
	private var fillAlpha:Number = 60;
	private var lineAlpha:Number = 60;

	var parent_mc:MovieClip;	//main module
	var fillSlider:MovieClip;
	
	//default tool settings
	private var dotSize:Number;
	private var dotColor:Number;
	private var dotColorLine:Number;
	private var dotAlpha:Number;
	
	private var sizeMenu:Array;
	private var strokeMenu:Array;
	private var colorMenu:Array;
	
	private var sampleDot:MovieClip;
	private var myDraw:DrawingFunctions;
	private var bgInput:TextField;
	private var scaleInput:TextField;
	private var scaleLabel:TextField;
	private var inputLabel:TextField;
	private var unitsLabel:TextField;
	private var widthInput:TextField;
	private var widthLabel:TextField;
	private var heightInput:TextField;
	private var heightLabel:TextField;

	private var imageWidth:Number;
	private var imageHeight:Number;

	private var sizeButtons;
	private var lineColorButtons;
	private var colorButtons;
	private var backgroundOption;

	private var backgroundMode:Number;

	function ConfigPanel() {

		d = this.getNextHighestDepth();
		
		myDraw = new DrawingFunctions;
		
		var xmargin:Number = 10;
		var ymargin:Number = 0;
		var yArray:Array = [0, 50, 100, 160, 250, 270, 324, 350];
		var spacer:Number = 60;
		
		dotSize = parent_mc.dotSize;
		dotColor = parent_mc.dotColor;
		dotColorLine = parent_mc.dotColorLine;
		dotAlpha = parent_mc.dotAlpha;
		sizeMenu = parent_mc.sizeMenu;
		strokeMenu = parent_mc.strokeMenu;
		colorMenu = parent_mc.colorMenu;
		backgroundMode = parent_mc.backgroundMode;
		imageWidth = parent_mc.imageWidth;
		imageHeight = parent_mc.imageHeight;
		
		//tools
		sizeButtons = this.attachMovie("ButtonGroup", "sizeButtons", d++, {numberOfButtons:sizeMenu.length, label:"Click size", _x:xmargin, _y:yArray[0], iconSet:['iSmall','iMed','iLarge', 'iXL'], callback:setSize, parent_mc:this});
		
		lineColorButtons = this.attachMovie("PalletteGroup", "lineColorButtons", d++, {label:"Outline", _x:xmargin, _y:yArray[1], colorSet:colorMenu, callback:setColorLine, parent_mc:this, buttonSize:22, spacer:2});
		
		colorButtons = this.attachMovie("PalletteGroup", "colorButtons", d++, {label:"Fill", _x:xmargin, _y:yArray[2], colorSet:colorMenu, callback:setColor, parent_mc:this, buttonSize:22, spacer:2});
		
		fillSlider = this.attachMovie("SmallSlider","fillSlider", d++, {_x:xmargin+8, _y:yArray[3], titleLabel:"Transparency"});
		fillSlider.init(100, 0, "dotAlpha", 0, false, false);
		
		//bg for sample
		var dotBg = this.createEmptyMovieClip("dotBg", d++); 
		var rectSize:Number = 60;
		var ysample:Number =yArray[4];
		myDraw.drawRect(dotBg, (w-rectSize)/2, (ysample - rectSize/2), rectSize, rectSize, 0xD1CFC2, 1, 0xFFFFFF);
		
		//sample dot
		sampleDot = this.createEmptyMovieClip("sampleDot", d++); 
		sampleDot._x = w/2;
		sampleDot._y = ysample;
		updateDotSample();

		xmargin = 5;

		//chooser between image and blank background
		backgroundOption = this.attachMovie("RadioGroup", "backgroundOption", d++, {numberOfButtons:2, _x:xmargin, _y:yArray[5], labelSet:['Image','Blank'], callback:setBackgroundMode, parent_mc:this});
		
		//background image input
		var inputFormat = new TextFormat();
		inputFormat.font = 'Arial';
		inputFormat.color = 0x333333;
		inputFormat.size = 12;
		inputFormat.align = 'left';
		
		bgInput = createInputField(this, inputFormat, "", xmargin + 42, yArray[6], 80, 18);
		bgInput.restrict = "^";
		
		inputLabel = this.createTextField("inputLabel", d++, xmargin, yArray[6], 80, 18);
		inputLabel.setNewTextFormat(inputFormat);
		inputLabel.selectable = false;
		inputLabel.text = "Image:";
		
		bgInput.onChanged = function() {
			_parent.updateBackground(this.text);
		}
			
		if (parent_mc.backgroundImageName) {
			bgInput.text = parent_mc.backgroundImageName;
		}
		
		//scale input
		scaleInput = createInputField(this, inputFormat, parent_mc.myBackground.xyScale, xmargin + 40, yArray[7], 30, 18);
		//scaleInput.restrict = "^";
		scaleLabel = this.createTextField("scaleLabel", d++, xmargin, yArray[7], 100, 18);
		scaleLabel.setNewTextFormat(inputFormat);
		scaleLabel.selectable = false;
		scaleInput.maxChars = 3;
		scaleLabel.text = "Scale:";
		unitsLabel = this.createTextField("unitsLabel", d++, xmargin+70, yArray[7], 100, 18);
		unitsLabel.setNewTextFormat(inputFormat);
		unitsLabel.selectable = false;
		unitsLabel.text = "%";
		
		scaleInput.onChanged = function() {
			_parent.updateScale(this.text);
		}

		//for blank mode, inputs for image width and height
		widthInput = createInputField(this, inputFormat, String(imageWidth), xmargin + 45, yArray[6], 40, 18);
		widthInput.maxChars = 4;

		widthLabel = this.createTextField("widthLabel", d++, xmargin, yArray[6], 80, 18);
		widthLabel.setNewTextFormat(inputFormat);
		widthLabel.selectable = false;
		widthLabel.text = "Width:";

		heightInput = createInputField(this, inputFormat, String(imageHeight), xmargin + 45, yArray[7], 40, 18);
		heightInput.maxChars = 4;

		heightLabel = this.createTextField("widthLabel", d++, xmargin, yArray[7], 80, 18);
		heightLabel.setNewTextFormat(inputFormat);
		heightLabel.selectable = false;
		heightLabel.text = "Height:";

		widthInput.onChanged = function() {
			_parent.updateWidth(this.text);
		}
		heightInput.onChanged = function() {
			_parent.updateHeight(this.text);
		}

		//set which background inputs are visible
		bgMode(backgroundMode);
		
	}

	public function reload() {
		if (parent_mc.backgroundImageName != undefined) {
			bgInput.text = parent_mc.backgroundImageName;
			scaleInput.text = parent_mc.backgroundScale;
		}

		if (parent_mc.imageWidth != undefined) {
			widthInput.text = parent_mc.imageWidth;
			heightInput.text = parent_mc.imageHeight;
		}

		dotSize = parent_mc.dotSize;
		dotColor = parent_mc.dotColor;
		dotColorLine = parent_mc.dotColorLine;
		dotAlpha = parent_mc.dotAlpha;
		sizeMenu = parent_mc.sizeMenu;
		strokeMenu = parent_mc.strokeMenu;
		colorMenu = parent_mc.colorMenu;
		backgroundMode = parent_mc.backgroundMode;

		sizeButtons.setChoice(dotSize);
		lineColorButtons.setChoice(dotColorLine);
		colorButtons.setChoice(dotColor);
		fillSlider.setHandle(dotAlpha);
		backgroundOption.setChoice(backgroundMode);

		updateDotSample();
		bgMode(backgroundMode);
	}

	function bgMode(m:Number) {
		if (m==0) {
			bgInput._visible = true;
			inputLabel._visible = true;
			scaleInput._visible = true;
			scaleLabel._visible = true;
			unitsLabel._visible = true;

			widthInput._visible = false;
			widthLabel._visible = false;
			heightInput._visible = false;
			heightLabel._visible = false;
		} else {
			bgInput._visible = false;
			inputLabel._visible = false;
			scaleInput._visible = false;
			scaleLabel._visible = false;
			unitsLabel._visible = false;

			widthInput._visible = true;
			widthLabel._visible = true;
			heightInput._visible = true;
			heightLabel._visible = true;
		}
	}
	
	function updateBackground(url:String) {
		if (url.indexOf(".jpg") > 0 or url.indexOf(".jpeg") > 0 or url.indexOf(".swf") > 0 or url.indexOf(".png") > 0) {
			parent_mc.myBackground.updateBg(url);
		}
		parent_mc.updateQuestionSettings();
	}
	function updateScale(scale:String) {
		if (!isNaN(Number(scale))) {
			parent_mc.myBackground.updateZoom(Number(scale));
		}
		parent_mc.updateQuestionSettings();
	}

	var maxWidth = 594;
	var maxHeight = 394;
	function updateWidth(w:String) {
		if (!isNaN(Number(w))) {
			var newWidth = Math.min(Number(w), maxWidth);
			parent_mc.myBackground.updateWidth(newWidth);
			widthInput.text = newWidth;
		}
		parent_mc.updateQuestionSettings();
	}
	function updateHeight(h:String) {
		if (!isNaN(Number(h))) {
			var newHeight = Math.min(Number(h), maxHeight);
			parent_mc.myBackground.updateHeight(newHeight);
			heightInput.text = newHeight;
		}
		parent_mc.updateQuestionSettings();
	}
	
	function updateDotSample() {
		sampleDot.clear();
		var myDotSize = sizeMenu[dotSize];
		var myFillColor = colorMenu[dotColor];
		var myDotStroke = strokeMenu[dotSize];
		var myLineColor = colorMenu[dotColorLine];
		var myFillAlpha = dotAlpha;
		var myLineAlpha = 100;
		if (myLineColor == null) {
			//no outline, so make outline same color and alpha as fill
			myLineAlpha = 0;
			myDotSize += myDotStroke/2;	//adjust size to make up for lost outline
		}
		if (myFillColor == null) {
			//no fill, override transparency and set alpha to 0
			myFillAlpha = 0;
		}
		myDraw.drawCircle(sampleDot, 0, 0, myDotSize, myFillColor, myFillAlpha, myDotStroke, myLineColor, myLineAlpha);
		if (parent_mc.myDrawing.studentPoint) {
			parent_mc.myDrawing.updateStudentPoint();
		}
	}
	
	public function setSize(newSize:Number) {
		_parent.dotSize = newSize;
		_parent.parent_mc.dotSize = newSize;	//update the main module
		_parent.updateDotSample();
		_parent.parent_mc.updateQuestionSettings();
	}
	
	public function setColor(newColor:Number) {
		_parent.dotColor = newColor;
		_parent.parent_mc.dotColor = newColor;	//update the main module
		_parent.updateDotSample();
		_parent.parent_mc.updateQuestionSettings();
	}
	
	public function setColorLine(newColor:Number) {
		_parent.dotColorLine = newColor;
		_parent.parent_mc.dotColorLine = newColor;	//update the main module
		_parent.updateDotSample();
		_parent.parent_mc.updateQuestionSettings();
	}
	
	//called when slider is moved
	public function sliderUpdate() {
		dotAlpha = fillSlider.myVal;
		parent_mc.dotAlpha = dotAlpha;	//update the main module
		updateDotSample();
		parent_mc.updateQuestionSettings();
	}

	function setBackgroundMode(newMode:Number) {
		_parent.backgroundMode = newMode;
		_parent.parent_mc.myBackground.updateBgMode(newMode);
		_parent.bgMode(newMode);
		_parent.parent_mc.updateQuestionSettings();
	}
}