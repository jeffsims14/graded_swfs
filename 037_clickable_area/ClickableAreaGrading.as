import flash.geom.Point;

class ClickableAreaGrading extends MovieClip {
	var gradeable_id:String;
	var focused:Boolean;
	var directions:MovieClip;
	var ClickableAreaModule:MovieClip;	//Instance Name of the graded module's MovieClip. This is usually the MovieClip already on Stage.
	var tabType:String;
	
	function ClickableAreaGrading() {
	}
	
	/***** REQUIRED FOR SIMPLE AND COMPLEX SWF GRADING *******/
	/********* 
		this function will receive the exact string/number/object or whatever was sent from the getGradeableData() function
	    it will be called once for each response that was authored by the user until a match is found
		for simple graded swf's -> return true/false based on whether what the student has currently entered in the module is correct
		for complex graded swf's -> return true/false based on whether what the student has currently entered in the module matches the response sent in (not necessarily whether it is right or wrong)
	**********/	

	function checkAnswer($authored_object) {

		var match:Boolean = false;
		var overlap:Boolean;

		var myPoint:Point = this.ClickableAreaModule.myDrawing.studentPoint;
		var authorShapes:Array = $authored_object.tabSettings.authorShapes;
		var stroke:Number = $authored_object.tabSettings.authorSize;

		//this.ClickableAreaModule.debug.text = 'click: ' + String(myPoint);

		//check author shapes if overlapping with student point

		for (var i=0; i<authorShapes.length; i++) {
			var testShape =  authorShapes[i];

			//this.ClickableAreaModule.debug.text += '\nshape x,y: ' + testShape.startPoint.x + ', ' + testShape.startPoint.y;

			//studentPoint = new Point(120, 175);
			overlap = this.ClickableAreaModule.myDrawing.checkForOverlap(myPoint, testShape, stroke);

			//this.ClickableAreaModule.debug.text += '\noverlap: ' + overlap;

			if (overlap) {
				match = true;
				if (testShape.cutout) {
					match = false;
					break;
				}
			}
		}
		
		//this.ClickableAreaModule.debug.text += '\nshape count: ' + String(authorShapes.length);
		

		return match;
	}
	/********
	this function is called when the user clicks on this module to edit it
	include registering of all key listeners and mouse listeners as well as enabling buttons, etc.
	********/
	function setFocus() {
		this.directions._visible = true;
		this.focused = true;
	}
	/*******
	this function is called when the user clicks on a different module or on the stage and thus kills focus on this module
	include removal of all key listeners and mouse listeners as well as disabling any buttons, etc.
	*******/
	function killFocus() {
		this.focused = false;
		this.directions._visible = false;
	}
	/***** REQUIRED FUNCTIONS FOR COMPLEX SWF GRADING *******/
	/*********
	this function is called each time the user switches between tabs so that the current gradeable data will be stored
	return any object/array/number/string or combination of the above
	the object should be as simple as possible but must contain all information required for uniquely grading different responses
	**********/
	function getGradeableData() {
		var rtn:Object = new Object();
		
		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)

		//get settings constant across whole question
		if (tabType == "question") {
			rtn.questionSettings = this.ClickableAreaModule.questionSettings;
		}

		//get other objects that can be saved differently on each tab
		rtn.tabSettings = this.ClickableAreaModule.tabSettings;
		
		return rtn;
	}
	/**********
	this function is called each time the user switches to a response tab so that the gradeable data for that tab will be displayed
	the incoming parameter will match exactly the format of whatever is returned from the getGradeableData function
	at this point the module should be modified to redisplay exactly what would have been present when the user last viewed the tab with the associated gradeableData
	***********/
	var isSetup:Boolean = false;
	function setGradeableData($v) {

		tabType = _global.core.Globals.tabType;	//get the global tab type ("question" or "response" will be the only ones we check for)

		//set settings constant across whole question, or when module is loaded for the first time
		if (tabType == "question" or isSetup==false) {
			
			this.ClickableAreaModule.questionSettings = $v.questionSettings;	//recall any saved general settings
			
			isSetup = true;
		}

		//recall any saved settings for other objects
		this.ClickableAreaModule.tabSettings = $v.tabSettings;
		
		//reload the module with saved settings
		this.ClickableAreaModule.reload();
	}
}
