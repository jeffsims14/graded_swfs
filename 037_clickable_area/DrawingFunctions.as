class DrawingFunctions {

	function DrawingFunctions() {
	}
	
	//DRAWING CIRCLES
	public function drawCircle(mc:MovieClip, x:Number, y:Number, size:Number, color:Number, alpha:Number, dotStroke:Number, lineColor:Number, lineAlpha:Number){
		if (lineAlpha == undefined) {
			lineAlpha = 100;
		}
		var px:Number;
		var py:Number;
		var centerX:Number = x;
		var centerY:Number = y;
		mc.lineStyle(dotStroke, lineColor, lineAlpha);
		mc.beginFill(color, alpha);
		var a:Number;
		mc.moveTo(size + centerX, centerY);
		a = Math.tan(22.5 * Math.PI/180);
		for (var angle = 45; angle<=360; angle += 45) {
			// endpoint:
			px = size * Math.cos(angle * Math.PI/180)+centerX;
			py = size * Math.sin(angle * Math.PI/180)+centerY;
			// control:
			// (angle-90 is used to give the correct sign)
			var cx =px + size * a * Math.cos((angle-90) * Math.PI/180);
			var cy =py + size * a * Math.sin((angle-90) * Math.PI/180);
			mc.curveTo(cx, cy, px, py);
		}
		mc.endFill();
	}
	//DRAWING RECTANGLES
	public function drawRect(mc:MovieClip, x:Number, y:Number, w:Number, h:Number, fill:Number, stroke:Number, lineColor:Number) {
		mc.beginFill(fill, 100);
		mc.lineStyle(stroke, lineColor, 100, true, "none");
		mc.moveTo(x+w, y);
		mc.lineTo(x, y);
		mc.lineTo(x, y+h);
		mc.lineTo(x+w, y+h);
		mc.endFill();
	}
}