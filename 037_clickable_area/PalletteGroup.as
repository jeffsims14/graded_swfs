class PalletteGroup extends MovieClip {
	
	var d:Number;
	var numberOfButtons:Number;
	var label:String;
	var iconSet:Array;
	var colorSet:Array;
	var parent_mc:MovieClip;	//my parent
	var myGroup:Array;
	var defaultChoice:Number = 0;
	var callback:Function;
	var buttonSize:Number = 26;
	var spacer:Number = 3;
	
	function PalletteGroup() {
		d = parent_mc.d;
		
		var labelFormat = new TextFormat();
		labelFormat.font = 'Arial';
		labelFormat.color = 0x333333;
		labelFormat.size = 12;
		labelFormat.align = 'left';
		
		
		//header text
		if (label) {
			var headerLabel:TextField = this.createTextField("headerLabel", d++, 0, 5, 200, 20);
			headerLabel.setNewTextFormat(labelFormat);
			headerLabel.selectable = false;
			headerLabel.html = true;
			headerLabel.htmlText = label;
		}
		myGroup = new Array();
		for (var i=0; i<colorSet.length; i++) {
			var newButton = this.attachMovie("PalletteButton", "myButton"+i, d++, {w:buttonSize, h:buttonSize, _x:i*(buttonSize+spacer), _y:24, roundedCorner:0, id:i, myColor:colorSet[i], active:true});
			myGroup.push(newButton);
			newButton.setAction( toggleGroup );
			if (defaultChoice == i) {
				newButton.highlighted = true;	
			}
		}
	}
	
	public function toggleGroup(id, parent) {
		for (var i=0; i<parent.myGroup.length; i++) {
			parent.myGroup[i].highlighted = false;
		}
		parent.callback( id );
	}

	public function setChoice(c:Number) {
		for (var i=0; i<myGroup.length; i++) {
			if (i==c) {
				myGroup[c].highlighted = true;
			} else {
				myGroup[i].highlighted = false;
			}
		}
		
	}
	
}