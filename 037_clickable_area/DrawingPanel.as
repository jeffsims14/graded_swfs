class DrawingPanel extends Panel {

	var d:Number;
	
	//background
	private var w:Number = 134;
	private var h:Number = 365;
	private var fillColor:Number = 0xEEEEEE;//0xE0DED0;
	private var fillAlpha:Number = 60;
	private var lineAlpha:Number = 60;

	var parent_mc:MovieClip;	//main module
	var fillSlider:MovieClip;
	
	//default tool settings
	private var drawingMode:Number;
	
	private var authorSize:Number;	//color for author-drawn shapes
	private var authorColor:Number;
	private var authorColorLine:Number;
	private var authorAlpha:Number;
	
	private var authorStrokeMenu:Array;
	private var authorColorMenu:Array;

	private var sizeButtons;
	private var lineColorButtons;
	private var colorButtons;

	function DrawingPanel() {
		d = this.getNextHighestDepth();
		
		var xmargin:Number = 10;
		var ymargin:Number = 0;
		var yArray:Array = [0, 100, 150, 200, 265, 335];
		var spacer:Number = 60;
		
		drawingMode = parent_mc.myDrawing.drawingMode;
		
		authorSize = parent_mc.authorSize;
		authorColorLine = parent_mc.authorColorLine;
		authorColor = parent_mc.authorColor;
		authorAlpha = parent_mc.authorAlpha;
		
		authorStrokeMenu = parent_mc.authorStrokeMenu;
		authorColorMenu = parent_mc.authorColorMenu;
		
		//tools
		var toolButtons = this.attachMovie("ButtonGroup", "toolButtons", d++, {numberOfButtons:7, label:"Shapes", extraLabel:"Tools", _x:xmargin, _y:yArray[0], iconSet:['iFreehand','iPolygon','iSquare','iCircle', 'iSelect', 'iDel', 'iCutout'], callback:setTool, parent_mc:this});
		
		var sizeButtons = this.attachMovie("ButtonGroup", "sizeButtons", d++, {numberOfButtons:authorStrokeMenu.length, label:"Brush size", _x:xmargin, _y:yArray[1], iconSet:['bSmall','bMed','bLarge', 'bXL'], callback:setSize, parent_mc:this});
		
		var lineColorButtons = this.attachMovie("PalletteGroup", "lineColorButtons", d++, {label:"Outline", _x:xmargin, _y:yArray[2], colorSet:authorColorMenu, callback:setColorLine, parent_mc:this});
		
		var colorButtons = this.attachMovie("PalletteGroup", "colorButtons", d++, {numberOfButtons:authorColorMenu.length, label:"Fill", _x:xmargin, _y:yArray[3], colorSet:authorColorMenu, callback:setColor, parent_mc:this});
		
		fillSlider = this.attachMovie("SmallSlider","fillSlider", d++, {_x:xmargin+8, _y:yArray[4], titleLabel:"Transparency"});
		fillSlider.init(100, 0, "authorAlpha", 0, false, false);

		var copyButton = this.attachMovie("SimpleButton", "copyButton", d++, {w:53, h:24, _x:5, _y:yArray[5], roundedCorner:0, labelText:'Copy All', active:true});
		copyButton._highlight._visible = false;
		copyButton.setAction( copyShapes );

		var pasteButton = this.attachMovie("SimpleButton", "pasteButton", d++, {w:68, h:24, _x:62, _y:yArray[5], roundedCorner:0, labelText:'Replace All', active:true});
		pasteButton._highlight._visible = false;
		pasteButton.setAction( pasteShapes );
		
		/*var undoButton = this.attachMovie("SimpleButton", "undoButton", d++, {w:28, h:28, _x:xmargin, _y:yArray[5], roundedCorner:0, iconId:'iUndo', active:true});
		undoButton._highlight._visible = false;
		undoButton.setAction( undo );*/
		
		/*var undoLabel:TextField = this.createTextField("undoLabel", d++, xmargin + 30, yArray[5]+6, 100, 20);
		undoLabel.setNewTextFormat(toolButtons.labelFormat);
		undoLabel.selectable = false;
		undoLabel.text = "Undo";*/
		
		/*var clearAllButton = this.attachMovie("SimpleButton", "resetButton", d++, {w:28, h:28, _x:xmargin + 30, _y:yArray[5], roundedCorner:0, iconId:'clearIcon', active:true});
		clearAllButton._highlight._visible = false;
		clearAllButton.setAction( clearAll );*/
		
		//make panel draggable
		this.myBG.onPress = function() {
			_parent.startDrag(false);
		}
		this.myBG.onRelease = this.myBG.onReleaseOutside = function() {
			_parent.stopDrag();
		}
		this.myBG.useHandCursor = false;
	}

	public function reload() {
		authorSize = parent_mc.authorSize;
		authorColorLine = parent_mc.authorColorLine;
		authorColor = parent_mc.authorColor;
		authorAlpha = parent_mc.authorAlpha;

		sizeButtons.setChoice(authorSize);
		lineColorButtons.setChoice(authorColorLine);
		colorButtons.setChoice(authorColor);
		fillSlider.setHandle(authorAlpha);
	}
	
	public function setTool(t:Number) {
		_parent.drawingMode = t;
		_parent.parent_mc.drawingMode = t;	//update the main module
		_parent.parent_mc.myDrawing.changeTool(t);
	}
	
	public function setSize(newSize:Number) {
		_parent.authorSize = newSize;
		_parent.parent_mc.authorSize = newSize;	//update the main module
		_parent.parent_mc.myDrawing.updateAuthorShapes();
		_parent.parent_mc.updateTabSettings();
	}
	
	public function setColor(newColor:Number) {
		_parent.authorColor = newColor;
		_parent.parent_mc.authorColor = newColor;	//update the main module
		_parent.parent_mc.myDrawing.updateAuthorShapes();
		_parent.parent_mc.updateTabSettings();
	}
	
	public function setColorLine(newColor:Number) {
		_parent.authorColorLine = newColor;
		_parent.parent_mc.authorColorLine = newColor;	//update the main module
		_parent.parent_mc.myDrawing.updateAuthorShapes();
		_parent.parent_mc.updateTabSettings();
	}
	
	//called when slider is moved
	public function sliderUpdate() {
		authorAlpha = fillSlider.myVal;
		parent_mc.authorAlpha = authorAlpha;	//update the main module
		parent_mc.myDrawing.updateAuthorShapes();
		parent_mc.updateTabSettings();
	}

	//copy and paste shapes
	public function copyShapes(id, parent) {
		parent.parent_mc.myDrawing.copyShapes();
	}
	public function pasteShapes(id, parent) {
		parent.parent_mc.myDrawing.pasteShapes();
	}
	
	//undo last shape
	public function undo(id, parent) {
		parent.parent_mc.myDrawing.undo();
	}
	
	//clear all shapes
	public function clearAll(id, parent) {
		parent.parent_mc.myDrawing.clearAll();
	}
}